<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if( ! function_exists('main_url') ) {
	function main_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_MAIN, "/");
		return str_replace($base_url, $main_url, site_url( $uri ));
	}
}

if( ! function_exists('main_base_url') ) {
	function main_base_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_MAIN, "/");
		return str_replace($base_url, $main_url, base_url( $uri ));
	}
}

if( ! function_exists('assets_url') ) {
	function assets_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_ASSETS, "/");
		return str_replace($base_url, $main_url, base_url( $uri ));
	}
}

if( ! function_exists('account_url') ) {
	function account_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_ACCOUNT, "/");
		return str_replace($base_url, $main_url, site_url( $uri ));
	}
}

if( ! function_exists('account_base_url') ) {
	function account_base_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_ACCOUNT, "/");
		return str_replace($base_url, $main_url, base_url( $uri ));
	}
}

if( ! function_exists('schools_url') ) {
	function schools_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_SCHOOLS, "/");
		return str_replace($base_url, $main_url, site_url( $uri ));
	}
}

if( ! function_exists('schools_base_url') ) {
	function schools_base_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_SCHOOLS, "/");
		return str_replace($base_url, $main_url, base_url( $uri ));
	}
}

if( ! function_exists('companies_url') ) {
	function companies_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_COMPANIES, "/");
		return str_replace($base_url, $main_url, site_url( $uri ));
	}
}

if( ! function_exists('companies_base_url') ) {
	function companies_base_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_COMPANIES, "/");
		return str_replace($base_url, $main_url, base_url( $uri ));
	}
}

if( ! function_exists('realestate_url') ) {
	function realestate_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_REALESTATE, "/");
		return str_replace($base_url, $main_url, site_url( $uri ));
	}
}

if( ! function_exists('realestate_base_url') ) {
	function realestate_base_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_REALESTATE, "/");
		return str_replace($base_url, $main_url, base_url( $uri ));
	}
}

if( ! function_exists('jobs_url') ) {
	function jobs_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_JOBS, "/");
		return str_replace($base_url, $main_url, site_url( $uri ));
	}
}

if( ! function_exists('jobs_base_url') ) {
	function jobs_base_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_JOBS, "/");
		return str_replace($base_url, $main_url, base_url( $uri ));
	}
}

if( ! function_exists('vehicles_url') ) {
	function vehicles_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_VEHICLES, "/");
		return str_replace($base_url, $main_url, site_url( $uri ));
	}
}

if( ! function_exists('vehicles_base_url') ) {
	function vehicles_base_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_VEHICLES, "/");
		return str_replace($base_url, $main_url, base_url( $uri ));
	}
}

if( ! function_exists('motorcycles_url') ) {
	function motorcycles_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_MOTORCYCLES, "/");
		return str_replace($base_url, $main_url, site_url( $uri ));
	}
}

if( ! function_exists('motorcycles_base_url') ) {
	function motorcycles_base_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_MOTORCYCLES, "/");
		return str_replace($base_url, $main_url, base_url( $uri ));
	}
}

if( ! function_exists('events_url') ) {
	function events_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_EVENTS, "/");
		return str_replace($base_url, $main_url, site_url( $uri ));
	}
}

if( ! function_exists('events_base_url') ) {
	function events_base_url($uri='') {
		$base_url = rtrim(base_url(), "/");
		$main_url = rtrim(APP_HOST_EVENTS, "/");
		return str_replace($base_url, $main_url, base_url( $uri ));
	}
}