<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if( ! function_exists('trokis_cookie') ) {
	function trokis_cookie($arg1, $arg2=NULL) {
        $CI = get_instance();
        $cookie_domain = $CI->config->item('cookie_domain');
        $cookie_path = $CI->config->item('cookie_path');

        $vars = "";
        if( gettype($arg1) == 'array' ) {
            $arg1_r = array();
            foreach($arg1 as $arg1_k=>$arg1_v) {
                $arg1_r[] = "{$arg1_k}={$arg1_v}";
            }
            $vars = implode(",", $arg1_r);
        } else {
            $vars = "{$arg1}={$arg2}";
        }
        $CI->output->set_header("Set-Cookie: {$vars}; path={$cookie_path}; domain={$cookie_domain};");
	}
}

if( ! function_exists('current_location_cookie') ) {
    function current_location_cookie($id,$r=NULL) {
        $CI = get_instance();
        $cookie_domain = $CI->config->item('cookie_domain');
        $cookie_path = $CI->config->item('cookie_path');
        $CI->output->set_header("Set-Cookie: current_location={$id}; path={$cookie_path}; domain={$cookie_domain};");
        if( $r ) {
            redirect($r);
        }
    }
}

if( ! function_exists('schema_breadcrumbs') ) {
    function schema_breadcrumbs($items) {
        $n = 1;
        echo '<ol class="breadcrumb hidden-xs" itemscope itemtype="http://schema.org/BreadcrumbList">';
        foreach($items as $item) {
            $active_class = ($n==count($items)) ? "class=\"active\"" : "";
            $link_tag = ($n==count($items)) ? "span" : "a";
            echo "\n<li itemprop=\"itemListElement\" itemscope itemtype=\"http://schema.org/ListItem\" $active_class><{$link_tag} itemprop=\"item\" href=\"{$item['url']}\"><span itemprop=\"name\">{$item['title']}</span></{$link_tag}><meta itemprop=\"position\" content=\"{$n}\" /></li>";
            $n++;
        }
        echo "\n</ol>";
    }
}

if( ! function_exists('slugify') ) {
    function slugify($text)
    {
      // replace non letter or digits by -
      $text = preg_replace('~[^\pL\d]+~u', '-', $text);

      // transliterate
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

      // remove unwanted characters
      $text = preg_replace('~[^-\w]+~', '', $text);

      // trim
      $text = trim($text, '-');

      // remove duplicate -
      $text = preg_replace('~-+~', '-', $text);

      // lowercase
      $text = strtolower($text);

      if (empty($text)) {
        return 'n-a';
      }

      return $text;
    }
}