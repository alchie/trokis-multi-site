<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| AUTO-LOADER
| -------------------------------------------------------------------
| This file specifies which systems should be loaded by default.
|
| In order to keep the framework as light-weight as possible only the
| absolute minimal resources are loaded by default. For example,
| the database is not connected to automatically since no assumption
| is made regarding whether you intend to use it.  This file lets
| you globally define which systems you would like loaded with every
| request.
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
|
| These are the things you can load automatically:
|
| 1. Packages
| 2. Libraries
| 3. Drivers
| 4. Helper files
| 5. Custom config files
| 6. Language files
| 7. Models
|
*/

/*
| -------------------------------------------------------------------
|  Auto-load Packages
| -------------------------------------------------------------------
| Prototype:
|
|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
|
*/
$autoload['packages'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Libraries
| -------------------------------------------------------------------
| These are the classes located in system/libraries/ or your
| application/libraries/ directory, with the addition of the
| 'database' library, which is somewhat of a special case.
|
| Prototype:
|
|	$autoload['libraries'] = array('database', 'email', 'session');
|
| You can also supply an alternative library name to be assigned
| in the controller:
|
|	$autoload['libraries'] = array('user_agent' => 'ua');
*/
$autoload['libraries'] = array('database', 'form_validation', 'template_data', 'session', 'encrypt', 'user_agent', 'pagination');

/*
| -------------------------------------------------------------------
|  Auto-load Drivers
| -------------------------------------------------------------------
| These classes are located in system/libraries/ or in your
| application/libraries/ directory, but are also placed inside their
| own subdirectory and they extend the CI_Driver_Library class. They
| offer multiple interchangeable driver options.
|
| Prototype:
|
|	$autoload['drivers'] = array('cache');
|
| You can also supply an alternative property name to be assigned in
| the controller:
|
|	$autoload['drivers'] = array('cache' => 'cch');
|
*/
$autoload['drivers'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Helper Files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['helper'] = array('url', 'file');
*/
$autoload['helper'] = array('url', 'auth', 'bootstrap', 'error', 'app_url', 'cookie', 'global');

/*
| -------------------------------------------------------------------
|  Auto-load Config files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['config'] = array('config1', 'config2');
|
| NOTE: This item is intended for use ONLY if you have created custom
| config files.  Otherwise, leave it blank.
|
*/
$autoload['config'] = array('database','trokis');

/*
| -------------------------------------------------------------------
|  Auto-load Language files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['language'] = array('lang1', 'lang2');
|
| NOTE: Do not include the "_lang" part of your file.  For example
| "codeigniter_lang.php" would be referenced as array('codeigniter');
|
*/
$autoload['language'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Models
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['model'] = array('first_model', 'second_model');
|
| You can also supply an alternative model name to be assigned
| in the controller:
|
|	$autoload['model'] = array('first_model' => 'first');
*/
$models = array();
// ./account:
$models[] = 'account/Users_model'; 

// ./companies:
$models[] = 'companies/Category_model'; 
$models[] = 'companies/Companies_category_model'; 
$models[] = 'companies/Companies_data_model'; 
$models[] = 'companies/Companies_keyword_model'; 
$models[] = 'companies/Companies_media_model'; 
$models[] = 'companies/Companies_meta_model'; 
$models[] = 'companies/Companies_model'; 
$models[] = 'companies/Keywords_model'; 

// ./locations:
$models[] = 'locations/Municipals_model'; 
$models[] = 'locations/Provinces_model'; 
$models[] = 'locations/Provinces_municipals_model'; 
$models[] = 'locations/Regions_model'; 


// ./realestate:
$models[] = 'realestate/Developers_model'; 
$models[] = 'realestate/Messages_model'; 
$models[] = 'realestate/Partners_model'; 
$models[] = 'realestate/Projects_model'; 
$models[] = 'realestate/Properties_details_model'; 
$models[] = 'realestate/Properties_media_model'; 
$models[] = 'realestate/Properties_model'; 


// ./schools:
$models[] = 'schools/City_classes_model'; 
$models[] = 'schools/Class_orgs_model'; 
$models[] = 'schools/Curriculars_model'; 
$models[] = 'schools/Districts_model'; 
$models[] = 'schools/Divisions_model'; 
$models[] = 'schools/General_classes_model'; 
$models[] = 'schools/Income_classes_model'; 
$models[] = 'schools/K12_programs_model'; 
$models[] = 'schools/Legislatives_model'; 
$models[] = 'schools/Municipals_model'; 
$models[] = 'schools/Provinces_model'; 
$models[] = 'schools/Provinces_municipals_model'; 
$models[] = 'schools/Regions_model'; 
$models[] = 'schools/School_categories_model'; 
$models[] = 'schools/Schools_logo_model'; 
$models[] = 'schools/Schools_meta_model'; 
$models[] = 'schools/Schools_model'; 
$models[] = 'schools/Schools_programs_model'; 
$models[] = 'schools/Schools_slug_model'; 
$models[] = 'schools/Schools_tvl_model'; 
$models[] = 'schools/School_types_model'; 
$models[] = 'schools/Seo_blogger_model'; 
$models[] = 'schools/Seo_google_maps_model'; 
$models[] = 'schools/Seo_tumblr_model'; 
$models[] = 'schools/Seo_webs_model'; 
$models[] = 'schools/Seo_weebly_model'; 
$models[] = 'schools/Seo_wix_model'; 
$models[] = 'schools/Seo_yola_model'; 
$models[] = 'schools/Sub_classes_model'; 
$models[] = 'schools/Tvl_specializations_model'; 
$models[] = 'schools/Urps_model'; 

// ./sessions:
$models[] = 'sessions/App_sessions_model'; 
$models[] = 'sessions/Ci_sessions_model'; 
$models[] = 'sessions/User_sessions_model'; 

// ./wildcard:
$models[] = 'wildcard/Subdomains_model'; 

$autoload['model'] = $models;
