<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Properties_media_model Class
 *
 * Manipulates `properties_media` table on database

CREATE TABLE `properties_media` (
  `media_id` int(20) NOT NULL AUTO_INCREMENT,
  `property_id` int(20) NOT NULL,
  `media_type` varchar(100) NOT NULL,
  `media_key` varchar(100) NOT NULL,
  `media_value` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `primary` int(1) NOT NULL DEFAULT '0',
  `thumbnail` text,
  `alt_text` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`media_id`),
  KEY `property_id` (`property_id`)
);

 ALTER TABLE  `properties_media` ADD  `media_id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `properties_media` ADD  `property_id` int(20) NOT NULL   ;
 ALTER TABLE  `properties_media` ADD  `media_type` varchar(100) NOT NULL   ;
 ALTER TABLE  `properties_media` ADD  `media_key` varchar(100) NOT NULL   ;
 ALTER TABLE  `properties_media` ADD  `media_value` text NOT NULL   ;
 ALTER TABLE  `properties_media` ADD  `active` int(1) NOT NULL   DEFAULT '1';
 ALTER TABLE  `properties_media` ADD  `primary` int(1) NOT NULL   DEFAULT '0';
 ALTER TABLE  `properties_media` ADD  `thumbnail` text NULL   ;
 ALTER TABLE  `properties_media` ADD  `alt_text` varchar(200) NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Properties_media_model extends MY_Model {

	protected $media_id;
	protected $property_id;
	protected $media_type;
	protected $media_key;
	protected $media_value;
	protected $active;
	protected $primary;
	protected $thumbnail;
	protected $alt_text;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'properties_media';
		$this->_short_name = 'properties_media';
		$this->_fields = array("media_id","property_id","media_type","media_key","media_value","active","primary","thumbnail","alt_text");
		$this->_required = array("property_id","media_type","media_key","media_value","active","primary");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: media_id -------------------------------------- 

	/** 
	* Sets a value to `media_id` variable
	* @access public
	*/

		public function setMediaId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('media_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `media_id` variable
	* @access public
	*/

		public function getMediaId() {
			return $this->media_id;
		}
	
// ------------------------------ End Field: media_id --------------------------------------


// ---------------------------- Start Field: property_id -------------------------------------- 

	/** 
	* Sets a value to `property_id` variable
	* @access public
	*/

		public function setPropertyId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('property_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `property_id` variable
	* @access public
	*/

		public function getPropertyId() {
			return $this->property_id;
		}
	
// ------------------------------ End Field: property_id --------------------------------------


// ---------------------------- Start Field: media_type -------------------------------------- 

	/** 
	* Sets a value to `media_type` variable
	* @access public
	*/

		public function setMediaType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('media_type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `media_type` variable
	* @access public
	*/

		public function getMediaType() {
			return $this->media_type;
		}
	
// ------------------------------ End Field: media_type --------------------------------------


// ---------------------------- Start Field: media_key -------------------------------------- 

	/** 
	* Sets a value to `media_key` variable
	* @access public
	*/

		public function setMediaKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('media_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `media_key` variable
	* @access public
	*/

		public function getMediaKey() {
			return $this->media_key;
		}
	
// ------------------------------ End Field: media_key --------------------------------------


// ---------------------------- Start Field: media_value -------------------------------------- 

	/** 
	* Sets a value to `media_value` variable
	* @access public
	*/

		public function setMediaValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('media_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `media_value` variable
	* @access public
	*/

		public function getMediaValue() {
			return $this->media_value;
		}
	
// ------------------------------ End Field: media_value --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: primary -------------------------------------- 

	/** 
	* Sets a value to `primary` variable
	* @access public
	*/

		public function setPrimary($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('primary', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `primary` variable
	* @access public
	*/

		public function getPrimary() {
			return $this->primary;
		}
	
// ------------------------------ End Field: primary --------------------------------------


// ---------------------------- Start Field: thumbnail -------------------------------------- 

	/** 
	* Sets a value to `thumbnail` variable
	* @access public
	*/

		public function setThumbnail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('thumbnail', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `thumbnail` variable
	* @access public
	*/

		public function getThumbnail() {
			return $this->thumbnail;
		}
	
// ------------------------------ End Field: thumbnail --------------------------------------


// ---------------------------- Start Field: alt_text -------------------------------------- 

	/** 
	* Sets a value to `alt_text` variable
	* @access public
	*/

		public function setAltText($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('alt_text', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `alt_text` variable
	* @access public
	*/

		public function getAltText() {
			return $this->alt_text;
		}
	
// ------------------------------ End Field: alt_text --------------------------------------




}

/* End of file Properties_media_model.php */
/* Location: ./application/models/Properties_media_model.php */
