<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Partners_model Class
 *
 * Manipulates `partners` table on database

CREATE TABLE `partners` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `website` text,
  `logo_url` text,
  `active` int(1) DEFAULT '1',
  `description` text,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `partners` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `partners` ADD  `name` varchar(100) NOT NULL   ;
 ALTER TABLE  `partners` ADD  `website` text NULL   ;
 ALTER TABLE  `partners` ADD  `logo_url` text NULL   ;
 ALTER TABLE  `partners` ADD  `active` int(1) NULL   DEFAULT '1';
 ALTER TABLE  `partners` ADD  `description` text NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Partners_model extends MY_Model {

	protected $id;
	protected $name;
	protected $website;
	protected $logo_url;
	protected $active;
	protected $description;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'partners';
		$this->_short_name = 'partners';
		$this->_fields = array("id","name","website","logo_url","active","description");
		$this->_required = array("name");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

		public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

		public function getName() {
			return $this->name;
		}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: website -------------------------------------- 

	/** 
	* Sets a value to `website` variable
	* @access public
	*/

		public function setWebsite($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('website', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `website` variable
	* @access public
	*/

		public function getWebsite() {
			return $this->website;
		}
	
// ------------------------------ End Field: website --------------------------------------


// ---------------------------- Start Field: logo_url -------------------------------------- 

	/** 
	* Sets a value to `logo_url` variable
	* @access public
	*/

		public function setLogoUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('logo_url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `logo_url` variable
	* @access public
	*/

		public function getLogoUrl() {
			return $this->logo_url;
		}
	
// ------------------------------ End Field: logo_url --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: description -------------------------------------- 

	/** 
	* Sets a value to `description` variable
	* @access public
	*/

		public function setDescription($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('description', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `description` variable
	* @access public
	*/

		public function getDescription() {
			return $this->description;
		}
	
// ------------------------------ End Field: description --------------------------------------




}

/* End of file Partners_model.php */
/* Location: ./application/models/Partners_model.php */
