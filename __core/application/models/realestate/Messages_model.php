<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Messages_model Class
 *
 * Manipulates `messages` table on database

CREATE TABLE `messages` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `property_id` int(20) NOT NULL,
  `hash` varchar(200) NOT NULL,
  `full_name` varchar(200) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `phone_number` varchar(200) NOT NULL,
  `message` text,
  PRIMARY KEY (`id`),
  KEY `msg_property_id` (`property_id`)
);

 ALTER TABLE  `messages` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `messages` ADD  `property_id` int(20) NOT NULL   ;
 ALTER TABLE  `messages` ADD  `hash` varchar(200) NOT NULL   ;
 ALTER TABLE  `messages` ADD  `full_name` varchar(200) NOT NULL   ;
 ALTER TABLE  `messages` ADD  `email` varchar(200) NULL   ;
 ALTER TABLE  `messages` ADD  `phone_number` varchar(200) NOT NULL   ;
 ALTER TABLE  `messages` ADD  `message` text NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Messages_model extends MY_Model {

	protected $id;
	protected $property_id;
	protected $hash;
	protected $full_name;
	protected $email;
	protected $phone_number;
	protected $message;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'messages';
		$this->_short_name = 'messages';
		$this->_fields = array("id","property_id","hash","full_name","email","phone_number","message");
		$this->_required = array("property_id","hash","full_name","phone_number");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: property_id -------------------------------------- 

	/** 
	* Sets a value to `property_id` variable
	* @access public
	*/

		public function setPropertyId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('property_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `property_id` variable
	* @access public
	*/

		public function getPropertyId() {
			return $this->property_id;
		}
	
// ------------------------------ End Field: property_id --------------------------------------


// ---------------------------- Start Field: hash -------------------------------------- 

	/** 
	* Sets a value to `hash` variable
	* @access public
	*/

		public function setHash($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('hash', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `hash` variable
	* @access public
	*/

		public function getHash() {
			return $this->hash;
		}
	
// ------------------------------ End Field: hash --------------------------------------


// ---------------------------- Start Field: full_name -------------------------------------- 

	/** 
	* Sets a value to `full_name` variable
	* @access public
	*/

		public function setFullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('full_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `full_name` variable
	* @access public
	*/

		public function getFullName() {
			return $this->full_name;
		}
	
// ------------------------------ End Field: full_name --------------------------------------


// ---------------------------- Start Field: email -------------------------------------- 

	/** 
	* Sets a value to `email` variable
	* @access public
	*/

		public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `email` variable
	* @access public
	*/

		public function getEmail() {
			return $this->email;
		}
	
// ------------------------------ End Field: email --------------------------------------


// ---------------------------- Start Field: phone_number -------------------------------------- 

	/** 
	* Sets a value to `phone_number` variable
	* @access public
	*/

		public function setPhoneNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('phone_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `phone_number` variable
	* @access public
	*/

		public function getPhoneNumber() {
			return $this->phone_number;
		}
	
// ------------------------------ End Field: phone_number --------------------------------------


// ---------------------------- Start Field: message -------------------------------------- 

	/** 
	* Sets a value to `message` variable
	* @access public
	*/

		public function setMessage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('message', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `message` variable
	* @access public
	*/

		public function getMessage() {
			return $this->message;
		}
	
// ------------------------------ End Field: message --------------------------------------




}

/* End of file Messages_model.php */
/* Location: ./application/models/Messages_model.php */
