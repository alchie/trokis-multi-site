<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Developers_model Class
 *
 * Manipulates `developers` table on database

CREATE TABLE `developers` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `content` text,
  `website` text,
  `logo_url` text,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `developers` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `developers` ADD  `name` varchar(200) NOT NULL   ;
 ALTER TABLE  `developers` ADD  `slug` varchar(200) NOT NULL   ;
 ALTER TABLE  `developers` ADD  `content` text NULL   ;
 ALTER TABLE  `developers` ADD  `website` text NULL   ;
 ALTER TABLE  `developers` ADD  `logo_url` text NULL   ;
 ALTER TABLE  `developers` ADD  `active` int(1) NOT NULL   DEFAULT '1';


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Developers_model extends MY_Model {

	protected $id;
	protected $name;
	protected $slug;
	protected $content;
	protected $website;
	protected $logo_url;
	protected $active;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'developers';
		$this->_short_name = 'developers';
		$this->_fields = array("id","name","slug","content","website","logo_url","active");
		$this->_required = array("name","slug","active");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

		public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

		public function getName() {
			return $this->name;
		}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: slug -------------------------------------- 

	/** 
	* Sets a value to `slug` variable
	* @access public
	*/

		public function setSlug($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('slug', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `slug` variable
	* @access public
	*/

		public function getSlug() {
			return $this->slug;
		}
	
// ------------------------------ End Field: slug --------------------------------------


// ---------------------------- Start Field: content -------------------------------------- 

	/** 
	* Sets a value to `content` variable
	* @access public
	*/

		public function setContent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('content', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `content` variable
	* @access public
	*/

		public function getContent() {
			return $this->content;
		}
	
// ------------------------------ End Field: content --------------------------------------


// ---------------------------- Start Field: website -------------------------------------- 

	/** 
	* Sets a value to `website` variable
	* @access public
	*/

		public function setWebsite($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('website', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `website` variable
	* @access public
	*/

		public function getWebsite() {
			return $this->website;
		}
	
// ------------------------------ End Field: website --------------------------------------


// ---------------------------- Start Field: logo_url -------------------------------------- 

	/** 
	* Sets a value to `logo_url` variable
	* @access public
	*/

		public function setLogoUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('logo_url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `logo_url` variable
	* @access public
	*/

		public function getLogoUrl() {
			return $this->logo_url;
		}
	
// ------------------------------ End Field: logo_url --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------




}

/* End of file Developers_model.php */
/* Location: ./application/models/Developers_model.php */
