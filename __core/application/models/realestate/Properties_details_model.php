<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Properties_details_model Class
 *
 * Manipulates `properties_details` table on database

CREATE TABLE `properties_details` (
  `detail_id` int(20) NOT NULL AUTO_INCREMENT,
  `property_id` int(20) NOT NULL,
  `detail_key` varchar(100) NOT NULL,
  `detail_content` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`detail_id`)
);

 ALTER TABLE  `properties_details` ADD  `detail_id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `properties_details` ADD  `property_id` int(20) NOT NULL   ;
 ALTER TABLE  `properties_details` ADD  `detail_key` varchar(100) NOT NULL   ;
 ALTER TABLE  `properties_details` ADD  `detail_content` text NOT NULL   ;
 ALTER TABLE  `properties_details` ADD  `active` int(1) NOT NULL   DEFAULT '1';


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Properties_details_model extends MY_Model {

	protected $detail_id;
	protected $property_id;
	protected $detail_key;
	protected $detail_content;
	protected $active;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'properties_details';
		$this->_short_name = 'properties_details';
		$this->_fields = array("detail_id","property_id","detail_key","detail_content","active");
		$this->_required = array("property_id","detail_key","detail_content","active");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: detail_id -------------------------------------- 

	/** 
	* Sets a value to `detail_id` variable
	* @access public
	*/

		public function setDetailId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('detail_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `detail_id` variable
	* @access public
	*/

		public function getDetailId() {
			return $this->detail_id;
		}
	
// ------------------------------ End Field: detail_id --------------------------------------


// ---------------------------- Start Field: property_id -------------------------------------- 

	/** 
	* Sets a value to `property_id` variable
	* @access public
	*/

		public function setPropertyId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('property_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `property_id` variable
	* @access public
	*/

		public function getPropertyId() {
			return $this->property_id;
		}
	
// ------------------------------ End Field: property_id --------------------------------------


// ---------------------------- Start Field: detail_key -------------------------------------- 

	/** 
	* Sets a value to `detail_key` variable
	* @access public
	*/

		public function setDetailKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('detail_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `detail_key` variable
	* @access public
	*/

		public function getDetailKey() {
			return $this->detail_key;
		}
	
// ------------------------------ End Field: detail_key --------------------------------------


// ---------------------------- Start Field: detail_content -------------------------------------- 

	/** 
	* Sets a value to `detail_content` variable
	* @access public
	*/

		public function setDetailContent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('detail_content', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `detail_content` variable
	* @access public
	*/

		public function getDetailContent() {
			return $this->detail_content;
		}
	
// ------------------------------ End Field: detail_content --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------




}

/* End of file Properties_details_model.php */
/* Location: ./application/models/Properties_details_model.php */
