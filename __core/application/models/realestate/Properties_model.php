<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Properties_model Class
 *
 * Manipulates `properties` table on database

CREATE TABLE `properties` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `details` text,
  `slug` varchar(200) NOT NULL,
  `type` varchar(50) NOT NULL,
  `rent` int(1) DEFAULT '0',
  `price` int(20) DEFAULT NULL,
  `beds` int(2) DEFAULT NULL,
  `baths` int(2) DEFAULT NULL,
  `floor_area` int(20) DEFAULT NULL,
  `lot_area` int(20) DEFAULT NULL,
  `dev_id` int(20) DEFAULT NULL,
  `proj_id` int(20) DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `properties` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `properties` ADD  `title` varchar(200) NOT NULL   ;
 ALTER TABLE  `properties` ADD  `details` text NULL   ;
 ALTER TABLE  `properties` ADD  `slug` varchar(200) NOT NULL   ;
 ALTER TABLE  `properties` ADD  `type` varchar(50) NOT NULL   ;
 ALTER TABLE  `properties` ADD  `rent` int(1) NULL   DEFAULT '0';
 ALTER TABLE  `properties` ADD  `price` int(20) NULL   ;
 ALTER TABLE  `properties` ADD  `beds` int(2) NULL   ;
 ALTER TABLE  `properties` ADD  `baths` int(2) NULL   ;
 ALTER TABLE  `properties` ADD  `floor_area` int(20) NULL   ;
 ALTER TABLE  `properties` ADD  `lot_area` int(20) NULL   ;
 ALTER TABLE  `properties` ADD  `dev_id` int(20) NULL   ;
 ALTER TABLE  `properties` ADD  `proj_id` int(20) NULL   ;
 ALTER TABLE  `properties` ADD  `active` int(1) NULL   DEFAULT '1';


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Properties_model extends MY_Model {

	protected $id;
	protected $title;
	protected $details;
	protected $slug;
	protected $type;
	protected $rent;
	protected $price;
	protected $beds;
	protected $baths;
	protected $floor_area;
	protected $lot_area;
	protected $dev_id;
	protected $proj_id;
	protected $active;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'properties';
		$this->_short_name = 'properties';
		$this->_fields = array("id","title","details","slug","type","rent","price","beds","baths","floor_area","lot_area","dev_id","proj_id","active");
		$this->_required = array("title","slug","type");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: title -------------------------------------- 

	/** 
	* Sets a value to `title` variable
	* @access public
	*/

		public function setTitle($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('title', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `title` variable
	* @access public
	*/

		public function getTitle() {
			return $this->title;
		}
	
// ------------------------------ End Field: title --------------------------------------


// ---------------------------- Start Field: details -------------------------------------- 

	/** 
	* Sets a value to `details` variable
	* @access public
	*/

		public function setDetails($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('details', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `details` variable
	* @access public
	*/

		public function getDetails() {
			return $this->details;
		}
	
// ------------------------------ End Field: details --------------------------------------


// ---------------------------- Start Field: slug -------------------------------------- 

	/** 
	* Sets a value to `slug` variable
	* @access public
	*/

		public function setSlug($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('slug', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `slug` variable
	* @access public
	*/

		public function getSlug() {
			return $this->slug;
		}
	
// ------------------------------ End Field: slug --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

		public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

		public function getType() {
			return $this->type;
		}
	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: rent -------------------------------------- 

	/** 
	* Sets a value to `rent` variable
	* @access public
	*/

		public function setRent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('rent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `rent` variable
	* @access public
	*/

		public function getRent() {
			return $this->rent;
		}
	
// ------------------------------ End Field: rent --------------------------------------


// ---------------------------- Start Field: price -------------------------------------- 

	/** 
	* Sets a value to `price` variable
	* @access public
	*/

		public function setPrice($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('price', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `price` variable
	* @access public
	*/

		public function getPrice() {
			return $this->price;
		}
	
// ------------------------------ End Field: price --------------------------------------


// ---------------------------- Start Field: beds -------------------------------------- 

	/** 
	* Sets a value to `beds` variable
	* @access public
	*/

		public function setBeds($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('beds', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `beds` variable
	* @access public
	*/

		public function getBeds() {
			return $this->beds;
		}
	
// ------------------------------ End Field: beds --------------------------------------


// ---------------------------- Start Field: baths -------------------------------------- 

	/** 
	* Sets a value to `baths` variable
	* @access public
	*/

		public function setBaths($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('baths', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `baths` variable
	* @access public
	*/

		public function getBaths() {
			return $this->baths;
		}
	
// ------------------------------ End Field: baths --------------------------------------


// ---------------------------- Start Field: floor_area -------------------------------------- 

	/** 
	* Sets a value to `floor_area` variable
	* @access public
	*/

		public function setFloorArea($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('floor_area', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `floor_area` variable
	* @access public
	*/

		public function getFloorArea() {
			return $this->floor_area;
		}
	
// ------------------------------ End Field: floor_area --------------------------------------


// ---------------------------- Start Field: lot_area -------------------------------------- 

	/** 
	* Sets a value to `lot_area` variable
	* @access public
	*/

		public function setLotArea($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('lot_area', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `lot_area` variable
	* @access public
	*/

		public function getLotArea() {
			return $this->lot_area;
		}
	
// ------------------------------ End Field: lot_area --------------------------------------


// ---------------------------- Start Field: dev_id -------------------------------------- 

	/** 
	* Sets a value to `dev_id` variable
	* @access public
	*/

		public function setDevId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('dev_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `dev_id` variable
	* @access public
	*/

		public function getDevId() {
			return $this->dev_id;
		}
	
// ------------------------------ End Field: dev_id --------------------------------------


// ---------------------------- Start Field: proj_id -------------------------------------- 

	/** 
	* Sets a value to `proj_id` variable
	* @access public
	*/

		public function setProjId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('proj_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `proj_id` variable
	* @access public
	*/

		public function getProjId() {
			return $this->proj_id;
		}
	
// ------------------------------ End Field: proj_id --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------




}

/* End of file Properties_model.php */
/* Location: ./application/models/Properties_model.php */
