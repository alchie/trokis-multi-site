-- Table structure for table `developers` 

CREATE TABLE `developers` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `content` text,
  `website` text,
  `logo_url` text,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `messages` 

CREATE TABLE `messages` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `property_id` int(20) NOT NULL,
  `hash` varchar(200) NOT NULL,
  `full_name` varchar(200) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `phone_number` varchar(200) NOT NULL,
  `message` text,
  PRIMARY KEY (`id`),
  KEY `msg_property_id` (`property_id`)
);

-- Table structure for table `partners` 

CREATE TABLE `partners` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `website` text,
  `logo_url` text,
  `active` int(1) DEFAULT '1',
  `description` text,
  PRIMARY KEY (`id`)
);

-- Table structure for table `projects` 

CREATE TABLE `projects` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `dev_id` int(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `content` text,
  `url` text,
  `logo_url` text,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `properties` 

CREATE TABLE `properties` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `details` text,
  `slug` varchar(200) NOT NULL,
  `type` varchar(50) NOT NULL,
  `rent` int(1) DEFAULT '0',
  `price` int(20) DEFAULT NULL,
  `beds` int(2) DEFAULT NULL,
  `baths` int(2) DEFAULT NULL,
  `floor_area` int(20) DEFAULT NULL,
  `lot_area` int(20) DEFAULT NULL,
  `dev_id` int(20) DEFAULT NULL,
  `proj_id` int(20) DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `properties_details` 

CREATE TABLE `properties_details` (
  `detail_id` int(20) NOT NULL AUTO_INCREMENT,
  `property_id` int(20) NOT NULL,
  `detail_key` varchar(100) NOT NULL,
  `detail_content` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`detail_id`)
);

-- Table structure for table `properties_media` 

CREATE TABLE `properties_media` (
  `media_id` int(20) NOT NULL AUTO_INCREMENT,
  `property_id` int(20) NOT NULL,
  `media_type` varchar(100) NOT NULL,
  `media_key` varchar(100) NOT NULL,
  `media_value` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `primary` int(1) NOT NULL DEFAULT '0',
  `thumbnail` text,
  `alt_text` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`media_id`),
  KEY `property_id` (`property_id`)
);

