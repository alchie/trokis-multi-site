<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Projects_model Class
 *
 * Manipulates `projects` table on database

CREATE TABLE `projects` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `dev_id` int(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `content` text,
  `url` text,
  `logo_url` text,
  `active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `projects` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `projects` ADD  `dev_id` int(20) NOT NULL   ;
 ALTER TABLE  `projects` ADD  `name` varchar(200) NOT NULL   ;
 ALTER TABLE  `projects` ADD  `slug` varchar(200) NOT NULL   ;
 ALTER TABLE  `projects` ADD  `content` text NULL   ;
 ALTER TABLE  `projects` ADD  `url` text NULL   ;
 ALTER TABLE  `projects` ADD  `logo_url` text NULL   ;
 ALTER TABLE  `projects` ADD  `active` int(1) NOT NULL   DEFAULT '1';


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Projects_model extends MY_Model {

	protected $id;
	protected $dev_id;
	protected $name;
	protected $slug;
	protected $content;
	protected $url;
	protected $logo_url;
	protected $active;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'projects';
		$this->_short_name = 'projects';
		$this->_fields = array("id","dev_id","name","slug","content","url","logo_url","active");
		$this->_required = array("dev_id","name","slug","active");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: dev_id -------------------------------------- 

	/** 
	* Sets a value to `dev_id` variable
	* @access public
	*/

		public function setDevId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('dev_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `dev_id` variable
	* @access public
	*/

		public function getDevId() {
			return $this->dev_id;
		}
	
// ------------------------------ End Field: dev_id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

		public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

		public function getName() {
			return $this->name;
		}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: slug -------------------------------------- 

	/** 
	* Sets a value to `slug` variable
	* @access public
	*/

		public function setSlug($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('slug', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `slug` variable
	* @access public
	*/

		public function getSlug() {
			return $this->slug;
		}
	
// ------------------------------ End Field: slug --------------------------------------


// ---------------------------- Start Field: content -------------------------------------- 

	/** 
	* Sets a value to `content` variable
	* @access public
	*/

		public function setContent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('content', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `content` variable
	* @access public
	*/

		public function getContent() {
			return $this->content;
		}
	
// ------------------------------ End Field: content --------------------------------------


// ---------------------------- Start Field: url -------------------------------------- 

	/** 
	* Sets a value to `url` variable
	* @access public
	*/

		public function setUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `url` variable
	* @access public
	*/

		public function getUrl() {
			return $this->url;
		}
	
// ------------------------------ End Field: url --------------------------------------


// ---------------------------- Start Field: logo_url -------------------------------------- 

	/** 
	* Sets a value to `logo_url` variable
	* @access public
	*/

		public function setLogoUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('logo_url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `logo_url` variable
	* @access public
	*/

		public function getLogoUrl() {
			return $this->logo_url;
		}
	
// ------------------------------ End Field: logo_url --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------




}

/* End of file Projects_model.php */
/* Location: ./application/models/Projects_model.php */
