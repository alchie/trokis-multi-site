<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users_model Class
 *
 * Manipulates `users` table on database

CREATE TABLE `users` (
  `uid` int(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `passwd` text NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'user',
  `verified` int(1) NOT NULL DEFAULT '0',
  `code` text,
  `date_registered` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `email` (`email`)
);

 * @package			        Model
 * @version_number	        3.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Users_model extends MY_Model {

	protected $uid;
	protected $email;
	protected $full_name;
	protected $passwd;
	protected $type;
	protected $verified;
	protected $code;
	protected $date_registered;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'users';
		$this->_short_name = 'users';
		$this->_fields = array("uid","email","full_name","passwd","type","verified","code","date_registered");
		$this->_required = array("email","full_name","passwd","type","verified");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: uid -------------------------------------- 

	/** 
	* Sets a value to `uid` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setUid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('uid', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `uid` variable
	* @access public
	* @return String;
	*/

		public function getUid() {
			return $this->uid;
		}
	
// ------------------------------ End Field: uid --------------------------------------


// ---------------------------- Start Field: email -------------------------------------- 

	/** 
	* Sets a value to `email` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `email` variable
	* @access public
	* @return String;
	*/

		public function getEmail() {
			return $this->email;
		}
	
// ------------------------------ End Field: email --------------------------------------


// ---------------------------- Start Field: full_name -------------------------------------- 

	/** 
	* Sets a value to `full_name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setFullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('full_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `full_name` variable
	* @access public
	* @return String;
	*/

		public function getFullName() {
			return $this->full_name;
		}
	
// ------------------------------ End Field: full_name --------------------------------------


// ---------------------------- Start Field: passwd -------------------------------------- 

	/** 
	* Sets a value to `passwd` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setPasswd($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('passwd', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `passwd` variable
	* @access public
	* @return String;
	*/

		public function getPasswd() {
			return $this->passwd;
		}
	
// ------------------------------ End Field: passwd --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `type` variable
	* @access public
	* @return String;
	*/

		public function getType() {
			return $this->type;
		}
	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: verified -------------------------------------- 

	/** 
	* Sets a value to `verified` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setVerified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('verified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `verified` variable
	* @access public
	* @return String;
	*/

		public function getVerified() {
			return $this->verified;
		}
	
// ------------------------------ End Field: verified --------------------------------------


// ---------------------------- Start Field: code -------------------------------------- 

	/** 
	* Sets a value to `code` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('code', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `code` variable
	* @access public
	* @return String;
	*/

		public function getCode() {
			return $this->code;
		}
	
// ------------------------------ End Field: code --------------------------------------


// ---------------------------- Start Field: date_registered -------------------------------------- 

	/** 
	* Sets a value to `date_registered` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setDateRegistered($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('date_registered', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `date_registered` variable
	* @access public
	* @return String;
	*/

		public function getDateRegistered() {
			return $this->date_registered;
		}
	
// ------------------------------ End Field: date_registered --------------------------------------




}

/* End of file Users_model.php */
/* Location: ./application/models/Users_model.php */
