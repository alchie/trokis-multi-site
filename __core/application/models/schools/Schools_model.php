<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Schools_model Class
 *
 * Manipulates `schools` table on database

CREATE TABLE `schools` (
  `id` int(20) NOT NULL,
  `name` text NOT NULL,
  `region` int(20) DEFAULT NULL,
  `province` int(20) DEFAULT NULL,
  `division` int(20) DEFAULT NULL,
  `municipality` int(20) DEFAULT NULL,
  `district` int(20) DEFAULT NULL,
  `legislative` int(20) DEFAULT NULL,
  `school_type` int(20) DEFAULT NULL,
  `abbr` varchar(100) DEFAULT NULL,
  `prev_name` text,
  `mother_id` int(20) DEFAULT NULL,
  `address` text,
  `school_head` varchar(200) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `telephone` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `date_established` date DEFAULT NULL,
  `general_class` int(20) DEFAULT NULL,
  `sub_class` int(20) DEFAULT NULL,
  `cur_class` int(20) DEFAULT NULL,
  `class_org` int(20) DEFAULT NULL,
  `income_class` int(20) DEFAULT NULL,
  `city_class` int(20) DEFAULT NULL,
  `urp` int(20) DEFAULT NULL,
  `category` int(20) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `lat` decimal(10,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL
);

 ALTER TABLE  `schools` ADD  `id` int(20) NOT NULL   ;
 ALTER TABLE  `schools` ADD  `name` text NOT NULL   ;
 ALTER TABLE  `schools` ADD  `region` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `province` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `division` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `municipality` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `district` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `legislative` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `school_type` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `abbr` varchar(100) NULL   ;
 ALTER TABLE  `schools` ADD  `prev_name` text NULL   ;
 ALTER TABLE  `schools` ADD  `mother_id` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `address` text NULL   ;
 ALTER TABLE  `schools` ADD  `school_head` varchar(200) NULL   ;
 ALTER TABLE  `schools` ADD  `designation` varchar(100) NULL   ;
 ALTER TABLE  `schools` ADD  `telephone` varchar(100) NULL   ;
 ALTER TABLE  `schools` ADD  `mobile` varchar(100) NULL   ;
 ALTER TABLE  `schools` ADD  `email` varchar(200) NULL   ;
 ALTER TABLE  `schools` ADD  `date_established` date NULL   ;
 ALTER TABLE  `schools` ADD  `general_class` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `sub_class` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `cur_class` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `class_org` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `income_class` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `city_class` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `urp` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `category` int(20) NULL   ;
 ALTER TABLE  `schools` ADD  `phone_number` varchar(50) NULL   ;
 ALTER TABLE  `schools` ADD  `lat` decimal(10,8) NULL   ;
 ALTER TABLE  `schools` ADD  `lng` decimal(11,8) NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Schools_model extends MY_Model {

	protected $id;
	protected $name;
	protected $region;
	protected $province;
	protected $division;
	protected $municipality;
	protected $district;
	protected $legislative;
	protected $school_type;
	protected $abbr;
	protected $prev_name;
	protected $mother_id;
	protected $address;
	protected $school_head;
	protected $designation;
	protected $telephone;
	protected $mobile;
	protected $email;
	protected $date_established;
	protected $general_class;
	protected $sub_class;
	protected $cur_class;
	protected $class_org;
	protected $income_class;
	protected $city_class;
	protected $urp;
	protected $category;
	protected $phone_number;
	protected $lat;
	protected $lng;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'schools';
		$this->_short_name = 'schools';
		$this->_fields = array("id","name","region","province","division","municipality","district","legislative","school_type","abbr","prev_name","mother_id","address","school_head","designation","telephone","mobile","email","date_established","general_class","sub_class","cur_class","class_org","income_class","city_class","urp","category","phone_number","lat","lng");
		$this->_required = array("id","name");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

		public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

		public function getName() {
			return $this->name;
		}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: region -------------------------------------- 

	/** 
	* Sets a value to `region` variable
	* @access public
	*/

		public function setRegion($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('region', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `region` variable
	* @access public
	*/

		public function getRegion() {
			return $this->region;
		}
	
// ------------------------------ End Field: region --------------------------------------


// ---------------------------- Start Field: province -------------------------------------- 

	/** 
	* Sets a value to `province` variable
	* @access public
	*/

		public function setProvince($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('province', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `province` variable
	* @access public
	*/

		public function getProvince() {
			return $this->province;
		}
	
// ------------------------------ End Field: province --------------------------------------


// ---------------------------- Start Field: division -------------------------------------- 

	/** 
	* Sets a value to `division` variable
	* @access public
	*/

		public function setDivision($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('division', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `division` variable
	* @access public
	*/

		public function getDivision() {
			return $this->division;
		}
	
// ------------------------------ End Field: division --------------------------------------


// ---------------------------- Start Field: municipality -------------------------------------- 

	/** 
	* Sets a value to `municipality` variable
	* @access public
	*/

		public function setMunicipality($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('municipality', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `municipality` variable
	* @access public
	*/

		public function getMunicipality() {
			return $this->municipality;
		}
	
// ------------------------------ End Field: municipality --------------------------------------


// ---------------------------- Start Field: district -------------------------------------- 

	/** 
	* Sets a value to `district` variable
	* @access public
	*/

		public function setDistrict($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('district', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `district` variable
	* @access public
	*/

		public function getDistrict() {
			return $this->district;
		}
	
// ------------------------------ End Field: district --------------------------------------


// ---------------------------- Start Field: legislative -------------------------------------- 

	/** 
	* Sets a value to `legislative` variable
	* @access public
	*/

		public function setLegislative($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('legislative', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `legislative` variable
	* @access public
	*/

		public function getLegislative() {
			return $this->legislative;
		}
	
// ------------------------------ End Field: legislative --------------------------------------


// ---------------------------- Start Field: school_type -------------------------------------- 

	/** 
	* Sets a value to `school_type` variable
	* @access public
	*/

		public function setSchoolType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('school_type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `school_type` variable
	* @access public
	*/

		public function getSchoolType() {
			return $this->school_type;
		}
	
// ------------------------------ End Field: school_type --------------------------------------


// ---------------------------- Start Field: abbr -------------------------------------- 

	/** 
	* Sets a value to `abbr` variable
	* @access public
	*/

		public function setAbbr($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('abbr', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `abbr` variable
	* @access public
	*/

		public function getAbbr() {
			return $this->abbr;
		}
	
// ------------------------------ End Field: abbr --------------------------------------


// ---------------------------- Start Field: prev_name -------------------------------------- 

	/** 
	* Sets a value to `prev_name` variable
	* @access public
	*/

		public function setPrevName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('prev_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `prev_name` variable
	* @access public
	*/

		public function getPrevName() {
			return $this->prev_name;
		}
	
// ------------------------------ End Field: prev_name --------------------------------------


// ---------------------------- Start Field: mother_id -------------------------------------- 

	/** 
	* Sets a value to `mother_id` variable
	* @access public
	*/

		public function setMotherId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('mother_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `mother_id` variable
	* @access public
	*/

		public function getMotherId() {
			return $this->mother_id;
		}
	
// ------------------------------ End Field: mother_id --------------------------------------


// ---------------------------- Start Field: address -------------------------------------- 

	/** 
	* Sets a value to `address` variable
	* @access public
	*/

		public function setAddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('address', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `address` variable
	* @access public
	*/

		public function getAddress() {
			return $this->address;
		}
	
// ------------------------------ End Field: address --------------------------------------


// ---------------------------- Start Field: school_head -------------------------------------- 

	/** 
	* Sets a value to `school_head` variable
	* @access public
	*/

		public function setSchoolHead($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('school_head', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `school_head` variable
	* @access public
	*/

		public function getSchoolHead() {
			return $this->school_head;
		}
	
// ------------------------------ End Field: school_head --------------------------------------


// ---------------------------- Start Field: designation -------------------------------------- 

	/** 
	* Sets a value to `designation` variable
	* @access public
	*/

		public function setDesignation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('designation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `designation` variable
	* @access public
	*/

		public function getDesignation() {
			return $this->designation;
		}
	
// ------------------------------ End Field: designation --------------------------------------


// ---------------------------- Start Field: telephone -------------------------------------- 

	/** 
	* Sets a value to `telephone` variable
	* @access public
	*/

		public function setTelephone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('telephone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `telephone` variable
	* @access public
	*/

		public function getTelephone() {
			return $this->telephone;
		}
	
// ------------------------------ End Field: telephone --------------------------------------


// ---------------------------- Start Field: mobile -------------------------------------- 

	/** 
	* Sets a value to `mobile` variable
	* @access public
	*/

		public function setMobile($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('mobile', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `mobile` variable
	* @access public
	*/

		public function getMobile() {
			return $this->mobile;
		}
	
// ------------------------------ End Field: mobile --------------------------------------


// ---------------------------- Start Field: email -------------------------------------- 

	/** 
	* Sets a value to `email` variable
	* @access public
	*/

		public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `email` variable
	* @access public
	*/

		public function getEmail() {
			return $this->email;
		}
	
// ------------------------------ End Field: email --------------------------------------


// ---------------------------- Start Field: date_established -------------------------------------- 

	/** 
	* Sets a value to `date_established` variable
	* @access public
	*/

		public function setDateEstablished($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('date_established', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `date_established` variable
	* @access public
	*/

		public function getDateEstablished() {
			return $this->date_established;
		}
	
// ------------------------------ End Field: date_established --------------------------------------


// ---------------------------- Start Field: general_class -------------------------------------- 

	/** 
	* Sets a value to `general_class` variable
	* @access public
	*/

		public function setGeneralClass($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('general_class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `general_class` variable
	* @access public
	*/

		public function getGeneralClass() {
			return $this->general_class;
		}
	
// ------------------------------ End Field: general_class --------------------------------------


// ---------------------------- Start Field: sub_class -------------------------------------- 

	/** 
	* Sets a value to `sub_class` variable
	* @access public
	*/

		public function setSubClass($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('sub_class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `sub_class` variable
	* @access public
	*/

		public function getSubClass() {
			return $this->sub_class;
		}
	
// ------------------------------ End Field: sub_class --------------------------------------


// ---------------------------- Start Field: cur_class -------------------------------------- 

	/** 
	* Sets a value to `cur_class` variable
	* @access public
	*/

		public function setCurClass($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('cur_class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `cur_class` variable
	* @access public
	*/

		public function getCurClass() {
			return $this->cur_class;
		}
	
// ------------------------------ End Field: cur_class --------------------------------------


// ---------------------------- Start Field: class_org -------------------------------------- 

	/** 
	* Sets a value to `class_org` variable
	* @access public
	*/

		public function setClassOrg($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('class_org', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `class_org` variable
	* @access public
	*/

		public function getClassOrg() {
			return $this->class_org;
		}
	
// ------------------------------ End Field: class_org --------------------------------------


// ---------------------------- Start Field: income_class -------------------------------------- 

	/** 
	* Sets a value to `income_class` variable
	* @access public
	*/

		public function setIncomeClass($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('income_class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `income_class` variable
	* @access public
	*/

		public function getIncomeClass() {
			return $this->income_class;
		}
	
// ------------------------------ End Field: income_class --------------------------------------


// ---------------------------- Start Field: city_class -------------------------------------- 

	/** 
	* Sets a value to `city_class` variable
	* @access public
	*/

		public function setCityClass($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('city_class', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `city_class` variable
	* @access public
	*/

		public function getCityClass() {
			return $this->city_class;
		}
	
// ------------------------------ End Field: city_class --------------------------------------


// ---------------------------- Start Field: urp -------------------------------------- 

	/** 
	* Sets a value to `urp` variable
	* @access public
	*/

		public function setUrp($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('urp', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `urp` variable
	* @access public
	*/

		public function getUrp() {
			return $this->urp;
		}
	
// ------------------------------ End Field: urp --------------------------------------


// ---------------------------- Start Field: category -------------------------------------- 

	/** 
	* Sets a value to `category` variable
	* @access public
	*/

		public function setCategory($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('category', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `category` variable
	* @access public
	*/

		public function getCategory() {
			return $this->category;
		}
	
// ------------------------------ End Field: category --------------------------------------


// ---------------------------- Start Field: phone_number -------------------------------------- 

	/** 
	* Sets a value to `phone_number` variable
	* @access public
	*/

		public function setPhoneNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('phone_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `phone_number` variable
	* @access public
	*/

		public function getPhoneNumber() {
			return $this->phone_number;
		}
	
// ------------------------------ End Field: phone_number --------------------------------------


// ---------------------------- Start Field: lat -------------------------------------- 

	/** 
	* Sets a value to `lat` variable
	* @access public
	*/

		public function setLat($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('lat', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `lat` variable
	* @access public
	*/

		public function getLat() {
			return $this->lat;
		}
	
// ------------------------------ End Field: lat --------------------------------------


// ---------------------------- Start Field: lng -------------------------------------- 

	/** 
	* Sets a value to `lng` variable
	* @access public
	*/

		public function setLng($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('lng', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `lng` variable
	* @access public
	*/

		public function getLng() {
			return $this->lng;
		}
	
// ------------------------------ End Field: lng --------------------------------------




}

/* End of file Schools_model.php */
/* Location: ./application/models/Schools_model.php */
