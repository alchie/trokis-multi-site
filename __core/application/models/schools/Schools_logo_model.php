<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Schools_logo_model Class
 *
 * Manipulates `schools_logo` table on database

CREATE TABLE `schools_logo` (
  `school_id` int(20) NOT NULL,
  `url` text NOT NULL,
  UNIQUE KEY `school_id` (`school_id`)
);

 ALTER TABLE  `schools_logo` ADD  `school_id` int(20) NOT NULL   PRIMARY KEY;
 ALTER TABLE  `schools_logo` ADD  `url` text NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Schools_logo_model extends MY_Model {

	protected $school_id;
	protected $url;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'schools_logo';
		$this->_short_name = 'schools_logo';
		$this->_fields = array("school_id","url");
		$this->_required = array("url");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: school_id -------------------------------------- 

	/** 
	* Sets a value to `school_id` variable
	* @access public
	*/

		public function setSchoolId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('school_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `school_id` variable
	* @access public
	*/

		public function getSchoolId() {
			return $this->school_id;
		}
	
// ------------------------------ End Field: school_id --------------------------------------


// ---------------------------- Start Field: url -------------------------------------- 

	/** 
	* Sets a value to `url` variable
	* @access public
	*/

		public function setUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `url` variable
	* @access public
	*/

		public function getUrl() {
			return $this->url;
		}
	
// ------------------------------ End Field: url --------------------------------------




}

/* End of file Schools_logo_model.php */
/* Location: ./application/models/Schools_logo_model.php */
