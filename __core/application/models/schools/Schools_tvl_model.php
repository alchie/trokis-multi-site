<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Schools_tvl_model Class
 *
 * Manipulates `schools_tvl` table on database

CREATE TABLE `schools_tvl` (
  `school_id` int(20) NOT NULL,
  `tvl_id` int(20) NOT NULL,
  KEY `school_id` (`school_id`,`tvl_id`)
);

 ALTER TABLE  `schools_tvl` ADD  `school_id` int(20) NOT NULL   ;
 ALTER TABLE  `schools_tvl` ADD  `tvl_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Schools_tvl_model extends MY_Model {

	protected $school_id;
	protected $tvl_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'schools_tvl';
		$this->_short_name = 'schools_tvl';
		$this->_fields = array("school_id","tvl_id");
		$this->_required = array("school_id","tvl_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: school_id -------------------------------------- 

	/** 
	* Sets a value to `school_id` variable
	* @access public
	*/

		public function setSchoolId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('school_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `school_id` variable
	* @access public
	*/

		public function getSchoolId() {
			return $this->school_id;
		}
	
// ------------------------------ End Field: school_id --------------------------------------


// ---------------------------- Start Field: tvl_id -------------------------------------- 

	/** 
	* Sets a value to `tvl_id` variable
	* @access public
	*/

		public function setTvlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('tvl_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `tvl_id` variable
	* @access public
	*/

		public function getTvlId() {
			return $this->tvl_id;
		}
	
// ------------------------------ End Field: tvl_id --------------------------------------




}

/* End of file Schools_tvl_model.php */
/* Location: ./application/models/Schools_tvl_model.php */
