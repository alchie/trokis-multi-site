<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Seo_google_maps_model Class
 *
 * Manipulates `seo_google_maps` table on database

CREATE TABLE `seo_google_maps` (
  `school_id` int(20) NOT NULL,
  UNIQUE KEY `school_id` (`school_id`)
);

 ALTER TABLE  `seo_google_maps` ADD  `school_id` int(20) NOT NULL   PRIMARY KEY;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Seo_google_maps_model extends MY_Model {

	protected $school_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'seo_google_maps';
		$this->_short_name = 'seo_google_maps';
		$this->_fields = array("school_id");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: school_id -------------------------------------- 

	/** 
	* Sets a value to `school_id` variable
	* @access public
	*/

		public function setSchoolId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('school_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `school_id` variable
	* @access public
	*/

		public function getSchoolId() {
			return $this->school_id;
		}
	
// ------------------------------ End Field: school_id --------------------------------------




}

/* End of file Seo_google_maps_model.php */
/* Location: ./application/models/Seo_google_maps_model.php */
