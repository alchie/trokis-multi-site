<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Schools_programs_model Class
 *
 * Manipulates `schools_programs` table on database

CREATE TABLE `schools_programs` (
  `school_id` int(20) NOT NULL,
  `program_id` int(20) NOT NULL,
  KEY `school_id` (`school_id`,`program_id`)
);

 ALTER TABLE  `schools_programs` ADD  `school_id` int(20) NOT NULL   ;
 ALTER TABLE  `schools_programs` ADD  `program_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Schools_programs_model extends MY_Model {

	protected $school_id;
	protected $program_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'schools_programs';
		$this->_short_name = 'schools_programs';
		$this->_fields = array("school_id","program_id");
		$this->_required = array("school_id","program_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: school_id -------------------------------------- 

	/** 
	* Sets a value to `school_id` variable
	* @access public
	*/

		public function setSchoolId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('school_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `school_id` variable
	* @access public
	*/

		public function getSchoolId() {
			return $this->school_id;
		}
	
// ------------------------------ End Field: school_id --------------------------------------


// ---------------------------- Start Field: program_id -------------------------------------- 

	/** 
	* Sets a value to `program_id` variable
	* @access public
	*/

		public function setProgramId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('program_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `program_id` variable
	* @access public
	*/

		public function getProgramId() {
			return $this->program_id;
		}
	
// ------------------------------ End Field: program_id --------------------------------------




}

/* End of file Schools_programs_model.php */
/* Location: ./application/models/Schools_programs_model.php */
