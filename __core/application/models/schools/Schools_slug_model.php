<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Schools_slug_model Class
 *
 * Manipulates `schools_slug` table on database

CREATE TABLE `schools_slug` (
  `school_id` int(20) NOT NULL,
  `slug` varchar(50) NOT NULL,
  PRIMARY KEY (`school_id`),
  UNIQUE KEY `slug` (`slug`)
);

 ALTER TABLE  `schools_slug` ADD  `school_id` int(20) NOT NULL   PRIMARY KEY;
 ALTER TABLE  `schools_slug` ADD  `slug` varchar(50) NOT NULL   UNIQUE KEY;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Schools_slug_model extends MY_Model {

	protected $school_id;
	protected $slug;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'schools_slug';
		$this->_short_name = 'schools_slug';
		$this->_fields = array("school_id","slug");
		$this->_required = array("slug");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: school_id -------------------------------------- 

	/** 
	* Sets a value to `school_id` variable
	* @access public
	*/

		public function setSchoolId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('school_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `school_id` variable
	* @access public
	*/

		public function getSchoolId() {
			return $this->school_id;
		}
	
// ------------------------------ End Field: school_id --------------------------------------


// ---------------------------- Start Field: slug -------------------------------------- 

	/** 
	* Sets a value to `slug` variable
	* @access public
	*/

		public function setSlug($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('slug', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `slug` variable
	* @access public
	*/

		public function getSlug() {
			return $this->slug;
		}
	
// ------------------------------ End Field: slug --------------------------------------




}

/* End of file Schools_slug_model.php */
/* Location: ./application/models/Schools_slug_model.php */
