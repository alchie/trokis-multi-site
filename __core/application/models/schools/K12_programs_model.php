<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * K12_programs_model Class
 *
 * Manipulates `k12_programs` table on database

CREATE TABLE `k12_programs` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `abbr` varchar(50) NOT NULL,
  `name` text,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `k12_programs` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `k12_programs` ADD  `abbr` varchar(50) NOT NULL   ;
 ALTER TABLE  `k12_programs` ADD  `name` text NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class K12_programs_model extends MY_Model {

	protected $id;
	protected $abbr;
	protected $name;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'k12_programs';
		$this->_short_name = 'k12_programs';
		$this->_fields = array("id","abbr","name");
		$this->_required = array("abbr");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: abbr -------------------------------------- 

	/** 
	* Sets a value to `abbr` variable
	* @access public
	*/

		public function setAbbr($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('abbr', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `abbr` variable
	* @access public
	*/

		public function getAbbr() {
			return $this->abbr;
		}
	
// ------------------------------ End Field: abbr --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

		public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

		public function getName() {
			return $this->name;
		}
	
// ------------------------------ End Field: name --------------------------------------




}

/* End of file K12_programs_model.php */
/* Location: ./application/models/K12_programs_model.php */
