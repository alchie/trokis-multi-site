-- Table structure for table `city_classes` 

CREATE TABLE `city_classes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `class_orgs` 

CREATE TABLE `class_orgs` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `curriculars` 

CREATE TABLE `curriculars` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `districts` 

CREATE TABLE `districts` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `divisions` 

CREATE TABLE `divisions` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `general_classes` 

CREATE TABLE `general_classes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `income_classes` 

CREATE TABLE `income_classes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `k12_programs` 

CREATE TABLE `k12_programs` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `abbr` varchar(50) NOT NULL,
  `name` text,
  PRIMARY KEY (`id`)
);

-- Table structure for table `legislatives` 

CREATE TABLE `legislatives` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `municipals` 

CREATE TABLE `municipals` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `provinces` 

CREATE TABLE `provinces` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `region` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `region` (`region`)
);

-- Table structure for table `provinces_municipals` 

CREATE TABLE `provinces_municipals` (
  `province_id` int(20) NOT NULL,
  `municipal_id` int(20) NOT NULL,
  KEY `province_id` (`province_id`,`municipal_id`)
);

-- Table structure for table `regions` 

CREATE TABLE `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `school_categories` 

CREATE TABLE `school_categories` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `school_types` 

CREATE TABLE `school_types` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `schools` 

CREATE TABLE `schools` (
  `id` int(20) NOT NULL,
  `name` text NOT NULL,
  `region` int(20) DEFAULT NULL,
  `province` int(20) DEFAULT NULL,
  `division` int(20) DEFAULT NULL,
  `municipality` int(20) DEFAULT NULL,
  `district` int(20) DEFAULT NULL,
  `legislative` int(20) DEFAULT NULL,
  `school_type` int(20) DEFAULT NULL,
  `abbr` varchar(100) DEFAULT NULL,
  `prev_name` text,
  `mother_id` int(20) DEFAULT NULL,
  `address` text,
  `school_head` varchar(200) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `telephone` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `date_established` date DEFAULT NULL,
  `general_class` int(20) DEFAULT NULL,
  `sub_class` int(20) DEFAULT NULL,
  `cur_class` int(20) DEFAULT NULL,
  `class_org` int(20) DEFAULT NULL,
  `income_class` int(20) DEFAULT NULL,
  `city_class` int(20) DEFAULT NULL,
  `urp` int(20) DEFAULT NULL,
  `category` int(20) DEFAULT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `lat` decimal(10,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL
);

-- Table structure for table `schools_logo` 

CREATE TABLE `schools_logo` (
  `school_id` int(20) NOT NULL,
  `url` text NOT NULL,
  UNIQUE KEY `school_id` (`school_id`)
);

-- Table structure for table `schools_meta` 

CREATE TABLE `schools_meta` (
  `school_id` int(20) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` text NOT NULL,
  `active` int(1) DEFAULT '1',
  KEY `school_id` (`school_id`)
);

-- Table structure for table `schools_programs` 

CREATE TABLE `schools_programs` (
  `school_id` int(20) NOT NULL,
  `program_id` int(20) NOT NULL,
  KEY `school_id` (`school_id`,`program_id`)
);

-- Table structure for table `schools_slug` 

CREATE TABLE `schools_slug` (
  `school_id` int(20) NOT NULL,
  `slug` varchar(50) NOT NULL,
  PRIMARY KEY (`school_id`),
  UNIQUE KEY `slug` (`slug`)
);

-- Table structure for table `schools_tvl` 

CREATE TABLE `schools_tvl` (
  `school_id` int(20) NOT NULL,
  `tvl_id` int(20) NOT NULL,
  KEY `school_id` (`school_id`,`tvl_id`)
);

-- Table structure for table `seo_blogger` 

CREATE TABLE `seo_blogger` (
  `school_id` int(20) NOT NULL,
  `blog_url` text,
  UNIQUE KEY `school_id` (`school_id`)
);

-- Table structure for table `seo_google_maps` 

CREATE TABLE `seo_google_maps` (
  `school_id` int(20) NOT NULL,
  UNIQUE KEY `school_id` (`school_id`)
);

-- Table structure for table `seo_tumblr` 

CREATE TABLE `seo_tumblr` (
  `school_id` int(20) NOT NULL,
  `blog_url` text,
  UNIQUE KEY `school_id` (`school_id`)
);

-- Table structure for table `seo_webs` 

CREATE TABLE `seo_webs` (
  `school_id` int(20) NOT NULL,
  `blog_url` text,
  UNIQUE KEY `school_id` (`school_id`)
);

-- Table structure for table `seo_weebly` 

CREATE TABLE `seo_weebly` (
  `school_id` int(20) NOT NULL,
  `blog_url` text,
  UNIQUE KEY `school_id` (`school_id`)
);

-- Table structure for table `seo_wix` 

CREATE TABLE `seo_wix` (
  `school_id` int(20) NOT NULL,
  `blog_url` text,
  UNIQUE KEY `school_id` (`school_id`)
);

-- Table structure for table `seo_yola` 

CREATE TABLE `seo_yola` (
  `school_id` int(20) NOT NULL,
  `blog_url` text,
  UNIQUE KEY `school_id` (`school_id`)
);

-- Table structure for table `sub_classes` 

CREATE TABLE `sub_classes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `tvl_specializations` 

CREATE TABLE `tvl_specializations` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
);

-- Table structure for table `urps` 

CREATE TABLE `urps` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

