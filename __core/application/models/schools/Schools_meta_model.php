<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Schools_meta_model Class
 *
 * Manipulates `schools_meta` table on database

CREATE TABLE `schools_meta` (
  `school_id` int(20) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` text NOT NULL,
  `active` int(1) DEFAULT '1',
  KEY `school_id` (`school_id`)
);

 ALTER TABLE  `schools_meta` ADD  `school_id` int(20) NOT NULL   ;
 ALTER TABLE  `schools_meta` ADD  `meta_key` varchar(200) NOT NULL   ;
 ALTER TABLE  `schools_meta` ADD  `meta_value` text NOT NULL   ;
 ALTER TABLE  `schools_meta` ADD  `active` int(1) NULL   DEFAULT '1';


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Schools_meta_model extends MY_Model {

	protected $school_id;
	protected $meta_key;
	protected $meta_value;
	protected $active;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'schools_meta';
		$this->_short_name = 'schools_meta';
		$this->_fields = array("school_id","meta_key","meta_value","active");
		$this->_required = array("school_id","meta_key","meta_value");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: school_id -------------------------------------- 

	/** 
	* Sets a value to `school_id` variable
	* @access public
	*/

		public function setSchoolId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('school_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `school_id` variable
	* @access public
	*/

		public function getSchoolId() {
			return $this->school_id;
		}
	
// ------------------------------ End Field: school_id --------------------------------------


// ---------------------------- Start Field: meta_key -------------------------------------- 

	/** 
	* Sets a value to `meta_key` variable
	* @access public
	*/

		public function setMetaKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('meta_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `meta_key` variable
	* @access public
	*/

		public function getMetaKey() {
			return $this->meta_key;
		}
	
// ------------------------------ End Field: meta_key --------------------------------------


// ---------------------------- Start Field: meta_value -------------------------------------- 

	/** 
	* Sets a value to `meta_value` variable
	* @access public
	*/

		public function setMetaValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('meta_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `meta_value` variable
	* @access public
	*/

		public function getMetaValue() {
			return $this->meta_value;
		}
	
// ------------------------------ End Field: meta_value --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------




}

/* End of file Schools_meta_model.php */
/* Location: ./application/models/Schools_meta_model.php */
