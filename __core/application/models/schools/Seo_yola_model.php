<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Seo_yola_model Class
 *
 * Manipulates `seo_yola` table on database

CREATE TABLE `seo_yola` (
  `school_id` int(20) NOT NULL,
  `blog_url` text,
  UNIQUE KEY `school_id` (`school_id`)
);

 ALTER TABLE  `seo_yola` ADD  `school_id` int(20) NOT NULL   PRIMARY KEY;
 ALTER TABLE  `seo_yola` ADD  `blog_url` text NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Seo_yola_model extends MY_Model {

	protected $school_id;
	protected $blog_url;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'seo_yola';
		$this->_short_name = 'seo_yola';
		$this->_fields = array("school_id","blog_url");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: school_id -------------------------------------- 

	/** 
	* Sets a value to `school_id` variable
	* @access public
	*/

		public function setSchoolId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('school_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `school_id` variable
	* @access public
	*/

		public function getSchoolId() {
			return $this->school_id;
		}
	
// ------------------------------ End Field: school_id --------------------------------------


// ---------------------------- Start Field: blog_url -------------------------------------- 

	/** 
	* Sets a value to `blog_url` variable
	* @access public
	*/

		public function setBlogUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('blog_url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `blog_url` variable
	* @access public
	*/

		public function getBlogUrl() {
			return $this->blog_url;
		}
	
// ------------------------------ End Field: blog_url --------------------------------------




}

/* End of file Seo_yola_model.php */
/* Location: ./application/models/Seo_yola_model.php */
