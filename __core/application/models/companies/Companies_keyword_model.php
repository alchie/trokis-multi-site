<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Companies_keyword_model Class
 *
 * Manipulates `companies_keyword` table on database

CREATE TABLE `companies_keyword` (
  `company_id` int(20) NOT NULL,
  `keyword_id` int(20) NOT NULL,
  KEY `company` (`company_id`)
);

 ALTER TABLE  `companies_keyword` ADD  `company_id` int(20) NOT NULL   ;
 ALTER TABLE  `companies_keyword` ADD  `keyword_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Companies_keyword_model extends MY_Model {

	protected $company_id;
	protected $keyword_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'companies_keyword';
		$this->_short_name = 'companies_keyword';
		$this->_fields = array("company_id","keyword_id");
		$this->_required = array("company_id","keyword_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: company_id -------------------------------------- 

	/** 
	* Sets a value to `company_id` variable
	* @access public
	*/

		public function setCompanyId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('company_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `company_id` variable
	* @access public
	*/

		public function getCompanyId() {
			return $this->company_id;
		}
	
// ------------------------------ End Field: company_id --------------------------------------


// ---------------------------- Start Field: keyword_id -------------------------------------- 

	/** 
	* Sets a value to `keyword_id` variable
	* @access public
	*/

		public function setKeywordId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('keyword_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `keyword_id` variable
	* @access public
	*/

		public function getKeywordId() {
			return $this->keyword_id;
		}
	
// ------------------------------ End Field: keyword_id --------------------------------------




}

/* End of file Companies_keyword_model.php */
/* Location: ./application/models/Companies_keyword_model.php */
