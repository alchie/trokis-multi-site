<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Companies_data_model Class
 *
 * Manipulates `companies_data` table on database

CREATE TABLE `companies_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `streetAddress` varchar(200) DEFAULT NULL,
  `addressLocality` varchar(200) DEFAULT NULL,
  `addressRegion` varchar(200) DEFAULT NULL,
  `telephone` varchar(200) DEFAULT NULL,
  `fax` varchar(200) DEFAULT NULL,
  `geolocation` varchar(200) DEFAULT NULL,
  `email` text,
  `website` text,
  `lastmod` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `phcom_id` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
);

 ALTER TABLE  `companies_data` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `companies_data` ADD  `name` varchar(200) NOT NULL   ;
 ALTER TABLE  `companies_data` ADD  `slug` varchar(200) NOT NULL   UNIQUE KEY;
 ALTER TABLE  `companies_data` ADD  `streetAddress` varchar(200) NULL   ;
 ALTER TABLE  `companies_data` ADD  `addressLocality` varchar(200) NULL   ;
 ALTER TABLE  `companies_data` ADD  `addressRegion` varchar(200) NULL   ;
 ALTER TABLE  `companies_data` ADD  `telephone` varchar(200) NULL   ;
 ALTER TABLE  `companies_data` ADD  `fax` varchar(200) NULL   ;
 ALTER TABLE  `companies_data` ADD  `geolocation` varchar(200) NULL   ;
 ALTER TABLE  `companies_data` ADD  `email` text NULL   ;
 ALTER TABLE  `companies_data` ADD  `website` text NULL   ;
 ALTER TABLE  `companies_data` ADD  `lastmod` timestamp NULL   DEFAULT 'CURRENT_TIMESTAMP';
 ALTER TABLE  `companies_data` ADD  `phcom_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Companies_data_model extends MY_Model {

	protected $id;
	protected $name;
	protected $slug;
	protected $streetAddress;
	protected $addressLocality;
	protected $addressRegion;
	protected $telephone;
	protected $fax;
	protected $geolocation;
	protected $email;
	protected $website;
	protected $lastmod;
	protected $phcom_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'companies_data';
		$this->_short_name = 'companies_data';
		$this->_fields = array("id","name","slug","streetAddress","addressLocality","addressRegion","telephone","fax","geolocation","email","website","lastmod","phcom_id");
		$this->_required = array("name","slug","phcom_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

		public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

		public function getName() {
			return $this->name;
		}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: slug -------------------------------------- 

	/** 
	* Sets a value to `slug` variable
	* @access public
	*/

		public function setSlug($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('slug', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `slug` variable
	* @access public
	*/

		public function getSlug() {
			return $this->slug;
		}
	
// ------------------------------ End Field: slug --------------------------------------


// ---------------------------- Start Field: streetAddress -------------------------------------- 

	/** 
	* Sets a value to `streetAddress` variable
	* @access public
	*/

		public function setStreetaddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('streetAddress', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `streetAddress` variable
	* @access public
	*/

		public function getStreetaddress() {
			return $this->streetAddress;
		}
	
// ------------------------------ End Field: streetAddress --------------------------------------


// ---------------------------- Start Field: addressLocality -------------------------------------- 

	/** 
	* Sets a value to `addressLocality` variable
	* @access public
	*/

		public function setAddresslocality($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('addressLocality', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `addressLocality` variable
	* @access public
	*/

		public function getAddresslocality() {
			return $this->addressLocality;
		}
	
// ------------------------------ End Field: addressLocality --------------------------------------


// ---------------------------- Start Field: addressRegion -------------------------------------- 

	/** 
	* Sets a value to `addressRegion` variable
	* @access public
	*/

		public function setAddressregion($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('addressRegion', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `addressRegion` variable
	* @access public
	*/

		public function getAddressregion() {
			return $this->addressRegion;
		}
	
// ------------------------------ End Field: addressRegion --------------------------------------


// ---------------------------- Start Field: telephone -------------------------------------- 

	/** 
	* Sets a value to `telephone` variable
	* @access public
	*/

		public function setTelephone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('telephone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `telephone` variable
	* @access public
	*/

		public function getTelephone() {
			return $this->telephone;
		}
	
// ------------------------------ End Field: telephone --------------------------------------


// ---------------------------- Start Field: fax -------------------------------------- 

	/** 
	* Sets a value to `fax` variable
	* @access public
	*/

		public function setFax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('fax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `fax` variable
	* @access public
	*/

		public function getFax() {
			return $this->fax;
		}
	
// ------------------------------ End Field: fax --------------------------------------


// ---------------------------- Start Field: geolocation -------------------------------------- 

	/** 
	* Sets a value to `geolocation` variable
	* @access public
	*/

		public function setGeolocation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('geolocation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `geolocation` variable
	* @access public
	*/

		public function getGeolocation() {
			return $this->geolocation;
		}
	
// ------------------------------ End Field: geolocation --------------------------------------


// ---------------------------- Start Field: email -------------------------------------- 

	/** 
	* Sets a value to `email` variable
	* @access public
	*/

		public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `email` variable
	* @access public
	*/

		public function getEmail() {
			return $this->email;
		}
	
// ------------------------------ End Field: email --------------------------------------


// ---------------------------- Start Field: website -------------------------------------- 

	/** 
	* Sets a value to `website` variable
	* @access public
	*/

		public function setWebsite($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('website', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `website` variable
	* @access public
	*/

		public function getWebsite() {
			return $this->website;
		}
	
// ------------------------------ End Field: website --------------------------------------


// ---------------------------- Start Field: lastmod -------------------------------------- 

	/** 
	* Sets a value to `lastmod` variable
	* @access public
	*/

		public function setLastmod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('lastmod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `lastmod` variable
	* @access public
	*/

		public function getLastmod() {
			return $this->lastmod;
		}
	
// ------------------------------ End Field: lastmod --------------------------------------


// ---------------------------- Start Field: phcom_id -------------------------------------- 

	/** 
	* Sets a value to `phcom_id` variable
	* @access public
	*/

		public function setPhcomId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('phcom_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `phcom_id` variable
	* @access public
	*/

		public function getPhcomId() {
			return $this->phcom_id;
		}
	
// ------------------------------ End Field: phcom_id --------------------------------------




}

/* End of file Companies_data_model.php */
/* Location: ./application/models/Companies_data_model.php */
