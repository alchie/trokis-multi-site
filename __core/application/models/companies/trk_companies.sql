-- Table structure for table `category` 

CREATE TABLE `category` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `parent` int(20) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
);

-- Table structure for table `companies` 

CREATE TABLE `companies` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id2` int(20) NOT NULL,
  `md5` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `streetAddress` varchar(200) DEFAULT NULL,
  `addressLocality` varchar(200) DEFAULT NULL,
  `addressRegion` varchar(200) DEFAULT NULL,
  `telephone` varchar(200) DEFAULT NULL,
  `fax` varchar(200) DEFAULT NULL,
  `keywords` text,
  `geolocation` varchar(200) DEFAULT NULL,
  `category` text,
  `category_id` int(20) DEFAULT NULL,
  `email` text,
  `website` text,
  `lastmod` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

-- Table structure for table `companies_category` 

CREATE TABLE `companies_category` (
  `company_id` int(20) NOT NULL,
  `category_id` int(20) NOT NULL,
  KEY `company` (`company_id`)
);

-- Table structure for table `companies_data` 

CREATE TABLE `companies_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `streetAddress` varchar(200) DEFAULT NULL,
  `addressLocality` varchar(200) DEFAULT NULL,
  `addressRegion` varchar(200) DEFAULT NULL,
  `telephone` varchar(200) DEFAULT NULL,
  `fax` varchar(200) DEFAULT NULL,
  `geolocation` varchar(200) DEFAULT NULL,
  `email` text,
  `website` text,
  `lastmod` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `phcom_id` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
);

-- Table structure for table `companies_keyword` 

CREATE TABLE `companies_keyword` (
  `company_id` int(20) NOT NULL,
  `keyword_id` int(20) NOT NULL,
  KEY `company` (`company_id`)
);

-- Table structure for table `companies_media` 

CREATE TABLE `companies_media` (
  `company_id` int(20) NOT NULL,
  `media_type` varchar(100) NOT NULL,
  `media_url` text NOT NULL,
  `media_title` varchar(200) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  KEY `company_id` (`company_id`)
);

-- Table structure for table `companies_meta` 

CREATE TABLE `companies_meta` (
  `company_id` int(20) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` text NOT NULL,
  `active` int(1) DEFAULT '1',
  KEY `company_id` (`company_id`)
);

-- Table structure for table `keywords` 

CREATE TABLE `keywords` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `parent` int(20) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `slug` (`slug`)
);

