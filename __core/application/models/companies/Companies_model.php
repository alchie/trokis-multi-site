<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Companies_model Class
 *
 * Manipulates `companies` table on database

CREATE TABLE `companies` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id2` int(20) NOT NULL,
  `md5` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `streetAddress` varchar(200) DEFAULT NULL,
  `addressLocality` varchar(200) DEFAULT NULL,
  `addressRegion` varchar(200) DEFAULT NULL,
  `telephone` varchar(200) DEFAULT NULL,
  `fax` varchar(200) DEFAULT NULL,
  `keywords` text,
  `geolocation` varchar(200) DEFAULT NULL,
  `category` text,
  `category_id` int(20) DEFAULT NULL,
  `email` text,
  `website` text,
  `lastmod` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

 ALTER TABLE  `companies` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
 ALTER TABLE  `companies` ADD  `id2` int(20) NOT NULL   ;
 ALTER TABLE  `companies` ADD  `md5` varchar(200) NOT NULL   ;
 ALTER TABLE  `companies` ADD  `name` varchar(200) NOT NULL   ;
 ALTER TABLE  `companies` ADD  `streetAddress` varchar(200) NULL   ;
 ALTER TABLE  `companies` ADD  `addressLocality` varchar(200) NULL   ;
 ALTER TABLE  `companies` ADD  `addressRegion` varchar(200) NULL   ;
 ALTER TABLE  `companies` ADD  `telephone` varchar(200) NULL   ;
 ALTER TABLE  `companies` ADD  `fax` varchar(200) NULL   ;
 ALTER TABLE  `companies` ADD  `keywords` text NULL   ;
 ALTER TABLE  `companies` ADD  `geolocation` varchar(200) NULL   ;
 ALTER TABLE  `companies` ADD  `category` text NULL   ;
 ALTER TABLE  `companies` ADD  `category_id` int(20) NULL   ;
 ALTER TABLE  `companies` ADD  `email` text NULL   ;
 ALTER TABLE  `companies` ADD  `website` text NULL   ;
 ALTER TABLE  `companies` ADD  `lastmod` timestamp NULL   DEFAULT 'CURRENT_TIMESTAMP';


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Companies_model extends MY_Model {

	protected $id;
	protected $id2;
	protected $md5;
	protected $name;
	protected $streetAddress;
	protected $addressLocality;
	protected $addressRegion;
	protected $telephone;
	protected $fax;
	protected $keywords;
	protected $geolocation;
	protected $category;
	protected $category_id;
	protected $email;
	protected $website;
	protected $lastmod;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'companies';
		$this->_short_name = 'companies';
		$this->_fields = array("id","id2","md5","name","streetAddress","addressLocality","addressRegion","telephone","fax","keywords","geolocation","category","category_id","email","website","lastmod");
		$this->_required = array("id2","md5","name");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: id2 -------------------------------------- 

	/** 
	* Sets a value to `id2` variable
	* @access public
	*/

		public function setId2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id2` variable
	* @access public
	*/

		public function getId2() {
			return $this->id2;
		}
	
// ------------------------------ End Field: id2 --------------------------------------


// ---------------------------- Start Field: md5 -------------------------------------- 

	/** 
	* Sets a value to `md5` variable
	* @access public
	*/

		public function setMd5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('md5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `md5` variable
	* @access public
	*/

		public function getMd5() {
			return $this->md5;
		}
	
// ------------------------------ End Field: md5 --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

		public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

		public function getName() {
			return $this->name;
		}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: streetAddress -------------------------------------- 

	/** 
	* Sets a value to `streetAddress` variable
	* @access public
	*/

		public function setStreetaddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('streetAddress', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `streetAddress` variable
	* @access public
	*/

		public function getStreetaddress() {
			return $this->streetAddress;
		}
	
// ------------------------------ End Field: streetAddress --------------------------------------


// ---------------------------- Start Field: addressLocality -------------------------------------- 

	/** 
	* Sets a value to `addressLocality` variable
	* @access public
	*/

		public function setAddresslocality($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('addressLocality', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `addressLocality` variable
	* @access public
	*/

		public function getAddresslocality() {
			return $this->addressLocality;
		}
	
// ------------------------------ End Field: addressLocality --------------------------------------


// ---------------------------- Start Field: addressRegion -------------------------------------- 

	/** 
	* Sets a value to `addressRegion` variable
	* @access public
	*/

		public function setAddressregion($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('addressRegion', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `addressRegion` variable
	* @access public
	*/

		public function getAddressregion() {
			return $this->addressRegion;
		}
	
// ------------------------------ End Field: addressRegion --------------------------------------


// ---------------------------- Start Field: telephone -------------------------------------- 

	/** 
	* Sets a value to `telephone` variable
	* @access public
	*/

		public function setTelephone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('telephone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `telephone` variable
	* @access public
	*/

		public function getTelephone() {
			return $this->telephone;
		}
	
// ------------------------------ End Field: telephone --------------------------------------


// ---------------------------- Start Field: fax -------------------------------------- 

	/** 
	* Sets a value to `fax` variable
	* @access public
	*/

		public function setFax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('fax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `fax` variable
	* @access public
	*/

		public function getFax() {
			return $this->fax;
		}
	
// ------------------------------ End Field: fax --------------------------------------


// ---------------------------- Start Field: keywords -------------------------------------- 

	/** 
	* Sets a value to `keywords` variable
	* @access public
	*/

		public function setKeywords($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('keywords', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `keywords` variable
	* @access public
	*/

		public function getKeywords() {
			return $this->keywords;
		}
	
// ------------------------------ End Field: keywords --------------------------------------


// ---------------------------- Start Field: geolocation -------------------------------------- 

	/** 
	* Sets a value to `geolocation` variable
	* @access public
	*/

		public function setGeolocation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('geolocation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `geolocation` variable
	* @access public
	*/

		public function getGeolocation() {
			return $this->geolocation;
		}
	
// ------------------------------ End Field: geolocation --------------------------------------


// ---------------------------- Start Field: category -------------------------------------- 

	/** 
	* Sets a value to `category` variable
	* @access public
	*/

		public function setCategory($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('category', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `category` variable
	* @access public
	*/

		public function getCategory() {
			return $this->category;
		}
	
// ------------------------------ End Field: category --------------------------------------


// ---------------------------- Start Field: category_id -------------------------------------- 

	/** 
	* Sets a value to `category_id` variable
	* @access public
	*/

		public function setCategoryId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('category_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `category_id` variable
	* @access public
	*/

		public function getCategoryId() {
			return $this->category_id;
		}
	
// ------------------------------ End Field: category_id --------------------------------------


// ---------------------------- Start Field: email -------------------------------------- 

	/** 
	* Sets a value to `email` variable
	* @access public
	*/

		public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `email` variable
	* @access public
	*/

		public function getEmail() {
			return $this->email;
		}
	
// ------------------------------ End Field: email --------------------------------------


// ---------------------------- Start Field: website -------------------------------------- 

	/** 
	* Sets a value to `website` variable
	* @access public
	*/

		public function setWebsite($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('website', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `website` variable
	* @access public
	*/

		public function getWebsite() {
			return $this->website;
		}
	
// ------------------------------ End Field: website --------------------------------------


// ---------------------------- Start Field: lastmod -------------------------------------- 

	/** 
	* Sets a value to `lastmod` variable
	* @access public
	*/

		public function setLastmod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('lastmod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `lastmod` variable
	* @access public
	*/

		public function getLastmod() {
			return $this->lastmod;
		}
	
// ------------------------------ End Field: lastmod --------------------------------------




}

/* End of file Companies_model.php */
/* Location: ./application/models/Companies_model.php */
