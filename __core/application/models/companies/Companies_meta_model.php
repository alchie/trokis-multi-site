<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Companies_meta_model Class
 *
 * Manipulates `companies_meta` table on database

CREATE TABLE `companies_meta` (
  `company_id` int(20) NOT NULL,
  `meta_key` varchar(200) NOT NULL,
  `meta_value` text NOT NULL,
  `active` int(1) DEFAULT '1',
  KEY `company_id` (`company_id`)
);

 ALTER TABLE  `companies_meta` ADD  `company_id` int(20) NOT NULL   ;
 ALTER TABLE  `companies_meta` ADD  `meta_key` varchar(200) NOT NULL   ;
 ALTER TABLE  `companies_meta` ADD  `meta_value` text NOT NULL   ;
 ALTER TABLE  `companies_meta` ADD  `active` int(1) NULL   DEFAULT '1';


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Companies_meta_model extends MY_Model {

	protected $company_id;
	protected $meta_key;
	protected $meta_value;
	protected $active;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'companies_meta';
		$this->_short_name = 'companies_meta';
		$this->_fields = array("company_id","meta_key","meta_value","active");
		$this->_required = array("company_id","meta_key","meta_value");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: company_id -------------------------------------- 

	/** 
	* Sets a value to `company_id` variable
	* @access public
	*/

		public function setCompanyId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('company_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `company_id` variable
	* @access public
	*/

		public function getCompanyId() {
			return $this->company_id;
		}
	
// ------------------------------ End Field: company_id --------------------------------------


// ---------------------------- Start Field: meta_key -------------------------------------- 

	/** 
	* Sets a value to `meta_key` variable
	* @access public
	*/

		public function setMetaKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('meta_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `meta_key` variable
	* @access public
	*/

		public function getMetaKey() {
			return $this->meta_key;
		}
	
// ------------------------------ End Field: meta_key --------------------------------------


// ---------------------------- Start Field: meta_value -------------------------------------- 

	/** 
	* Sets a value to `meta_value` variable
	* @access public
	*/

		public function setMetaValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('meta_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `meta_value` variable
	* @access public
	*/

		public function getMetaValue() {
			return $this->meta_value;
		}
	
// ------------------------------ End Field: meta_value --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------




}

/* End of file Companies_meta_model.php */
/* Location: ./application/models/Companies_meta_model.php */
