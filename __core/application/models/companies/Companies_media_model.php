<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Companies_media_model Class
 *
 * Manipulates `companies_media` table on database

CREATE TABLE `companies_media` (
  `company_id` int(20) NOT NULL,
  `media_type` varchar(100) NOT NULL,
  `media_url` text NOT NULL,
  `media_title` varchar(200) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  KEY `company_id` (`company_id`)
);

 ALTER TABLE  `companies_media` ADD  `company_id` int(20) NOT NULL   ;
 ALTER TABLE  `companies_media` ADD  `media_type` varchar(100) NOT NULL   ;
 ALTER TABLE  `companies_media` ADD  `media_url` text NOT NULL   ;
 ALTER TABLE  `companies_media` ADD  `media_title` varchar(200) NOT NULL   ;
 ALTER TABLE  `companies_media` ADD  `active` int(1) NOT NULL   DEFAULT '1';


 * @package			        Model
 * @version_number	        4.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Companies_media_model extends MY_Model {

	protected $company_id;
	protected $media_type;
	protected $media_url;
	protected $media_title;
	protected $active;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'companies_media';
		$this->_short_name = 'companies_media';
		$this->_fields = array("company_id","media_type","media_url","media_title","active");
		$this->_required = array("company_id","media_type","media_url","media_title","active");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: company_id -------------------------------------- 

	/** 
	* Sets a value to `company_id` variable
	* @access public
	*/

		public function setCompanyId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('company_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `company_id` variable
	* @access public
	*/

		public function getCompanyId() {
			return $this->company_id;
		}
	
// ------------------------------ End Field: company_id --------------------------------------


// ---------------------------- Start Field: media_type -------------------------------------- 

	/** 
	* Sets a value to `media_type` variable
	* @access public
	*/

		public function setMediaType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('media_type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `media_type` variable
	* @access public
	*/

		public function getMediaType() {
			return $this->media_type;
		}
	
// ------------------------------ End Field: media_type --------------------------------------


// ---------------------------- Start Field: media_url -------------------------------------- 

	/** 
	* Sets a value to `media_url` variable
	* @access public
	*/

		public function setMediaUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('media_url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `media_url` variable
	* @access public
	*/

		public function getMediaUrl() {
			return $this->media_url;
		}
	
// ------------------------------ End Field: media_url --------------------------------------


// ---------------------------- Start Field: media_title -------------------------------------- 

	/** 
	* Sets a value to `media_title` variable
	* @access public
	*/

		public function setMediaTitle($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('media_title', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `media_title` variable
	* @access public
	*/

		public function getMediaTitle() {
			return $this->media_title;
		}
	
// ------------------------------ End Field: media_title --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

		public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

		public function getActive() {
			return $this->active;
		}
	
// ------------------------------ End Field: active --------------------------------------




}

/* End of file Companies_media_model.php */
/* Location: ./application/models/Companies_media_model.php */
