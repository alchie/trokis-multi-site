<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Subdomains_model Class
 *
 * Manipulates `subdomains` table on database

CREATE TABLE `subdomains` (
  `subdomain` varchar(200) NOT NULL,
  `type` varchar(10) NOT NULL,
  `id` int(20) NOT NULL,
  UNIQUE KEY `subdomain` (`subdomain`)
);

 * @package			        Model
 * @version_number	        3.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Subdomains_model extends MY_Model {

	protected $subdomain;
	protected $type;
	protected $id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'subdomains';
		$this->_short_name = 'subdomains';
		$this->_fields = array("subdomain","type","id");
		$this->_required = array("type","id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: subdomain -------------------------------------- 

	/** 
	* Sets a value to `subdomain` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setSubdomain($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('subdomain', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `subdomain` variable
	* @access public
	* @return String;
	*/

		public function getSubdomain() {
			return $this->subdomain;
		}
	
// ------------------------------ End Field: subdomain --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `type` variable
	* @access public
	* @return String;
	*/

		public function getType() {
			return $this->type;
		}
	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	* @return String;
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------




}

/* End of file Subdomains_model.php */
/* Location: ./application/models/Subdomains_model.php */
