-- Table structure for table `subdomains` 

CREATE TABLE `subdomains` (
  `subdomain` varchar(200) NOT NULL,
  `type` varchar(10) NOT NULL,
  `id` int(20) NOT NULL,
  UNIQUE KEY `subdomain` (`subdomain`)
);

