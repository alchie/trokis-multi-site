<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Municipals_model Class
 *
 * Manipulates `municipals` table on database

CREATE TABLE `municipals` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `region` int(20) DEFAULT NULL,
  `province` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `region` (`region`,`province`)
);

 * @package			        Model
 * @version_number	        3.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Municipals_model extends MY_Model {

	protected $id;
	protected $name;
	protected $region;
	protected $province;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'municipals';
		$this->_short_name = 'municipals';
		$this->_fields = array("id","name","region","province");
		$this->_required = array("name");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `id` variable
	* @access public
	* @return String;
	*/

		public function getId() {
			return $this->id;
		}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `name` variable
	* @access public
	* @return String;
	*/

		public function getName() {
			return $this->name;
		}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: region -------------------------------------- 

	/** 
	* Sets a value to `region` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setRegion($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('region', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `region` variable
	* @access public
	* @return String;
	*/

		public function getRegion() {
			return $this->region;
		}
	
// ------------------------------ End Field: region --------------------------------------


// ---------------------------- Start Field: province -------------------------------------- 

	/** 
	* Sets a value to `province` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setProvince($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('province', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `province` variable
	* @access public
	* @return String;
	*/

		public function getProvince() {
			return $this->province;
		}
	
// ------------------------------ End Field: province --------------------------------------




}

/* End of file Municipals_model.php */
/* Location: ./application/models/Municipals_model.php */
