-- Table structure for table `municipals` 

CREATE TABLE `municipals` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `region` int(20) DEFAULT NULL,
  `province` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `region` (`region`,`province`)
);

-- Table structure for table `provinces` 

CREATE TABLE `provinces` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `region` int(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `region` (`region`)
);

-- Table structure for table `provinces_municipals` 

CREATE TABLE `provinces_municipals` (
  `province_id` int(20) NOT NULL,
  `municipal_id` int(20) NOT NULL,
  KEY `province_id` (`province_id`,`municipal_id`)
);

-- Table structure for table `regions` 

CREATE TABLE `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
);

