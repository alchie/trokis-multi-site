<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Provinces_municipals_model Class
 *
 * Manipulates `provinces_municipals` table on database

CREATE TABLE `provinces_municipals` (
  `province_id` int(20) NOT NULL,
  `municipal_id` int(20) NOT NULL,
  KEY `province_id` (`province_id`,`municipal_id`)
);

 * @package			        Model
 * @version_number	        3.0.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG)
 */
 
class Provinces_municipals_model extends MY_Model {

	protected $province_id;
	protected $municipal_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'provinces_municipals';
		$this->_short_name = 'provinces_municipals';
		$this->_fields = array("province_id","municipal_id");
		$this->_required = array("province_id","municipal_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: province_id -------------------------------------- 

	/** 
	* Sets a value to `province_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setProvinceId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('province_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `province_id` variable
	* @access public
	* @return String;
	*/

		public function getProvinceId() {
			return $this->province_id;
		}
	
// ------------------------------ End Field: province_id --------------------------------------


// ---------------------------- Start Field: municipal_id -------------------------------------- 

	/** 
	* Sets a value to `municipal_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

		public function setMunicipalId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
			return $this->_set_field('municipal_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
		}
	
	/** 
	* Get the value of `municipal_id` variable
	* @access public
	* @return String;
	*/

		public function getMunicipalId() {
			return $this->municipal_id;
		}
	
// ------------------------------ End Field: municipal_id --------------------------------------




}

/* End of file Provinces_municipals_model.php */
/* Location: ./application/models/Provinces_municipals_model.php */
