<?php 
$manage = null;
switch($app_id) {
	case 'www':
  		$manage = array(
  			main_url('manage/regions')=>'Regions',
  			main_url('manage/provinces')=>'Provinces',
  			main_url('manage/municipals')=>'Municipals'
  		);
  	break;
  	case 'realestate':
  		$manage = array(
          realestate_url('manage/messages')=>'Messages',
          realestate_url('manage/partners')=>'Partners',
          realestate_url('manage/properties')=>'Properties',
          realestate_url('manage/developers')=>'Developers',
        );
  	break;
    case 'companies':
      $manage = array(
          companies_url('manage/messages')=>'Messages',
        );
    break;
}
if( $manage ) {
?>
<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage <span class="caret"></span></a>
  <ul class="dropdown-menu">
  <?php if(count($manage)) foreach($manage as $u=>$t) { ?>
    <li><a href="<?php echo $u; ?>"><?php echo $t; ?></a></li>
  <?php } ?>
  </ul>
</li>
<?php } ?>