<?php 
$seo = null;
switch($app_id) {
    case 'schools':
      $seo = array(
          schools_url('seo/google_maps')=>'Google Maps',
          schools_url('seo/blogger')=>'Blogger',
          schools_url('seo/wix')=>'Wix.com',
          schools_url('seo/webs')=>'Webs.com',
          schools_url('seo/weebly')=>'Weebly.com',
          schools_url('seo/tumblr')=>'Tumblr.com',
          schools_url('seo/yola')=>'Yola.com',
        );
    break;
    case 'companies':
      $seo = array(
          companies_url('seo/google_maps')=>'Google Maps',
          companies_url('seo/blogger')=>'Blogger',
          companies_url('seo/wix')=>'Wix.com',
          companies_url('seo/webs')=>'Webs.com',
          companies_url('seo/weebly')=>'Weebly.com',
          companies_url('seo/tumblr')=>'Tumblr.com',
          companies_url('seo/yola')=>'Yola.com',
        );
    break;
}
if( $seo ) {
?>
<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SEO <span class="caret"></span></a>
  <ul class="dropdown-menu">
  <?php if(count($seo)) foreach($seo as $u=>$t) { ?>
    <li><a href="<?php echo $u; ?>"><?php echo $t; ?></a></li>
  <?php } ?>
  </ul>
</li>
<?php } ?>