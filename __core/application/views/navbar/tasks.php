<?php 
$tasks = array();

if( !$this->session->userdata('emailVerified') ) { 
  $tasks[] = array(
    'title' => 'Verify Email Address',
    'percent' => '80',
    'url' => account_url("profile/verify"),
    );
}

if( count($tasks) > 0 ) {
?>
<li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger"><?php echo count($tasks); ?></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
<?php foreach($tasks as $task) { ?>
                  <li><!-- Task item -->
                    <a href="<?php echo $task['url']; ?>">
                      <h3>
                        <?php echo $task['title']; ?>
                        <small class="pull-right"><?php echo $task['percent']; ?>%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-danger" style="width: <?php echo $task['percent']; ?>%" role="progressbar" aria-valuenow="<?php echo $task['percent']; ?>" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only"><?php echo $task['percent']; ?>% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
<?php } ?>
                </ul>
              </li>
            </ul>
          </li>
<?php } ?>