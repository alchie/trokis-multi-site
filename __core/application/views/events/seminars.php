<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
 
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Seminars
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">


<div class="row">
<div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-body">

<ul class="list-unstyled">
<?php foreach($municipals as $municipal) { 
$slug = url_title($municipal->name, "-", TRUE);
  ?>
  <li class="col-md-3"><a href="<?php echo jobs_url("seminars/in/{$slug}/{$municipal->id}"); ?>"><span class="glyphicon glyphicon-arrow-right"></span> Seminars in <?php echo $municipal->name; ?></a></li>
<?php } ?>
</ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
</div>
<!-- /.row -->
    <?php $this->load->view('admin_controls/admin_tools'); ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>