<?php $this->load->view("account/header"); ?>

  <!-- /.login-logo -->
  <div class="login-box-body">

	<h4 class="text-center">Verification Successful</h4>
	<p class="text-center">You have successfully verified your account. Thank you!</p>

  </div>
  <!-- /.login-box-body -->


<?php $this->load->view("account/footer"); ?>