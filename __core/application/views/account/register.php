<?php $this->load->view("account/header"); ?>

  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="text" name="full_name" class="form-control" placeholder="Full name" value="<?php echo $this->input->post('full_name'); ?>">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo $this->input->post('email'); ?>">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo $this->input->post('password'); ?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password2" class="form-control" placeholder="Retype password" value="<?php echo $this->input->post('password2'); ?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="agree" value="1"> I agree to the <a href="<?php echo account_url("terms"); ?>" target="_blank">terms</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

<?php $this->load->view("account/social_auth"); ?>
<p></p>
    <a href="<?php echo account_url("login"); ?><?php echo ($this->input->get('app')) ? "?app=" . $this->input->get('app') : ""; ?>">I already have a membership</a>

  </div>
  <!-- /.login-box-body -->

<!-- /.login-box -->

<?php $this->load->view("account/footer"); ?>
