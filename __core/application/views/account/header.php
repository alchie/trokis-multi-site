
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php if( isset($meta_refresh) && $meta_refresh ) { ?>
    <meta http-equiv="refresh" content="5; url=<?php echo $redirect_url; ?>">
  <?php } ?>
  <title><?php echo $page_title; ?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo assets_url('css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo assets_url('font-awesome/css/font-awesome.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo assets_url('icons/ionicons/css/ionicons.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo assets_url('adminlte/css/AdminLTE.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo assets_url('plugins/iCheck/square/blue.css'); ?>">
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo main_url(); ?>"><b>TrokisPH</b></a>
  </div>
  