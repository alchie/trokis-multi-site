 <!-- Profile Image -->
<div class="box box-primary">
  <div class="box-body box-profile">
    <img class="profile-user-img img-responsive img-circle" src="<?php echo assets_url('adminlte/img/no-avatar.jpg'); ?>" alt="User profile picture">

    <h3 class="profile-username text-center"><?php echo $this->session->fullName; ?></h3>

    <ul class="list-group list-group-unbordered">
      <li class="list-group-item">
        <b>Followers</b> <a href="<?php echo account_url("followers"); ?>" class="pull-right">1,322</a>
      </li>
      <li class="list-group-item">
        <b>Following</b> <a href="<?php echo account_url("following"); ?>" class="pull-right">543</a>
      </li>
      <li class="list-group-item">
        <b>Friends</b> <a href="<?php echo account_url("friends"); ?>" class="pull-right">13,287</a>
      </li>
    </ul>

    <a href="<?php echo account_url("profile/edit"); ?>" class="btn btn-warning btn-block"><b>Edit Profile</b></a>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->