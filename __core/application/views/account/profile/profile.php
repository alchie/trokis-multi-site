<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>

    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo main_url(); ?>">Home</a></li>
        <li class="active">User Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

 <?php $this->load->view('account/profile/profile_sidebar'); ?>
         
        </div>
        <!-- /.col -->
        <div class="col-md-9">
         
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

<?php $this->load->view('footer'); ?>