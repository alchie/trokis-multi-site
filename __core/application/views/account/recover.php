<?php $this->load->view("account/header"); ?>

  <!-- /.login-logo -->
  <div class="login-box-body">

    <p class="login-box-msg">Forgot your password?</p>

    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Recover Password</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

<?php $this->load->view("account/social_auth"); ?>
<p></p>
    <a href="<?php echo account_url("login"); ?><?php echo ($this->input->get('app')) ? "?app=" . $this->input->get('app') : ""; ?>">I already have a membership</a><br>
    <a href="<?php echo account_url("register"); ?><?php echo ($this->input->get('app')) ? "?app=" . $this->input->get('app') : ""; ?>" class="text-center">Register a new membership</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php $this->load->view("account/footer"); ?>