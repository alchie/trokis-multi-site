<?php $this->load->view("account/header"); ?>

  <!-- /.login-logo -->
  <div class="login-box-body">

	<h4 class="text-center">A verification link has been sent to your email account</h4>
	<p class="text-center">Please click on the link that has just been sent to your email account to verify your email.</p>

  </div>
  <!-- /.login-box-body -->


<?php $this->load->view("account/footer"); ?>