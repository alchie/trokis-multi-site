<?php $this->load->view("account/header"); ?>

  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <form action="" method="post">
    <?php if( $this->input->get('app') ) {
      echo '<input type="hidden" name="app_id" value="'.$this->input->get('app').'">';
      } ?>
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

<?php $this->load->view("account/social_auth"); ?>
<p></p>
    <a href="<?php echo account_url("recover"); ?><?php echo ($this->input->get('app')) ? "?app=" . $this->input->get('app') : ""; ?>">I forgot my password</a><br>
    <a href="<?php echo account_url("register"); ?><?php echo ($this->input->get('app')) ? "?app=" . $this->input->get('app') : ""; ?>" class="text-center">Register a new membership</a>

  </div>
  <!-- /.login-box-body -->


<?php $this->load->view("account/footer"); ?>
