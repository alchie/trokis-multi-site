<?php $this->load->view("account/header"); ?>


<div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

            <div class="info-box-content">
             <a href="<?php echo finance_url(); ?>">
              <span class="info-box-text">Finance</span>
              </a>
              <div class="progress">
                <div class="progress-bar" style="width: 50%"></div>
              </div>
                  <span class="progress-description">
                    50% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>



<?php $this->load->view("account/footer"); ?>