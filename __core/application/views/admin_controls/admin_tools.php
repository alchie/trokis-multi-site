<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
<section class="content admin_controls">
	<div class="callout callout-info">
	<h4>Admin Tools</h4>
	                <a type="button" class="btn btn-box-tool" href="<?php echo site_url("manage/clear_cache?uri=" . uri_string()); ?>">Clear Cache</a> &middot; <a type="button" class="btn btn-box-tool" href="<?php echo site_url("manage/clear_all_cache?uri=" . uri_string()); ?>">Clear All Cache</a>
	</div>
</section>
<?php } ?>