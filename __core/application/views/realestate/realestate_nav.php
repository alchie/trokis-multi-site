<div class="container">
  <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-tabs">
<?php 

foreach(array(
	'index'=>'Properties',
	'projects'=>'Projects',
	'developers'=>'Developers',
) as $uri=>$name) { ?>
          <li role="presentation" class="<?php echo (isset($page_id)&&($page_id==$uri)) ? "active" : ""; ?>"><a href="<?php echo realestate_url($uri); ?>"><?php echo $name; ?></a></li>
<?php } ?>
        </ul>
      </div>
  </div>
</div>