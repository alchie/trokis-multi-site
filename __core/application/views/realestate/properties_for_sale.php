<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
 
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Properties for Sale
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">


<?php $this->load->view('realestate/realestate_card'); ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>