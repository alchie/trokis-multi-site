<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>
 
<div class="content-wrapper">

<div class="container">

<div class="row" style="margin-top: 15px">
	<div class="col-md-8">
		
<div class="box box-widget widget-user" itemscope itemtype="http://schema.org/Property">

<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
<div class="box-header with-border">
              <div class="box-title"><center>

<div class="btn-group">

    <a class="btn btn-success btn-xs" href="<?php echo realestate_url("manage/properties_edit/{$property->id}"); ?>"> Edit Info</a>
   <a class="btn btn-primary btn-xs" href="<?php echo realestate_url("manage/properties_media/{$property->id}"); ?>"> Media</a>
    <a class="btn btn-info btn-xs" href="<?php echo realestate_url("manage/properties_details/{$property->id}"); ?>"> Details</a>
 
</div>

</div>

</center>

</div>
<?php } ?>

<?php 
$logo_url = $this->config->item('trokis_logo_url');
if( $property->logo_url != '' ) {
  $logo_url = $property->logo_url;
}
?>
            <div class="widget-user-header bg-black" style="background: url(<?php echo $logo_url; ?>) center center;height:200px!important;">
            </div>

            <div class="box-body">
              <h3 class="widget-user-username" itemprop="name"><?php echo $property->title; ?></h3>


<?php if( $property->address ) { ?>
              <h5 class="widget-user-desc"><?php echo $property->address; ?></h5>
<?php } ?>
<hr>

<?php if( !$property->active ) { ?>
  <center><img src="http://www.hypebot.com/.a/6a00d83451b36c69e201bb0870c212970d-600wi"></center><hr>
<?php } ?>

<?php if($medias) { ?>
<div class="row">
  <?php foreach($medias as $media) { ?>
  <div class="col-xs-6 col-md-3 <?php echo ($media->active==1) ? '' : 'btn btn-danger'; ?>">
    <a href="<?php echo realestate_url("property/media/{$media->media_id}"); ?>" class="thumbnail">
      <img src="<?php echo ($media->thumbnail!='') ? $media->thumbnail : $media->media_value; ?>" alt="<?php echo $media->alt_text; ?>">
    </a>
  </div>
  <?php } ?>
</div>
<hr style="margin-top:0;">
<?php } ?>

<div class="row">
	<div class="col-md-7">
              <p class="widget-user-details">
              	<?php echo str_replace("\n", "<br>\n", $property->details); ?>
              </p>
    </div>
    <div class="col-md-5">
    	<ul class="list-group">
		  <li class="list-group-item"><strong>Price:</strong> <a target="_blank" href="https://www.google.com.ph/search?q=<?php echo urlencode( $property->price . " PHP to USD" ); ?>" class="pull-right">&#8369;<?php echo number_format($property->price,2); ?></a></li>
<?php if( $property->lot_area ) { ?>
		  <li class="list-group-item"><strong>Lot Area:</strong> <span class="pull-right"><?php echo $property->lot_area; ?> sqm</span></li>
<?php } ?>
<?php if( $property->floor_area ) { ?>
		  <li class="list-group-item"><strong>Floor Area:</strong> <span class="pull-right"><?php echo $property->floor_area; ?> sqm</span></li>
<?php } ?>
<?php if( $property->beds ) { ?>
		  <li class="list-group-item"><strong>Beds:</strong>  <span class="pull-right"><?php echo $property->beds; ?></span></li>
<?php } ?>
<?php if( $property->baths ) { ?>
		  <li class="list-group-item"><strong>Baths:</strong>  <span class="pull-right"><?php echo $property->baths; ?></span></li>
<?php } ?>
		</ul>
    </div>
</div>
            </div>


<?php if( ($property->geo_lat > 0) && ($property->geo_lng > 0)) { ?>

<style type="text/css">
  <!--
#map {
    width: 100%; height: 400px
}
#mapInfoWindowHeading {
  font-size: 15px;
  text-align: center;
    width: 100%;
    padding: 0;
    margin: 0;
    font-weight: bold;
}
  -->
</style>
<div id="map"></div>
    <script>
      function initMap() {
        var coordinates = {lat: <?php echo $property->geo_lat; ?>, lng: <?php echo $property->geo_lng; ?>};
        var center_coor = {lat: <?php echo $property->geo_lat; ?>, lng: <?php echo $property->geo_lng; ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: center_coor
        });
        var marker = new google.maps.Marker({
          position: coordinates,
          map: map,
          title: '<?php echo addslashes($property->title); ?>'
        });

      }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item('google_api_key'); ?>&callback=initMap"></script>

<?php } else { ?>

<?php if($property->address != '') { ?>
<iframe width="100%" height="400" frameborder="0" style="border:0"
src="https://www.google.com/maps/embed/v1/place?q=<?php echo urlencode($property->address); ?>&key=AIzaSyBCfHj_vkfbi-MJN6d4neNIqVz8MtUAF8w" allowfullscreen></iframe>
<?php } ?>

<?php } ?>


</div>

</div>
<div class="col-md-4 affix-relative">

    <?php $this->load->view('realestate/realestate_sidebar'); ?>

</div>
	</div>
</div>

<?php $this->load->view('admin_controls/admin_tools'); ?>

</div>


<!-- /.content-wrapper -->
<?php $this->load->view('footer'); ?>