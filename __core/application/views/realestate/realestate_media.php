<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

<div class="content-wrapper">

<div class="container">

<div class="row" style="margin-top: 15px">
  <div class="col-md-12">

<div class="panel panel-default">
  <div class="panel-heading">

<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
  <?php if( $media->active==1 ) { ?>
    <a class="btn btn-danger btn-xs pull-right" href="<?php echo realestate_url("manage/properties_media_disable/{$media->media_id}"); ?>?back=<?php echo uri_string(); ?>"> Disable</a>
  <?php } else { ?>
    <a class="btn btn-success btn-xs pull-right" href="<?php echo realestate_url("manage/properties_media_enable/{$media->media_id}"); ?>?back=<?php echo uri_string(); ?>"> Enable</a>
  <?php } ?>
<?php } ?>

    <h3 class="panel-title">

<a href="<?php echo realestate_url("property/view/{$property->slug}"); ?>">
    <?php echo $property->title; ?>
</a>
</h3>
  </div>
  <div class="panel-body">
    
<?php if( $media->media_type=='picture') { ?>
        <img src="<?php echo $media->media_value; ?>" alt="<?php echo $media->alt_text; ?>" width="100%" />
      <?php } elseif( $media->media_type=='youtube') { ?>
        <div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="<?php echo $media->media_value; ?>" width="640" height="360" frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen></iframe></div>
      <?php } ?>

  </div>
</div>

  </div>
</div>

</div>

<?php $this->load->view('realestate/realestate_card'); ?>

</div>
<?php $this->load->view('footer'); ?>