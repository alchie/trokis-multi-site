<style type="text/css">
  .search_box {
    background: url("<?php echo assets_url("images/re_cover.jpg"); ?>");
    height: 450px;
    padding-top: 200px;
  }
  .search_box .title {
    text-align: center;
    color: #fff;
    font-weight: 600;
    font-size: 32px;
    text-shadow: 0 0 25px #000;
    text-transform: uppercase;
  }
  .partners {
    background-color: #FFF;
    padding: 10px 0;
  }
</style>
<section class="content search_box">
<form method="get" action="">
<div class="row">
    <div class="col-md-10 col-md-offset-1">
      <h1 class="title"><?php echo $page_title; ?></h1>

<div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
<?php 
$default_status = (isset($property_status)) ? $property_status : 'all';
foreach(array('all'=>'All Properties', 'sale'=>'For Sale', 'rent'=>'For Rent') as $status=>$title) { ?>
              <li class="<?php echo ($status==$default_status) ? 'active' : ''; ?>"><a href="<?php echo realestate_url("properties/{$status}"); ?>"><?php echo strtoupper($title); ?></a></li>
<?php } ?>
            </ul>
            <div class="tab-content">
              <div class="row">
                <div class="col-md-3">
                <select class="form-control" name="property_type">
<?php foreach(array('all'=>'All Properties', 'land'=>'Land', 'house'=>'House', 'forclosures'=>'Forclosures', 'apartment'=>'Apartment', 'commercial'=>'Commercial', 'condominium'=>'Condominium') as $type=>$title) { ?>
                    <option value="<?php echo $type; ?>"><?php echo $title; ?></option>
<?php } ?>
                  </select>
                </div>
                <div class="col-md-4">
                  <input type="text" class="form-control" placeholder="Where do you want to live?" name="where">
                </div>
                <div class="col-md-3">
                <input type="text" class="form-control" placeholder="Maximum Budget?" name="budget">
                </div>
                <div class="col-md-2">
                <button type="submit" class="btn btn-success btn-block"><i class="glyphicon glyphicon-search"></i> Search</button>
                </div>
              </div>
            </div>
            <div class="tab-content">
              <div class="row">
                <div class="col-md-3">
                  <strong>Popular Searches: </strong>
                </div>
              </div>
            </div>
            <!-- /.tab-content -->
          </div>

    </div>
</div>
</form>
</section>
