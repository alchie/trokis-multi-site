<?php if( isset($developers) && ($developers) ) { ?>
<div class="container-fluid">
<div class="container container-pad" id="property-listings">
<div class="row">
<?php foreach($developers as $developer) { 
$logo_url = $this->config->item('trokis_logo_url');
if( $developer->logo_url != '' ) {
  $logo_url = $developer->logo_url;
}
?>
<div class="col-md-6">
            <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing">
                <div class="media">
                    <a class="pull-left" href="<?php echo realestate_url("developer/view/{$developer->id}/{$developer->slug}"); ?>" target="_parent">
                    <img alt="image" class="img-responsive" src="<?php echo $logo_url; ?>"></a>

                    <div class="clearfix visible-sm"></div>

                    <div class="media-body fnt-smaller">
                        <a href="<?php echo realestate_url("developer/view/{$developer->id}/{$developer->slug}"); ?>" target="_parent"></a>

                        <h4 class="media-heading">
                          <a href="<?php echo realestate_url("developer/view/{$developer->id}/{$developer->slug}"); ?>" target="_parent"><?php echo $developer->name; ?></a></h4>


                        <p class="hidden-xs"><?php echo substr($developer->content, 0, 160); ?></p>

                        <span class="fnt-smaller fnt-lighter fnt-arial"><?php echo $developer->website; ?></span>
                    </div>
                </div>
            </div><!-- End Listing-->
</div>
<?php } ?>
</div>
</div>
</div>

<center><?php echo $pagination; ?></center>

<?php } ?>