
		<div class="box box-widget">
			 <div class="box-header"><h3 class="box-title">Call or Text Me</h3></div>
			 <div class="box-body text-center">
			 <img itemprop="image" class="profile-user-img img-responsive img-circle" src="https://4.bp.blogspot.com/-BlMRNLmtvFk/VYA05pyBYHI/AAAAAAAAIJg/uxvUjm5ryUU/s100/avatar.jpg" alt="Nava Motor Repair Shop Logo">
			 	<h4><strong>Alchie Tagudin</strong><br>+63 (946) 255-9955</h4>

<div class="btn-group">
  <a href="https://www.facebook.com/alchietagudin" class="btn btn-facebook" target="_blank"><i class="fa fa-facebook-square"></i> Facebook</a>
</div>
			 </div>
		</div>

		<div class="box box-widget">
		<div class="box-header"><h3 class="box-title">Leave a message &amp; I'll call you.</h3></div>
			 <div class="box-body">
			 	
<form method="post" action="<?php echo realestate_url("property/add_message/{$property->slug}"); ?>">
  <div class="form-group">
    <label for="sidebarContactName">Name</label>
    <input type="text" class="form-control" id="sidebarContactName" placeholder="Full Name" name="full_name"  required="required">
  </div>
  <div class="form-group">
    <label for="sidebarContactEmail">Email</label>
    <input type="email" class="form-control" id="sidebarContactEmail" placeholder="Email" name="email">
  </div>
  <div class="form-group">
    <label for="sidebarContactPhone">Phone Number</label>
    <input type="text" class="form-control" id="sidebarContactPhone" placeholder="Phone Number" name="phone_number" required="required">
  </div>
    <div class="form-group">
    <label for="sidebarContactPhone">Message</label>
    <textarea type="text" class="form-control" id="sidebarContactMessage" placeholder="Message" name="message"></textarea>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>

			 </div>
		</div>


<?php if( $related ) { ?>

          <div class="box box-primary">

            <div class="box-header with-border">
              <small class="pull-right"><a href="<?php echo realestate_url(); ?>">Show More</a></small>
                          <h3 class="box-title">More Properties for Sale</h3>
            </div>

            <div class="box-body box-profile">
                 
<ul class="list-group">
<?php foreach($related as $property) { ?>
<?php 
$plogo_url = $this->config->item('trokis_logo_url');
if( $property->logo_url != '' ) {
  $plogo_url = $property->logo_url;
}
?>
<div class="list-group-item" itemscope itemtype="http://schema.org/RealEstate">
  <a itemprop="url" href="<?php echo realestate_url("property/view/{$property->slug}"); ?>">

<div class="row">
  <div class="col-md-4"><img itemprop="logo" class="profile-user-img" src="<?php echo $plogo_url; ?>" alt=""></div>
  <div class="col-md-8"><span itemprop="name"><?php echo $property->title; ?></span></div>
</div>
  </a>
</div>
<?php } ?>
</ul>

            </div>
            <!-- /.box-body -->
          </div>

<?php } ?>

