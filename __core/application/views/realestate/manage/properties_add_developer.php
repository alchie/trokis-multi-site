<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

          <div class="box box-primary">
            <div class="box-header with-border">
            <a href="<?php echo realestate_url("manage/properties_edit/{$property->id}"); ?>" class="btn btn-warning btn-xs pull-right">Back</a>
              <h3 class="box-title"><?php echo ( $property->dev_id ) ? "Change" : "Add"; ?> Developer</h3><br>
              <small><?php echo $property->title; ?></small>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<div class="row">
<div class="col-md-8 col-md-offset-2">
<?php if( $property->dev_id ) { ?>
<div class="well text-center"><?php echo $property->developer_name; ?></div>
<?php } ?>
<div class="well">
  <form method="get">
  <div class="input-group">
        <input name="q" type="text" class="form-control" placeholder="Search projects">
        <span class="input-group-btn">
          <button class="btn btn-default" type="submit">Search</button>
        </span>
  </div><!-- /input-group -->
  </form>
</div>

<?php if( $this->input->get("q") ) { ?>
<?php if( $developers ) { ?>
<div class="well">
  <form method="post">
    <ul class="list-group">
    <?php foreach($developers as $developer) { ?>
      <li class="list-group-item">

<div class="checkbox">
    <label>
      <input type="radio" name="dev_id" value="<?php echo $developer->id; ?>"> <?php echo $developer->name; ?>
    </label>
  </div>
      </li>
    <?php } ?>
  </ul>
                <button type="submit" class="btn btn-primary">Submit</button>

  </form>
  </div>
<?php } ?>
<?php } ?>

</div>
</div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->

    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>