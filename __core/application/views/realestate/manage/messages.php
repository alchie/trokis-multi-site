<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">


          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Messages</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
<?php if( $messages ) { ?>
              <div class="table-responsive mailbox-messages">
              
                <table class="table table-hover table-striped">
                  <tbody>
<?php foreach($messages as $message) { ?>
                  <tr>
                    <td width="1%"><?php echo $message->id; ?></td>
                    <td><?php echo $message->full_name; ?></td>
                    <td><?php echo $message->email; ?></td>
                    <td><?php echo $message->phone_number; ?></td>
                    <td><?php echo $message->message; ?></td>
                    <td class="mailbox-date text-right"><a target="_blank" class="btn btn-success btn-xs" href="<?php echo realestate_url("manage/message_property/{$message->property_id}"); ?>">View Property</a></td>
                  </tr>
<?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
<?php } else { ?>
  <p class="text-center" style="margin:20px;">Nothing Found!</p>
<?php } ?>
            </div>
            <!-- /.box-body -->
<?php if( $pagination ) { ?>
            <div class="box-footer">
            <p class="text-center"><?php echo $pagination; ?></p>
            </div>
<?php } ?>
          </div>
          <!-- /. box -->
    
    </section>
    <!-- /.content -->

<?php $this->load->view('admin_controls/admin_tools'); ?>

  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>