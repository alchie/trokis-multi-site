<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->


    <!-- Main content -->
    <section class="content">

<ul class="nav nav-tabs">
  <li role="presentation" class="pull-right"><a target="_blank" href="<?php echo realestate_url("property/view/{$property->slug}"); ?>">View</a></li>
  
  <li role="presentation"><a href="<?php echo realestate_url("manage/properties"); ?>">List</a></li>
  <li role="presentation" class="active"><a href="<?php echo realestate_url("manage/properties_edit/{$property->id}"); ?>">Property</a></li>
  <li role="presentation"><a href="<?php echo realestate_url("manage/properties_media/{$property->id}"); ?>">Media</a></li>
  <li role="presentation"><a href="<?php echo realestate_url("manage/properties_details/{$property->id}"); ?>">Details</a></li>
</ul>

<form method="post">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $property->title; ?></h3>
<br><small>
<a href="<?php echo realestate_url("manage/properties_add_project/{$property->id}"); ?>">
<span class="badge">
<?php if( $property->proj_id ) { ?>
   <?php echo $property->project_name; ?>
<?php } else { ?>
  Add Project
<?php } ?>
</span>
</a>

<span class="badge">
<?php if( $property->dev_id ) { ?>
   <?php echo $property->developer_name; ?>
<?php } else { ?>
  Add Developer
<?php } ?>
</span>
</small>

            </div>
            <!-- /.box-header -->
            <div class="box-body">


<div class="checkbox pull-right">
    <label>
      <input type="checkbox" name="rent" <?php echo ($property->rent) ? ' CHECKED ' : ''; ?> value="1"> For Rent
    </label>

    <label>
      <input type="checkbox" name="active" <?php echo ($property->active) ? ' CHECKED ' : ''; ?> value="1"> Active
    </label>

  </div>

                <div class="form-group">
                  <label>Property Type</label>
                  <select class="form-control" name="type">
                      <?php foreach(array('land'=>'Land', 'house'=>'House', 'forclosures'=>'Forclosures', 'apartment'=>'Apartment', 'commercial'=>'Commercial', 'condominium'=>'Condominium') as $type=>$title) { ?>
                    <option value="<?php echo $type; ?>" <?php echo ($property->type==$type) ? ' SELECTED ' : ''; ?>><?php echo $title; ?></option>
<?php } ?>
                  </select>
                </div>

              <div class="form-group">
                  <label>Title</label>
                  <input type="text" class="form-control" placeholder="Property Title" name="title" value="<?php echo $property->title; ?>">
                </div>

              <div class="form-group">
                  <label>Slug</label>
                  <input type="text" class="form-control" placeholder="Property Slug" name="slug" value="<?php echo $property->slug; ?>">
                </div>


<div class="row">
  <div class="col-md-9">
        <div class="form-group">
      <label>Details</label>
      <textarea rows="17" class="form-control" placeholder="Details" name="details"><?php echo $property->details; ?></textarea>
    </div>
  </div>
  <div class="col-md-3">
                <div class="form-group">
                  <label>Price</label>
                  <input type="text" class="form-control text-right" placeholder="Property Price" name="price" value="<?php echo $property->price; ?>">
                </div>
                <div class="form-group">
                  <label>Beds</label>
                  <input type="text" class="form-control text-right" placeholder="Number of Beds" name="beds" value="<?php echo $property->beds; ?>">
                </div>
                <div class="form-group">
                  <label>Baths</label>
                  <input type="text" class="form-control text-right" placeholder="Number of Baths" name="baths" value="<?php echo $property->baths; ?>">
                </div>
                <div class="form-group">
                  <label>Floor Area</label>
                  <input type="text" class="form-control text-right" placeholder="Floor Area" name="floor_area" value="<?php echo $property->floor_area; ?>">
                </div>
                <div class="form-group">
                  <label>Lot Area</label>
                  <input type="text" class="form-control text-right" placeholder="Lot Area" name="lot_area" value="<?php echo $property->lot_area; ?>">
                </div>
  </div>
</div>

            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo realestate_url("manage/properties/{$property->dev_id}/{$property->proj_id}"); ?>" class="btn btn-default">Back</a>
              </div>
          </div>
          <!-- /. box -->
</form>
    
    </section>
    <!-- /.content -->

    <?php $this->load->view('admin_controls/admin_tools'); ?>
    
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>