<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<form method="post">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Detail : <a href="<?php echo realestate_url("manage/properties_edit/{$property->id}"); ?>"><small><?php echo $property->title; ?></small></a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<div class="checkbox pull-right">

    <label>
      <input type="checkbox" name="active" CHECKED value="1"> Active
    </label>

  </div>


              <div class="form-group">
                  <label>Detail Key</label>
                  <input type="text" class="form-control" placeholder="Detail Key" name="detail_key" value="">
                  <small class="help-block">address, geo_lat, geo_lng</small>
                </div>

              <div class="form-group">
                  <label>Detail Content</label>
                  <textarea type="text" class="form-control" placeholder="Detail Content" name="detail_content" rows="5"></textarea>
                </div>

            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo realestate_url("manage/properties_details/{$property->id}"); ?>" class="btn btn-default">Back</a>
              </div>
          </div>
          <!-- /. box -->
</form>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>