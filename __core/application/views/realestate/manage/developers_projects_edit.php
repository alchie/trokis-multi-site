<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<form method="post">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Project <span class="badge"><?php echo $developer->name; ?></span></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<div class="checkbox pull-right">
    <label>
      <input type="checkbox" name="active" <?php echo ($project->active==1) ? 'CHECKED' : ''; ?> value="1"> Active
    </label>
  </div>

              <div class="form-group">
                  <label>Project Name</label>
                  <input type="text" class="form-control" placeholder="Project Name" name="name" value="<?php echo $project->name; ?>">
                </div>

              <div class="form-group">
                  <label>Project Slug</label>
                  <input type="text" class="form-control" placeholder="Project Slug" name="slug" value="<?php echo $project->slug; ?>">
                </div>

            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo realestate_url("manage/developers_projects/{$developer->id}"); ?>" class="btn btn-default">Back</a>
              </div>
          </div>
          <!-- /. box -->
</form>
    
    </section>
    <!-- /.content -->

    <?php $this->load->view('admin_controls/admin_tools'); ?>
    
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>