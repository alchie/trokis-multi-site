<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<form method="post">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Media : <a href="<?php echo realestate_url("manage/properties_edit/{$property->id}"); ?>"><small><?php echo $property->title; ?></small></a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<div class="checkbox pull-right">
    <label>
      <input type="checkbox" name="primary" value="1"> Primary
    </label>

    <label>
      <input type="checkbox" name="active" CHECKED value="1"> Active
    </label>

  </div>

                <div class="form-group">
                  <label>Media Type</label>
                  <select class="form-control" name="media_type">
                      <?php foreach(array('picture'=>'Picture', 'youtube'=>'Youtube') as $type=>$title) { ?>
                    <option value="<?php echo $type; ?>"><?php echo $title; ?></option>
<?php } ?>
                  </select>
                </div>

              <div class="form-group">
                  <label>Media Key</label>
                  <input type="text" class="form-control" placeholder="Media Key" name="media_key" value="default">
                  <small class="help-block">default, cover, thumbnail</small>
                </div>

              <div class="form-group">
                  <label>Media URL</label>
                  <textarea class="form-control" placeholder="Media URL" name="media_value"></textarea>
                </div>

              <div class="form-group">
                  <label>Media Thumbnail</label>
                  <textarea class="form-control" placeholder="Media Thumbnail" name="media_thumbnail"></textarea>
                </div>

              <div class="form-group">
                  <label>Alt Text</label>
                  <input type="text" class="form-control" placeholder="Alt Text" name="alt_text" value="">
                </div>

            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo realestate_url("manage/properties_media/{$property->id}"); ?>" class="btn btn-default">Back</a>
              </div>
          </div>
          <!-- /. box -->
</form>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>