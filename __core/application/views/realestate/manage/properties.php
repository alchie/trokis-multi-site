<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">


          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Properties 
<?php if( isset($developer) && ($developer) ) { ?>
              <span class="badge"><?php echo $developer->name; ?> <a href="<?php echo realestate_url("manage/developers"); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>
<?php } ?>

<?php if( isset($project) && ($project) ) { ?>
              <span class="badge"><?php echo $project->name; ?> <a href="<?php echo realestate_url("manage/developers_projects/{$project->dev_id}"); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>
<?php } ?>

<?php if( $this->input->get('q') ) { ?>
              <span class="badge"><?php echo $this->input->get('q'); ?> <a href="<?php echo realestate_url(uri_string()); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>

<?php } ?>

              </h3>

              <div class="box-tools">
              <form method="get">
                <div class="input-group input-group-sm" style="width: 250px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search Properties..." value="<?php echo $this->input->get('q'); ?>">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                     <a class="btn btn-success" href="<?php echo realestate_url("manage/properties_add/{$dev_id}/{$proj_id}"); ?>"><i class="fa fa-plus"></i> Add</a>
                  </div>
                </div>
              </div>
              </form>

            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
<?php if( $properties ) { ?>
              <div class="table-responsive mailbox-messages">
              
                <table class="table table-hover table-striped">
                  <tbody>
<?php foreach($properties as $property) { ?>
                  <tr class="<?php echo ($property->active) ? '' : 'danger'; ?>">
                    <td width="1%"><?php echo $property->id; ?></td>
                    <td><?php echo $property->title; ?></td>
                    <td><?php echo ucwords($property->type); ?></td>
                    <td><?php echo ($property->rent) ? 'For Rent' : 'For Sale'; ?></td>
                    <td class="mailbox-date text-right">
                    <a target="_blank" class="btn btn-success btn-xs" href="<?php echo realestate_url("property/view/{$property->slug}"); ?>">View</a>
                    <a class="btn btn-warning btn-xs" href="<?php echo realestate_url("manage/properties_edit/{$property->id}") . "?back=" . urlencode(uri_string()); ?>">Edit</a>
                    <a class="btn btn-primary btn-xs" href="<?php echo realestate_url("manage/properties_media/{$property->id}") . "?back=" . urlencode(uri_string()); ?>">Media</a>
                    <a class="btn btn-info btn-xs" href="<?php echo realestate_url("manage/properties_details/{$property->id}") . "?back=" . urlencode(uri_string()); ?>">Details</a>
                    <a class="btn btn-danger btn-xs btn-confirm" href="<?php echo realestate_url("manage/properties_delete/{$property->id}") . "?back=" . urlencode(uri_string()); ?>">Delete</a></td>
                  </tr>
<?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
<?php } else { ?>
  <p class="text-center" style="margin:20px;">Nothing Found!</p>
<?php } ?>
            </div>
            <!-- /.box-body -->
<?php if( $pagination ) { ?>
            <div class="box-footer">
            <p class="text-center"><?php echo $pagination; ?></p>
            </div>
<?php } ?>
          </div>
          <!-- /. box -->

    <?php $this->load->view('admin_controls/admin_tools'); ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>