<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">


          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Partners 
              <?php if( $this->input->get('q') ) { ?>
              <small>Search for: <a href="<?php echo site_url(uri_string()); ?>"><strong><?php echo $this->input->get('q'); ?></strong></a></small>
              <?php } ?>
              </h3>

              <div class="box-tools">
              <form method="get">
                <div class="input-group input-group-sm" style="width: 250px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search Partners..." value="<?php echo $this->input->get('q'); ?>">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                     <a class="btn btn-success" href="<?php echo realestate_url("manage/partners_add"); ?>"><i class="fa fa-plus"></i> Add</a>
                  </div>
                </div>
              </div>
              </form>

            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
<?php if( $partners ) { ?>
              <div class="table-responsive mailbox-messages">
              
                <table class="table table-hover table-striped">
                  <tbody>
<?php foreach($partners as $partner) { ?>
                  <tr class="<?php echo ($partner->active) ? '' : 'danger'; ?>">
                    <td width="1%"><?php echo $partner->id; ?></td>
                    <td><?php echo $partner->name; ?></td>
                    <td><?php echo $partner->logo_url; ?></td>
                    <td><?php echo $partner->website; ?></td>
                    <td class="mailbox-date text-right"><a class="btn btn-warning btn-xs" href="<?php echo realestate_url("manage/partners_edit/{$partner->id}") . "?back=" . urlencode(uri_string()); ?>">Edit</a> <a class="btn btn-danger btn-xs btn-confirm" href="<?php echo realestate_url("manage/partners_delete/{$partner->id}") . "?back=" . urlencode(uri_string()); ?>">Delete</a></td>
                  </tr>
<?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
<?php } else { ?>
  <p class="text-center" style="margin:20px;">Nothing Found!</p>
<?php } ?>
            </div>
            <!-- /.box-body -->
<?php if( $pagination ) { ?>
            <div class="box-footer">
            <p class="text-center"><?php echo $pagination; ?></p>
            </div>
<?php } ?>
          </div>
          <!-- /. box -->
    
    </section>
    <!-- /.content -->

<?php $this->load->view('admin_controls/admin_tools'); ?>

  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>