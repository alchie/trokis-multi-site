<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<form method="post">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Media</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<div class="checkbox pull-right">
    <label>
      <input type="checkbox" name="primary" value="1" <?php echo ($media->primary=='1') ? 'CHECKED' : ''; ?>> Primary
    </label>

    <label>
      <input type="checkbox" name="active" value="1" <?php echo ($media->active=='1') ? 'CHECKED' : ''; ?>> Active
    </label>

  </div>

                <div class="form-group">
                  <label>Media Type</label>
                  <select class="form-control" name="media_type">
                      <?php foreach(array('picture'=>'Picture', 'youtube'=>'Youtube') as $type=>$title) { ?>
                    <option value="<?php echo $type; ?>" <?php echo ($media->media_type==$type) ? 'SELECTED' : ''; ?>><?php echo $title; ?></option>
<?php } ?>
                  </select>
                </div>

              <div class="form-group">
                  <label>Media Key</label>
                  <input type="text" class="form-control" placeholder="Media Key" name="media_key" value="<?php echo $media->media_key; ?>">
                  <small class="help-block">default, cover, thumbnail</small>
                </div>

              <div class="form-group">
                  <label>Media URL</label>
                  <textarea class="form-control" placeholder="Media URL" name="media_value"><?php echo $media->media_value; ?></textarea>
                </div>

              <div class="form-group">
                  <label>Media Thumbnail</label>
                  <textarea class="form-control" placeholder="Media Thumbnail" name="media_thumbnail"><?php echo $media->thumbnail; ?></textarea>
                </div>

              <div class="form-group">
                  <label>Alt Text</label>
                  <input type="text" class="form-control" placeholder="Alt Text" name="alt_text" value="<?php echo $media->alt_text; ?>">
                </div>

            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo realestate_url("manage/properties_media/{$media->property_id}"); ?>" class="btn btn-default">Back</a>
              </div>
          </div>
          <!-- /. box -->
</form>
    
    </section>
    <!-- /.content -->

    <?php $this->load->view('admin_controls/admin_tools'); ?>
    
  </div>
  <!-- /.content-wrapper -->


<?php $this->load->view('footer'); ?>