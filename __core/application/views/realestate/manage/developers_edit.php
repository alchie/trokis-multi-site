<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<form method="post">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Developer: <small><?php echo $developer->name; ?></small></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<div class="checkbox pull-right">
    <label>
      <input type="checkbox" name="active" <?php echo ($developer->active==1) ? 'CHECKED' : ''; ?> value="1"> Active
    </label>
  </div>

              <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" placeholder="Developer Name" name="name" value="<?php echo $developer->name; ?>">
                </div>

              <div class="form-group">
                  <label>Slug</label>
                  <input type="text" class="form-control" placeholder="Developer Slug" name="slug" value="<?php echo $developer->slug; ?>">
                </div>

             <div class="form-group">
                  <label>Logo</label>
                  <input type="text" class="form-control" placeholder="Logo URL" name="logo_url" value="<?php echo $developer->logo_url; ?>">
                </div>
              <div class="form-group">
                  <label>Website</label>
                  <input type="text" class="form-control" placeholder="Website URL" name="website" value="<?php echo $developer->website; ?>">
                </div>

<div class="form-group">
  <label>Content</label>
  <textarea rows="10" class="form-control" placeholder="Content" name="content"><?php echo $developer->content; ?></textarea>
</div>

            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo realestate_url("manage/developers"); ?>" class="btn btn-default">Back</a>
              </div>
          </div>
          <!-- /. box -->
</form>
    
    </section>
    <!-- /.content -->

    <?php $this->load->view('admin_controls/admin_tools'); ?>
    
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>