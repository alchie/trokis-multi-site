<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">


          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Projects <span class="badge"><?php echo $developer->name; ?> <a href="<?php echo realestate_url("manage/developers"); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>
              <?php if( $this->input->get('q') ) { ?>
              <small>Search for: <a href="<?php echo site_url(uri_string()); ?>"><strong><?php echo $this->input->get('q'); ?></strong></a></small>
              <?php } ?>
              </h3>

              <div class="box-tools">
              <form method="get">
                <div class="input-group input-group-sm" style="width: 250px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search projects..." value="<?php echo $this->input->get('q'); ?>">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                     <a class="btn btn-success" href="<?php echo realestate_url("manage/developers_projects_add/{$developer->id}"); ?>"><i class="fa fa-plus"></i> Add Project</a>
                  </div>
                </div>
              </div>
              </form>

            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
<?php if( $projects ) { ?>
              <div class="table-responsive mailbox-messages">
              
                <table class="table table-hover table-striped">
                  <tbody>
<?php foreach($projects as $project) { ?>
                  <tr class="<?php echo ($project->active) ? '' : 'danger'; ?>">
                    <td width="1%"><?php echo $project->id; ?></td>
                    <td><?php echo $project->name; ?></td>
                   <td class="mailbox-date text-right">
<a class="btn btn-primary btn-xs" href="<?php echo realestate_url("manage/properties/{$developer->id}/{$project->id}"); ?>">Properties</a> 
                   <a class="btn btn-warning btn-xs" href="<?php echo realestate_url("manage/developers_projects_edit/{$project->id}") . "?back=" . urlencode(uri_string()); ?>">Edit</a> 
                   <a class="btn btn-danger btn-xs btn-confirm" href="<?php echo realestate_url("manage/developers_projects_delete/{$project->id}") . "?back=" . urlencode(uri_string()); ?>">Delete</a></td>
                  </tr>
<?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
<?php } else { ?>
  <p class="text-center" style="margin:20px;">Nothing Found!</p>
<?php } ?>
            </div>
            <!-- /.box-body -->
<?php if( $pagination ) { ?>
            <div class="box-footer">
            <p class="text-center"><?php echo $pagination; ?></p>
            </div>
<?php } ?>
          </div>
          <!-- /. box -->
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>