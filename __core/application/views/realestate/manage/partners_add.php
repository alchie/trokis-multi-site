<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<form method="post">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Partner</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="form-group">
                  <label>Name</label>
                  <input type="text" class="form-control" placeholder="Partner Name" name="name" value="">
                </div>
              <div class="form-group">
                  <label>Logo</label>
                  <input type="text" class="form-control" placeholder="Logo URL" name="logo" value="">
                </div>
              <div class="form-group">
                  <label>Website</label>
                  <input type="text" class="form-control" placeholder="Website URL" name="website" value="">
                </div>

<div class="checkbox">
    <label>
      <input type="checkbox" name="active" CHECKED value="1"> Active
    </label>
  </div>
            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo realestate_url("manage/partners"); ?>" class="btn btn-default">Back</a>
              </div>
          </div>
          <!-- /. box -->
</form>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>