<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<form method="post">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Property
<?php if( isset($developer) ) { ?>
              <span class="badge"><?php echo $developer->name; ?> <a href="<?php echo realestate_url("manage/developers"); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>
<?php } ?>

<?php if( isset($project) ) { ?>
              <span class="badge"><?php echo $project->name; ?> <a href="<?php echo realestate_url("manage/developers_projects/{$project->dev_id}"); ?>"><span class="glyphicon glyphicon-remove"></span></a></span>
<?php } ?>
              </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<div class="checkbox pull-right">
    <label>
      <input type="checkbox" name="rent" value="1"> For Rent
    </label>

    <label>
      <input type="checkbox" name="active" CHECKED value="1"> Active
    </label>

  </div>

                <div class="form-group">
                  <label>Property Type</label>
                  <select class="form-control" name="type">
                      <?php foreach(array('land'=>'Land', 'house'=>'House', 'forclosures'=>'Forclosures', 'apartment'=>'Apartment', 'commercial'=>'Commercial', 'condominium'=>'Condominium') as $type=>$title) { ?>
                    <option value="<?php echo $type; ?>"><?php echo $title; ?></option>
<?php } ?>
                  </select>
                </div>

              <div class="form-group">
                  <label>Title</label>
                  <input type="text" class="form-control" placeholder="Property Title" name="title" value="">
                </div>
<!--
              <div class="form-group">
                  <label>Slug</label>
                  <input type="text" class="form-control" placeholder="Property Slug" name="slug" value="">
                </div>
-->

<div class="row">
  <div class="col-md-9">
<div class="form-group">
  <label>Details</label>
  <textarea rows="17" class="form-control" placeholder="Details" name="details"></textarea>
</div>
  </div>
  <div class="col-md-3">
                <div class="form-group">
                  <label>Price</label>
                  <input type="text" class="form-control text-right" placeholder="Property Price" name="price" value="0.00">
                </div>
                <div class="form-group">
                  <label>Beds</label>
                  <input type="text" class="form-control text-right" placeholder="Number of Beds" name="beds" value="1">
                </div>
                <div class="form-group">
                  <label>Baths</label>
                  <input type="text" class="form-control text-right" placeholder="Number of Baths" name="baths" value="1">
                </div>
                <div class="form-group">
                  <label>Floor Area</label>
                  <input type="text" class="form-control text-right" placeholder="Floor Area" name="floor_area" value="1">
                </div>
                <div class="form-group">
                  <label>Lot Area</label>
                  <input type="text" class="form-control text-right" placeholder="Lot Area" name="lot_area" value="1">
                </div>
  </div>
</div>



            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo realestate_url("manage/properties"); ?>" class="btn btn-default">Back</a>
              </div>
          </div>
          <!-- /. box -->
</form>

    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>