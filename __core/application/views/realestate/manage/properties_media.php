<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<ul class="nav nav-tabs">
  
  <li role="presentation" class="pull-right"><a target="_blank" href="<?php echo realestate_url("property/view/{$property->slug}"); ?>">View</a></li>

  <li role="presentation"><a href="<?php echo realestate_url("manage/properties"); ?>">List</a></li>
  <li role="presentation"><a href="<?php echo realestate_url("manage/properties_edit/{$property->id}"); ?>">Property</a></li>
  <li role="presentation" class="active"><a href="<?php echo realestate_url("manage/properties_media/{$property->id}"); ?>">Media</a></li>
  <li role="presentation"><a href="<?php echo realestate_url("manage/properties_details/{$property->id}"); ?>">Details</a></li>
</ul>

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Media : <a href="<?php echo realestate_url("manage/properties_edit/{$property->id}"); ?>"><small><?php echo $property->title; ?></small></a>
              <?php if( $this->input->get('q') ) { ?>
              <small>Search for: <a href="<?php echo realestate_url(uri_string()); ?>"><strong><?php echo $this->input->get('q'); ?></strong></a></small>
              <?php } ?>
              </h3>

              <div class="box-tools">
              <form method="get">
                <div class="input-group input-group-sm" style="width: 250px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search Media..." value="<?php echo $this->input->get('q'); ?>">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                     <a class="btn btn-success" href="<?php echo realestate_url("manage/properties_media_add/{$property->id}"); ?>"><i class="fa fa-plus"></i> Add</a>
                  </div>
                </div>
              </div>
              </form>

            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
<?php if( $medias ) { ?>
              <div class="table-responsive mailbox-messages">
              
                <table class="table table-hover table-striped">
                  <tbody>
<?php foreach($medias as $media) { ?>
                  <tr class="<?php echo ($media->active==1) ? '' : 'danger'; ?>">
                    <td width="1%"><?php echo $media->media_id; ?></td>
                    <td><?php echo $media->media_key; ?></td>
                    <td><a href="<?php echo realestate_url("property/media/{$media->media_id}"); ?>" target="_blank"><?php echo substr($media->media_value,0,60); ?></a></td>
                    <td><?php echo ucwords($media->media_type); ?></td>
                    <td><?php echo $media->alt_text; ?></td>
                    <td class="mailbox-date text-right">
                    <a class="btn btn-warning btn-xs" href="<?php echo realestate_url("manage/properties_media_edit/{$media->media_id}") . "?back=" . urlencode(uri_string()); ?>">Edit</a>
                    <a class="btn btn-danger btn-xs btn-confirm" href="<?php echo realestate_url("manage/properties_media_delete/{$media->media_id}") . "?back=" . urlencode(uri_string()); ?>">Delete</a></td>
                  </tr>
<?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
<?php } else { ?>
  <p class="text-center" style="margin:20px;">Nothing Found!</p>
<?php } ?>
            </div>
            <!-- /.box-body -->
<?php if( $pagination ) { ?>
            <div class="box-footer">
            <p class="text-center"><?php echo $pagination; ?></p>
            </div>
<?php } ?>
          </div>
          <!-- /. box -->

    <?php $this->load->view('admin_controls/admin_tools'); ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>