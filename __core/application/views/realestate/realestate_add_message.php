<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

<div class="content-wrapper">

<div class="container">

<div class="row" style="margin-top: 15px">
  <div class="col-md-8 col-md-offset-2">

<div class="panel panel-success">
  <div class="panel-heading">

    <h3 class="panel-title">Message Sent!</h3>
  </div>
  <div class="panel-body">
    
<h2><?php echo htmlentities($message->full_name); ?>, thank you so much for contacting us! </h2>
<p>We have received your message and will contact you ASAP!</p>
<p><strong>Property title:</strong> <?php echo $property->title; ?></p>
<hr>
<a class="btn btn-warning" href="<?php echo realestate_url("property/view/{$property->slug}"); ?>">
    Back
</a>

  </div>
</div>

  </div>
</div>

</div>

<?php $this->load->view('realestate/realestate_card'); ?>

</div>
<?php $this->load->view('footer'); ?>