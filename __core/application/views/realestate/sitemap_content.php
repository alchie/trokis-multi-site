<?php 
echo '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
foreach($properties as $property) { 
	echo '<url><loc>'.realestate_url("property/view/{$property->slug}").'</loc></url>';
}
echo '</urlset>';
