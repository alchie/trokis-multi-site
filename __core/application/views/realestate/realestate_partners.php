<?php if( isset($partners) && ($partners) ) { ?>
<section class="partners">
<div class="row">
  <div class="col-md-10 col-md-offset-1">
<div id="carousel-example-generic" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="item active">
<div class="row">
<?php 
$n=6;
foreach($partners as $partner) { ?>
  <div class="col-md-2"><a target="_blank" href="<?php echo $partner->website; ?>"><img src="<?php echo $partner->logo_url; ?>" alt="<?php echo $partner->name; ?>" title="<?php echo $partner->name; ?>" height="55px"></a></div>
<?php $n--;  ?>
<?php if($n==0) { ?>
  </div>
  </div>
        <div class="item">
          <div class="row">
<?php $n=6; } ?>
<?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<?php } ?>