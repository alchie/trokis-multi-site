<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>
 
<div class="content-wrapper">

<div class="container">

<div class="row" style="margin-top: 15px">
	 <div class="col-md-12">
      <div class="box box-widget widget-user" itemscope itemtype="http://schema.org/Property">
          <div class="box-header with-border">
              <h3 class="box-title"><strong><?php echo $developer->name; ?></strong></h3>
              <p><?php echo $developer->url; ?></p>
          </div>
          <div class="box-body">
              <?php echo $developer->content; ?>
          </div>
      </div>


   </div>
	</div>

</div>

<?php $this->load->view('admin_controls/admin_tools'); ?>

</div>


<!-- /.content-wrapper -->
<?php $this->load->view('footer'); ?>