<?php if( isset($properties) && ($properties) ) { ?>
<div class="container" style="margin-top: 15px">
<div class="row">
<?php $mn=0; 
foreach($properties as $property) { 
?>
<?php 
$logo_url = $this->config->item('trokis_logo_url');
if( $property->logo_url != '' ) {
  $logo_url = $property->logo_url;
}
?>
<div class="col-md-3 col-sm-6">
<div class="box box-widget widget-user" itemscope itemtype="http://schema.org/Property">
            <div class="widget-user-header bg-black" style="background: url('<?php echo $logo_url; ?>') center center;">
            </div>
            <div class="box-body">
              <a itemprop="url" href="<?php echo realestate_url("property/view/{$property->slug}"); ?>"><h3 class="widget-user-username" itemprop="name"><?php echo $property->title; ?></h3></a>
              <h5 class="widget-user-desc"></h5>
              <p class="widget-user-desc" property="address" typeof="PostalAddress"><span property="streetAddress"></span></p>
            </div>
          </div>
  </div>
<?php 
$mn++;
if( $mn==4 ) {
  echo '</div><div class="row">';
  $mn=0;
}
?>
<?php } ?>
</div>
</div>
<?php } ?>