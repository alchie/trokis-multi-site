<?php if( isset($properties) && ($properties) ) { ?>
<div class="container-fluid">
<div class="container container-pad" id="property-listings">
<div class="row">
<?php foreach($properties as $property) { 
$logo_url = $this->config->item('trokis_logo_url');
if( $property->logo_url != '' ) {
  $logo_url = $property->logo_url;
}
?>
<div class="col-md-6">
            <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing">
                <div class="media">
                    <a class="pull-left" href="<?php echo realestate_url("property/view/{$property->slug}"); ?>" target="_parent">
                    <img alt="image" class="img-responsive" src="<?php echo $logo_url; ?>"></a>

                    <div class="clearfix visible-sm"></div>

                    <div class="media-body fnt-smaller">
                        <a href="<?php echo realestate_url("property/view/{$property->slug}"); ?>" target="_parent"></a>

                        <h4 class="media-heading">
                          <a href="<?php echo realestate_url("property/view/{$property->slug}"); ?>" target="_parent"><?php echo $property->title; ?> <small>&#x20B1; <?php echo number_format($property->price,2); ?></small></a></h4>

                        <ul class="list-inline mrg-0 btm-mrg-10 clr-535353">
                            <li><?php echo $property->lot_area; ?> SqFt</li>
<?php if( $property->beds ) { ?>
                            <li style="list-style: none">|</li>
                            <li><?php echo $property->beds; ?> Beds</li>
<?php } ?>
<?php if( $property->baths ) { ?>
                            <li style="list-style: none">|</li>
                            <li><?php echo $property->baths; ?> Baths</li>
<?php } ?>
                        </ul>

                        <p class="hidden-xs"><?php echo substr($property->details, 0, 60); ?></p>
                    </div>
                </div>
            </div><!-- End Listing-->
</div>
<?php } ?>
</div>
</div>
</div>
<?php } ?>