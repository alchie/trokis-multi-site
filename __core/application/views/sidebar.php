 <aside class="main-sidebar">
    <section class="sidebar">
<?php /* if( $this->session->loggedIn ) { ?>
      <!-- search form -->
      <form action="<?php echo site_url("search"); ?>" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
<?php } */ ?>

      <ul class="sidebar-menu">
      <li class="visible-xs">
 <a href="#changeLocation" data-toggle="modal">
<?php if( isset($current_location) && ($current_location) ) { ?>
           <?php echo $current_location->name; ?>
<?php } else { ?>
Select a location
<?php } ?>
            </a>
      </li>
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-home"></i> <span>Real Estate</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo realestate_url(); ?>"><i class="fa fa-circle-o"></i> All Properties</a></li>
            <li><a href="<?php echo realestate_url("land"); ?>"><i class="fa fa-circle-o"></i> Land</a></li>
            <li><a href="<?php echo realestate_url("house"); ?>"><i class="fa fa-circle-o"></i> House</a></li>
            <li><a href="<?php echo realestate_url("forclosures"); ?>"><i class="fa fa-circle-o"></i> Forclosures</a></li>
            <li><a href="<?php echo realestate_url("apartment"); ?>"><i class="fa fa-circle-o"></i> Apartment</a></li>
            <li><a href="<?php echo realestate_url("commercial"); ?>"><i class="fa fa-circle-o"></i> Commercial</a></li>
            <li><a href="<?php echo realestate_url("condominium"); ?>"><i class="fa fa-circle-o"></i> Condominium</a></li>
            <li><a href="<?php echo realestate_url("projects"); ?>"><i class="fa fa-circle-o"></i> Projects</a></li>
            <li><a href="<?php echo realestate_url("developers"); ?>"><i class="fa fa-circle-o"></i> Developers</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-briefcase"></i> <span>Jobs</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo jobs_url("full_time"); ?>"><i class="fa fa-circle-o"></i> Full Time</a></li>
            <li><a href="<?php echo jobs_url("part_time"); ?>"><i class="fa fa-circle-o"></i> Part Time</a></li>
            <li><a href="<?php echo jobs_url("internship"); ?>"><i class="fa fa-circle-o"></i> Internship</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-car"></i> <span>Vehicles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo vehicles_url("new"); ?>"><i class="fa fa-circle-o"></i> New</a></li>
            <li><a href="<?php echo vehicles_url("used"); ?>"><i class="fa fa-circle-o"></i> Used</a></li>
            <li><a href="<?php echo vehicles_url("brands"); ?>"><i class="fa fa-circle-o"></i> Brands</a></li>
            <li><a href="<?php echo vehicles_url("types"); ?>"><i class="fa fa-circle-o"></i> Type</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-bicycle"></i> <span>Motorcycles</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo motorcycles_url("new"); ?>"><i class="fa fa-circle-o"></i> New</a></li>
            <li><a href="<?php echo motorcycles_url("used"); ?>"><i class="fa fa-circle-o"></i> Used</a></li>
            <li><a href="<?php echo motorcycles_url("for_assume"); ?>"><i class="fa fa-circle-o"></i> For Assume</a></li>
            <li><a href="<?php echo motorcycles_url("brands"); ?>"><i class="fa fa-circle-o"></i> Brands</a></li>
            <li><a href="<?php echo motorcycles_url("types"); ?>"><i class="fa fa-circle-o"></i> Type</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-calendar"></i> <span>Events</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo events_url("seminars"); ?>"><i class="fa fa-circle-o"></i> Seminars</a></li>
            <li><a href="<?php echo events_url("concerts"); ?>"><i class="fa fa-circle-o"></i> Concerts</a></li>
            <li><a href="<?php echo events_url("fiesta"); ?>"><i class="fa fa-circle-o"></i> Fiesta</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-map-marker"></i> <span>Directory</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo schools_url(); ?>"><i class="fa fa-circle-o"></i> Schools</a></li>
            <li><a href="<?php echo companies_url(); ?>"><i class="fa fa-circle-o"></i> Companies</a></li>
          </ul>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>