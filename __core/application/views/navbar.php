  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo main_url(); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>T</b>PH</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Trokis</b>PH</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
<?php /*
        <ul class="nav navbar-nav hidden-xs">
            <li>
            <a href="#changeLocation" data-toggle="modal">
<?php if( isset($current_location) && ($current_location) ) { ?>
            <?php echo $app_name; ?> in <?php echo $current_location->name; ?>
<?php } else { ?>
Select a location
<?php } ?>
            </a>
          </li>
        </ul>
*/ ?>


      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
<?php if( $this->session->userdata('loggedIn') ) { ?>

<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
    <?php $this->load->view('navbar/seo'); ?>
    <?php $this->load->view('navbar/manage'); ?>
<?php } ?>


 <?php $this->load->view('navbar/tasks'); ?>

          <li class="dropdown user user-menu">
          
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $this->config->item('trokis_logo_url'); ?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->fullName; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $this->config->item('trokis_logo_url'); ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $this->session->fullName; ?>
                  <small>Member since <?php echo date('F Y', strtotime($this->session->dateRegistered)); ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo account_url("my-account"); ?>" class="btn btn-default btn-flat">My Account</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo main_url("logout"); ?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
 <?php } else { ?>

            <li>
            <a href="<?php echo account_url("login"); ?>?app=<?php echo $app_id; ?>"><i class="glyphicon glyphicon-user"></i> Login</a>
          </li>

 <?php } ?>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->