<?php
echo '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
foreach($regions as $region) {
  echo '<sitemap><loc>'.base_url("sitemap_region_{$region->id}.xml").'</loc>';
  echo '<lastmod>'.date("Y-m-01", strtotime("-1 month")).'</lastmod></sitemap>';
}
foreach($provinces as $province) {
  echo '<sitemap><loc>'.base_url("sitemap_province_{$province->id}.xml").'</loc>';
  echo '<lastmod>'.date("Y-m-01", strtotime("-1 month")).'</lastmod></sitemap>';
}
foreach($municipals as $municipal) {
  echo '<sitemap><loc>'.base_url("sitemap_municipal_{$municipal->id}.xml").'</loc>';
  echo '<lastmod>'.date("Y-m-01", strtotime("-1 month")).'</lastmod></sitemap>';
}
for($i=0;$i<ceil($schools_count/$schools_limit);$i++) {
	echo '<sitemap><loc>'.base_url("sitemap_wildcard_{$i}.xml").'</loc>';
  	echo '<lastmod>'.date("Y-m-01", strtotime("-1 month")).'</lastmod></sitemap>';
}
 echo '<sitemap><loc>'.base_url("sitemap_schools_in.xml").'</loc>';
 echo '<lastmod>'.date("Y-m-01", strtotime("-1 month")).'</lastmod></sitemap>';
echo '</sitemapindex>';
