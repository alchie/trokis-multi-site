<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<?php if( $schools ) { ?>

<?php foreach($schools as $school) {
$school_slug = ($school->school_slug) ? $school->school_slug : slugify($current_school->name);
 ?>


      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              
              <strong><?php echo $school->name; ?></strong><br><small><?php echo $school->address; ?>, <?php echo $school->municipal_name; ?>, <?php echo $school->province_name; ?>, <?php echo $school->region_name; ?></small>
              </h3>
              <div class="box-tools">
                <a class="btn btn-danger btn-xs" href="<?php echo schools_url(uri_string()); ?>?hide=<?php echo $school->id; ?>">Hide</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<div class="row">
  <div class="col-md-12">
    <div class="form-horizontal">
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Telephone</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="<?php echo $school->telephone; ?>" onfocus="this.select();">
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Mobile</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="<?php echo $school->mobile; ?>" onfocus="this.select();">
        </div>
      </div>
      <div class="form-group">
        <label for="inputPassword3" class="col-sm-3 control-label">Trokis Site</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="http://<?php echo $school_slug; ?>.schools.trokis.com" onfocus="this.select();">
        </div>
      </div>
    </div>
</div>
</div>

<div class="row">
    <div class="col-md-12">
  <center>
<a class="btn btn-warning" href="https://www.google.com.ph/maps/search/<?php echo urlencode($school->name . ", " . $school->municipal_name . ", " . $school->province_name . ", " . $school->region_name); ?>" target="_google_maps">Google Maps</a> 
<a class="btn btn-primary" href="http://<?php echo $school_slug; ?>.schools.trokis.com" target="_blank">View</a> 
</center>
  </div>
</div>


            </div>

          </div>
          <!-- /. box -->


<?php } ?>

<?php } else { ?>

  <p class="text-center" style="margin:20px;">Nothing Found!</p>

<?php } ?>

<?php if( $pagination ) { ?>
            <center>
            <p class="text-center"><?php echo $pagination; ?></p>
            </center>
<?php } ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>