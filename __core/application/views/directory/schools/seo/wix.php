<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<?php if( $schools ) { ?>

<?php foreach($schools as $school) {
$school_slug = ($school->school_slug) ? $school->school_slug : slugify($current_school->name);
 ?>

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $school->name; ?> 
              <?php if( $this->input->get('q') ) { ?>
              <small>Search for: <a href="<?php echo site_url(uri_string()); ?>"><strong><?php echo $this->input->get('q'); ?></strong></a></small>
              <?php } ?>
              </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<div class="row">
  <div class="col-md-12">
    <div class="form-horizontal">
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Blog Title</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="<?php echo htmlentities($school->name); ?>" onfocus="this.select();">
        </div>
      </div>
      <div class="form-group">
        <label for="inputPassword3" class="col-sm-3 control-label">Blog Address</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="<?php echo $school_slug; ?>" onfocus="this.select();">
        </div>
      </div>
    </div>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="form-group">
  <textarea class="form-control" rows="20" onfocus="this.select();"><?php $this->load->view('directory/schools/seo/blogger_template', array('school'=>$school)); ?></textarea>
</div>
</div>
</div>

<div class="row">
  <div class="col-md-12">
    <form class="form-horizontal" method="post">
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-2 control-label">Blog URL</label>
        <div class="col-sm-8">
          <input type="hidden" class="form-control" value="<?php echo $school->id; ?>" name="school_id">
          <input type="text" class="form-control" value="" name="blog_url">
        </div>
         <div class="col-sm-2">
           <button class="btn btn-success">Save &amp; Hide</button>
         </div>
      </div>
    </form>
</div>

</div>

            </div>

          </div>
          <!-- /. box -->

<?php } ?>

<?php } else { ?>

  <p class="text-center" style="margin:20px;">Nothing Found!</p>

<?php } ?>

<?php if( $pagination ) { ?>
            <center>
            <p class="text-center"><?php echo $pagination; ?></p>
            </center>
<?php } ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>