<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="<$BlogLanguageDirection$>">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><$BlogPageTitle$></title>
  <$BlogMetaData$>
    <!-- Bootstrap core CSS -->
    <link href="https://blackrockdigital.github.io/startbootstrap-landing-page/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template -->
    <link href="https://blackrockdigital.github.io/startbootstrap-landing-page/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template -->
    <link href="https://blackrockdigital.github.io/startbootstrap-landing-page/css/landing-page.css" rel="stylesheet">
  </head>
  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <div class="container">
        <a class="navbar-brand" href="https://www.trokis.com/">Trokis Philippines</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo site_url("services/claim-page"); ?>?page_url=<$BlogURL$>">Claim this page</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Header -->
    <header class="intro-header">
      <div class="container">
        <div class="intro-message">
          <h1><?php echo $school->name; ?></h1>
          <h3><?php echo $school->address; ?> &amp;middot; <?php echo "school{$school->id}@trokis.com"; ?></h3>
          <hr class="intro-divider">
          <ul class="list-inline intro-social-buttons">
<?php 
$school_slug = ($school->school_slug) ? $school->school_slug : slugify($school->name);
foreach(array(
  'facebook'=>'Facebook',
  'twitter'=>'Twitter',
  'linkedin'=>'LinkedIn',
  'youtube'=>'Youtube',
  'pinterest'=>'Pinterest',
  'tumblr'=>'Tumblr',
  'google'=>'Google+',
  ) as $key=>$title) {
  $url = schools_url("school/redirect/{$key}/{$school->id}/{$school_slug}");
?>
            <li class="list-inline-item">
              <a href="<?php echo $url; ?>" class="btn btn-secondary btn-lg">
                <i class="fa fa-<?php echo $key; ?> fa-fw"></i>
                <span class="network-name"><?php echo $title; ?></span>
              </a>
            </li>
<?php } ?>
          </ul>
        </div>
      </div>
    </header>

    <!-- Page Content -->
    <section class="content-section-a">

      <div class="container">
        <div class="row">
          <div class="col-lg-5 ml-auto">
            <hr class="section-heading-spacer">
            <div class="clearfix"></div>
            <h2 class="section-heading">Death to the Stock Photo:<br>Special Thanks</h2>
            <p class="lead">A special thanks to
              <a target="_blank" href="http://join.deathtothestockphoto.com/">Death to the Stock Photo</a>
              for providing the photographs that you see in this template. Visit their website to become a member.</p>
          </div>
          <div class="col-lg-5 mr-auto">
            <img class="img-fluid" src="https://blackrockdigital.github.io/startbootstrap-landing-page/img/ipad.png" alt="">
          </div>
        </div>

      </div>
      <!-- /.container -->
    </section>

    <section class="content-section-b">

      <div class="container">

        <div class="row">
          <div class="col-lg-5 mr-auto order-lg-2">
            <hr class="section-heading-spacer">
            <div class="clearfix"></div>
            <h2 class="section-heading">3D Device Mockups<br>by PSDCovers</h2>
            <p class="lead">Turn your 2D designs into high quality, 3D product shots in seconds using free Photoshop actions by
              <a target="_blank" href="http://www.psdcovers.com/">PSDCovers</a>! Visit their website to download some of their awesome, free photoshop actions!</p>
          </div>
          <div class="col-lg-5 ml-auto order-lg-1">
            <img class="img-fluid" src="https://blackrockdigital.github.io/startbootstrap-landing-page/img/dog.png" alt="">
          </div>
        </div>

      </div>
      <!-- /.container -->

    </section>
    <!-- /.content-section-b -->

    <section class="content-section-a">

      <div class="container">

        <div class="row">
          <div class="col-lg-5 ml-auto">
            <hr class="section-heading-spacer">
            <div class="clearfix"></div>
            <h2 class="section-heading">Google Web Fonts and<br>Font Awesome Icons</h2>
            <p class="lead">This template features the 'Lato' font, part of the
              <a target="_blank" href="http://www.google.com/fonts">Google Web Font library</a>, as well as
              <a target="_blank" href="http://fontawesome.io">icons from Font Awesome</a>.</p>
          </div>
          <div class="col-lg-5 mr-auto ">
            <img class="img-fluid" src="https://blackrockdigital.github.io/startbootstrap-landing-page/img/phones.png" alt="">
          </div>
        </div>

      </div>
      <!-- /.container -->

    </section>
    <!-- /.content-section-a -->

    <aside class="banner">

      <div class="container">

        <div class="row">
          <div class="col-lg-6 my-auto">
            <h2>Connect to <?php echo $school->name; ?></h2>
          </div>
          <div class="col-lg-6 my-auto">
            <ul class="list-inline banner-social-buttons">
              <?php 
$school_slug = ($school->school_slug) ? $school->school_slug : slugify($school->name);
foreach(array(
  'facebook'=>'Facebook',
  'twitter'=>'Twitter',
  'linkedin'=>'LinkedIn',
  'youtube'=>'Youtube',
  'pinterest'=>'Pinterest',
  'tumblr'=>'Tumblr',
  'google'=>'Google+',
  ) as $key=>$title) {
  $url = schools_url("school/redirect/{$key}/{$school->id}/{$school_slug}");
?>
            <li class="list-inline-item">
              <a href="<?php echo $url; ?>" class="btn btn-secondary btn-lg">
                <i class="fa fa-<?php echo $key; ?> fa-fw"></i>
                <span class="network-name"><?php echo $title; ?></span>
              </a>
            </li>
<?php } ?>
            </ul>
          </div>
        </div>

      </div>
      <!-- /.container -->

    </aside>
    <!-- /.banner -->

    <!-- Footer -->
    <footer>
      <div class="container">
        <ul class="list-inline">
          <li class="list-inline-item">
                          <a class="nav-link" href="<?php echo site_url("services/claim-page"); ?>?page_url=<$BlogURL$>">Claim this page</a>
          </li>
          <li class="footer-menu-divider list-inline-item">&amp;sdot;</li>
        </ul>
        <p class="copyright text-muted small">Trokis Philippines &amp;middot; <?php echo $school->name; ?> &amp;middot; <a href="https://startbootstrap.com/template-overviews/landing-page/">Start Bootstrap</a></p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="https://blackrockdigital.github.io/startbootstrap-landing-page/vendor/jquery/jquery.min.js"></script>
    <script src="https://blackrockdigital.github.io/startbootstrap-landing-page/vendor/popper/popper.min.js"></script>
    <script src="https://blackrockdigital.github.io/startbootstrap-landing-page/vendor/bootstrap/js/bootstrap.min.js"></script>

  </body>

</html>