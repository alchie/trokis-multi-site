<?php $this->load->view('header'); ?>
<style type="text/css">
  <!--
#GoogleMap_map {
    width: 100%; height: 400px
}
#mapInfoWindowHeading {
  font-size: 15px;
  text-align: center;
    width: 100%;
    padding: 0;
    margin: 0;
    font-weight: bold;
}
  -->
</style>
 <?php $this->load->view('sidebar'); ?>


  <div class="content-wrapper">


<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">

<form method="post">
          <div class="box box-primary">

<div class="box-header with-border">
              <h3 class="box-title"><?php echo $current_school->name; ?></h3>

              <div class="box-tools pull-right">
                <a class="btn btn-box-tool" href="<?php echo schools_url("school/view/{$current_school->id}/" . urlencode(url_title($current_school->name,"-",TRUE))); ?>"><i class="fa fa-eye"></i> View</a>
              </div>
              <!-- /.box-tools -->
</div>

            <div class="box-body box-profile">
   

 <div class="form-group">
    <label>School Name</label>
    <input type="text" class="form-control" placeholder="School Name" name="name" value="<?php echo $current_school->name; ?>">
  </div>

<div class="row">
<div class="col-md-6">
 <div class="form-group">
    <label>Address</label>
    <input type="text" class="form-control" placeholder="School Address" name="address" value="<?php echo $current_school->address; ?>">
  </div>
  </div>
<div class="col-md-6">
   <div class="form-group">
    <label>Phone Number</label>
    <input type="text" class="form-control" placeholder="School Phone Number" name="phone_number" value="<?php echo $current_school->phone_number; ?>">
  </div>
  </div>
  </div>

  <div class="row">
<div class="col-md-12">
   <div class="form-group">
    <label>Municipality</label>
   <select class="form-control" name="municipal_province_region">
   <option>- - Select Municipality - -</option>
     <?php foreach($municipals as $municipal) { ?>
      <option value="<?php echo $municipal->id; ?>|<?php echo $municipal->province; ?>|<?php echo $municipal->region; ?>" <?php echo ($municipal->id==$current_school->municipality) ? 'SELECTED' : ''; ?>><?php echo $municipal->name; ?> - <?php echo $municipal->province_name; ?> - <?php echo $municipal->region_name; ?></option>
     <?php } ?>
   </select>
  </div>
  </div>
</div>

  <div class="row">
<div class="col-md-6">
 <div class="form-group">
    <label>Latitude <small><a data-toggle="modal" href="#GoogleMap">Show Map</a></small></label>
    <input id="school_lat" type="text" class="form-control" placeholder="Latitude" name="lat" value="<?php echo $current_school->lat; ?>">
  </div>
  </div>
<div class="col-md-6">
   <div class="form-group">
    <label>Longtitude <small><a data-toggle="modal" href="#GoogleMap" >Show Map</a></small></label>
    <input id="school_lng" type="text" class="form-control" placeholder="Longtitude" name="lng" value="<?php echo $current_school->lng; ?>">
  </div>
  </div>

  </div>

            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

          </div>
          <!-- /.box -->
</form>

        </div>
      </div>
      <!-- /.row -->

    <?php $this->load->view('admin_controls/admin_tools'); ?>

    </section>
    <!-- /.content -->

<?php } ?>

  </div>
  <!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="GoogleMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width: 80%">
    <div class="modal-content">
        <div id="GoogleMap_map"></div>
        <script type="text/javascript">
          initMap();
        </script>
    </div>
  </div>
</div>

    <script>
      function initMap() {
        var coordinates = {lat: <?php echo ($current_school->lat) ? $current_school->lat : 0; ?>, lng: <?php echo ($current_school->lng) ? $current_school->lng : 0; ?>};
        var map = new google.maps.Map(document.getElementById('GoogleMap_map'), {
          zoom: 16,
          center: coordinates
        });

        var marker = new google.maps.Marker({
          position: coordinates,
          map: map,
          title: '<?php echo addslashes($current_school->name); ?>',
          draggable: true,
        });
        google.maps.event.addListener(marker, 'drag', function(event) {
          //console.debug('new position is '+event.latLng.lat()+' / '+event.latLng.lng()); 
          $('#school_lat').val( event.latLng.lat() );
          $('#school_lng').val( event.latLng.lng() );
        });
        google.maps.event.addListener(marker, 'dragend', function(event) {
          //console.debug('final position is '+event.latLng.lat()+' / '+event.latLng.lng()); 
          $('#school_lat').val( event.latLng.lat() );
          $('#school_lng').val( event.latLng.lng() );
        });
      }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7-Ku_hI74A2xZnAu-xHjE4RIjEWjsQMw&callback=initMap"></script>


<?php $this->load->view('footer'); ?>