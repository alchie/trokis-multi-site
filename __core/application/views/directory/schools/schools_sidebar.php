<?php if( isset($provinces) ) { ?>
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Provinces</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
<?php foreach($provinces as $province) { ?>
                <li class="<?php echo (isset($province_id) && ($province_id==$province->id)) ? 'active' : ''; ?>"><a href="<?php echo schools_url("index/province/{$category_id}/{$region_id}/{$province->id}"); ?>"><i class="fa fa-map-pin"></i> <?php echo $province->name; ?></a></li>
<?php } ?>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
<?php } ?>

<?php if( isset($regions) ) { ?>
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Regions</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
<?php foreach($regions as $region) { ?>
                <li class="<?php echo (isset($region_id) && ($region_id==$region->id)) ? 'active' : ''; ?>"><a href="<?php echo schools_url("index/region/{$category_id}/{$region->id}"); ?>"><i class="fa fa-map-pin"></i> <?php echo $region->name; ?>
                  </a></li>
<?php } ?>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
<?php } ?>