<?php if( isset($provinces) ) { ?>
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Provinces</h3>
            </div>
            <div class="box-body">
<?php foreach($provinces as $province) { ?>
<span itemscope itemtype="http://schema.org/State">
<?php if (isset($province_id) && ($province_id==$province->id)) { ?>
  <strong itemprop="url" href="<?php echo schools_url("index/province/{$category_id}/{$region_id}/{$province->id}"); ?>"><span itemprop="name"><?php echo $province->name; ?></span></strong>
<?php } else { ?>
  <a itemprop="url" href="<?php echo schools_url("index/province/{$category_id}/{$region_id}/{$province->id}"); ?>"><span itemprop="name"><?php echo $province->name; ?></span></a>
<?php } ?>
</span>
&middot;         
<?php } ?>
     
            </div>
            <!-- /.box-body -->
          </div>
<?php } ?>

<?php if( isset($regions) ) { ?>
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Regions</h3>
            </div>
            <div class="box-body">
<?php foreach($regions as $region) { ?>
<span itemscope itemtype="http://schema.org/State">
<?php if (isset($region_id) && ($region_id==$region->id)) { ?>
<strong itemprop="url" href="<?php echo schools_url("index/region/{$category_id}/{$region->id}"); ?>"><span itemprop="name"><?php echo $region->name; ?></span></strong>
<?php } else { ?>
<a itemprop="url" href="<?php echo schools_url("index/region/{$category_id}/{$region->id}"); ?>"><span itemprop="name"><?php echo $region->name; ?></span></a>
<?php } ?>
</span>
&middot;
<?php } ?>

            </div>
            <!-- /.box-body -->
          </div>
<?php } ?>