<?php 
echo '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
if( isset($municipal) && ($municipal)) {
	echo '<url><loc>'.schools_url("schools/in/" . urlencode(url_title($municipal->name,"-",TRUE)) . "/" . $municipal->id).'</loc></url>';
}
foreach($schools as $school) { 
	echo '<url><loc>'.schools_url("school/view/{$school->id}/" . urlencode(url_title($school->name,"-",TRUE))).'</loc></url>';
}
echo '</urlset>';
