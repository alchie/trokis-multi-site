<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
 
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
<center>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Trokis Page Not Found -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="3675832127"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</center>
      <div class="row">
        <div class="col-md-4">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Trokis Page not Found 2 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="8810708924"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
        </div>
        <div class="col-md-4">
          <?php if( $current_school->social_url ) { ?>
          <h3><i class="fa fa-warning text-yellow"></i> You are about to leave this website.</h3>

          <p>
            Are you sure you want to leave this site?
          </p>

<a href="" class="btn btn-success btn-md">Yes</a>
<a href="" class="btn btn-danger btn-md">No</a>
<?php } else { ?>

<h3><i class="fa fa-warning text-yellow"></i> No [<?php echo $meta_key; ?>] URL Assigned!</h3>

 <p>Suggestions feature is not yet available! Please contact admin through this email: <a href="mailto:school<?php echo $school_id; ?>@trokis.com">school<?php echo $school_id; ?>@trokis.com</a></p>
<p>You may try other social media links as well.</p>
<?php $slug = urlencode(url_title($current_school->name,"-",TRUE)); ?>
<a href="<?php echo schools_url("school/view/{$current_school->id}/{$slug}"); ?>" class="btn btn-sm btn-default" style="margin-bottom: 10px;"><i class="fa fa-arrow-left"></i> Back</a>

<?php 
foreach(array(
  'facebook'=>'Facebook',
  'twitter'=>'Twitter',
  'linkedin'=>'LinkedIn',
  'youtube'=>'Youtube',
  'pinterest'=>'Pinterest',
  'tumblr'=>'Tumblr',
  'google'=>'Google+',
  ) as $key=>$title) {
  if($meta_key==$key) continue;
  $slug = urlencode(url_title($current_school->name,"-",TRUE));
  $url = schools_url("school/redirect/{$key}/{$current_school->id}/{$slug}");
?>
  <a href="<?php echo $url; ?>" class="btn btn-sm btn-<?php echo $key; ?>" style="margin-bottom: 10px;"><i class="fa fa-<?php echo $key; ?>-square"></i> <?php echo $title; ?></a>
<?php } ?>

<?php } ?>
        </div>

        <div class="col-md-4">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Trokis Page not Found 2 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="8810708924"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
        </div>
      </div>


<center>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Trokis Page Not Found -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="3675832127"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</center>


<?php if( $municipal_schools ) { ?>
<div class="row">
<?php $mn=0; foreach($municipal_schools as $mschool) { ?>
<?php 
$logo_url = 'https://2.bp.blogspot.com/-K2igEG7gPmE/VH7EfO49J5I/AAAAAAAAEuU/l2sXdmy3EMk/s400/trokis%2B-128.png';
if( $mschool->logo_url != '' ) {
  $logo_url = $mschool->logo_url;
}
?>
  <div class="col-md-2 col-sm-6">
<div class="box box-widget widget-user" itemscope itemtype="http://schema.org/School">
            <div class="widget-user-header bg-black" style="background: url('<?php echo $logo_url; ?>') center center;">
            </div>
            <div class="box-body">
              <a href="<?php echo schools_url("school/view/{$mschool->id}/" . urlencode(url_title($mschool->name,"-",TRUE))); ?>"><h3 class="widget-user-username" itemprop="name"><?php echo $mschool->name; ?></h3></a>
              <h5 class="widget-user-desc"><?php echo $mschool->category_name; ?></h5>
              <p class="widget-user-desc" property="address" typeof="PostalAddress"><span property="streetAddress"><?php echo $mschool->address; ?></span></p>
            </div>
          </div>
  </div>
<?php $mn++;
if( $mn==6 ) {
  echo '</div><div class="row">';
  $mn=0;
}
?>
<?php } ?>
</div>
<?php } ?>

    </section>
    <!-- /.content -->

     <?php $this->load->view('admin_controls/admin_tools'); ?>
     
  </div>
  <!-- /.content-wrapper -->


<?php $this->load->view('footer'); ?>