          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Schools</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">

              <div class="table-responsive mailbox-messages">
              
                <table class="table table-hover table-striped">
                  <tbody>
<?php foreach($schools_list as $school) { ?>
                  <tr>
                    <td class="mailbox-name"><a href="<?php echo schools_url("school/view/{$school->id}/" . urlencode(url_title($school->name,"-",TRUE))); ?>"><?php echo $school->name; ?></a></td>
                    <td class="mailbox-subject"><?php echo $school->address; ?></td>
                    <td class="mailbox-date"><?php echo $school->phone_number; ?></td>
<?php if( isset($school->municipal_name) ) { ?>
                    <td class="mailbox-date"><?php echo $school->municipal_name; ?></td>
<?php } ?>
                  </tr>
<?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
            <p class="text-center"><?php echo $pagination; ?></p>
            </div>
          </div>
          <!-- /. box -->