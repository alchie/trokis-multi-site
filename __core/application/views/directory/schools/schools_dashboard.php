<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <h1>
        Schools <?php echo ( isset($current_location) && ($current_location) ) ? 'in ' . $current_location->name : ''; ?>
      </h1>
<?php 
$breadcrumbs[] = array('title'=>'Home', 'url'=>main_url());
$breadcrumbs[] = array('title'=>'Directory', 'url'=>main_url("directory"));
if( isset($current_location) && ($current_location) ) { 
  $current_location_url = schools_url("schools/in/" . urlencode( url_title( $current_location->name, "-", true) ) . "/{$current_location->id}");
  $breadcrumbs[] = array('title'=>'Schools in ' . $current_location->name, 'url'=>$current_location_url);
} else { 
  $breadcrumbs[] = array('title'=>'Schools', 'url'=>schools_url());
}
schema_breadcrumbs($breadcrumbs); 
?>
    </section>

    <!-- Main content -->
    <section class="content">

  


<div class="row">
        <div class="col-md-4">
          <div class="box box-solid">
<div class="box-header with-border">
              <h3 class="box-title">Search School</h3>
</div>
            <div class="box-body">

<script>
  (function() {
    var cx = '006082807627053211932:oufeqr1f_gm';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->

        <div class="col-md-8">

    <div class="row">
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo number_format($public_elem_schools->total,0); ?></h3>

              <p>Public Elementary Schools</p>
            </div>
            <div class="icon">
              <i class="glyphicon glyphicon-flag"></i>
            </div>
<?php 
$public_es_url = ( isset($current_location) && ($current_location) ) ? "schools/in/" . urlencode( url_title( $current_location->name, "-", true) ) . "/{$current_location->id}" : "index/category/1";
?>
            <a href="<?php echo schools_url($public_es_url); ?>" class="small-box-footer">View List <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo number_format($public_high_schools->total,0); ?></h3>

              <p>Public High Schools</p>
            </div>
            <div class="icon">
              <i class="glyphicon glyphicon-flag"></i>
            </div>
<?php 
$public_hs_url = ( isset($current_location) && ($current_location) ) ? "schools/in/" . urlencode( url_title( $current_location->name, "-", true) ) . "/{$current_location->id}" : "index/category/3";
?>
            <a href="<?php echo schools_url($public_hs_url); ?>" class="small-box-footer">View List <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo number_format($private_schools->total,0); ?></h3>

              <p>Private Schools</p>
            </div>
            <div class="icon">
              <i class="fa fa-flag-checkered"></i>
            </div>
<?php 
$public_ps_url = ( isset($current_location) && ($current_location) ) ? "schools/in/" . urlencode( url_title( $current_location->name, "-", true) ) . "/{$current_location->id}" : "index/category/2";
?>
            <a href="<?php echo schools_url($public_ps_url); ?>" class="small-box-footer">View List <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

      </div>
      <!-- /.row -->

        </div>
        <!-- ./col -->
</div>
<!-- /.row -->

<?php if( $municipal_schools ) { ?>
<div class="row">
<?php $mn=0; foreach($municipal_schools as $mschool) { ?>
<?php 
$logo_url = $this->config->item('trokis_logo_url');
if( $mschool->logo_url != '' ) {
  $logo_url = $mschool->logo_url;
}
?>
  <div class="col-md-3 col-sm-6">
<div class="box box-widget widget-user" itemscope itemtype="http://schema.org/School">
            <div class="widget-user-header bg-black" style="background: url('<?php echo $logo_url; ?>') center center;">
            </div>
            <div class="box-body">
              <a href="<?php echo schools_url("school/view/{$mschool->id}/" . urlencode(url_title($mschool->name,"-",TRUE))); ?>"><h3 class="widget-user-username" itemprop="name"><?php echo $mschool->name; ?></h3></a>
              <h5 class="widget-user-desc"><?php echo $mschool->category_name; ?></h5>
              <p class="widget-user-desc" property="address" typeof="PostalAddress"><span property="streetAddress"><?php echo $mschool->address; ?></span></p>
            </div>
          </div>
  </div>
<?php $mn++;
if( $mn==4 ) {
  echo '</div><div class="row">';
  $mn=0;
}
?>
<?php } ?>
</div>
<?php } ?>

<div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-body">

<ul class="list-unstyled">
<?php foreach($municipals as $municipal) { 
$slug = urlencode(url_title($municipal->name, "-", TRUE));
  ?>
  <li class="col-md-3 col-sm-6"><a href="<?php echo schools_url("schools/in/{$slug}/{$municipal->id}"); ?>"><span class="glyphicon glyphicon-arrow-right"></span> Schools in <?php echo $municipal->name; ?></a></li>
<?php } ?>
</ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
</div>
<!-- /.row -->    
    </section>
    <!-- /.content -->

    <?php $this->load->view('admin_controls/admin_tools'); ?>
    
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>