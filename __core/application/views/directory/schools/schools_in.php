<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">

      <h1>
        Schools in <?php echo $municipal->name; ?>
      </h1>

<?php 
$breadcrumbs[] = array('title'=>'Home', 'url'=>main_url());
$breadcrumbs[] = array('title'=>'Directory', 'url'=>main_url("directory"));
$breadcrumbs[] = array('title'=>'Schools', 'url'=>schools_url());
$breadcrumbs[] = array('title'=>'All Schools', 'url'=>schools_url("index/category/0"));
$breadcrumbs[] = array('title'=>$municipal->region_name, 'url'=>schools_url("index/region/0/{$municipal->region}"));
$breadcrumbs[] = array('title'=>$municipal->province_name, 'url'=>schools_url("index/province/0/{$municipal->region}/{$municipal->province}"));
$breadcrumbs[] = array('title'=>$municipal->name, 'url'=>site_url(uri_string()));

schema_breadcrumbs($breadcrumbs); 
?>

    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- /.col -->
 <div class="col-md-12">
 <?php $this->load->view('directory/schools/schools_card'); ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <?php $this->load->view('admin_controls/admin_tools'); ?>
    
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>