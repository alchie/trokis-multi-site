<!doctype html>
<html ⚡>
<head>
  <meta charset="utf-8">
  <title><?php echo $page_title; ?></title>
<?php 
echo (isset($canonical_url)) ? '<link rel="canonical" href="'.$canonical_url.'" />'."\n" : ''; 
?>
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
  <script async src="https://cdn.ampproject.org/v0.js"></script>

  <style amp-custom>
    body {
      margin: 0;
      font-family: 'Georgia', Serif;
    }
    .brand-logo {
      font-family: 'Open Sans';
    }
    .ad-container {
      display: flex;
      justify-content: center;
    }
    .content-container p {
      line-height: 24px;
    }
    header,
    .article-body {
      padding: 15px;
    }
    /* In the shadow-doc mode, the parent shell already has header and nav. */
    .amp-shadow header {
      display: none;
    }
    .lightbox {
      background: #222;
    }
    .full-bleed {
      margin: 0 -15px;
    }
    .lightbox-content {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      display: flex;
      flex-direction: column;
      flex-wrap: nowrap;
      justify-content: center;
      align-items: center;
    }
    .lightbox-content p {
      color: #fff;
      padding: 15px;
    }
    .lightbox amp-img {
      width: 100%;
    }
    figure {
      margin: 0;
    }
    figcaption {
      color: #6f757a;
      padding: 15px 0;
      font-size: .9em;
    }
    .author {
      display: flex;
      align-items: center;
      background: #f4f4f4;
      padding: 0 15px;
      font-size: .8em;
      border: solid #dcdcdc;
      border-width: 1px 0;
    }
    .header-time {
      color: #a8a3ae;
      font-family: 'Roboto';
      font-size: 12px;
    }
    .author p {
      margin: 5px;
    }
    .byline {
      font-family: 'Roboto';
      display: inline-block;
    }
    .byline p {
      line-height: normal;
    }
    .byline .brand {
      color: #6f757a;
    }
    .standfirst {
      color: #6f757a;
    }
    .mailto {
      text-decoration: none;
    }
    #author-avatar {
      margin: 10px;
      border: 5px solid #fff;
      width: 50px;
      height: 50px;
      border-radius: 50%;
    }
    h1 {
      margin: 5px 0;
      font-weight: normal;
    }
    footer {
      display: flex;
      align-items: center;
      justify-content: center;
      height: 226px;
      background: #f4f4f4;
      margin-bottom: 20px;
    }
    hr {
      margin: 0;
    }
    amp-app-banner {
      padding: 10px;
    }
    amp-app-banner .content {
      display: flex;
      align-items: center;
      justify-content: center;
    }
    amp-img {
      background-color: #f4f4f4;
    }
    .slot-fallback {
      padding: 16px;
      background: yellow;
    }
    amp-app-banner amp-img {
      background-color: transparent;
      margin-right: 15px
    }
    amp-app-banner .description {
      margin-right: 20px;
    }
    amp-app-banner h5 {
      margin: 0;
    }
    amp-app-banner p {
      font-size: 10px;
      margin: 3px 0;
    }
    amp-app-banner .actions button {
      padding: 5px;
      width: 70px;
      border: 1px solid #aaa;
      font-size: 8px;
      background: #fff;
    }
    }
    amp-app-banner .actions {
      text-align: right;
      font-size: 14px;
    }
    amp-sidebar {
      width: 150px;
    }
    nav li {
      list-style: none;
      margin-bottom: 10px;
    }
    nav li a {
      text-decoration: none;
      color: #666666;
    }
  </style>
  <link href='https://fonts.googleapis.com/css?family=Georgia|Open+Sans|Roboto' rel='stylesheet' type='text/css'>
  <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
</head>
<body>
  <main role="main">
    <article>
      <div class="content-container">
        <header>
          <h1 itemprop="headline"><?php echo $current_school->name; ?></h1>
          <p class="standfirst">
            <?php echo $current_school->category_name; ?>
          </p>
        </header>

        <div class="article-body" itemprop="articleBody">
         <p>
           <strong>Address:</strong> <?php echo $current_school->address; ?>
         </p>
          <p>
           <strong>Municipality:</strong> <?php echo $current_school->municipal_name; ?>
         </p>
         <p>
           <strong>Province:</strong> <?php echo $current_school->province_name; ?>
         </p>
         <p>
           <strong>Region:</strong> <?php echo $current_school->region_name; ?>
         </p>
         <p>
         <strong>Phone:</strong> <?php echo ( $current_school->phone_number ) ? $current_school->phone_number : 'Not Set'; ?>
         </p>
         <p>
         <strong>Email:</strong> school<?php echo "{$current_school->id}@trokis.com"; ?>
         </p>
         <p>
         <strong>Website:</strong> http://<?php echo slugify($current_school->name . " " . $current_school->municipal_name); ?>.schools.trokis.com
         </p>
         <p>
         <strong>Category:</strong> <?php echo ( $current_school->category_name ) ? $current_school->category_name : 'Not Set'; ?>
         </p>
        </div>
      </div>
    </article>
  </main>

</body>
</html>