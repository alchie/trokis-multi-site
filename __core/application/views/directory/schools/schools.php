<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
<?php if(isset($current_region)) { ?>
  <?php if(isset($current_province)) { ?>
    <?php if(isset($current_municipal)) { ?>
      <h1>
        <?php echo $current_municipal->name; ?>
      </h1>
    <?php } else { ?>
      <h1>
        <?php echo $current_province->name; ?>
      </h1>
    <?php } ?>
  <?php } else { ?>
      <h1>
        <?php echo $current_region->name; ?>
      </h1>
  <?php } ?>
<?php } else { ?>
      <h1>
        <?php echo $category->name; ?>
      </h1>
<?php } ?>


<?php 
$breadcrumbs[] = array('title'=>'Home', 'url'=>main_url());
$breadcrumbs[] = array('title'=>'Directory', 'url'=>main_url("directory"));
$breadcrumbs[] = array('title'=>'Schools', 'url'=>schools_url());
if(isset($category)) {
  $breadcrumbs[] = array('title'=>$category->name, 'url'=>schools_url("index/category/{$category->id}"));
}
if(isset($current_region)) {
  $breadcrumbs[] = array('title'=>$current_region->name, 'url'=>schools_url("index/region/{$category_id}/{$region_id}"));
}
if(isset($current_province)) {
  $breadcrumbs[] = array('title'=>$current_province->name, 'url'=>schools_url("index/province/{$category_id}/{$region_id}/{$province_id}"));
}
schema_breadcrumbs($breadcrumbs); 
?>
</section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
 <?php $this->load->view('directory/schools/schools_card'); ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-12">

 <?php $this->load->view('directory/schools/schools_sidebar2'); ?>

        </div>
        </div>
    </section>
    <!-- /.content -->

<?php $this->load->view('admin_controls/admin_tools'); ?>

  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>