<?php 
echo '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
foreach($schools as $school) { 
	echo "<url><loc>http://{$school->slug}.schools.trokis.com</loc></url>";
}
echo '</urlset>';
