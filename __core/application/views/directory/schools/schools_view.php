<?php 
$slug = urlencode(url_title($current_school->name,"-",TRUE));

$this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

<?php 
$breadcrumbs[] = array('title'=>'Home', 'url'=>main_url());
$breadcrumbs[] = array('title'=>'Directory', 'url'=>main_url("directory"));
$breadcrumbs[] = array('title'=>'Schools', 'url'=>schools_url());
$breadcrumbs[] = array('title'=>$current_school->category_name, 'url'=>schools_url("index/category/{$current_school->category}"));
$breadcrumbs[] = array('title'=>$current_school->region_name, 'url'=>schools_url("index/region/{$current_school->category}/{$current_school->region}"));
$breadcrumbs[] = array('title'=>$current_school->province_name, 'url'=>schools_url("index/province/{$current_school->category}/{$current_school->region}/{$current_school->province}"));
$breadcrumbs[] = array('title'=>$current_school->name, 'url'=>site_url(uri_string()));
schema_breadcrumbs($breadcrumbs); 
?>

      <div class="row">
        <div class="col-md-8">


<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
      <table class="table table-striped table-hover">
        <tbody>
          <tr>
            <th>School Head</th>
            <td><?php echo $current_school->school_head; ?></td>
          </tr>
           <tr>
            <th>Designation</th>
            <td><?php echo $current_school->designation; ?></td>
          </tr>
           <tr>
            <th>Telephone</th>
            <td><?php echo $current_school->telephone; ?></td>
          </tr>
           <tr>
            <th>Mobile</th>
            <td><?php echo $current_school->mobile; ?></td>
          </tr>
           <tr>
            <th>Email</th>
            <td><?php echo $current_school->email; ?></td>
          </tr>
        </tbody>
      </table>
      </div>
    </div>
  </div>
</div>
<?php } ?>

          <div class="box box-primary">
<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
<div class="box-header with-border">
              <h3 class="box-title"><?php echo $current_school->name; ?></h3>

              <div class="box-tools pull-right">

<div class="btn-group">
  <button type="button" class="btn btn-success dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-pencil"></i>
    Edit <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?php echo schools_url("manage/details/{$current_school->id}"); ?>"> Details</a></li>
    <li><a href="<?php echo schools_url("manage/slug/{$current_school->id}"); ?>"> Slug</a></li>
    <li><a href="<?php echo schools_url("manage/logo/{$current_school->id}"); ?>"> Logo</a></li>
    <li><a href="<?php echo schools_url("manage/social_media/{$current_school->id}"); ?>"> Social Media</a></li>
  </ul>
</div>

              </div>
              <!-- /.box-tools -->
</div>
<?php } ?>
            <div class="box-body box-profile box-school-view" itemscope itemtype="http://schema.org/School">

<div class="row">
  <div class="col-md-7">

<div class="panel panel-default">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
<?php 
$logo_url = $this->config->item('trokis_logo_url');
if( $current_school->logo_url != '' ) {
  $logo_url = $current_school->logo_url;
}
?>
              <img itemprop="logo" class="profile-user-img img-responsive img-circle" src="<?php echo $logo_url; ?>" alt="<?php echo $current_school->name; ?> Logo">

              <h1 class="profile-username text-center" itemprop="name"><?php echo $current_school->name; ?></h1>
      </div>
    </div>
    <div class="row" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
      <div class="col-md-4"><strong>Address:</strong></div>
      <div class="col-md-8"> <span itemprop="streetAddress"><?php echo $current_school->address; ?></span><br>
<a href="<?php echo schools_url("index/municipal/{$current_school->category}/{$current_school->region}/{$current_school->province}/{$current_school->municipality}"); ?>"><span  itemprop="addressLocality"><?php echo $current_school->municipal_name; ?></span></a>,
 <a href="<?php echo schools_url("index/province/{$current_school->category}/{$current_school->region}/{$current_school->province}"); ?>"><span itemprop="addressLocality"><?php echo $current_school->province_name; ?></span></a>,
 <a href="<?php echo schools_url("index/region/{$current_school->category}/{$current_school->region}"); ?>"><span itemprop="addressRegion"><?php echo $current_school->region_name; ?></span></a>

      </div>
    </div>
    <div class="row">
      <div class="col-md-4"><strong>Phone:</strong></div>
      <div class="col-md-8"> <span itemprop="telephone"><?php echo ( $current_school->phone_number ) ? $current_school->phone_number : 'Not Set'; ?></span></div>
    </div>
 <div class="row">
      <div class="col-md-4"><strong>Email:</strong></div>
      <div class="col-md-8"> <span itemprop="email"><?php echo "school{$current_school->id}@trokis.com"; ?></span></div>
    </div>
    <div class="row">
      <div class="col-md-4"><strong>Website:</strong></div>
<?php 
$school_slug = ($current_school->school_slug) ? $current_school->school_slug : slugify($current_school->name);
?>
      <div class="col-md-8"> <a target="_blank" itemprop="url" href="http://<?php echo $school_slug; ?>.schools.trokis.com"><small><?php echo $school_slug; ?>.schools.trokis.com</small></a></div>
    </div>
   <div class="row">
      <div class="col-md-4"><strong>Category:</strong></div>
      <div class="col-md-8"><?php echo ( $current_school->category_name ) ? $current_school->category_name : 'Not Set'; ?></div>
    </div>
    <div class="row">
      <div class="col-md-4"><strong>Hours Open:</strong></div>
      <div class="col-md-8"> <span itemprop="openingHours" content="">Not Set</span></div>
    </div>

     <div class="row">
      <div class="col-md-6"><strong>Rating:</strong> <a href="#" property="aggregateRating">0</a></div>
      <div class="col-md-6"><strong>Votes:</strong> <a href="#">0</a></div>
      
    </div>
 </div>
</div>
  </div>
  <div class="col-md-5 no-padding">
<center>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Schools View Page -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="2024948920"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

<p>
<div class="fb-like" data-href="<?php echo $canonical_url; ?>" data-layout="button" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
</p>
</center>
  </div>
</div>
 
<div class="row">
  <div class="col-md-5 hidden-xs">
<center>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Schools View Page -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="2024948920"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</center>
  </div>
  <div class="col-md-7">
<?php if( ($current_school->lat > 0) && ($current_school->lng > 0)) { ?>
<style type="text/css">
  <!--
#map {
    width: 100%; height: 300px
}
#mapInfoWindowHeading {
  font-size: 15px;
  text-align: center;
    width: 100%;
    padding: 0;
    margin: 0;
    font-weight: bold;
}
  -->
</style>
<div id="map"></div>
    <script>
      function initMap() {
        var coordinates = {lat: <?php echo $current_school->lat; ?>, lng: <?php echo $current_school->lng; ?>};
        var center_coor = {lat: <?php echo $current_school->lat; ?>, lng: <?php echo $current_school->lng; ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: center_coor
        });
 var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="mapInfoWindowHeading" class="firstHeading"><?php echo addslashes($current_school->name); ?></h1>'+
      '<div id="bodyContent">'+
<?php if( $current_school->address ) { ?>
      '<br>Address: <?php echo addslashes($current_school->address); ?></p>'+
<?php } ?>
<?php if( $current_school->phone_number ) { ?>
      '<p>Telephone Number: <?php echo $current_school->phone_number; ?>'+
<?php } ?>
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
        var marker = new google.maps.Marker({
          position: coordinates,
          map: map,
          title: '<?php echo addslashes($current_school->name); ?>'
        });
marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
      infowindow.open(map, marker);

      }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item('google_api_key'); ?>&callback=initMap"></script>

<?php } else { ?>

<iframe width="100%" height="300" frameborder="0" style="border:0"
src="https://www.google.com/maps/embed/v1/place?q=<?php echo urlencode( $current_school->name . "," . $current_school->address ); ?>&key=AIzaSyBCfHj_vkfbi-MJN6d4neNIqVz8MtUAF8w" allowfullscreen></iframe>

<?php } ?>
</div>
</div>

<?php 
$social_metas = array();
if($school_metas) {
  foreach($school_metas as $meta) {
    $social_metas[$meta->meta_key] = $meta;
  }
}
?>
<div class="btn-group btn-group-justified">
<?php 
foreach(array(
  'facebook'=>'Facebook',
  'twitter'=>'Twitter',
  'linkedin'=>'LinkedIn',
  'youtube'=>'Youtube',
  'pinterest'=>'Pinterest',
  'tumblr'=>'Tumblr',
  'google'=>'Google+',
  ) as $key=>$title) {
  $url = schools_url("school/redirect/{$key}/{$current_school->id}/{$slug}");
?>
  <a href="<?php echo $url; ?>" class="btn btn-<?php echo $key; ?>" target="_blank"><i class="fa fa-<?php echo $key; ?>-square"></i> <?php echo $title; ?></a>
<?php } ?>
</div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


<div class="row">
  <div class="col-md-12">
<div class="panel panel-default">
  <div class="panel-body">
<div class="fb-comments" data-width="100%" data-href="<?php echo $canonical_url; ?>" data-numposts="5"></div>
</div>
</div>
  </div>
</div>
        </div>
        <!-- /.col -->
        <div class="col-md-4">

<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
      <form method="get" action="<?php echo schools_url("schools/search_id"); ?>">
<div class="input-group">
  <input type="text" class="form-control" name="school_id">
  <div class="input-group-btn"><button type="submit" class="btn btn-default dropdown-toggle">Search</button></div>
</div>
      </form>
  </div>
  </div>
 </div>
</div>
<?php } else { ?>
<div class="panel panel-default">
<div class="box-header with-border">
              <h3 class="box-title">Search School</h3>
</div>
  <div class="panel-body">

<script>
  (function() {
    var cx = '006082807627053211932:oufeqr1f_gm';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

 </div>
</div>
<?php } ?>


<?php if( $properties ) { ?>

          <div class="box box-primary">

            <div class="box-header with-border">
              <small class="pull-right"><a href="<?php echo realestate_url(); ?>">Show More</a></small>
                          <h3 class="box-title">Properties for Sale</h3>
            </div>

            <div class="box-body box-profile">
                 
<ul class="list-group">
<?php foreach($properties as $property) { ?>
<?php 
$plogo_url = $this->config->item('trokis_logo_url');
if( $property->logo_url != '' ) {
  $plogo_url = $property->logo_url;
}
?>
<div class="list-group-item" itemscope itemtype="http://schema.org/RealEstate">
  <a itemprop="url" href="<?php echo realestate_url("property/view/{$property->slug}"); ?>">

<div class="row">
  <div class="col-md-4"><img itemprop="logo" class="profile-user-img" src="<?php echo $plogo_url; ?>" alt=""></div>
  <div class="col-md-8"><span itemprop="name"><?php echo $property->title; ?></span></div>
</div>
  </a>
</div>
<?php } ?>
</ul>

            </div>
            <!-- /.box-body -->
          </div>

<?php } ?>

          <div class="box box-primary">

            <div class="box-header with-border">
              <small class="pull-right"><a href="<?php echo schools_url("index/municipal/{$current_school->category}/{$current_school->region}/{$current_school->province}/{$current_school->municipality}"); ?>">Show More</a></small>
                          <h3 class="box-title">Schools in <?php echo $current_school->municipal_name; ?> </h3>
            </div>

            <div class="box-body box-profile">
                 
<ul class="list-group">
<?php foreach($municipal_schools as $mschool) { ?>
<div class="list-group-item" itemscope itemtype="http://schema.org/School">
  <a itemprop="url" href="<?php echo schools_url("school/view/{$mschool->id}/" . urlencode(url_title($mschool->name,"-",TRUE))); ?>"><span itemprop="name"><?php echo $mschool->name; ?></span></a>
</div>
<?php } ?>
</ul>

            </div>
            <!-- /.box-body -->
          </div>


          <div class="box box-primary">

            <div class="box-header with-border">
              <small class="pull-right"><a href="<?php echo schools_url("index/province/{$current_school->category}/{$current_school->region}/{$current_school->province}"); ?>">Show More</a></small>
                          <h3 class="box-title">Schools in <?php echo $current_school->province_name; ?></h3>
            </div>

            <div class="box-body box-profile">
                 
<ul class="list-group">
<?php foreach($provincial_schools as $mschool) { ?>
<div class="list-group-item" itemscope itemtype="http://schema.org/School">
  <a itemprop="url" href="<?php echo schools_url("school/view/{$mschool->id}/" . urlencode(url_title($mschool->name,"-",TRUE))); ?>"><span itemprop="name"><?php echo $mschool->name; ?></span></a>
</div>
<?php } ?>
</ul>
            </div>
            <!-- /.box-body -->
          </div>


          <div class="box box-primary">

            <div class="box-header with-border">
            <small class="pull-right"><a href="<?php echo schools_url("index/region/{$current_school->category}/{$current_school->region}"); ?>">Show More</a></small>
                          <h3 class="box-title">Schools in <?php echo $current_school->region_name; ?></h3>
            </div>

            <div class="box-body box-profile">
                 
<ul class="list-group">
<?php foreach($regional_schools as $mschool) { ?>
<div class="list-group-item" itemscope itemtype="http://schema.org/School">
  <a itemprop="url" href="<?php echo schools_url("school/view/{$mschool->id}/" . urlencode(url_title($mschool->name,"-",TRUE))); ?>"><span itemprop="name"><?php echo $mschool->name; ?></span></a>
</div>
<?php } ?>
</ul>

            </div>
            <!-- /.box-body -->
          </div>


        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <?php $this->load->view('admin_controls/admin_tools'); ?>
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>