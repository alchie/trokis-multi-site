<?php 
echo '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
foreach($municipals as $municipal) { 
	echo '<url><loc>'.schools_url("schools/in/" . urlencode(url_title($municipal->name,"-",TRUE)) . "/{$municipal->id}").'</loc></url>';
}
echo '</urlset>';
