<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <h1>
        Companies <?php echo ( isset($current_location) && ($current_location) ) ? 'in ' . $current_location->name : ''; ?>
      </h1>
<?php 
$breadcrumbs[] = array('title'=>'Home', 'url'=>main_url());
$breadcrumbs[] = array('title'=>'Directory', 'url'=>main_url("directory"));
$breadcrumbs[] = array('title'=>'Companies', 'url'=>companies_url());

schema_breadcrumbs($breadcrumbs); 
?>
    </section>

    <!-- Main content -->
    <section class="content">

  


<div class="row">
        <div class="col-md-6">
          <div class="box box-solid">
<div class="box-header with-border">
              <h3 class="box-title">Search Companies</h3>
</div>
            <div class="box-body">

<script>
  (function() {
    var cx = '006082807627053211932:ys90cxlkdwg';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->

        <div class="col-md-6">

            <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo number_format($companies_all,0); ?></h3>

              <p>Companies</p>
            </div>
            <div class="icon">
              <i class="glyphicon glyphicon-flag"></i>
            </div>
            <a href="<?php echo companies_url(); ?>" class="small-box-footer">View List <i class="fa fa-arrow-circle-right"></i></a>
          </div>

        </div>
        <!-- ./col -->
</div>
<!-- /.row -->

<?php if( $companies ) { ?>
<div class="row">
<?php $mn=0; foreach($companies as $company) { ?>
<?php 
$logo_url = $this->config->item('trokis_logo_url');
?>
  <div class="col-md-3 col-sm-6">
<div class="box box-widget widget-user" itemscope itemtype="http://schema.org/LocalBusiness">
            <div class="widget-user-header bg-black" style="background: url('<?php echo $logo_url; ?>') center center;">
            </div>
            <div class="box-body">
              <a href="<?php echo companies_url("company/view/{$company->slug}"); ?>"><h3 class="widget-user-username" itemprop="name"><?php echo $company->name; ?></h3></a>
              <h5 class="widget-user-desc"><?php echo $company->streetAddress; ?></h5>
              <p class="widget-user-desc" property="address" typeof="PostalAddress"><span property="streetAddress"><?php echo $company->telephone; ?></span></p>
            </div>
          </div>
  </div>
<?php $mn++;
if( $mn==4 ) {
  echo '</div><div class="row">';
  $mn=0;
}
?>
<?php } ?>
</div>
<?php } ?>
    
    </section>
    <!-- /.content -->
    <?php $this->load->view('admin_controls/admin_tools'); ?>
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>