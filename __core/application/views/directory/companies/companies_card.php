    
<?php if( $schools_list ) { ?>
<div class="row">
<?php $mn=0; foreach($schools_list as $mschool) { ?>
<?php 
$logo_url = $this->config->item('trokis_logo_url');
if( $mschool->logo_url != '' ) {
  $logo_url = $mschool->logo_url;
}
?>
  <div class="col-md-2 col-sm-6">
<div class="box box-widget widget-user" itemscope itemtype="http://schema.org/School">
            <div class="widget-user-header bg-black" style="background: url('<?php echo $logo_url; ?>') center center;">
            </div>
            <div class="box-body">
              <a itemprop="url" href="<?php echo companies_url("school/view/{$mschool->id}/" . urlencode(url_title($mschool->name,"-",TRUE))); ?>"><h3 class="widget-user-username" itemprop="name"><?php echo $mschool->name; ?></h3></a>
              <h5 class="widget-user-desc"><?php echo $mschool->category_name; ?></h5>
              <p class="widget-user-desc" property="address" typeof="PostalAddress"><span property="streetAddress"><?php echo $mschool->address; ?></span></p>
            </div>
          </div>
  </div>
<?php $mn++;
if( $mn==6 ) {
  echo '</div><div class="row">';
  $mn=0;
}
?>
<?php } ?>
</div>
<?php } ?>

<div class="text-center" style="margin-bottom:10px;"><?php echo $pagination; ?></div>

