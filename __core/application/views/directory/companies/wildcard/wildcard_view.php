
<!DOCTYPE html>
<html lang="en">

<head>

      <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title><?php echo substr($page_title, 0, 70); ?></title>
  <meta name="description" content="<?php echo $meta_description; ?>">
  <meta name="keywords" content="<?php echo $meta_keywords; ?>">
  <?php echo (isset($opengraph)) ? $opengraph : ''; ?>

     <link rel="stylesheet" href="<?php echo assets_url('css/bootstrap.min.css'); ?>">
     <link rel="stylesheet" href="<?php echo assets_url('css/bootstrap-social.css'); ?>">
     <link rel="stylesheet" href="<?php echo assets_url('font-awesome/css/font-awesome.min.css'); ?>">
     <link rel="stylesheet" href="<?php echo assets_url('one-page-wonder/one-page-wonder.css'); ?>">

</head>

<body>

       <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo main_url(); ?>">Trokis Philippines</a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?php echo main_url('services/claim-page'); ?>?page_url=<?php echo urlencode("http://" . $current_company->slug . ".company.trokis.com"); ?>">Claim this page</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Full Width Image Header -->
    <header class="header-image">
        <div class="headline">
            <div class="container">
                <h1><?php echo $current_company->name; ?></h1>
                <h2><?php echo $current_company->streetAddress; ?></h2>
                <h4 class="text-white"><?php echo ( $current_company->telephone ) ? $current_company->telephone : '{No Phone Number}'; ?> &middot; <?php echo "company{$current_company->id}@trokis.com"; ?></h4>
            </div>
        </div>
    </header>

    <!-- Page Content -->
    <div class="container">

<div data-spy="affix" data-offset-top="500" data-offset-bottom="0" class="panel panel-default fix-ads">

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Trokis Page Not Found -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="3675832127"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

</div>

        <hr class="featurette-divider">

        <!-- First Featurette -->
        <div class="featurette" id="about">
            <img class="featurette-image img-circle img-responsive pull-right" src="https://unsplash.it/500/500?image=836">
            <h2 class="featurette-heading">This First Heading
                <span class="text-muted">Will Catch Your Eye</span>
            </h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>

        <hr class="featurette-divider">

        <!-- Second Featurette -->
        <div class="featurette" id="services">
            <img class="featurette-image img-circle img-responsive pull-left" src="https://unsplash.it/500/500?image=452">
            <h2 class="featurette-heading">The Second Heading
                <span class="text-muted">Is Pretty Cool Too.</span>
            </h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>

        <hr class="featurette-divider">

        <!-- Third Featurette -->
        <div class="featurette" id="contact">
            <img class="featurette-image img-circle img-responsive pull-right" src="https://unsplash.it/500/500?image=453">
            <h2 class="featurette-heading">The Third Heading
                <span class="text-muted">Will Seal the Deal.</span>
            </h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>

        <hr class="featurette-divider">

        <!-- Third Featurette -->
        <div class="featurette" id="contact">
<p class="btn-group btn-group-justified">
<?php 
foreach(array(
  'facebook'=>'Facebook',
  'twitter'=>'Twitter',
  'linkedin'=>'LinkedIn',
  'youtube'=>'Youtube',
  'pinterest'=>'Pinterest',
  'tumblr'=>'Tumblr',
  'google'=>'Google+',
  ) as $key=>$title) {
  $url = companies_url("company/redirect/{$key}/{$current_company->slug}");
?>
  <a href="<?php echo $url; ?>" class="btn btn-<?php echo $key; ?>" target="_blank"><i class="fa fa-<?php echo $key; ?>-square"></i> <?php echo $title; ?></a>
<?php } ?>
</p>
        </div>


        <div class="featurette" id="contact">


<form method="post">
<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Contact <strong><?php echo $current_company->name; ?></strong></h3>
  </div>
  <div class="panel-body">
  <div class="row">
      <div class="col-md-4">
      <div class="form-group">
    <label for="exampleInputEmail1">Full Name</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Full Name">
  </div>
  </div>
      <div class="col-md-4">
                <div class="form-group">
    <label for="exampleInputEmail1">Phone Number</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Phone Number">
  </div>
      </div>
      <div class="col-md-4">
           <div class="form-group">
            <label for="exampleInputEmail1">Email Address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email Address">
          </div>
      </div>
  </div>

   <div class="form-group">
            <label for="exampleInputEmail1">Message</label>
            <textarea name="message" class="form-control" rows="10"></textarea>
          </div>

  </div>
    <div class="panel-footer">
     <button type="submit" class="btn btn-success">Submit</button>
  </div>
</div>


 
</form>

        </div>


        <hr class="featurette-divider">

</div>

 <div class="featurette">
<?php if( $current_company->geolocation ) { ?>
<style type="text/css">
  <!--
#map {
    width: 100%; height: 400px
}
#mapInfoWindowHeading {
  font-size: 15px;
  text-align: center;
    width: 100%;
    padding: 0;
    margin: 0;
    font-weight: bold;
}
  -->
</style>
<div id="map"></div>
    <script>
      function initMap() {
        var coordinates = {lat: <?php echo $current_company->lat; ?>, lng: <?php echo $current_company->lng; ?>};
        var center_coor = {lat: <?php echo $current_company->lat; ?>, lng: <?php echo $current_company->lng; ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: center_coor
        });
 var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="mapInfoWindowHeading" class="firstHeading"><?php echo addslashes($current_company->name); ?></h1>'+
      '<div id="bodyContent">'+
<?php if( $current_company->address ) { ?>
      '<br>Address: <?php echo addslashes($current_company->address); ?></p>'+
<?php } ?>
<?php if( $current_company->phone_number ) { ?>
      '<p>Telephone Number: <?php echo $current_company->phone_number; ?>'+
<?php } ?>
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
        var marker = new google.maps.Marker({
          position: coordinates,
          map: map,
          title: '<?php echo addslashes($current_company->name); ?>'
        });
marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
      infowindow.open(map, marker);

      }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item('google_api_key'); ?>&callback=initMap"></script>

<?php } else { ?>

<iframe width="100%" height="400" frameborder="0" style="border:0"
src="https://www.google.com/maps/embed/v1/place?q=<?php echo urlencode( $current_company->name . "," . $current_company->streetAddress ); ?>&key=AIzaSyBCfHj_vkfbi-MJN6d4neNIqVz8MtUAF8w" allowfullscreen></iframe>

<?php } ?>

</div>

<div class="container">

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p class=""><a href="<?php echo main_url('services/claim-page'); ?>?page_url=<?php echo urlencode("http://" . $current_company->slug . ".company.trokis.com"); ?>">Claim this page</a></p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->






<script src="<?php echo assets_url('plugins/jQuery/jquery-2.2.3.min.js'); ?>"></script>
<script src="<?php echo assets_url('js/bootstrap.min.js'); ?>"></script>

</body>

</html>
