<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

<?php 
$breadcrumbs[] = array('title'=>'Home', 'url'=>main_url());
$breadcrumbs[] = array('title'=>'Directory', 'url'=>main_url("directory"));
$breadcrumbs[] = array('title'=>'Companies', 'url'=>companies_url());
$breadcrumbs[] = array('title'=>$current_company->name, 'url'=>site_url(uri_string()));
schema_breadcrumbs($breadcrumbs); 
?>

      <div class="row">
        <div class="col-md-8">


          <div class="box box-primary">
<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
<div class="box-header with-border">
              <h3 class="box-title"><?php echo $current_company->name; ?></h3>

              <div class="box-tools pull-right">

<div class="btn-group">
  <button type="button" class="btn btn-success dropdown-toggle btn-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-pencil"></i>
    Edit <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="<?php echo companies_url("manage/details/{$current_company->id}"); ?>"> Details</a></li>
    <li><a href="<?php echo companies_url("manage/logo/{$current_company->id}"); ?>"> Logo</a></li>
    <li><a href="<?php echo companies_url("manage/social_media/{$current_company->id}"); ?>"> Social Media</a></li>
  </ul>
</div>

              </div>
              <!-- /.box-tools -->
</div>
<?php } ?>
            <div class="box-body box-profile box-school-view">

<div class="row">
  <div class="col-md-7">

<div class="panel panel-default">
  <div itemscope itemtype="http://schema.org/LocalBusiness" class="panel-body">
    <div class="row">
      <div class="col-md-12">
<?php 
$logo_url = $this->config->item('trokis_logo_url');
?>
              <img itemprop="image" class="profile-user-img img-responsive img-circle" src="<?php echo $logo_url; ?>" alt="<?php echo $current_company->name; ?> Logo">

              <h1 itemprop="name" class="profile-username text-center"><?php echo $current_company->name; ?></h1>
      </div>
    </div>
    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress" class="row">
      <div class="col-md-4"><strong>Address:</strong></div>
      <div class="col-md-8"> <span itemprop="streetAddress"><?php echo $current_company->streetAddress; ?></span><br>
<span  itemprop="addressLocality"><?php echo $current_company->addressLocality; ?></span>, <span itemprop="addressRegion"><?php echo $current_company->addressRegion; ?></span>

      </div>
    </div>
    <div class="row">
      <div class="col-md-4"><strong>Phone:</strong></div>
      <div class="col-md-8"> <span itemprop="telephone"><?php echo ( $current_company->telephone ) ? $current_company->telephone : 'Not Set'; ?></span></div>
    </div>
 <div class="row">
      <div class="col-md-4"><strong>Email:</strong></div>
      <div class="col-md-8"> <span itemprop="email"><?php echo "company{$current_company->id}@trokis.com"; ?></span></div>
    </div>
    <div class="row">
      <div class="col-md-4"><strong>Website:</strong></div>
      <div class="col-md-8"> <a target="_blank" itemprop="url" href="http://<?php echo $current_company->slug; ?>.company.trokis.com"><small><?php echo $current_company->slug; ?>.company.trokis.com</small></a></div>
    </div>
   <div class="row">
      <div class="col-md-4"><strong>Category:</strong></div>
      <div class="col-md-8">Not Set</div>
    </div>
    <div class="row">
      <div class="col-md-4"><strong>Hours Open:</strong></div>
      <div class="col-md-8"> <span itemprop="openingHours" content="">Not Set</span></div>
    </div>

     <div class="row">
      <div class="col-md-6"><strong>Rating:</strong> <a href="#" property="aggregateRating">0</a></div>
      <div class="col-md-6"><strong>Votes:</strong> <a href="#">0</a></div>
      
    </div>
 </div>
</div>
  </div>
  <div class="col-md-5 no-padding">
<center>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Trokis - Companies -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="6093107976"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<p>
<div class="fb-like" data-href="<?php echo $canonical_url; ?>" data-layout="button" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
</p>
</center>
  </div>
</div>
 
<div class="row">
  <div class="col-md-5 hidden-xs">
<center>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Trokis - Companies -->
<ins class="adsbygoogle"
     style="display:inline-block;width:336px;height:280px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="6093107976"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</center>
  </div>
  <div class="col-md-7">
<?php if( $current_company->geolocation ) { ?>
<style type="text/css">
  <!--
#map {
    width: 100%; height: 300px
}
#mapInfoWindowHeading {
  font-size: 15px;
  text-align: center;
    width: 100%;
    padding: 0;
    margin: 0;
    font-weight: bold;
}
  -->
</style>
<div id="map"></div>
    <script>
      function initMap() {
        var coordinates = {lat: <?php echo $current_company->lat; ?>, lng: <?php echo $current_company->lng; ?>};
        var center_coor = {lat: <?php echo $current_company->lat; ?>, lng: <?php echo $current_company->lng; ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: center_coor
        });
 var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="mapInfoWindowHeading" class="firstHeading"><?php echo addslashes($current_company->name); ?></h1>'+
      '<div id="bodyContent">'+
<?php if( $current_company->address ) { ?>
      '<br>Address: <?php echo addslashes($current_company->address); ?></p>'+
<?php } ?>
<?php if( $current_company->phone_number ) { ?>
      '<p>Telephone Number: <?php echo $current_company->phone_number; ?>'+
<?php } ?>
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
        var marker = new google.maps.Marker({
          position: coordinates,
          map: map,
          title: '<?php echo addslashes($current_company->name); ?>'
        });
marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
      infowindow.open(map, marker);

      }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item('google_api_key'); ?>&callback=initMap"></script>

<?php } else { ?>

<iframe width="100%" height="300" frameborder="0" style="border:0"
src="https://www.google.com/maps/embed/v1/place?q=<?php echo urlencode( $current_company->name . "," . $current_company->streetAddress ); ?>&key=AIzaSyBCfHj_vkfbi-MJN6d4neNIqVz8MtUAF8w" allowfullscreen></iframe>

<?php } ?>
</div>
</div>

<?php 
$social_metas = array();
if($company_metas) {
  foreach($company_metas as $meta) {
    $social_metas[$meta->meta_key] = $meta;
  }
}
?>
<div class="btn-group btn-group-justified">
<?php 
foreach(array(
  'facebook'=>'Facebook',
  'twitter'=>'Twitter',
  'linkedin'=>'LinkedIn',
  'youtube'=>'Youtube',
  'pinterest'=>'Pinterest',
  'tumblr'=>'Tumblr',
  'google'=>'Google+',
  ) as $key=>$title) {
  $url = companies_url("company/redirect/{$key}/{$current_company->slug}");
?>
  <a href="<?php echo $url; ?>" class="btn btn-<?php echo $key; ?>" target="_blank"><i class="fa fa-<?php echo $key; ?>-square"></i> <?php echo $title; ?></a>
<?php } ?>
</div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


<div class="row">
  <div class="col-md-12">
<div class="panel panel-default">
  <div class="panel-body">
<div class="fb-comments" data-width="100%" data-href="<?php echo $canonical_url; ?>" data-numposts="5"></div>
</div>
</div>
  </div>
</div>
        </div>
        <!-- /.col -->
        <div class="col-md-4">

<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12">
      <form method="get" action="<?php echo companies_url("manage/search_id"); ?>">
<div class="input-group">
  <input type="text" class="form-control" name="company_id">
  <div class="input-group-btn"><button type="submit" class="btn btn-default dropdown-toggle">Search</button></div>
</div>
      </form>
  </div>
  </div>
 </div>
</div>
<?php } else { ?>
<div class="panel panel-default">
<div class="box-header with-border">
              <h3 class="box-title">Search Companies</h3>
</div>
  <div class="panel-body">

<script>
  (function() {
    var cx = '006082807627053211932:ys90cxlkdwg';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

 </div>
</div>
<?php } ?>


        <div class="box box-widget">
        <div class="box-header"><h3 class="box-title">Contact <strong><?php echo $current_company->name; ?></strong></h3></div>
             <div class="box-body">
                
<form>
  <div class="form-group">
    <label for="sidebarContactName">Name</label>
    <input type="text" class="form-control" id="sidebarContactName" placeholder="Full Name" name="full_name">
  </div>
  <div class="form-group">
    <label for="sidebarContactEmail">Email</label>
    <input type="email" class="form-control" id="sidebarContactEmail" placeholder="Email" name="email">
  </div>
  <div class="form-group">
    <label for="sidebarContactPhone">Phone Number</label>
    <input type="text" class="form-control" id="sidebarContactPhone" placeholder="Phone Number" name="phone_number">
  </div>
    <div class="form-group">
    <label for="sidebarContactPhone">Message</label>
    <textarea type="text" class="form-control" id="sidebarContactMessage" placeholder="Message" name="message"></textarea>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>

             </div>
        </div>



<?php if( $properties ) { ?>

          <div class="box box-primary">

            <div class="box-header with-border">
              <small class="pull-right"><a href="<?php echo realestate_url(); ?>">Show More</a></small>
                          <h3 class="box-title">Properties for Sale</h3>
            </div>

            <div class="box-body box-profile">
                 
<ul class="list-group">
<?php foreach($properties as $property) { ?>
<?php 
$plogo_url = $this->config->item('trokis_logo_url');
if( $property->logo_url != '' ) {
  $plogo_url = $property->logo_url;
}
?>
<div class="list-group-item" itemscope itemtype="http://schema.org/RealEstate">
  <a itemprop="url" href="<?php echo realestate_url("property/view/{$property->slug}"); ?>">

<div class="row">
  <div class="col-md-4"><img itemprop="logo" class="profile-user-img" src="<?php echo $plogo_url; ?>" alt=""></div>
  <div class="col-md-8"><span itemprop="name"><?php echo $property->title; ?></span></div>
</div>
  </a>
</div>
<?php } ?>
</ul>

            </div>
            <!-- /.box-body -->
          </div>

<?php } ?>


          <div class="box box-primary">

            <div class="box-header with-border">
                          <h3 class="box-title">More Companies</h3>
            </div>

            <div class="box-body box-profile">
                 
<ul class="list-group">
<?php foreach($other_companies as $ocompanies) { ?>
<div class="list-group-item" itemscope itemtype="http://schema.org/LocalBusiness">
  <a itemprop="url" href="<?php echo companies_url("company/view/{$ocompanies->slug}"); ?>"><span itemprop="name"><?php echo $ocompanies->name; ?></span></a>
</div>
<?php } ?>
</ul>

            </div>
            <!-- /.box-body -->
          </div>



        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  <?php $this->load->view('admin_controls/admin_tools'); ?>
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>