<?php
echo '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
for($i=1;$i<=ceil($companies/5000);$i++){
	echo '<sitemap><loc>'.base_url("sitemap_companies_{$i}.xml").'</loc>';
 	echo '<lastmod>'.date("Y-m-01", strtotime("-1 month")).'</lastmod></sitemap>';
}
for($i=1;$i<=ceil($companies/5000);$i++){
	echo '<sitemap><loc>'.base_url("sitemap_wildcard_{$i}.xml").'</loc>';
 	echo '<lastmod>'.date("Y-m-01", strtotime("-1 month")).'</lastmod></sitemap>';
}
echo '</sitemapindex>';
