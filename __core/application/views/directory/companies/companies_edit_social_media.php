<?php $this->load->view('header'); ?>
<style type="text/css">
  <!--
#GoogleMap_map {
    width: 100%; height: 400px
}
#mapInfoWindowHeading {
  font-size: 15px;
  text-align: center;
    width: 100%;
    padding: 0;
    margin: 0;
    font-weight: bold;
}
  -->
</style>
 <?php $this->load->view('sidebar'); ?>


  <div class="content-wrapper">


<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-12">

<form method="post">
          <div class="box box-primary">

<div class="box-header with-border">

<h3 class="box-title"><?php echo $current_company->name; ?> - Social Media</h3>

    <div class="box-tools pull-right">
      <a class="btn btn-box-tool" href="<?php echo companies_url("company/view/{$current_company->slug}"); ?>"><i class="fa fa-eye"></i> View</a>
    </div>
              <!-- /.box-tools -->
</div>

            <div class="box-body box-profile">

<?php 
$social_metas = array();
if($company_metas) {
  foreach($company_metas as $meta) {
    $social_metas[$meta->meta_key] = $meta;
  }
}
foreach(array(
  'facebook' => 'Facebook URL',
  'twitter' => 'Twitter URL',
  'linkedin' => 'LinkedIn URL',
  'youtube' => 'Youtube URL',
  'pinterest' => 'Pinterest URL',
  'tumblr' => 'Tumblr URL',
  'google' => 'Google+ URL',
) as $key=>$title) { ?>
 <div class="form-group">
    <label><?php echo $title; ?></label>
    <div class="input-group">
    <input type="text" class="form-control" placeholder="<?php echo $title; ?>" name="meta[<?php echo $key; ?>]" value="<?php echo (isset($social_metas[$key])) ? $social_metas[$key]->meta_value : ''; ?>">
    <span class="input-group-addon">
    <input type="checkbox" name="active[<?php echo $key; ?>]" value="1" <?php echo (isset($social_metas[$key])&&($social_metas[$key]->active==1)) ? 'checked' : ''; ?>>
    </span>
    </div>
  </div>
<?php } ?>
            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>

          </div>
          <!-- /.box -->
</form>

        </div>
      </div>
      <!-- /.row -->

    <?php $this->load->view('admin_controls/admin_tools'); ?>

    </section>
    <!-- /.content -->

<?php } ?>

  </div>
  <!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="GoogleMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width: 80%">
    <div class="modal-content">
        <div id="GoogleMap_map"></div>
        <script type="text/javascript">
          initMap();
        </script>
    </div>
  </div>
</div>

    <script>
      function initMap() {
        var coordinates = {lat: <?php echo ($current_company->lat) ? $current_company->lat : 0; ?>, lng: <?php echo ($current_company->lng) ? $current_company->lng : 0; ?>};
        var map = new google.maps.Map(document.getElementById('GoogleMap_map'), {
          zoom: 16,
          center: coordinates
        });

        var marker = new google.maps.Marker({
          position: coordinates,
          map: map,
          title: '<?php echo addslashes($current_company->name); ?>',
          draggable: true,
        });
        google.maps.event.addListener(marker, 'drag', function(event) {
          //console.debug('new position is '+event.latLng.lat()+' / '+event.latLng.lng()); 
          $('#school_lat').val( event.latLng.lat() );
          $('#school_lng').val( event.latLng.lng() );
        });
        google.maps.event.addListener(marker, 'dragend', function(event) {
          //console.debug('final position is '+event.latLng.lat()+' / '+event.latLng.lng()); 
          $('#school_lat').val( event.latLng.lat() );
          $('#school_lng').val( event.latLng.lng() );
        });
      }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7-Ku_hI74A2xZnAu-xHjE4RIjEWjsQMw&callback=initMap"></script>


<?php $this->load->view('footer'); ?>