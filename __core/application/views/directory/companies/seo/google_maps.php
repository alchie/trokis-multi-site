<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<?php if( $companies ) { ?>

<?php foreach($companies as $company) {
$company_slug = $company->slug;
 ?>


      <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
              
              <strong><?php echo $company->name; ?></strong><br><small><?php echo $company->streetAddress; ?>, <?php echo $company->addressLocality; ?>, <?php echo $company->addressRegion; ?></small>
              </h3>
              <div class="box-tools">
                <a class="btn btn-danger btn-xs" href="<?php echo companies_url(uri_string()); ?>?hide=<?php echo $company->id; ?>">Hide</a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<div class="row">
  <div class="col-md-12">
    <div class="form-horizontal">
      <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Telephone</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="<?php echo $company->telephone; ?>" onfocus="this.select();">
        </div>
      </div>

      <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">Telephone</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="<?php echo $company->telephone; ?>" onfocus="this.select();">
        </div>
      </div>

      <div class="form-group">
        <label for="inputPassword3" class="col-sm-3 control-label">Trokis Site</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" value="http://<?php echo $company_slug; ?>.schools.trokis.com" onfocus="this.select();">
        </div>
      </div>
    </div>
</div>
</div>

<div class="row">
    <div class="col-md-12">
  <center>
<a class="btn btn-warning" href="https://www.google.com.ph/maps/search/<?php echo urlencode($company->name . ", " . $company->streetAddress . ", " . $company->addressLocality . ", " . $company->addressRegion); ?>" target="_google_maps">Google Maps</a> 
<a class="btn btn-primary" href="http://<?php echo $company_slug; ?>.schools.trokis.com" target="_blank">View</a> 
</center>
  </div>
</div>


            </div>

          </div>
          <!-- /. box -->


<?php } ?>

<?php } else { ?>

  <p class="text-center" style="margin:20px;">Nothing Found!</p>

<?php } ?>

<?php if( $pagination ) { ?>
            <center>
            <p class="text-center"><?php echo $pagination; ?></p>
            </center>
<?php } ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>