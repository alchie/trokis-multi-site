<?php 
echo '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
foreach($companies as $company) { 
	echo "<url><loc>http://{$company->slug}.company.trokis.com</loc></url>";
}
echo '</urlset>';
