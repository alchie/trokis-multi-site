<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
 
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Claim a Page
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    <p>
      Thank you so much for the interest in claiming a page.</p>
    <p> We are still currently improving our system and we still don't have an automated way to claim the page. 
    </p>
    <p>However, we can still help you claim a page.</p>
    <p>
      Please email us (<a href="mailto:claimpage@trokis.com">claimpage@trokis.com</a>) the following details:
      <ol>
        <li>Your Full Name</li>
        <li>Your Phone Number</li>
        <li>Page URL: <strong><?php echo $this->input->get('page_url'); ?></strong></li>
        <li>Your Message, if any.</li>
      </ol>
    </p>
    <p>We will contact you as soon as we receive the request.</p>
    <p>Thank you so much and we apologize for this inconvenience!</p>
    <p><br>Sincerely yours, <br><br></p>
    <p><strong>Alchie Tagudin</strong><br>
    +63 (946) 255-9955</p>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>