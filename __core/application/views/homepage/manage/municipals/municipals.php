<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">


          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Municipals 
              <?php if( $this->input->get('q') ) { ?>
              <small>Search for: <a href="<?php echo main_url(uri_string()); ?>"><strong><?php echo $this->input->get('q'); ?></strong></a></small>
              <?php } ?>
              </h3>

              <div class="box-tools">
              <form method="get">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search" value="<?php echo $this->input->get('q'); ?>">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
              </form>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
<?php if( $municipals ) { ?>
              <div class="table-responsive mailbox-messages">
              
                <table class="table table-hover table-striped">
                  <tbody>
<?php foreach($municipals as $municipal) { ?>
                  <tr>
                    <td width="1%"><?php echo $municipal->id; ?></td>
                    <td><?php echo $municipal->name; ?></td>
                    <td><?php echo $municipal->province_name; ?>
                       <a href="<?php echo main_url("manage/provinces_edit/{$municipal->province}") . "?back=" . urlencode(uri_string()); ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                    </td>
                    <td class="mailbox-date text-right"><a class="btn btn-warning btn-xs" href="<?php echo main_url("manage/municipals_edit/{$municipal->id}") . "?back=" . urlencode(uri_string()); ?>">Edit</a> <a class="btn btn-danger btn-xs btn-confirm" href="<?php echo main_url("manage/municipals_delete/{$municipal->id}") . "?back=" . urlencode(uri_string()); ?>">Delete</a></td>
                  </tr>
<?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
<?php } else { ?>
  <p class="text-center" style="margin:20px;">Nothing Found!</p>
<?php } ?>
            </div>
            <!-- /.box-body -->
<?php if( $pagination ) { ?>
            <div class="box-footer">
            <p class="text-center"><?php echo $pagination; ?></p>
            </div>
<?php } ?>
          </div>
          <!-- /. box -->

    <?php $this->load->view('admin_controls/admin_tools'); ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>