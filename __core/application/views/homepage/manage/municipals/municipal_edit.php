<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<form method="post">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Municipal</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="form-group">
                  <label>Municipal Name</label>
                  <input type="text" class="form-control" placeholder="Municipal Name" name="municipal_name" value="<?php echo $municipal->name; ?>">
                </div>
              <div class="form-group">
                  <label>Province</label>
                  <select class="form-control" name="pr_id">
                      <option value="0">- - Select a Province - -</option>
                      <?php foreach($provinces as $province) { ?>
                      <option value="<?php echo $province->id; ?>|<?php echo $province->region; ?>" <?php echo ($province->id==$municipal->province) ? 'selected' : ''; ?>><?php echo $province->region_name; ?> - <?php echo $province->name; ?></option>
                      <?php } ?>
                  </select>
                </div>
            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo main_url(urldecode($this->input->get('back'))); ?>" class="btn btn-default">Back</a>
              </div>
          </div>
          <!-- /. box -->
</form>

    <?php $this->load->view('admin_controls/admin_tools'); ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>