<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<form method="post">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Region</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="form-group">
                  <label>Region Name</label>
                  <input type="text" class="form-control" placeholder="Region Name" name="region_name" value="<?php echo $region->name; ?>">
                </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo main_url(urldecode($this->input->get('back'))); ?>" class="btn btn-default">Back</a>
              </div>
          </div>
          <!-- /. box -->
</form>

    <?php $this->load->view('admin_controls/admin_tools'); ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>