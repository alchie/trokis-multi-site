<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

<form method="post">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Province</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="form-group">
                  <label>Province Name</label>
                  <input type="text" class="form-control" placeholder="Province Name" name="province_name" value="<?php echo $province->name; ?>">
                </div>
              <div class="form-group">
                  <label>Region</label>
                  <select class="form-control" name="region_id">
                      <option value="0">- - Select a Region - -</option>
                      <?php foreach($regions as $region) { ?>
                      <option value="<?php echo $region->id; ?>" <?php echo ($region->id==$province->region) ? 'selected' : ''; ?>><?php echo $region->name; ?></option>
                      <?php } ?>
                  </select>
                </div>
            </div>
            <!-- /.box-body -->
<div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo main_url(urldecode($this->input->get('back'))); ?>" class="btn btn-default">Back</a>
              </div>
          </div>
          <!-- /. box -->
</form>

    <?php $this->load->view('admin_controls/admin_tools'); ?>
    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>