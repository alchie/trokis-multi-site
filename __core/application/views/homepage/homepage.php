<?php $this->load->view('header'); ?>

 <?php $this->load->view('sidebar'); ?>

  <div class="content-wrapper">

<style type="text/css">
  .search_box {
    background: url("<?php echo assets_url("images/re_cover.jpg"); ?>");
    height: 450px;
    padding-top: 100px;
  }
  .search_box .title {
    text-align: center;
    color: #fff;
    font-weight: 600;
    font-size: 32px;
    text-shadow: 0 0 25px #000;
    text-transform: uppercase;
  }
  .partners {
    background-color: #FFF;
    padding: 10px 0;
  }
</style>
<section class="content search_box">
<form method="get" action="">
<div class="row">
    <div class="col-md-10 col-md-offset-1">
      <h1 class="title"><?php echo $page_title; ?></h1>

<div class="nav-tabs-custom">

  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-search"></i> Search All</a></li>
    <li role="presentation"><a href="#realestate" aria-controls="realestate" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-home"></i> Real Estate</a></li>
    <li role="presentation"><a href="#jobs" aria-controls="jobs" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-briefcase"></i> Jobs</a></li>
    <li role="presentation"><a href="#vehicles" aria-controls="vehicles" role="tab" data-toggle="tab"><i class="fa fa-car"></i>  Vehicles</a></li>
    <li role="presentation"><a href="#motorcycles" aria-controls="motorcycles" role="tab" data-toggle="tab"><i class="fa fa-bicycle"></i>  Motorcycles</a></li>
    <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-calendar"></i>  Events</a></li>
    <li role="presentation"><a href="#companies" aria-controls="companies" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-glass"></i> Companies</a></li>
    <li role="presentation"><a href="#schools" aria-controls="schools" role="tab" data-toggle="tab"><i class="glyphicon glyphicon-education"></i> Schools</a></li>
  </ul>

            <div class="tab-content">

<script>
  (function() {
    var cx = '006082807627053211932:ok9ah73zq5m';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>
<gcse:search></gcse:search>

<div role="tabpanel" class="tab-pane active" id="all">


</div>

            	<div role="tabpanel" class="tab-pane" id="realestate">
            	
            	</div>

            	<div role="tabpanel" class="tab-pane" id="jobs">
            		
            	</div>
            	<div role="tabpanel" class="tab-pane" id="vehicles">

            	</div>
            	<div role="tabpanel" class="tab-pane" id="motorcycles">

            	</div>
            	<div role="tabpanel" class="tab-pane" id="events">
            		
            	</div>
            	<div role="tabpanel" class="tab-pane" id="companies">

            	</div>
            	<div role="tabpanel" class="tab-pane" id="schools">
            		

            	</div>
            </div>
            <!-- /.tab-content -->
          </div>

    </div>
</div>
</form>
</section>

<section class="content">
    <div class="box box-widget">
       <div class="box-header"><h3 class="box-title">Schools</h3></div>
       <div class="box-body">

<?php if( $schools_list ) { ?>
<div class="row">
<?php $mn=0; foreach($schools_list as $mschool) { ?>
<?php 
$logo_url = $this->config->item('trokis_logo_url');
if( $mschool->logo_url != '' ) {
  $logo_url = $mschool->logo_url;
}
?>
  <div class="col-md-2 col-sm-6">
<div class="box box-widget widget-user" itemscope itemtype="http://schema.org/School">
            <div class="widget-user-header bg-black" style="background: url('<?php echo $logo_url; ?>') center center;">
            </div>
            <div class="box-body">
              <a itemprop="url" href="<?php echo schools_url("school/view/{$mschool->id}/" . urlencode(url_title($mschool->name,"-",TRUE))); ?>"><h3 class="widget-user-username" itemprop="name"><?php echo $mschool->name; ?></h3></a>
              <h5 class="widget-user-desc"><?php echo $mschool->category_name; ?></h5>
              <p class="widget-user-desc" property="address" typeof="PostalAddress"><span property="streetAddress"><?php echo $mschool->address; ?></span></p>
            </div>
          </div>
  </div>
<?php $mn++;
if( $mn==6 ) {
  echo '</div><div class="row">';
  $mn=0;
}
?>
<?php } ?>
</div>
<?php } ?>

       </div>
    </div>
</section>

<section class="content">
    <div class="box box-widget">
       <div class="box-header"><h3 class="box-title">Companies</h3></div>
       <div class="box-body">

<?php if( $companies ) { ?>
<div class="row">
<?php $mn=0; foreach($companies as $company) { ?>
<?php 
$logo_url = $this->config->item('trokis_logo_url');
?>
  <div class="col-md-2 col-sm-6">
<div class="box box-widget widget-user" itemscope itemtype="http://schema.org/LocalBusiness">
            <div class="widget-user-header bg-black" style="background: url('<?php echo $logo_url; ?>') center center;">
            </div>
            <div class="box-body">
              <a href="<?php echo companies_url("company/view/{$company->slug}"); ?>"><h3 class="widget-user-username" itemprop="name"><?php echo $company->name; ?></h3></a>
              <h5 class="widget-user-desc"><?php echo $company->streetAddress; ?></h5>
              <p class="widget-user-desc" property="address" typeof="PostalAddress"><span property="streetAddress"><?php echo $company->telephone; ?></span></p>
            </div>
          </div>
  </div>
<?php $mn++;
if( $mn==6 ) {
  echo '</div><div class="row">';
  $mn=0;
}
?>
<?php } ?>
</div>
<?php } ?>

       </div>
    </div>
</section>

  </div>

<?php $this->load->view('footer'); ?>