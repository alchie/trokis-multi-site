

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
      <strong>Memory Usage:</strong> <u><?php echo $this->benchmark->memory_usage();?></u> &middot; <strong>Time Elapsed:</strong> <u><?php echo $this->benchmark->elapsed_time();?></u> &middot; 
<?php } ?>
      All rights reserved.
    </div>
    <strong>Copyright &copy; 2017 <a href="http://www.trokis.com">Trokis Philippines</a></strong> 
  </footer>
</div>
<!-- ./wrapper -->


<div class="modal fade" id="changeLocation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Change Location</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search Location">
        </div>
      </div>
    </div>
  </div>
</div>


<script src="<?php echo assets_url('plugins/jQuery/jquery-2.2.3.min.js'); ?>"></script>
<script src="<?php echo assets_url('js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo assets_url('plugins/fastclick/fastclick.js'); ?>"></script>
<script src="<?php echo assets_url('adminlte/js/app.min.js'); ?>"></script>
<script src="<?php echo assets_url('plugins/iCheck/icheck.min.js'); ?>"></script>
<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
 <script src="<?php echo assets_url('js/bootstrap-select/js/bootstrap-select.min.js'); ?>"></script>
 <script src="<?php echo assets_url('js/admin.js'); ?>"></script>
 <script>
   (function($){
    $('select').selectpicker({
      liveSearch : true,
    });
  })(jQuery);
 </script>
 <?php } ?>
</body>
</html>
