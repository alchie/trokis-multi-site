<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title><?php echo substr($page_title, 0, 70); ?></title>
  <meta name="description" content="<?php echo $meta_description; ?>">
  <meta name="keywords" content="<?php echo $meta_keywords; ?>">
  <?php echo (isset($opengraph)) ? $opengraph : ''; ?>
<link rel="alternate" href="<?php echo base_url(); ?>" hreflang="x-default" />
  <link rel="stylesheet" href="<?php echo assets_url('css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo assets_url('font-awesome/css/font-awesome.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo assets_url('icons/ionicons/css/ionicons.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo assets_url('plugins/fullcalendar/fullcalendar.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo assets_url('plugins/fullcalendar/fullcalendar.print.css'); ?>" media="print">
  <link rel="stylesheet" href="<?php echo assets_url('adminlte/css/AdminLTE.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo assets_url('adminlte/css/skins/_all-skins.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo assets_url('plugins/iCheck/flat/blue.css'); ?>">
  <link rel="stylesheet" href="<?php echo assets_url('adminlte/css/custom.css'); ?>">
<?php if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) { ?>
  <link rel="stylesheet" href="<?php echo assets_url('js/bootstrap-select/css/bootstrap-select.min.css'); ?>">
  <link rel="stylesheet" href="<?php echo assets_url('adminlte/css/admin.css'); ?>">
  <?php } ?>
<?php 
echo (isset($canonical_url)) ? '<link rel="canonical" href="'.$canonical_url.'" />'."\n" : ''; 
echo (isset($amphtml_url)) ? '<link rel="amphtml" href="'.$amphtml_url.'" />'."\n" : ''; 
?>

  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body class="skin-blue sidebar-mini sidebar-collapse">
<?php if(ENVIRONMENT === 'production') { ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', '<?php echo $google_analytics_tracking_id; ?>', 'auto');
  ga('send', 'pageview');

</script>
<?php if( isset($facebook_app_id) && ($facebook_app_id!='') ) { ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.9&appId=<?php echo $facebook_app_id; ?>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php } ?>
<?php } ?>

<div class="wrapper">

<?php $this->load->view('navbar');  ?>


