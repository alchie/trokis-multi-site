<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends WILDCARD_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('companies/Companies_data_model');
		$this->load->model('companies/Companies_meta_model');
	}

	public function index() {

		$this->template_data->set('subdomain', $this->subdomain );
		
		$company = new $this->Companies_data_model('c', 'companies');
		$company->setSlug($this->subdomain,true);
		$company->set_select("c.*");

		if( $company->nonEmpty() ) {
			$company_data = $company->get();
			$this->template_data->set('current_company', $company_data);

			$this->template_data->set('page_title', $company_data->name);
	    	$this->template_data->set('meta_description', "{$company_data->name} is located at {$company_data->streetAddress}, {$company_data->addressLocality}, {$company_data->addressRegion}.");
	    	$this->template_data->set('meta_keywords', "{$company_data->name}, {$company_data->streetAddress}, {$company_data->addressLocality}, {$company_data->addressRegion}");
	    	
			$other_companies = new $this->Companies_data_model('c', 'companies');
			$other_companies->set_where('c.id !=', $company_data->id);
			$other_companies->set_limit(20);
			$other_companies->set_order('RAND()');
			$this->template_data->set('other_companies', $other_companies->populate());

			$metas = new $this->Companies_meta_model('m', 'companies');
			$metas->setCompanyId($company_data->id,true);
			$this->template_data->set('company_metas', $metas->populate() );

			$this->load->view('directory/companies/wildcard/wildcard_view', $this->template_data->get_data());	

		} else {
			$this->template_data->set('page_title', $this->subdomain . " - 404 Page Not Found | Trokis Philippines");
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}
	}

}
