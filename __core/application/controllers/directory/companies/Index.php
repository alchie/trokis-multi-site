<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends COMPANIES_Controller {
	
	var $list_limit = 24;

	public function __construct() {
		parent::__construct();

		$this->load->model('companies/Companies_data_model');

	}

	public function index() {
		
		$companies = new $this->Companies_data_model('c', 'companies');
		$companies->set_order('RAND()');
		$companies->set_limit($this->list_limit);
		$companies->cache_on();
		$this->template_data->set('companies', $companies->populate());

		$this->template_data->set('companies_all', $companies->count_all_results());

		$this->load->view('directory/companies/companies_dashboard', $this->template_data->get_data());
	}

}
