<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends COMPANIES_Controller {

	public function __construct() {
		parent::__construct();
		$this->must_admin();

		$this->load->model('companies/Companies_data_model');
		$this->load->model('companies/Companies_meta_model');

	}

	public function clear_cache()
	{
		$uri_string = ($this->input->get('uri') && ($this->input->get('uri') != '')) ? explode("/",$this->input->get('uri')) : array('default', 'index');
		$this->db->cache_delete($uri_string[0], $uri_string[1]); 
		redirect( site_url($this->input->get('uri')) . "?src=clear_cache" );
	}

	public function clear_all_cache()
	{
		$this->db->cache_delete_all(); 
		redirect( site_url($this->input->get('uri')) . "?src=clear_all_cache" );
	}

	public function details($id) {
		
		$this->must_admin();

		$companies = new $this->Companies_data_model('c', 'companies');
		$companies->setId($id,true,false);
		$companies->set_select("c.*");

		if( $companies->nonEmpty() ) {

			if( $this->input->post() ) {
				$this->form_validation->set_rules('name', 'Company Name', 'trim|required');
				$this->form_validation->set_rules('slug', 'Company Slug', 'trim|required');
				if( $this->form_validation->run() ) {
					$companies->setName($this->input->post('name'),false,true);
					$companies->setSlug(slugify($this->input->post('slug')),false,true);
					$companies->setAddress($this->input->post('address'),false,true);
					$companies->setPhoneNumber($this->input->post('phone_number'),false,true);
					$companies->setLat($this->input->post('lat'),false,true);
					$companies->setLng($this->input->post('lng'),false,true);

					if( $this->input->post('municipal_province_region') ) {
						$mpr = explode('|', $this->input->post('municipal_province_region'));
						if( isset($mpr[0]) ) {
							$schools->setMunicipality($mpr[0],false,true);
						}
						if( isset($mpr[1]) ) {
							$schools->setProvince($mpr[1],false,true);
						}
						if( isset($mpr[2]) ) {
							$schools->setRegion($mpr[2],false,true);
						}
					}
					$companies->update();
				}
			}

			$company_data = $companies->get_results();
			$this->template_data->set('current_company', $companies->get());


			$this->template_data->set('page_title', "Edit Details - " . $company_data->name );
			$this->load->view('directory/companies/companies_edit_details', $this->template_data->get_data());		

		} else {
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}


	}

	public function logo($id) {
		
		$this->must_admin();

		$schools = new $this->Schools_model('s', 'schools');
		$schools->setId($id,true);

		$logo = new $this->Schools_logo_model(NULL, 'schools');
		$logo->setSchoolId($id,true);

		if( $schools->nonEmpty() ) {

			$school_data = $schools->get_results();
			$this->template_data->set('current_school', $schools->get());

			if( $this->input->post('url') ) {
				$this->form_validation->set_rules('url', 'Logo URL', 'trim|required|valid_url');
				if( $this->form_validation->run() ) {
					if( $logo->nonEmpty() ) {
						$logo->setUrl($this->input->post('url'),false,true);
						$logo->update();
					} else {
						$logo->setUrl($this->input->post('url'));
						$logo->insert();
					}
				}

			} else {
				if( $this->input->get('clear') == 1) {
					$logo->delete();
				}
			}

			$logo_data = $logo->get();
			$this->template_data->set('logo', $logo_data);

			$this->template_data->set('page_title', "Edit Logo - " . $school_data->name );
		} 
		$this->load->view('directory/companies/companies_edit_logo', $this->template_data->get_data());	

	}

	public function social_media($id) {
		
		$this->must_admin();

		$company = new $this->Companies_data_model('c', 'companies');
		$company->setId($id,true,false);
		$company->set_select("c.*");
		

		if( $company->nonEmpty() ) {
			$company_data = $company->get_results();
			$this->template_data->set('current_company', $company->get());

			if( $this->input->post('meta') ) {
				$meta_active = $this->input->post('active');
				foreach($this->input->post('meta') as $meta_key=>$meta_value) {
					$meta = new $this->Companies_meta_model('m', 'companies');
					$meta->setCompanyId($id,true);
					$meta->setMetaKey($meta_key,true);
					$meta->setMetaValue($meta_value);
					$meta->setActive((isset($meta_active[$meta_key])) ? 1 : 0);
					if( $meta->nonEmpty() ) {
						$meta->update();
					} else {
						$meta->insert();
					}
				}
			}

			$metas = new $this->Companies_meta_model('m', 'companies');
			$metas->setCompanyId($id,true);
			$this->template_data->set('company_metas', $metas->populate() );

			$this->template_data->set('page_title', "Edit Social Media - " . $company_data->name );
		} 
		$this->load->view('directory/companies/companies_edit_social_media', $this->template_data->get_data());	

	}

	public function search_id() {
		
		$this->must_admin();

		if($this->input->get('company_id')) {
			$company = new $this->Companies_data_model('c', 'companies');
			$company->setId($this->input->get('company_id'),true);
			$company->set_select("c.*");

			if( $company->nonEmpty() ) {
				$company_data = $company->get();
				redirect(companies_url("company/view/{$company_data->slug}"));
			}
		}
		redirect(companies_url());

	}

}
