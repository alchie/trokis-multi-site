<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends COMPANIES_Controller {
	
	var $slugs_used;
	public function __construct() {
		parent::__construct(); 
		$this->must_admin();

		$this->load->model('companies/Companies_model');
		$this->load->model('companies/Companies_data_model');
		$this->load->model('companies/Companies_category_model');
		$this->load->model('companies/Companies_keyword_model');
		$this->load->model('companies/Category_model');
		$this->load->model('companies/Keywords_model');

		$this->slugs_used = array();
	}

	private function connect_category($company_id,$category_id) {
		$connect = new $this->Companies_category_model(NULL, 'companies');
		$connect->setCompanyId($company_id,true);
		$connect->setCategoryId($category_id,true);
		if(!$connect->nonEmpty()) {
			$connect->insert();
		}
	}

	private function import_company_categories($company) {
			$categories = stripslashes(str_replace('"', '', $company->category));
			$cats_arr = explode(",", $categories);
			foreach($cats_arr as $cat_name) {
				$cat_name1 = trim(ucwords(strtolower($cat_name)));
				$category = new $this->Category_model('cat', 'companies');
				$category->setName($cat_name1);
				$category->setSlug(slugify($cat_name1),true);
				$category->setParent(0);
				$category->setActive(1);
				if($category->nonEmpty()) {
					$result = $category->get_results();
					$this->connect_category($company->id,$result->id);
				} else {
					$category->insert();
					$this->connect_category($company->id,$category->get_inserted_id());
				}
			}
	}

	public function import_categories($start=0,$limit=5000) {
		$companies = new $this->Companies_model('c', 'companies');
		$companies->set_order('c.category', 'DESC');
		$companies->set_group_by('c.category');
		$companies->set_limit($limit);
		$companies->set_start($start);
		foreach($companies->populate() as $company) {
			$this->import_company_categories($company);
		}
		$next = $start + $limit;
		echo "<a href=\"".companies_url("tools/import_categories/{$next}/{$limit}")."\">Next</a>";
	}

	private function connect_keyword($company_id,$keyword_id) {
		$connect = new $this->Companies_keyword_model(NULL, 'companies');
		$connect->setCompanyId($company_id,true);
		$connect->setKeywordId($keyword_id,true);
		if(!$connect->nonEmpty()) {
			$connect->insert();
		}
	}

	private function import_company_keywords($company) {
		$keywords = stripslashes(str_replace('"', '', $company->keywords));
			$keywords_arr = explode(",", $keywords);
			foreach($keywords_arr as $keyword) {
				$keyword1 = trim(ucwords(strtolower($keyword)));
				$keywordm = new $this->Keywords_model('k', 'companies');
				$keywordm->setSlug(slugify($keyword1),true);
				$keywordm->setName($keyword1);
				$keywordm->setParent(0);
				$keywordm->setActive(1);
				if($keywordm->nonEmpty()) {
					$result = $keywordm->get_results();
					$this->connect_keyword($company->id,$result->id);
				} else {
					$keywordm->insert();
					$this->connect_keyword($company->id,$keywordm->get_inserted_id());
				}
			}
	}

	public function import_keywords($start=0,$limit=5000) {
		$companies = new $this->Companies_model('c', 'companies');
		$companies->set_order('c.keywords', 'DESC');
		$companies->set_group_by('c.keywords');
		$companies->set_limit($limit);
		$companies->set_start($start);
		foreach($companies->populate() as $company) {
			$this->import_company_keywords($company);
		}
		$next = $start + $limit;
		echo "<a href=\"".companies_url("tools/import_keywords/{$next}/{$limit}")."\">Next</a>";
	}

	private function company_slug($company) {
		$n = 0;
		$slug = rtrim(substr(slugify(trim(ucwords(strtolower($company->name)))),0,60),"-");

			$slug_in_array = in_array($slug, $this->slugs_used);
			while($slug_in_array) {
				$n++;
				$slug = rtrim(substr(slugify(trim(ucwords(strtolower($company->name)))),0,58),"-") . "-" . $n;
				$slug_in_array = in_array($slug, $this->slugs_used);
			}

		$comp = new $this->Companies_data_model(NULL,'companies');
		$comp->setSlug($slug,true);
		$slug_exists = $comp->nonEmpty();
		while($slug_exists) {
			$n++;
			$slug = rtrim(substr(slugify(trim(ucwords(strtolower($company->name)))),0,58),"-") . "-" . $n;
			$slug_in_array = in_array($slug, $this->slugs_used);
			while($slug_in_array) {
				$n++;
				$slug = rtrim(substr(slugify(trim(ucwords(strtolower($company->name)))),0,58),"-") . "-" . $n;
				$slug_in_array = in_array($slug, $this->slugs_used);
			}
			$comp = new $this->Companies_data_model(NULL,'companies');
			$comp->setSlug($slug,true);
			$slug_exists = $comp->nonEmpty();
		}
		$this->slugs_used[] = $slug;
		return $slug;
	}


	public function import_companies($limit=50) {
		
		$fname = "testfile.txt";		

		//$limit=5000;
		$start = file_get_contents($fname); 

		echo "TIMESTAMP: " . date('F d, Y H:i:s') . "<br>";
		echo "LIMIT: " .$limit . "<br>";
		echo "START: " .$start . "<br>";

		$companies = new $this->Companies_model('c', 'companies');
		//$companies->set_order('RAND()');
		$companies->set_limit($limit);
		$companies->set_start($start);
		//$companies->set_join('companies_data cd', 'c.id2=cd.phcom_id');
		$companies->set_select('c.*');
		//$companies->set_where('((SELECT COUNT(*) FROM companies_data cd WHERE cd.phcom_id=c.id2) = 0)');
		//$companies->set_order('cd.phcom_id', 'ASC');
		//$companies->set_where('cd.phcom_id IS NULL');
		$companies_data = $companies->populate();

		foreach($companies_data as $company) { 

			$company_name = trim(ucwords(strtolower($company->name)));
			$streetAddress = trim(ucwords(strtolower($company->streetAddress)));
			$addressLocality = trim(ucwords(strtolower($company->addressLocality)));
			$addressRegion = trim(ucwords(strtolower($company->addressRegion)));

			$new_comp = new $this->Companies_data_model(NULL,'companies');
			$new_comp->setName($company_name);
			$new_comp->setSlug($this->company_slug($company));
			$new_comp->setStreetaddress($streetAddress);
			$new_comp->setAddresslocality($addressLocality);
			$new_comp->setAddressregion($addressRegion);
			$new_comp->setTelephone($company->telephone);
			$new_comp->setFax($company->fax);
			//$new_comp->setGeolocation($company->geolocation);
			$new_comp->setEmail($company->email);
			$new_comp->setWebsite($company->website);
			$new_comp->setLastmod(date('Y-m-d H:i:s'));
			$new_comp->setPhcomId($company->id2,true);
			if( !$new_comp->nonEmpty() ) {
				$new_comp->insert();
			
				//$this->import_company_keywords($company);
				//$this->import_company_categories($company);

				//print_r( $company );
				echo $company_name . "<br>\n";
			}

		}


$next_url = companies_url(uri_string());
echo <<<HTML
<script>
<!--
window.setTimeout(function(){
	window.location = "{$next_url}";
},5000);
-->
</script>
HTML;

		$myfile = fopen($fname, "w");
		$next_start = $start + $limit;
		fwrite($myfile, $next_start);
		fclose($myfile);

	}

	public function fix_company_slugs($start=0) {
		$limit=0;
		$companies = new $this->Companies_data_model('c', 'companies');
		$companies->set_limit($limit);
		$companies->set_start($start);
		$companies->set_where('(CHAR_LENGTH(c.slug) > 60)');
		//$companies->set_where('(c.id < 10000)');

		foreach($companies->populate() as $company) { 
			//print_r( $company );
			$new_slug = $this->company_slug($company);
			echo "UPDATE `companies_data` SET slug='{$new_slug}' WHERE id={$company->id};\n";
			
		}
	}

}
