<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends COMPANIES_Controller {
	
	public function __construct() {
		parent::__construct();

	}

	public function view($slug) {
		
		$company = new $this->Companies_data_model('c', 'companies');
		$company->setSlug($slug,true);
		$company->set_select("c.*");
		$company->cache_on();

		if( $company->nonEmpty() ) {
			$company_data = $company->get();
			$this->template_data->set('current_company', $company_data);

			$this->template_data->set('page_title', $company_data->name );
	    	$this->template_data->set('meta_description', "{$company_data->name} ({$company_data->telephone}) is located at {$company_data->streetAddress}, {$company_data->addressLocality}, {$company_data->addressRegion}.");
	    	$this->template_data->set('meta_keywords', "{$company_data->name}, {$company_data->streetAddress}, {$company_data->addressLocality}, {$company_data->addressRegion}");

			$other_companies = new $this->Companies_data_model('c', 'companies');
			$other_companies->cache_on();
			$other_companies->set_where('c.id !=', $company_data->id);
			$other_companies->set_limit(10);
			$other_companies->set_order('RAND()');
			$this->template_data->set('other_companies', $other_companies->populate());

			$metas = new $this->Companies_meta_model('m', 'companies');
			$metas->setCompanyId($company_data->id,true);
			$this->template_data->set('company_metas', $metas->populate() );

			$this->template_data->set('amphtml_url', companies_url("company/amp/{$company_data->slug}") );

			$this->template_data->opengraph(array(
				'fb:app_id'=>$this->config->item('fbAppId'),
				'og:image' => ((isset($company_data->logo_url)) && ($company_data->logo_url)) ? $company_data->logo_url : $this->config->item('trokis_logo_url'),
				'og:description' => $this->template_data->get('meta_description'),
				'og:site_name' => 'Trokis Philippines',
				'og:title' => $company_data->name,
			));


				$properties = new $this->Properties_model('p', 'realestate');
				$properties->setActive('1',true);
				$properties->set_order('RAND()');
				$properties->set_limit(5);
				$properties->set_select('p.*');
				$properties->set_select('(SELECT pm.media_value FROM properties_media pm WHERE pm.media_type="picture" AND pm.media_key="thumbnail" AND pm.property_id=p.id LIMIT 1) as logo_url');
				$this->template_data->set('properties', $properties->populate());

			$this->load->view('directory/companies/companies_view', $this->template_data->get_data());	

		} else {
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}
	}

	public function amp($slug) {
		
		$company = new $this->Companies_data_model('c', 'companies');
		$company->setSlug($slug,true);
		$company->set_select("c.*");
		$company->cache_on();

		if( $company->nonEmpty() ) {
			$company_data = $company->get();
			$this->template_data->set('current_company', $company_data);

			$this->template_data->set('page_title', $company_data->name . " | Companies Directory | Trokis Philippines");
	    	$this->template_data->set('meta_description', "{$company_data->name} is located at {$company_data->streetAddress}, {$company_data->addressLocality}, {$company_data->addressRegion}.");
	    	$this->template_data->set('meta_keywords', "{$company_data->name}, {$company_data->streetAddress}, {$company_data->addressLocality}, {$company_data->addressRegion}");

			$other_companies = new $this->Companies_data_model('c', 'companies');
			$other_companies->cache_on();
			$other_companies->set_where('c.id !=', $company_data->id);
			$other_companies->set_limit(20);
			$other_companies->set_order('RAND()');
			$this->template_data->set('other_companies', $other_companies->populate());

			$metas = new $this->Companies_meta_model('m', 'companies');
			$metas->setCompanyId($company_data->id,true);
			$this->template_data->set('company_metas', $metas->populate() );

			$this->template_data->set('amphtml_url', companies_url("company/amp/{$company_data->slug}") );

			$this->load->view('directory/companies/companies_amp', $this->template_data->get_data());	

		} else {
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}
	}

	public function redirect($key, $slug) {
		
		$this->template_data->set('meta_key', $key);

		$company = new $this->Companies_data_model('c', 'companies');
		$company->setSlug($slug,true);
		$company->set_select("c.*");
		$company->set_select("(SELECT cm.meta_value FROM companies_meta cm WHERE cm.company_id=c.id AND cm.meta_key='{$key}' AND cm.active=1) as social_url");
		$company->cache_on();

		if( $company->nonEmpty() ) {
			$company_data = $company->get(); 
			$this->template_data->set('current_company', $company_data);
			$this->template_data->set('company_id', $company_data->id);

			$this->template_data->set('page_title', "Redirect to " . $company_data->name . "'s {$key} URL | Schools Directory | Trokis Philippines");
	        $this->template_data->set('meta_description', "Redirect to " . $company_data->name . "'s {$key} URL");
	        $this->template_data->set('meta_keywords', "{$key} url, {$key} redirect, {$key} link, {$key} external, {$key} social media, {$key} social");

			$other_companies = new $this->Companies_data_model('c', 'companies');
			$other_companies->cache_on();
			$other_companies->set_where('c.id !=', $company_data->id);
			$other_companies->set_limit(12);
			$other_companies->set_order('RAND()');
			$other_companies->cache_on();
			$this->template_data->set('other_companies', $other_companies->populate());

			$metas = new $this->Companies_meta_model('m', 'companies');
			$metas->setCompanyId($company_data->id,true);
			$this->template_data->set('company_metas', $metas->populate() );

			$this->load->view('directory/companies/companies_redirect', $this->template_data->get_data());	

		} else {
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}
	}

}
