<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sessions extends CI_Controller {
	
	protected $app_id = 'companies';

	public function __construct() {
		parent::__construct();
		$this->load->model('account/Users_model');
		$this->load->model('sessions/User_sessions_model');
		$this->load->model('sessions/App_sessions_model');
	}

	public function index() {		
		redirect( account_url("login") . "?app=" . $this->app_id );
	}

	public function start($session_id) {

		$session = new $this->App_sessions_model(NULL, 'default');
		$session->setAppId($this->app_id,true);
		$session->setSessionId($session_id,true);
		if( $session->nonEmpty() ) {
			$session_results = $session->get_results();
			$user = new $this->Users_model(NULL, 'account');
			$user->setUid($session_results->user_id,true);
			if( $user->nonEmpty() ) {
				$results = $user->get_results();
		        $this->session->set_userdata( 'loggedIn', true );
	            $this->session->set_userdata( 'userId', $results->uid );
	            $this->session->set_userdata( 'fullName', $results->full_name );
	            $this->session->set_userdata( 'email', $results->email );
	            $this->session->set_userdata( 'type', $results->type );
	            $this->session->set_userdata( 'dateRegistered', $results->date_registered );
	            $this->session->set_userdata( 'emailVerified', (bool) $results->verified );
	            $this->add_user_session($results->uid, session_id() );
			}
			$session->delete();
			redirect( companies_url() );
		}

		redirect( account_url("login") . "?app=" . $this->app_id );
	}

    private function add_user_session($user_id, $session_id) {
        $session = new $this->User_sessions_model(NULL, 'default');
        $session->setUserId($user_id,true);
        $session->setSessionId($session_id,true);
        if( !$session->nonEmpty() ) {
            $session->insert();
        }
    }

}
