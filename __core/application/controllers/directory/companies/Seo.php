<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seo extends COMPANIES_Controller {
	
	var $list_limit = 1;
	var $parent_dir = 'directory/companies/';
	var $database = 'companies';

	public function __construct() {
		parent::__construct();
	}

	private function _data($table='seo_google_maps', $name='google_maps', $start=0, $limit=1) {

		$companies = new $this->Companies_data_model('s', $this->database);
		$companies->cache_off();
		$companies->set_limit($limit);
		$companies->set_start($start);
		$companies->set_order('s.name', 'ASC');
		$companies->set_select("s.*");

		$companies->set_select("(SELECT school_id FROM {$table} seo WHERE seo.school_id=s.id) as school_id2");

		$companies->set_where("((SELECT school_id FROM {$table} seo WHERE seo.school_id=s.id) IS NULL)");

		$this->template_data->set('companies', $companies->populate()); 

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 5,
			'base_url' => companies_base_url( $this->config->item('index_page') . "/seo/{$name}"),
			'total_rows' => $companies->count_all_results(),
			'per_page' => $companies->get_limit()
		)));

	}

	public function google_maps($start=0) {

		if( $this->input->get("hide") ) {
			$seo = new $this->Seo_google_maps_model(NULL, $this->database);
			$seo->setSchoolId($this->input->get("hide"),true);
			if( !$seo->nonEmpty() ) {
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'google_maps';

		$this->_data('seo_google_maps', $name, $start, $limit);

		$this->load->view( $this->parent_dir . 'seo/google_maps', $this->template_data->get_data());
	}

	public function blogger($start=0) {

		if( $this->input->post("school_id") && $this->input->post("blog_url") ) {
			$seo = new $this->Seo_blogger_model(NULL, $this->database);
			$seo->setSchoolId($this->input->post("school_id"),true);
			if( $seo->nonEmpty() ) {
				$seo->setBlogUrl($this->input->post("blog_url"),false,true);
				$seo->update();
			} else {
				$seo->setBlogUrl($this->input->post("blog_url"));
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'blogger';

		$this->_data('seo_blogger', $name, $start, $limit);

		$this->load->view($this->parent_dir . 'seo/blogger', $this->template_data->get_data());
	}

	public function wix($start=0) {

		if( $this->input->post("school_id") && $this->input->post("blog_url") ) {
			$seo = new $this->Seo_wix_model(NULL, $this->database);
			$seo->setSchoolId($this->input->post("school_id"),true);
			if( $seo->nonEmpty() ) {
				$seo->setBlogUrl($this->input->post("blog_url"),false,true);
				$seo->update();
			} else {
				$seo->setBlogUrl($this->input->post("blog_url"));
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'wix';

		$this->_data('seo_wix', $name, $start, $limit);

		$this->load->view($this->parent_dir . 'seo/wix', $this->template_data->get_data());
	}

	public function webs($start=0) {

		if( $this->input->post("school_id") && $this->input->post("blog_url") ) {
			$seo = new $this->Seo_webs_model(NULL, $this->database);
			$seo->setSchoolId($this->input->post("school_id"),true);
			if( $seo->nonEmpty() ) {
				$seo->setBlogUrl($this->input->post("blog_url"),false,true);
				$seo->update();
			} else {
				$seo->setBlogUrl($this->input->post("blog_url"));
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'webs';

		$this->_data('seo_webs', $name, $start, $limit);

		$this->load->view($this->parent_dir . 'seo/webs', $this->template_data->get_data());
	}

	public function weebly($start=0) {

		if( $this->input->post("school_id") && $this->input->post("blog_url") ) {
			$seo = new $this->Seo_weebly_model(NULL, $this->database);
			$seo->setSchoolId($this->input->post("school_id"),true);
			if( $seo->nonEmpty() ) {
				$seo->setBlogUrl($this->input->post("blog_url"),false,true);
				$seo->update();
			} else {
				$seo->setBlogUrl($this->input->post("blog_url"));
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'weebly';

		$this->_data('seo_weebly', $name, $start, $limit);

		$this->load->view($this->parent_dir . 'seo/weebly', $this->template_data->get_data());
	}

	public function tumblr($start=0) {

		if( $this->input->post("school_id") && $this->input->post("blog_url") ) {
			$seo = new $this->Seo_tumblr_model(NULL, $this->database);
			$seo->setSchoolId($this->input->post("school_id"),true);
			if( $seo->nonEmpty() ) {
				$seo->setBlogUrl($this->input->post("blog_url"),false,true);
				$seo->update();
			} else {
				$seo->setBlogUrl($this->input->post("blog_url"));
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'tumblr';

		$this->_data('seo_tumblr', $name, $start, $limit);

		$this->load->view($this->parent_dir . 'seo/tumblr', $this->template_data->get_data());
	}

	public function yola($start=0) {

		if( $this->input->post("school_id") && $this->input->post("blog_url") ) {
			$seo = new $this->Seo_yola_model(NULL, $this->database);
			$seo->setSchoolId($this->input->post("school_id"),true);
			if( $seo->nonEmpty() ) {
				$seo->setBlogUrl($this->input->post("blog_url"),false,true);
				$seo->update();
			} else {
				$seo->setBlogUrl($this->input->post("blog_url"));
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'yola';

		$this->_data('seo_yola', $name, $start, $limit);

		$this->load->view($this->parent_dir . 'seo/yola', $this->template_data->get_data());
	}

}
