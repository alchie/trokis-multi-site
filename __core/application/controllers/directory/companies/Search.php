<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends COMPANIES_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index() {		
		$this->load->view('directory/companies/companies_dashboard', $this->template_data->get_data());
	}

}
