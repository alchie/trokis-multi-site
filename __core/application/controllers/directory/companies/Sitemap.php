<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends COMPANIES_Controller {

	var $limit = 5000;

	public function __construct() {
		parent::__construct();

		$this->load->model('companies/Companies_data_model');

		$this->output->set_content_type('application/xml');
	}

	public function index() {

		$companies = new $this->Companies_data_model('c', 'companies');
		$this->template_data->set('companies', $companies->count_all_results());

		$this->load->view('directory/companies/sitemap_list', $this->template_data->get_data());
	}

	public function companies($page=1) {

		$companies = new $this->Companies_data_model('c', 'companies');
		$companies->set_limit($this->limit);
		$companies->set_start(($page-1)*$this->limit);
		$companies->set_order('c.id', 'ASC');
		$this->template_data->set('companies', $companies->populate());

		$this->load->view('directory/companies/sitemap_content', $this->template_data->get_data());
	}

	public function wildcards($page=1) {

		$companies = new $this->Companies_data_model('c', 'companies');
		$companies->set_limit($this->limit);
		$companies->set_start(($page-1)*$this->limit);
		$companies->set_order('c.id', 'ASC');
		$this->template_data->set('companies', $companies->populate());

		$this->load->view('directory/companies/sitemap_wildcard', $this->template_data->get_data());
	}

}
