<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends WILDCARD_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('schools/Schools_model');
		$this->load->model('schools/Schools_meta_model');
		$this->load->model('schools/Schools_slug_model');
	}

	public function index() {

		$schools = new $this->Schools_slug_model('ss', 'schools');
		$schools->setSlug($this->subdomain,true);
		if( $schools->nonEmpty() ) {
				$schools->set_join('schools s', 's.id=ss.school_id');
				$schools->set_select('s.*');

				$schools->set_join("{$this->db_configs['locations']['database']}.regions r", 's.region=r.id');
				$schools->set_select("r.name as region_name");

				$schools->set_join("{$this->db_configs['locations']['database']}.provinces p", 's.province=p.id');
				$schools->set_select("p.name as province_name");

				$schools->set_join("{$this->db_configs['locations']['database']}.municipals m", 's.municipality=m.id');
				$schools->set_select("m.name as municipal_name");

				$schools->set_join("school_categories sc", 's.category=sc.id');
				$schools->set_select("sc.name as category_name");

				$schools->set_join("schools_logo sl", 'sl.school_id=s.id');
				$schools->set_select("sl.url as logo_url");

				$schools->set_select("ss.slug as school_slug");

				$school_data = $schools->get(); 

				$school_slug = url_title($school_data->name,"-",true);
				$this->template_data->set('slug', $school_slug);
				
				$this->template_data->set('current_school', $school_data);
				$this->template_data->set('page_title', $school_data->name);
	        	$this->template_data->set('meta_description', "{$school_data->name} is a {$school_data->category_name} located at {$school_data->municipal_name}, {$school_data->province_name}, {$school_data->region_name}.");
	        	$this->template_data->set('meta_keywords', "{$school_data->category_name}, {$school_data->municipal_name}, {$school_data->province_name}, {$school_data->region_name}");

	        	$this->template_data->set('current_school', $school_data);

				$municipal_schools = new $this->Schools_model('s', 'schools');
				$municipal_schools->cache_on();
				$municipal_schools->setMunicipality($school_data->municipality,true);
				$municipal_schools->set_where('id !=', $school_data->id);
				$municipal_schools->set_limit(5);
				$municipal_schools->set_order('RAND()');
				$this->template_data->set('municipal_schools', $municipal_schools->populate());

				$provincial_schools = new $this->Schools_model('s', 'schools');
				$provincial_schools->cache_on();
				$provincial_schools->setProvince($school_data->province,true);
				$provincial_schools->set_where('id !=', $school_data->id);
				$provincial_schools->set_limit(5);
				$provincial_schools->set_order('RAND()');
				$this->template_data->set('provincial_schools', $provincial_schools->populate());

				$regional_schools = new $this->Schools_model('s', 'schools');
				$regional_schools->cache_on();
				$regional_schools->setRegion($school_data->region,true);
				$regional_schools->set_where('id !=', $school_data->id);
				$regional_schools->set_limit(5);
				$regional_schools->set_order('RAND()');
				$this->template_data->set('regional_schools', $regional_schools->populate());

				$metas = new $this->Schools_meta_model('m', 'schools');
				$metas->cache_on();
				$metas->setSchoolId($school_data->id,true);
				$this->template_data->set('school_metas', $metas->populate() );

				$this->template_data->opengraph(array(
					'fb:app_id'=>$this->config->item('fbAppId'),
					'og:image' => ($school_data->logo_url) ? $school_data->logo_url : $this->config->item('trokis_logo_url'),
					'og:description' => $this->template_data->get('meta_description'),
					'og:site_name' => 'Trokis Philippines',
					'og:title' => $school_data->name,
				));

				$this->template_data->set('amphtml_url', schools_url("school/amp/{$school_data->id}/{$school_slug}") );

				$this->load->view('directory/schools/wildcard/wildcard_view', $this->template_data->get_data());

		} else {
			$this->template_data->set('page_title', $this->subdomain . " - 404 Page Not Found | Trokis Philippines");
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}
	}

}
