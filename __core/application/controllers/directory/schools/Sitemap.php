<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends SCHOOLS_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('schools/Schools_model');
		$this->load->model('schools/Schools_slug_model');
		$this->load->model('locations/Regions_model');
		$this->load->model('locations/Provinces_model');
		$this->load->model('locations/Municipals_model');
		$this->load->model('schools/School_categories_model');

		$this->output->set_content_type('application/xml');
	}

	public function index() {
		
		$regions = new $this->Regions_model(NULL, 'locations');
		$regions->cache_on();
		$regions->set_limit(0);
		$regions->set_order('name', 'ASC');
		$regions->set_select("*");
		$this->template_data->set('regions', $regions->populate());

		$provinces = new $this->Provinces_model(NULL, 'locations');
		$provinces->cache_on();
		$provinces->set_limit(0);
		$provinces->set_order('name', 'ASC');
		$provinces->set_select("*");
		$this->template_data->set('provinces', $provinces->populate());

		$municipals = new $this->Municipals_model(NULL, 'locations');
		$municipals->cache_on();
		$municipals->set_limit(0);
		$municipals->set_order('name', 'ASC');
		$municipals->set_select("*");
		$this->template_data->set('municipals', $municipals->populate());

		$schools = new $this->Schools_slug_model('ss', 'schools');
		$schools->cache_on();
		$schools->set_limit(10000);
		$this->template_data->set('schools_limit', $schools->get_limit());
		$this->template_data->set('schools_count', $schools->count_all());

		$this->load->view('directory/schools/sitemap_list', $this->template_data->get_data());
	}

	public function schools_in() {
		
		$municipals = new $this->Municipals_model(NULL, 'locations');
		$municipals->cache_on();
		$municipals->set_limit(0);
		$municipals->set_order('name', 'ASC');
		$municipals->set_select("*");
		$this->template_data->set('municipals', $municipals->populate());

		$this->load->view('directory/schools/sitemap_schools_in', $this->template_data->get_data());
	}

	public function region($region_id, $start=0) {

		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		$schools->setRegion($region_id,true);
		$schools->set_limit(0);
		$schools->set_order('s.name', 'ASC');
		$schools->set_select("s.*");
		$this->template_data->set('schools', $schools->populate());
		$this->load->view('directory/schools/sitemap_content', $this->template_data->get_data());
	}

	public function province($province_id, $start=0) {

		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		$schools->setProvince($province_id,true);
		$schools->set_limit(0);
		$schools->set_order('s.name', 'ASC');
		$schools->set_select("s.*");
		$this->template_data->set('schools', $schools->populate());
		$this->load->view('directory/schools/sitemap_content', $this->template_data->get_data());
	}

	public function municipal($municipal_id, $start=0) {

		$municipal = new $this->Municipals_model('m', 'locations');
		$municipal->setId($municipal_id, true);
		$municipal->cache_on();
		$municipal->set_select("*");
		$municipal_data = $municipal->get(); 
		$this->template_data->set('municipal', $municipal_data);

		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		$schools->setMunicipality($municipal_id,true);
		$schools->set_limit(0);
		$schools->set_order('s.name', 'ASC');
		$schools->set_select("s.*");
		$this->template_data->set('schools', $schools->populate());

		$this->load->view('directory/schools/sitemap_content', $this->template_data->get_data());
	}

	public function wildcards($start=0) {

		$schools = new $this->Schools_slug_model('ss', 'schools');
		$schools->cache_on();
		$schools->set_limit(10000);
		$schools->set_start($start);
		$schools->set_select("s.*");
		$schools->set_select("ss.*");
		$schools->set_join("schools s",'ss.school_id=s.id');
		$this->template_data->set('schools', $schools->populate());

		$this->load->view('directory/schools/sitemap_wildcard', $this->template_data->get_data());
	}

}
