<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends SCHOOLS_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index() {		
		$this->load->view('directory/schools/schools_dashboard', $this->template_data->get_data());
	}

}
