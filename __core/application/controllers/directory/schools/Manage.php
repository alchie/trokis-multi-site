<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends SCHOOLS_Controller {

	public function __construct() {
		parent::__construct();
		$this->must_admin();

		$this->load->model('locations/Regions_model');
		$this->load->model('locations/Provinces_model');
		$this->load->model('locations/Municipals_model');

		$this->load->model('schools/Schools_model');
		$this->load->model('schools/Schools_logo_model');
		$this->load->model('schools/Schools_slug_model');
		$this->load->model('schools/Schools_meta_model');
		$this->load->model('schools/School_categories_model');

	}

	public function clear_cache()
	{
		$uri_string = ($this->input->get('uri') && ($this->input->get('uri') != '')) ? explode("/",$this->input->get('uri')) : array('default', 'index');
		$this->db->cache_delete($uri_string[0], $uri_string[1]); 
		redirect( site_url($this->input->get('uri')) . "?src=clear_cache" );
	}

	public function clear_all_cache()
	{
		$this->db->cache_delete_all(); 
		redirect( site_url($this->input->get('uri')) . "?src=clear_all_cache" );
	}

	public function details($id) {
		
		$this->must_admin();

		$schools = new $this->Schools_model('s', 'schools');
		$schools->setId($id,true,false);
		$schools->set_select("s.*");

		if( $schools->nonEmpty() ) {

			if( $this->input->post() ) {
				$this->form_validation->set_rules('name', 'School Name', 'trim|required');
				$this->form_validation->set_rules('slug', 'School Slug', 'trim|required');
				if( $this->form_validation->run() ) {
					$schools->setName($this->input->post('name'),false,true);
					$schools->setSlug(slugify($this->input->post('slug')),false,true);
					$schools->setAddress($this->input->post('address'),false,true);
					$schools->setPhoneNumber($this->input->post('phone_number'),false,true);
					$schools->setLat($this->input->post('lat'),false,true);
					$schools->setLng($this->input->post('lng'),false,true);

					if( $this->input->post('municipal_province_region') ) {
						$mpr = explode('|', $this->input->post('municipal_province_region'));
						if( isset($mpr[0]) ) {
							$schools->setMunicipality($mpr[0],false,true);
						}
						if( isset($mpr[1]) ) {
							$schools->setProvince($mpr[1],false,true);
						}
						if( isset($mpr[2]) ) {
							$schools->setRegion($mpr[2],false,true);
						}
					}
					$schools->update();
				}
			}

			$school_data = $schools->get_results();
			$school_slug = url_title($school_data->name,"-",true);
			$this->template_data->set('current_school', $schools->get());

			$municipals = new $this->Municipals_model('m', 'locations');
			$municipals->cache_on();
			$municipals->set_limit(0);
			$municipals->set_order('name', 'ASC');
			$municipals->set_select('m.*');
			$municipals->set_select('(SELECT name FROM provinces WHERE id=m.province) as province_name');
			$municipals->set_select('(SELECT name FROM regions WHERE id=m.region) as region_name');
			$this->template_data->set('municipals', $municipals->populate());

			$this->template_data->set('page_title', "Edit Details - " . $school_data->name );
			$this->load->view('directory/schools/schools_edit_details', $this->template_data->get_data());		

		} else {
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}


	}

	public function logo($id) {
		
		$this->must_admin();

		$schools = new $this->Schools_model('s', 'schools');
		$schools->setId($id,true);

		$logo = new $this->Schools_logo_model(NULL, 'schools');
		$logo->setSchoolId($id,true);

		if( $schools->nonEmpty() ) {

			$school_data = $schools->get_results();
			$this->template_data->set('current_school', $schools->get());

			if( $this->input->post('url') ) {
				$this->form_validation->set_rules('url', 'Logo URL', 'trim|required|valid_url');
				if( $this->form_validation->run() ) {
					if( $logo->nonEmpty() ) {
						$logo->setUrl($this->input->post('url'),false,true);
						$logo->update();
					} else {
						$logo->setUrl($this->input->post('url'));
						$logo->insert();
					}
				}

			} else {
				if( $this->input->get('clear') == 1) {
					$logo->delete();
				}
			}

			$logo_data = $logo->get();
			$this->template_data->set('logo', $logo_data);

			$this->template_data->set('page_title', "Edit Logo - " . $school_data->name );
		} 
		$this->load->view('directory/schools/schools_edit_logo', $this->template_data->get_data());	

	}

	public function social_media($id) {
		
		$this->must_admin();

		$schools = new $this->Schools_model('s', 'schools');
		$schools->setId($id,true);

		if( $schools->nonEmpty() ) {
			$school_data = $schools->get_results();
			$this->template_data->set('current_school', $schools->get());

			if( $this->input->post('meta') ) {
				$meta_active = $this->input->post('active');
				foreach($this->input->post('meta') as $meta_key=>$meta_value) {
					$meta = new $this->Schools_meta_model('m', 'schools');
					$meta->setSchoolId($id,true);
					$meta->setMetaKey($meta_key,true);
					$meta->setMetaValue($meta_value);
					$meta->setActive((isset($meta_active[$meta_key])) ? 1 : 0);
					if( $meta->nonEmpty() ) {
						$meta->update();
					} else {
						$meta->insert();
					}
				}
			}

			$metas = new $this->Schools_meta_model('m', 'schools');
			$metas->setSchoolId($id,true);
			$this->template_data->set('school_metas', $metas->populate() );

			$this->template_data->set('page_title', "Edit Social Media - " . $school_data->name );
		} 
		$this->load->view('directory/schools/schools_edit_social_media', $this->template_data->get_data());	

	}

	public function slug($id) {

		$this->must_admin();

		$schools = new $this->Schools_model('s', 'schools');
		$schools->setId($id,true);

		if( $schools->nonEmpty() ) {
			$school_data = $schools->get_results();
			$this->template_data->set('current_school', $schools->get());

			$slug = new $this->Schools_slug_model('s', 'schools');
			$slug->setSchoolId($id,true);
			if( $this->input->post('slug') ) {
				$slug->setSlug(slugify($this->input->post('slug')),false,true);
				$slug->update();
			}
			$this->template_data->set('slug', $slug->get());
		}

		$this->load->view('directory/schools/schools_edit_slug', $this->template_data->get_data());	
	}

}
