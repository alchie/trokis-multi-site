<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends SCHOOLS_Controller {
	
	var $list_limit = 24;

	public function __construct() {
		parent::__construct();

		$this->load->model('schools/Schools_model');
		$this->load->model('locations/Regions_model');
		$this->load->model('locations/Provinces_model');
		$this->load->model('locations/Municipals_model');
		$this->load->model('schools/School_categories_model');

	}

	public function index() {
		
		$public_elem_schools = new $this->Schools_model(NULL, 'schools');
		$public_elem_schools->set_select("COUNT(*) as total");
		$public_elem_schools->cache_on();
		$public_elem_schools->setCategory(1,true);
		if( get_cookie('current_location') ) {
			$public_elem_schools->setMunicipality( get_cookie('current_location'), true );
		}
		$this->template_data->set('public_elem_schools', $public_elem_schools->get());

		$public_high_schools = new $this->Schools_model(NULL, 'schools');
		$public_high_schools->set_select("COUNT(*) as total");
		$public_high_schools->cache_on();
		$public_high_schools->setCategory(3,true);
		if( get_cookie('current_location') ) {
			$public_high_schools->setMunicipality( get_cookie('current_location'), true );
		}
		$this->template_data->set('public_high_schools', $public_high_schools->get());

		$private_schools = new $this->Schools_model(NULL, 'schools');
		$private_schools->set_select("COUNT(*) as total");
		$private_schools->cache_on();
		$private_schools->setCategory(2,true);
		if( get_cookie('current_location') ) {
			$private_schools->setMunicipality( get_cookie('current_location'), true );
		}
		$this->template_data->set('private_schools', $private_schools->get());

		$municipals = new $this->Municipals_model(NULL, 'locations');
		$municipals->cache_on();
		$municipals->set_limit(20);
		$municipals->set_order('RAND()');
		$municipals->set_select("*");
		$this->template_data->set('municipals', $municipals->populate());
		

		$municipal_schools = new $this->Schools_model('s', 'schools');
		$municipal_schools->cache_on();
		$municipal_schools->set_limit(12);
		$municipal_schools->set_order('RAND()');
		$municipal_schools->set_select('s.*');
		$municipal_schools->set_select('sc.name as category_name');
		$municipal_schools->set_select('sl.url as logo_url');
		$municipal_schools->set_join('school_categories sc', 'sc.id=s.category');
		$municipal_schools->set_join('schools_logo sl', 'sl.school_id=s.id');
		if( get_cookie('current_location') ) {
			$municipal_schools->setMunicipality( get_cookie('current_location'), true );
		}
		$this->template_data->set('municipal_schools', $municipal_schools->populate());


		$this->load->view('directory/schools/schools_dashboard', $this->template_data->get_data());
	}

	public function category($cat_id=0,$start=0) {

		$this->template_data->set('category_id', $cat_id);


if( $cat_id ) {
		$category = new $this->School_categories_model('sc', 'schools');
		$category->setId($cat_id, true);
		$category->cache_on();
		$category->set_select("*");
		$category_data = $category->get();
} else {
	$category_data = (object) array('name'=>'All Schools', 'id'=>0);
}
		$regions = new $this->Regions_model(NULL, 'locations');
		$regions->cache_on();
		$regions->set_limit(0);
		$regions->set_order('name', 'ASC');
		$regions->set_select("*");
		$this->template_data->set('regions', $regions->populate());

		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		if( $cat_id ) {
			$schools->setCategory($cat_id,true);
		}
		$schools->set_limit($this->list_limit);
		$schools->set_start($start);
		$schools->set_order('s.name', 'ASC');
		$schools->set_select("s.*");
		$schools->set_select('sc.name as category_name');
		$schools->set_select('sl.url as logo_url');
		$schools->set_join('school_categories sc', 'sc.id=s.category');
		$schools->set_join('schools_logo sl', 'sl.school_id=s.id');
		$schools->set_select("(SELECT m.name FROM {$this->db_configs['locations']['database']}.municipals m WHERE m.id=s.municipality) as municipal_name");
		$this->template_data->set('schools_list', $schools->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item('index_page') . "/index/category/{$cat_id}"),
			'total_rows' => $schools->count_all_results(),
			'per_page' => $schools->get_limit()
		)));

		$this->template_data->set('category', $category_data);

		$page_title = '';
		$meta_description = '';
		$meta_keywords = '';
		if( $start ) {
			$page_title .= "Page " . (ceil($start / $this->list_limit) + 1) . " - ";
			$meta_description .= "Page " . (ceil($start / $this->list_limit) + 1) . " on ";
			$meta_keywords .= "Page " . (ceil($start / $this->list_limit) + 1) . ", ";
		}
		$page_title .= $category_data->name . " | Schools Directory | Trokis Philippines";
		$meta_description .= "List of {$category_data->name} in the Philippines";
		$meta_keywords .= "{$category_data->name}, Schools, Business Directory, Trokis Philippines";

		$this->template_data->set('page_title', $page_title);
        $this->template_data->set('meta_description', $meta_description);
        $this->template_data->set('meta_keywords', $meta_keywords);

		$this->load->view('directory/schools/schools', $this->template_data->get_data());
	}

	public function region($cat_id, $region_id, $start=0) {

		$this->template_data->set('category_id', $cat_id);
		$this->template_data->set('region_id', $region_id);

if( $cat_id ) {
		$category = new $this->School_categories_model('sc', 'schools');
		$category->setId($cat_id, true);
		$category->cache_on();
		$category->set_select("*");
		$category_data = $category->get();
} else {
		$category_data = (object) array('name'=>'All Schools', 'id'=>0);
}
		$this->template_data->set('category', $category_data);

		$provinces = new $this->Provinces_model('p', 'locations');
		$provinces->setRegion($region_id,true);
		$provinces->cache_on();
		$provinces->set_limit(0);
		$provinces->set_order('p.name', 'ASC');
		$provinces->set_select("p.*");
		$provinces->set_select("(SELECT COUNT(*) FROM {$this->db_configs['schools']['database']}.schools s WHERE s.region=p.region AND s.province=p.id AND category={$cat_id}) as total");
		$this->template_data->set('provinces', $provinces->populate());

		$region = new $this->Regions_model(NULL, 'locations');
		$region->setId($region_id,true);
		$region->cache_on();
		$region->set_limit(0);
		$region->set_order('name', 'ASC');
		$region->set_select("*");
		$region->set_select("(SELECT COUNT(*) FROM {$this->db_configs['schools']['database']}.schools WHERE region=regions.id AND category={$cat_id}) as total");
		$region_data = $region->get();

		$regions = new $this->Regions_model(NULL, 'locations');
		$regions->cache_on();
		$regions->set_limit(0);
		$regions->set_order('name', 'ASC');
		$regions->set_select("*");
		$regions->set_select("(SELECT COUNT(*) FROM {$this->db_configs['schools']['database']}.schools WHERE region=regions.id AND category={$cat_id}) as total");
		$this->template_data->set('regions', $regions->populate());

		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		if( $cat_id ) {
			$schools->setCategory($cat_id,true);
		}
		$schools->setRegion($region_id,true);
		$schools->set_limit($this->list_limit);
		$schools->set_start($start);
		$schools->set_order('s.name', 'ASC');
		$schools->set_select("s.*");
		$schools->set_select('sc.name as category_name');
		$schools->set_select('sl.url as logo_url');
		$schools->set_join('school_categories sc', 'sc.id=s.category');
		$schools->set_join('schools_logo sl', 'sl.school_id=s.id');
		$schools->set_select("(SELECT m.name FROM {$this->db_configs['locations']['database']}.municipals m WHERE m.id=s.municipality) as municipal_name");
		$this->template_data->set('schools_list', $schools->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 5,
			'base_url' => base_url( $this->config->item('index_page') . "/index/region/{$cat_id}/{$region_id}"),
			'total_rows' => $schools->count_all_results(),
			'per_page' => $schools->get_limit()
		)));

		$this->template_data->set('current_region', $region_data);

		$page_title = '';
		$meta_description = '';
		$meta_keywords = '';
		if( $start ) {
			$page_title .= "Page " . (ceil($start / $this->list_limit) + 1) . " - ";
			$meta_description .= "Page " . (ceil($start / $this->list_limit) + 1) . " on ";
			$meta_keywords .= "Page " . (ceil($start / $this->list_limit) + 1) . ", ";
		}
		$page_title .= $region_data->name . " | " . $category_data->name . " | Schools Directory | Trokis Philippines";
		$meta_description .= "List of {$category_data->name} in {$region_data->name}, Philippines";
		$meta_keywords .= "{$category_data->name}, {$region_data->name}, Schools, Business Directory, Trokis Philippines";

		$this->template_data->set('page_title', $page_title);
        $this->template_data->set('meta_description', $meta_description);
        $this->template_data->set('meta_keywords', $meta_keywords);

		$this->load->view('directory/schools/schools', $this->template_data->get_data());
	}

	public function province($cat_id=0, $region_id=0, $province_id=0, $start=0) {

		$this->template_data->set('category_id', $cat_id);
		$this->template_data->set('region_id', $region_id);
		$this->template_data->set('province_id', $province_id);
if( $cat_id ) {
		$category = new $this->School_categories_model('sc', 'schools');
		$category->setId($cat_id, true);
		$category->cache_on();
		$category->set_select("*");
		$category->set_select("(SELECT COUNT(*) FROM schools s WHERE s.category=sc.id) as total");
		$category_data = $category->get();
} else {
	$category_data = (object) array('name'=>'All Schools', 'id'=>0);
}
		$this->template_data->set('category', $category_data);
		$region = new $this->Regions_model(NULL, 'locations');
		$region->setId($region_id,true);
		$region->cache_on();
		$region->set_limit(0);
		$region->set_order('name', 'ASC');
		$region->set_select("*");
		$region->set_select("(SELECT COUNT(*) FROM {$this->db_configs['schools']['database']}.schools WHERE region=regions.id AND category={$cat_id}) as total");
		$region_data = $region->get();
		$this->template_data->set('current_region', $region_data);

		$provinces = new $this->Provinces_model('p', 'locations');
		$provinces->setId($province_id,true);
		$provinces->cache_on();
		$provinces->set_limit(0);
		$provinces->set_select("p.*");
		$provinces->set_select("(SELECT COUNT(*) FROM {$this->db_configs['schools']['database']}.schools s WHERE s.region=p.region AND s.province=p.id AND category={$cat_id}) as total");
		$province_data = $provinces->get();

		$provinces = new $this->Provinces_model('p', 'locations');
		$provinces->setRegion($region_id,true);
		$provinces->cache_on();
		$provinces->set_limit(0);
		$provinces->set_order('p.name', 'ASC');
		$provinces->set_select("p.*");
		$provinces->set_select("(SELECT COUNT(*) FROM {$this->db_configs['schools']['database']}.schools s WHERE s.region=p.region AND s.province=p.id AND category={$cat_id}) as total");
		$this->template_data->set('provinces', $provinces->populate());

		$regions = new $this->Regions_model(NULL, 'locations');
		$regions->cache_on();
		$regions->set_limit(0);
		$regions->set_order('name', 'ASC');
		$regions->set_select("*");
		$regions->set_select("(SELECT COUNT(*) FROM {$this->db_configs['schools']['database']}.schools WHERE region=regions.id AND category={$cat_id}) as total");
		$this->template_data->set('regions', $regions->populate());

		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		if( $cat_id ) {
			$schools->setCategory($cat_id,true);
		}
		$schools->setRegion($region_id,true);
		$schools->setProvince($province_id,true);
		$schools->set_limit($this->list_limit);
		$schools->set_start($start);
		$schools->set_order('s.name', 'ASC');
		$schools->set_select("s.*");
		$schools->set_select('sc.name as category_name');
		$schools->set_select('sl.url as logo_url');
		$schools->set_join('school_categories sc', 'sc.id=s.category');
		$schools->set_join('schools_logo sl', 'sl.school_id=s.id');
		$schools->set_select("(SELECT m.name FROM {$this->db_configs['locations']['database']}.municipals m WHERE m.id=s.municipality) as municipal_name");

		if( $this->input->get('q') ) {
			$schools->set_where("s.name LIKE \"%{$this->input->get('q')}%\"");
		}

		$this->template_data->set('schools_list', $schools->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 6,
			'base_url' => base_url( $this->config->item('index_page') . "/index/province/{$cat_id}/{$region_id}/{$province_id}"),
			'total_rows' => $schools->count_all_results(),
			'per_page' => $schools->get_limit()
		)));

		$this->template_data->set('current_province', $province_data);

		$page_title = '';
		$meta_description = '';
		$meta_keywords = '';
		if( $start ) {
			$page_title .= "Page " . (ceil($start / $this->list_limit) + 1) . " - ";
			$meta_description .= "Page " . (ceil($start / $this->list_limit) + 1) . " on ";
			$meta_keywords .= "Page " . (ceil($start / $this->list_limit) + 1) . ", ";
		}
		$page_title .= $province_data->name . "," . $region_data->name . " | " . $category_data->name . " | Schools Directory | Trokis Philippines";
		$meta_description .= "List of {$category_data->name} in {$province_data->name}, {$region_data->name}, Philippines";
		$meta_keywords .="{$category_data->name}, {$province_data->name}, {$region_data->name}, Schools, Business Directory, Trokis Philippines";

		$this->template_data->set('page_title', $page_title);
        $this->template_data->set('meta_description', $meta_description);
        $this->template_data->set('meta_keywords', $meta_keywords);

		$this->load->view('directory/schools/schools', $this->template_data->get_data());
	}

	public function municipal($cat_id, $region_id, $province_id, $municipal_id, $start=0) {

		$this->template_data->set('category_id', $cat_id);
		$this->template_data->set('region_id', $region_id);
		$this->template_data->set('province_id', $province_id);
		$this->template_data->set('municipal_id', $municipal_id);

		$category = new $this->School_categories_model('sc', 'schools');
		$category->setId($cat_id, true);
		$category->cache_on();
		$category->set_select("*");
		$category->set_select("(SELECT COUNT(*) FROM schools s WHERE s.category=sc.id) as total");
		$category_data = $category->get();
		$this->template_data->set('category', $category_data);

		$region = new $this->Regions_model(NULL, 'locations');
		$region->setId($region_id,true);
		$region->cache_on();
		$region->set_limit(0);
		$region->set_order('name', 'ASC');
		$region->set_select("*");
		$region->set_select("(SELECT COUNT(*) FROM {$this->db_configs['schools']['database']}.schools WHERE region=regions.id AND category={$cat_id}) as total");
		$region_data = $region->get();
		$this->template_data->set('current_region', $region_data);

		$provinces = new $this->Provinces_model('p', 'locations');
		$provinces->setId($province_id,true);
		$provinces->cache_on();
		$provinces->set_limit(0);
		$provinces->set_select("p.*");
		$provinces->set_select("(SELECT COUNT(*) FROM {$this->db_configs['schools']['database']}.schools s WHERE s.region=p.region AND s.province=p.id AND category={$cat_id}) as total");
		$province_data = $provinces->get();

		$municipal = new $this->Municipals_model('p', 'locations');
		$municipal->setId($municipal_id,true);
		$municipal->cache_on();
		$municipal->set_limit(0);
		$municipal->set_select("p.*");
		$municipal->set_select("(SELECT COUNT(*) FROM {$this->db_configs['schools']['database']}.schools s WHERE s.municipality=p.id AND category={$cat_id}) as total");
		$municipal_data = $municipal->get();

		$provinces = new $this->Provinces_model('p', 'locations');
		$provinces->setRegion($region_id,true);
		$provinces->cache_on();
		$provinces->set_limit(0);
		$provinces->set_order('p.name', 'ASC');
		$provinces->set_select("p.*");
		$provinces->set_select("(SELECT COUNT(*) FROM {$this->db_configs['schools']['database']}.schools s WHERE s.region=p.region AND s.province=p.id AND category={$cat_id}) as total");
		$this->template_data->set('provinces', $provinces->populate());

		$regions = new $this->Regions_model(NULL, 'locations');
		$regions->cache_on();
		$regions->set_limit(0);
		$regions->set_order('name', 'ASC');
		$regions->set_select("*");
		$regions->set_select("(SELECT COUNT(*) FROM {$this->db_configs['schools']['database']}.schools WHERE region=regions.id AND category={$cat_id}) as total");
		$this->template_data->set('regions', $regions->populate());

		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		$schools->setCategory($cat_id,true);
		$schools->setRegion($region_id,true);
		$schools->setProvince($province_id,true);
		$schools->setMunicipality($municipal_id,true);
		$schools->set_limit($this->list_limit);
		$schools->set_start($start);
		$schools->set_order('s.name', 'ASC');
		$schools->set_select("s.*");
		$schools->set_select('sc.name as category_name');
		$schools->set_select('sl.url as logo_url');
		$schools->set_join('school_categories sc', 'sc.id=s.category');
		$schools->set_join('schools_logo sl', 'sl.school_id=s.id');
		$schools->set_select("(SELECT m.name FROM {$this->db_configs['locations']['database']}.municipals m WHERE m.id=s.municipality) as municipal_name");

		if( $this->input->get('q') ) {
			$schools->set_where("s.name LIKE \"%{$this->input->get('q')}%\"");
		}

		$this->template_data->set('schools_list', $schools->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 7,
			'base_url' => base_url( $this->config->item('index_page') . "/index/municipal/{$cat_id}/{$region_id}/{$province_id}/{$municipal_id}"),
			'total_rows' => $schools->count_all_results(),
			'per_page' => $schools->get_limit()
		)));

		$this->template_data->set('current_province', $province_data);
		$this->template_data->set('current_municipal', $municipal_data);

		$page_title = '';
		$meta_description = '';
		$meta_keywords = '';
		if( $start ) {
			$page_title .= "Page " . (ceil($start / $this->list_limit) + 1) . " - ";
			$meta_description .= "Page " . (ceil($start / $this->list_limit) + 1) . " on ";
			$meta_keywords .= "Page " . (ceil($start / $this->list_limit) + 1) . ", ";
		}
		$page_title .= $municipal_data->name . "," . $province_data->name . "," . $region_data->name . " | " . $category_data->name . " | Schools Directory | Trokis Philippines";
		$meta_description .= "List of {$category_data->name} in  {$municipal_data->name}, {$province_data->name}, {$region_data->name}, Philippines";
		$meta_keywords .= "{$category_data->name}, {$province_data->name}, {$region_data->name}, {$municipal_data->name},  Schools, Business Directory, Trokis Philippines";

		$this->template_data->set('page_title', $page_title);
        $this->template_data->set('meta_description', $meta_description);
        $this->template_data->set('meta_keywords', $meta_keywords);

		$this->load->view('directory/schools/schools', $this->template_data->get_data());
	}

}
