<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seo extends SCHOOLS_Controller {
	
	var $list_limit = 1;
	var $parent_dir = 'directory/schools/';
	var $database = 'schools';

	public function __construct() {
		parent::__construct();
	}

	private function _data($table='seo_google_maps', $name='google_maps', $start=0, $limit=1) {

		$schools = new $this->Schools_model('s', $this->database);
		$schools->cache_off();
		$schools->set_limit($limit);
		$schools->set_start($start);
		$schools->set_order('s.name', 'ASC');
		$schools->set_select("s.*");
		$schools->set_join("schools_slug ss", 'ss.school_id=s.id');
		$schools->set_select("ss.slug as school_slug");

		$schools->set_join("{$this->db_configs['locations']['database']}.regions r", 's.region=r.id');
		$schools->set_select("r.name as region_name");

		$schools->set_join("{$this->db_configs['locations']['database']}.provinces p", 's.province=p.id');
		$schools->set_select("p.name as province_name");

		$schools->set_join("{$this->db_configs['locations']['database']}.municipals m", 's.municipality=m.id');
		$schools->set_select("m.name as municipal_name");

		$schools->set_select("(SELECT school_id FROM {$table} seo WHERE seo.school_id=s.id) as school_id2");

		$schools->set_where("((SELECT school_id FROM {$table} seo WHERE seo.school_id=s.id) IS NULL)");

		$this->template_data->set('schools', $schools->populate()); 

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 5,
			'base_url' => schools_base_url( $this->config->item('index_page') . "/seo/{$name}"),
			'total_rows' => $schools->count_all_results(),
			'per_page' => $schools->get_limit()
		)));

	}

	public function google_maps($start=0) {

		if( $this->input->get("hide") ) {
			$seo = new $this->Seo_google_maps_model(NULL, $this->database);
			$seo->setSchoolId($this->input->get("hide"),true);
			if( !$seo->nonEmpty() ) {
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'google_maps';

		$this->_data('seo_google_maps', $name, $start, $limit);

		$this->load->view( $this->parent_dir . 'seo/google_maps', $this->template_data->get_data());
	}

	public function blogger($start=0) {

		if( $this->input->post("school_id") && $this->input->post("blog_url") ) {
			$seo = new $this->Seo_blogger_model(NULL, $this->database);
			$seo->setSchoolId($this->input->post("school_id"),true);
			if( $seo->nonEmpty() ) {
				$seo->setBlogUrl($this->input->post("blog_url"),false,true);
				$seo->update();
			} else {
				$seo->setBlogUrl($this->input->post("blog_url"));
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'blogger';

		$this->_data('seo_blogger', $name, $start, $limit);

		$this->load->view($this->parent_dir . 'seo/blogger', $this->template_data->get_data());
	}

	public function wix($start=0) {

		if( $this->input->post("school_id") && $this->input->post("blog_url") ) {
			$seo = new $this->Seo_wix_model(NULL, $this->database);
			$seo->setSchoolId($this->input->post("school_id"),true);
			if( $seo->nonEmpty() ) {
				$seo->setBlogUrl($this->input->post("blog_url"),false,true);
				$seo->update();
			} else {
				$seo->setBlogUrl($this->input->post("blog_url"));
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'wix';

		$this->_data('seo_wix', $name, $start, $limit);

		$this->load->view($this->parent_dir . 'seo/wix', $this->template_data->get_data());
	}

	public function webs($start=0) {

		if( $this->input->post("school_id") && $this->input->post("blog_url") ) {
			$seo = new $this->Seo_webs_model(NULL, $this->database);
			$seo->setSchoolId($this->input->post("school_id"),true);
			if( $seo->nonEmpty() ) {
				$seo->setBlogUrl($this->input->post("blog_url"),false,true);
				$seo->update();
			} else {
				$seo->setBlogUrl($this->input->post("blog_url"));
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'webs';

		$this->_data('seo_webs', $name, $start, $limit);

		$this->load->view($this->parent_dir . 'seo/webs', $this->template_data->get_data());
	}

	public function weebly($start=0) {

		if( $this->input->post("school_id") && $this->input->post("blog_url") ) {
			$seo = new $this->Seo_weebly_model(NULL, $this->database);
			$seo->setSchoolId($this->input->post("school_id"),true);
			if( $seo->nonEmpty() ) {
				$seo->setBlogUrl($this->input->post("blog_url"),false,true);
				$seo->update();
			} else {
				$seo->setBlogUrl($this->input->post("blog_url"));
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'weebly';

		$this->_data('seo_weebly', $name, $start, $limit);

		$this->load->view($this->parent_dir . 'seo/weebly', $this->template_data->get_data());
	}

	public function tumblr($start=0) {

		if( $this->input->post("school_id") && $this->input->post("blog_url") ) {
			$seo = new $this->Seo_tumblr_model(NULL, $this->database);
			$seo->setSchoolId($this->input->post("school_id"),true);
			if( $seo->nonEmpty() ) {
				$seo->setBlogUrl($this->input->post("blog_url"),false,true);
				$seo->update();
			} else {
				$seo->setBlogUrl($this->input->post("blog_url"));
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'tumblr';

		$this->_data('seo_tumblr', $name, $start, $limit);

		$this->load->view($this->parent_dir . 'seo/tumblr', $this->template_data->get_data());
	}

	public function yola($start=0) {

		if( $this->input->post("school_id") && $this->input->post("blog_url") ) {
			$seo = new $this->Seo_yola_model(NULL, $this->database);
			$seo->setSchoolId($this->input->post("school_id"),true);
			if( $seo->nonEmpty() ) {
				$seo->setBlogUrl($this->input->post("blog_url"),false,true);
				$seo->update();
			} else {
				$seo->setBlogUrl($this->input->post("blog_url"));
				$seo->insert();
			}
			redirect( schools_url( uri_string() ) );
		}

		$limit = 1;
		$name = 'yola';

		$this->_data('seo_yola', $name, $start, $limit);

		$this->load->view($this->parent_dir . 'seo/yola', $this->template_data->get_data());
	}

}
