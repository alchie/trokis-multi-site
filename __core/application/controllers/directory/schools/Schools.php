<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schools extends SCHOOLS_Controller {
	
	var $list_limit = 24;

	public function __construct() {
		parent::__construct();

		$this->load->model('schools/Schools_model');
		$this->load->model('locations/Regions_model');
		$this->load->model('locations/Provinces_model');
		$this->load->model('locations/Municipals_model');
		$this->load->model('schools/School_categories_model');

	}

	public function index() {
		redirect(schools_url());
	}

	public function in($slug,$id,$start=0) {

		$municipal = new $this->Municipals_model('m', 'locations');
		$municipal->setId($id, true);
		$municipal->cache_on();
		$municipal->set_select("m.*");
		$municipal->set_join("regions r", 'm.region=r.id');
		$municipal->set_select("r.name as region_name");
		$municipal->set_join("provinces p", 'm.province=p.id');
		$municipal->set_select("p.name as province_name");
		$municipal_data = $municipal->get(); 
		if( $municipal_data ) {
			if((int) get_cookie('current_location') != (int) $id) {
				current_location_cookie( $id, false );
			}
		}
		$this->template_data->set('municipal', $municipal_data);

		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		$schools->setMunicipality($id,true);
		$schools->set_limit($this->list_limit);
		$schools->set_start($start);
		$schools->set_order('s.name', 'ASC');
		$schools->set_select("s.*");
		$schools->set_select('sc.name as category_name');
		$schools->set_select('sl.url as logo_url');
		$schools->set_join('school_categories sc', 'sc.id=s.category');
		$schools->set_join('schools_logo sl', 'sl.school_id=s.id');

		if( $this->input->get('q') ) {
			$schools->set_where("s.name LIKE \"%{$this->input->get('q')}%\"");
		}

		$this->template_data->set('schools_list', $schools->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 5,
			'base_url' => base_url( $this->config->item('index_page') . "/schools/in/{$slug}/{$id}"),
			'total_rows' => $schools->count_all_results(),
			'per_page' => $schools->get_limit()
		)));

		$page_title = '';
		if( $start ) {
			$page_title .= "Page " . (ceil($start / $this->list_limit) + 1) . " - ";
		}
		$page_title .= "Schools in {$municipal_data->name} | Trokis Philippines";
		$this->template_data->set('page_title', $page_title);
        $this->template_data->set('meta_description', "List of Schools in {$municipal_data->name}, Philippines");
        $this->template_data->set('meta_keywords', "Schools, Business Directory, Trokis Philippines");

		$this->load->view('directory/schools/schools_in', $this->template_data->get_data());
	}

	public function search_id() {
		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		$schools->setId($this->input->get('school_id'),true);
		$schools->set_select("s.*");
		if( $schools->nonEmpty() ) {
			$school_data = $schools->get_results();
			$school_slug = url_title($school_data->name,"-",true);
			redirect("school/view/{$school_data->id}/{$school_slug}");
		} else {
			redirect(schools_url());
		}
	}

}
