<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends SCHOOLS_Controller {
	
	public function __construct() {
		parent::__construct(); 
		$this->must_admin();

		$this->load->model('schools/Schools_model');
		$this->load->model('locations/Regions_model');
		$this->load->model('locations/Provinces_model');
		$this->load->model('schools/Divisions_model');
		$this->load->model('locations/Municipals_model');
		$this->load->model('schools/Districts_model');
		$this->load->model('schools/Legislatives_model');
		$this->load->model('schools/School_types_model');
		$this->load->model('schools/General_classes_model');
		$this->load->model('schools/Sub_classes_model');
		$this->load->model('schools/Curriculars_model');
		$this->load->model('schools/Class_orgs_model');
		$this->load->model('schools/Income_classes_model');
		$this->load->model('schools/City_classes_model');
		$this->load->model('schools/Urps_model');
		$this->load->model('schools/K12_programs_model');
		$this->load->model('schools/Schools_programs_model');
		$this->load->model('schools/Tvl_specializations_model');
		$this->load->model('schools/Schools_tvl_model');
		$this->load->model('schools/Schools_slug_model');
	}

	private function _get_region($name) {
		$item = new $this->Regions_model(NULL, 'locations');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _get_province($name) {
		$item = new $this->Provinces_model(NULL, 'locations');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}


	private function _get_division($name) {
		$item = new $this->Divisions_model(NULL, 'schools');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _get_municipal($name) {
		$item = new $this->Municipals_model(NULL, 'locations');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _get_district($name) {
		$item = new $this->Districts_model(NULL, 'schools');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _get_legislative($name) {
		$item = new $this->Legislatives_model(NULL, 'schools');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _get_school_type($name) {
		$item = new $this->School_types_model(NULL, 'schools');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _get_general_class($name) {
		$item = new $this->General_classes_model(NULL, 'schools');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _get_sub_class($name) {
		$item = new $this->Sub_classes_model(NULL, 'schools');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _get_cur_class($name) {
		$item = new $this->Curriculars_model(NULL, 'schools');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _get_class_org($name) {
		$item = new $this->Class_orgs_model(NULL, 'schools');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _get_income_class($name) {
		$item = new $this->Income_classes_model(NULL, 'schools');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _get_city_class($name) {
		$item = new $this->City_classes_model(NULL, 'schools');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _get_urp($name) {
		$item = new $this->Urps_model(NULL, 'schools');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _insert_specializations($items,$school_id) {
		if( $school_id ) {
			$items = explode('</tr><tr>', $items);
			if( isset($items[1]) ) {
				$items = explode('<br><br>', $items[1]);
				foreach($items as $item) {
					$item = rtrim(trim(strip_tags($item)),";");
					$sId = $this->_get_specialization($item);
					$sp = new $this->Schools_tvl_model(NULL, 'schools');
					$sp->setSchoolId($school_id,true);
					$sp->setTvlId($sId,true);
					if(!$sp->nonEmpty()) {
						$sp->insert();
					}
				}
			}
		}
	}

	private function _get_specialization($name) {
		$item = new $this->Tvl_specializations_model(NULL, 'schools');
		$item->cache_on();
		$item->setName(trim($name),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _insert_programs($items,$school_id) {
		if( $school_id ) {
			$abbrs = explode(",", $items);
			foreach($abbrs as $abbr) {
				$pId = $this->_get_k12_programs($abbr);
				$sp = new $this->Schools_programs_model(NULL, 'schools');
				$sp->setSchoolId($school_id,true);
				$sp->setProgramId($pId,true);
				if(!$sp->nonEmpty()) {
					$sp->insert();
				}
			}
		}
	}

	private function _get_k12_programs($abbr) {
		$item = new $this->K12_programs_model(NULL, 'schools');
		$item->cache_on();
		$item->setAbbr(trim($abbr),true);
		if( $item->nonEmpty() ) {
			$results = $item->get_results();
			$item->db_close();
			return $results->id;
		} else {
			$item->insert();
			$item->db_close();
			return $item->get_inserted_id();
		}
	}

	private function _remove_quote($text) {
		$text = str_replace('"', '', $text);
		$text = addslashes($text);
		return $text;
	}

	private function pRegion($name) {
		$regions = array( 
					'Region I' => 1,
					'Region II' => 2,
					'Region III' => 3,
					'Region IV-A' => 4,
					'Region IV-B' => 5,
					'Region V' => 6,
					'Region VI' => 7,
					'Region VII' => 8,
					'Region VIII' => 9,
					'Region IX' => 10,
					'Region X' => 11,
					'Region XI' => 12,
					'Region XII' => 13,
					'CARAGA' => 14,
					'ARMM' => 15,
					'CAR' => 16,
					'NCR' => 17,
				 );
		return (isset($regions[$name])) ? $regions[$name] : 0;
	}
	public function add_multiple() {

		if( $this->input->post('schools') ) {
			$schools = explode('<div class="view-content">', $this->input->post('schools'));
			$schools = explode('<tbody>', $schools[1]);
			if( isset( $schools[1] ) ) {
				
				$schools = explode("</tbody>", $schools[1]);
				$schools = explode("<tr class=\"row", $schools[0]);
				foreach($schools as $school) {
					$items = explode(" >", $school);
					if( count($items) == 9) {
						$school_id = trim(strip_tags($items[4]));
						if( $school_id ) {
							$new_school = new $this->Schools_model(NULL, 'schools1');
							$new_school->setId($school_id,true);
							$programs = array();
							if( $new_school->nonEmpty() ) {
								$results = $new_school->get_results();
								$current_id = $results->id;
								$school_name = trim(strip_tags($items[5]));
								$new_school->setName($school_name);
								$municipality = ucwords(strtolower(trim(strip_tags($items[3]))));
								$new_school->setMunicipality($this->_get_municipal($municipality));
								$region = trim(strip_tags($items[1]));
								$new_school->setRegion($this->pRegion($region));
								$provinces = explode("<li class=\"odd last\">", $items[2]);
								$province = (isset($provinces[1])) ? trim(strip_tags($provinces[1])) : '';
								$new_school->setProvince($this->_get_province($province));
								$programs = trim(strip_tags($items[6]));
								$new_school->setCategory(2);
								$new_school->update();
								$this->_insert_programs($programs,$current_id);
								$this->_insert_specializations($items[8],$current_id);
							} else {
								$school_name = trim(strip_tags($items[5]));
								$new_school->setName($school_name);
								$municipality = ucwords(strtolower(trim(strip_tags($items[3]))));
								$new_school->setMunicipality($this->_get_municipal($municipality));
								$region = trim(strip_tags($items[1]));
								$new_school->setRegion($this->pRegion($region));
								$provinces = explode("<li class=\"odd last\">", $items[2]);
								$province = (isset($provinces[1])) ? trim(strip_tags($provinces[1])) : '';
								$new_school->setProvince($this->_get_province($province));
								$new_school->setCategory(2);
								$new_school->insert();
								$current_id = $new_school->get_inserted_id();
								$programs = trim(strip_tags($items[6]));
								$this->_insert_programs($programs,$current_id);
								$this->_insert_specializations($items[8],$current_id);
							}
						}
					}
				}
			}
		}
		$this->load->view('directory/schools/manage/add_multiple', $this->template_data->get_data());
	}

	public function add_multiple1() {

		if( $this->input->post('schools') ) {
			$schools = explode("\n", $this->input->post('schools'));
			foreach($schools as $school) {
				$line_item = explode("\t", $school);
				if( count($line_item) > 1 ) {
					
					$new_school = new $this->Schools_model(NULL, 'schools1');
					$new_school->setId($line_item[0],true);

					if( $new_school->nonEmpty() ) {
						$new_school->setName($this->_remove_quote($line_item[1]));
						$new_school->setRegion($this->_get_region($this->_remove_quote($line_item[2] . " - " . $line_item[3])));
						$new_school->setProvince($this->_get_province($line_item[4]));
						$new_school->setDivision($this->_get_division($line_item[6]));
						$new_school->setMunicipality($this->_get_municipal($line_item[7]));
						$new_school->setDistrict($this->_get_district($line_item[8]));
						$new_school->setLegislative($this->_get_legislative($line_item[9]));
						$new_school->setSchoolType($this->_get_school_type($line_item[10]));
						$new_school->setAbbr($line_item[11]);
						$new_school->setPrevName($this->_remove_quote($line_item[12]));
						$new_school->setMotherId($this->_remove_quote($line_item[13]));
						$new_school->setAddress($this->_remove_quote($line_item[14]));
						$new_school->setSchoolHead($this->_remove_quote($line_item[15]));
						$new_school->setDesignation($this->_remove_quote($line_item[16]));
						$new_school->setTelephone($this->_remove_quote($line_item[17]));
						$new_school->setMobile($this->_remove_quote($line_item[18]));
						$new_school->setEmail($this->_remove_quote($line_item[20]));
						$new_school->setDateEstablished( date('Y-m-d', strtotime($line_item[21])) );
						$new_school->setGeneralClass($this->_get_general_class($line_item[22]));
						$new_school->setSubClass($this->_get_sub_class($line_item[23]));
						$new_school->setCurClass($this->_get_cur_class($line_item[24]));
						$new_school->setClassOrg($this->_get_class_org($line_item[25]));
						$new_school->setIncomeClass($this->_get_income_class($line_item[26]));
						$new_school->setCityClass($this->_get_city_class($line_item[27]));
						$new_school->setUrp($this->_get_urp($line_item[28]));
						$new_school->update();
					} else {
						$new_school->setName($this->_remove_quote($line_item[1]));
						$new_school->setRegion($this->_get_region($this->_remove_quote($line_item[2] . " - " . $line_item[3])));
						$new_school->setProvince($this->_get_province($line_item[4]));
						$new_school->setDivision($this->_get_division($line_item[6]));
						$new_school->setMunicipality($this->_get_municipal($line_item[7]));
						$new_school->setDistrict($this->_get_district($line_item[8]));
						$new_school->setLegislative($this->_get_legislative($line_item[9]));
						$new_school->setSchoolType($this->_get_school_type($line_item[10]));
						$new_school->setAbbr($line_item[11]);
						$new_school->setPrevName($this->_remove_quote($line_item[12]));
						$new_school->setMotherId($this->_remove_quote($line_item[13]));
						$new_school->setAddress($this->_remove_quote($line_item[14]));
						$new_school->setSchoolHead($this->_remove_quote($line_item[15]));
						$new_school->setDesignation($this->_remove_quote($line_item[16]));
						$new_school->setTelephone($this->_remove_quote($line_item[17]));
						$new_school->setMobile($this->_remove_quote($line_item[18]));
						$new_school->setEmail($this->_remove_quote($line_item[20]));
						$new_school->setDateEstablished( date('Y-m-d', strtotime($line_item[21])) );
						$new_school->setGeneralClass($this->_get_general_class($line_item[22]));
						$new_school->setSubClass($this->_get_sub_class($line_item[23]));
						$new_school->setCurClass($this->_get_cur_class($line_item[24]));
						$new_school->setClassOrg($this->_get_class_org($line_item[25]));
						$new_school->setIncomeClass($this->_get_income_class($line_item[26]));
						$new_school->setCityClass($this->_get_city_class($line_item[27]));
						$new_school->setUrp($this->_get_urp($line_item[28]));
						$new_school->insert();
					}
				}
			}
		}
		$this->load->view('directory/schools/manage/add_multiple', $this->template_data->get_data());
	}

	public function add_multiple3() {

		if( $this->input->post('schools') ) {
			$schools = explode("\n", $this->input->post('schools'));
			foreach($schools as $school) {
				$line_item = explode("\t", $school);
				if( count($line_item) > 1 ) {
					
					$new_school = new $this->Schools_model(NULL, 'schools1');
					$new_school->setId($line_item[0],true);

					if( $new_school->nonEmpty() ) {
						$new_school->setName($this->_remove_quote($line_item[1]));
						$new_school->setRegion($this->_get_region($this->_remove_quote($line_item[2] . " - " . $line_item[3])));
						$new_school->setProvince($this->_get_province($line_item[4]));
						$new_school->setDivision($this->_get_division($line_item[6]));
						$new_school->setMunicipality($this->_get_municipal($line_item[7]));
						$new_school->setDistrict($this->_get_district($line_item[8]));
						$new_school->setLegislative($this->_get_legislative($line_item[9]));
						$new_school->setSchoolType($this->_get_school_type($line_item[10]));
						$new_school->setAbbr($line_item[11]);
						$new_school->setPrevName($this->_remove_quote($line_item[12]));
						$new_school->setMotherId($this->_remove_quote($line_item[13]));
						$new_school->setAddress($this->_remove_quote($line_item[14]));
						$new_school->setSchoolHead($this->_remove_quote($line_item[15]));
						$new_school->setDesignation($this->_remove_quote($line_item[16]));
						$new_school->setTelephone($this->_remove_quote($line_item[17]));
						$new_school->setMobile($this->_remove_quote($line_item[18]));
						$new_school->setEmail($this->_remove_quote($line_item[20]));
						$new_school->setDateEstablished( date('Y-m-d', strtotime($line_item[21])) );
						$new_school->setGeneralClass($this->_get_general_class($line_item[22]));
						$new_school->setSubClass($this->_get_sub_class($line_item[23]));
						$new_school->setCurClass($this->_get_cur_class($line_item[24]));
						$new_school->setClassOrg($this->_get_class_org($line_item[25]));
						$new_school->setIncomeClass($this->_get_income_class($line_item[26]));
						$new_school->setCityClass($this->_get_city_class($line_item[27]));
						$new_school->setUrp($this->_get_urp($line_item[28]));
						$new_school->setCategory(3);
						//print_r($new_school); exit;
						$new_school->update();
					} else {
						$new_school->setName($this->_remove_quote($line_item[1]));
						$new_school->setRegion($this->_get_region($this->_remove_quote($line_item[2] . " - " . $line_item[3])));
						$new_school->setProvince($this->_get_province($line_item[4]));
						$new_school->setDivision($this->_get_division($line_item[6]));
						$new_school->setMunicipality($this->_get_municipal($line_item[7]));
						$new_school->setDistrict($this->_get_district($line_item[8]));
						$new_school->setLegislative($this->_get_legislative($line_item[9]));
						$new_school->setSchoolType($this->_get_school_type($line_item[10]));
						$new_school->setAbbr($line_item[11]);
						$new_school->setPrevName($this->_remove_quote($line_item[12]));
						$new_school->setMotherId($this->_remove_quote($line_item[13]));
						$new_school->setAddress($this->_remove_quote($line_item[14]));
						$new_school->setSchoolHead($this->_remove_quote($line_item[15]));
						$new_school->setDesignation($this->_remove_quote($line_item[16]));
						$new_school->setTelephone($this->_remove_quote($line_item[17]));
						$new_school->setMobile($this->_remove_quote($line_item[18]));
						$new_school->setEmail($this->_remove_quote($line_item[20]));
						$new_school->setDateEstablished( date('Y-m-d', strtotime($line_item[21])) );
						$new_school->setGeneralClass($this->_get_general_class($line_item[22]));
						$new_school->setSubClass($this->_get_sub_class($line_item[23]));
						$new_school->setCurClass($this->_get_cur_class($line_item[24]));
						$new_school->setClassOrg($this->_get_class_org($line_item[25]));
						$new_school->setIncomeClass($this->_get_income_class($line_item[26]));
						$new_school->setCityClass($this->_get_city_class($line_item[27]));
						$new_school->setUrp($this->_get_urp($line_item[28]));
						$new_school->setCategory(3);
						//print_r($new_school); exit;
						$new_school->insert();
					}
				}
			}
		}
		$this->load->view('directory/schools/manage/add_multiple', $this->template_data->get_data());
	}

	public function fix_regions_provinces() {

		$regions = new $this->Regions_model(NULL, 'schools');
		$regions->set_limit(0);
		$regions->set_order('name', 'ASC');
		foreach($regions->populate() as $region) {
			$schools = new $this->Schools_model(NULL, 'schools');
			$schools->setRegion($region->id,true);
			$schools->set_select("region,province");
			$schools->set_group_by("province");
			$schools->set_limit(0);
			foreach($schools->populate() as $school) {
				$rp = new $this->Provinces_model(NULL,'schools');
				$rp->setRegion($school->region,false,true);
				$rp->setId($school->province,true,false);
				if($rp->nonEmpty()) {
					$rp->update();
				}
			}
		}

	}

	public function fix_provinces_municipals() {

		$this->load->model('schools/Provinces_municipals_model');
		$provinces = new $this->Provinces_model(NULL, 'schools');
		$provinces->set_limit(0);
		$provinces->set_order('name', 'ASC');
		foreach($provinces->populate() as $province) {
			//print_r($province);exit;
			$schools = new $this->Schools_model(NULL, 'schools');
			$schools->setProvince($province->id,true);
			$schools->set_select("province,municipality");
			$schools->set_group_by("municipality");
			$schools->set_limit(0);
			foreach($schools->populate() as $school) {
				//print_r($school);exit;
				$pm = new $this->Provinces_municipals_model(NULL,'schools');
				$pm->setProvinceId($school->province,true);
				$pm->setMunicipalId($school->municipality,true);
				if(!$pm->nonEmpty()) {
					$pm->insert();
				}
			}
		}

	}

	public function ucwords_municipals() {

		$municipals = new $this->Municipals_model(NULL, 'schools');
		$municipals->set_limit(0);
		$municipals->set_order('id', 'ASC');
		foreach($municipals->populate() as $municipal) {
			$municipals2 = new $this->Municipals_model(NULL, 'schools');
			$municipals2->setName(ucwords(strtolower($municipal->name)),false,true);
			$municipals2->setId($municipal->id,true,false);
			$municipals2->update();
		}

	}

	public function ucwords_provinces() {

		$items = new $this->Provinces_model(NULL, 'schools');
		$items->set_limit(0);
		$items->set_order('id', 'ASC');
		foreach($items->populate() as $item) {
			$items2 = new $this->Provinces_model(NULL, 'schools');
			$items2->setName(ucwords(strtolower($item->name)),false,true);
			$items2->setId($item->id,true,false);
			$items2->update();
		}

	}

	public function fix_schools_municipality() {

		$items = new $this->Schools_model('s', 'schools');
		$items->set_limit(0);
		$items->set_order('s.id', 'ASC');
		$items->set_join('municipals r', 'r.id=s.municipality');
		$items->set_select('s.*');
		$items->set_select('r.id2 as r_id2');

		foreach($items->populate() as $item) {
			//print_r( $item );
			if( $item->r_id2 ) {
				echo "UPDATE `schools` SET `municipality2` = {$item->r_id2} WHERE `id` = {$item->id};\n";
			}
		}

	}


	public function fix_schools_division() {

		$items = new $this->Schools_model('s', 'schools1');
		$items->set_limit(0);
		$items->set_order('s.id', 'ASC');
		$items->set_select('s.*');
		$items->set_select("(SELECT d1.id FROM trk_schools.divisions d1 JOIN test.divisions d2 ON d1.name LIKE d2.name WHERE s.division=d2.id) as div_id");

		foreach($items->populate() as $item) {
			//print_r( $item );
			if( $item->div_id ) {
				echo "UPDATE `schools` SET `division` = {$item->div_id} WHERE `id` = {$item->id};\n";
			}
		}

	}

	public function fix_schools_legislative() {

		$items = new $this->Schools_model('s', 'schools1');
		$items->set_limit(0);
		$items->set_order('s.id', 'ASC');
		$items->set_select('s.*');
		$items->set_select("(SELECT d1.id FROM trk_schools.legislatives d1 JOIN test.legislatives d2 ON d1.name LIKE d2.name WHERE s.division=d2.id) as div_id");

		foreach($items->populate() as $item) {
			//print_r( $item );
			if( $item->div_id ) {
				echo "UPDATE `schools` SET `district` = {$item->div_id} WHERE `id` = {$item->id};\n";
			}
		}

	}

	public function fix_schools_region() {

		$items = new $this->Schools_model('s', 'schools1');
		$items->set_limit(0);
		$items->set_order('s.id', 'ASC');
		$items->set_select('s.*');
		$items->set_select("(SELECT d1.id FROM trk_schools.legislatives d1 JOIN test.legislatives d2 ON d1.name LIKE d2.name WHERE s.division=d2.id) as div_id");

		foreach($items->populate() as $item) {
			//print_r( $item );
			if( $item->r_id2 ) {
				echo "UPDATE `schools` SET `region2` = {$item->r_id2} WHERE `id` = {$item->id};\n";
			}
		}

	}

	public function fix_schools_province() {

		$items = new $this->Schools_model('s', 'schools');
		$items->set_limit(0);
		$items->set_order('s.id', 'ASC');
		$items->set_join('provinces r', 'r.id=s.province');
		$items->set_select('s.*');
		$items->set_select('r.id2 as r_id2');

		foreach($items->populate() as $item) {
			//print_r( $item );
			if( $item->r_id2 ) {
				echo "UPDATE `schools` SET `province2` = {$item->r_id2} WHERE `id` = {$item->id};\n";
			}
		}

	}

	public function fix_regions() {

		$items = new $this->Regions_model('s', 'schools');
		$items->set_limit(0);
		$items->set_select("*");
		$items->set_select("(SELECT p.name as p_name FROM provinces p WHERE p.name LIKE s.name) as p_name");
		$items->set_select("(SELECT p.id as p_name FROM provinces p WHERE p.name LIKE s.name) as p_id");

		foreach($items->populate() as $item) {
			//print_r($item);
			if( $item->p_id ) {
				echo "UPDATE `provinces` SET `id2` = {$item->p_id} WHERE `id` = {$item->id};\n";
			}
		}

	}

	public function fix_provinces() {

		$items = new $this->Provinces_model('s', 'schools');
		$items->set_limit(0);
		$items->set_select("*");
		$items->set_select("(SELECT p.name as p_name FROM trk_locations.provinces p WHERE p.name LIKE s.name) as p_name");
		$items->set_select("(SELECT p.id as p_name FROM trk_locations.provinces p WHERE p.name LIKE s.name) as p_id");

		foreach($items->populate() as $item) {
			//print_r($item);
			if( $item->p_id ) {
				echo "UPDATE `provinces` SET `id2` = {$item->p_id} WHERE `id` = {$item->id}; \n";
			}
		}

	}

	public function fix_municipals() {

		$items = new $this->Schools_model('s', 'schools');
		$items->set_limit(0);
		$items->set_select("*");
		$items->set_group_by('s.municipality');

		foreach($items->populate() as $item) {
			//print_r($item);
				echo "UPDATE `municipals` SET `region` = {$item->region}, `province` = {$item->province} WHERE `id` = {$item->municipality};\n";
		}

	}

	public function fix_school_names() {

		$items = new $this->Schools_model(NULL, 'schools');
		$items->set_limit(0);
		$items->set_order('id', 'ASC');
		$items->set_where("name LIKE '% HS'");

		foreach($items->populate() as $item) {
			$new_name = str_replace(" HS", " High School", $item->name);
			echo "UPDATE `schools` SET `name` = '{$new_name}' WHERE `id` = {$item->id};\n";
		}

	}

	public function fix_school_slugs($limit=1000) {

		$items = new $this->Schools_model('s', 'schools');
		$items->set_limit($limit);
		$items->set_start($start);
		$items->set_order('s.id', 'ASC');
		$items->set_join("{$this->db_configs['locations']['database']}.municipals m", 's.municipality=m.id');
		$items->set_select("m.name as municipal_name");
		$items->set_select("s.*");
		$items->set_where("((SELECT school_id FROM {$this->db_configs['schools']['database']}.schools_slug ss WHERE ss.school_id=s.id) IS NULL)");

		$slugs_arr = array();
		$c = 1;
		header('Content-type: text/plain');
		foreach($items->populate() as $item) {
			$slug = slugify( $item->name );
			$school_id = new $this->Schools_slug_model('ss', 'schools');
			$school_id->setSchoolId($item->id,true);

			if( !$school_id->nonEmpty() ) {
				$school_slug = new $this->Schools_slug_model('ss', 'schools');
				$school_slug->setSlug($slug,true);
				$slug_exists = $school_slug->nonEmpty();
				$n = 0;
				while($slug_exists) {
					$n++;
					$slug = slugify( $item->name . $n );
					$slug_in_array = in_array($slug, $slugs_arr);
					while($slug_in_array) {
						$n++;
						$slug = slugify( $item->name . $n );
						$slug_in_array = in_array($slug, $slugs_arr);
					}
					$school_slug2 = new $this->Schools_slug_model('ss', 'schools');
					$school_slug2->setSlug($slug,true);
					$slug_exists = $school_slug2->nonEmpty();
				}
				$slugs_arr[] = $slug;
				$school_id->setSlug($slug);
				if( $school_id->insert() ) {
					echo "{$c} INSERTED: {$item->id} - {$slug}\n";
					$c++;
				}
				//echo "INSERT INTO `schools_slug` (`school_id`, `slug`) VALUES ('{$item->id}', '{$slug}');\n";
			}
		}

	}

}
