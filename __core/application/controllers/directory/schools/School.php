<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School extends SCHOOLS_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->model('locations/Regions_model');
		$this->load->model('locations/Provinces_model');
		$this->load->model('locations/Municipals_model');

		$this->load->model('schools/Schools_model');
		$this->load->model('schools/Schools_logo_model');
		$this->load->model('schools/Schools_meta_model');
		$this->load->model('schools/School_categories_model');

		$this->load->model('realestate/Properties_model');

	}

	public function view($id,$slug) {
		
		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		$schools->setId($id,true);
		$schools->set_select("s.*");

		if( $schools->nonEmpty() ) {
			$school_data = $schools->get_results();
			$school_slug = url_title($school_data->name,"-",true);
			if( $school_slug == urldecode($slug)) {

				$schools->set_join("{$this->db_configs['locations']['database']}.regions r", 's.region=r.id');
				$schools->set_select("r.name as region_name");

				$schools->set_join("{$this->db_configs['locations']['database']}.provinces p", 's.province=p.id');
				$schools->set_select("p.name as province_name");

				$schools->set_join("{$this->db_configs['locations']['database']}.municipals m", 's.municipality=m.id');
				$schools->set_select("m.name as municipal_name");

				$schools->set_join("school_categories sc", 's.category=sc.id');
				$schools->set_select("sc.name as category_name");

				$schools->set_join("schools_logo sl", 'sl.school_id=s.id');
				$schools->set_select("sl.url as logo_url");

				$schools->set_join("schools_slug ss", 'ss.school_id=s.id');
				$schools->set_select("ss.slug as school_slug");

				$school_data = $schools->get(); 
				$this->template_data->set('current_school', $school_data);
				$this->template_data->set('page_title', $school_data->name . " | Schools Directory | Trokis Philippines");
	        	$this->template_data->set('meta_description', "{$school_data->name} is a {$school_data->category_name} located at {$school_data->municipal_name}, {$school_data->province_name}, {$school_data->region_name}.");
	        	$this->template_data->set('meta_keywords', "{$school_data->category_name}, {$school_data->municipal_name}, {$school_data->province_name}, {$school_data->region_name}");

	        	$this->template_data->set('current_school', $school_data);

				$municipal_schools = new $this->Schools_model('s', 'schools');
				$municipal_schools->cache_on();
				$municipal_schools->setMunicipality($school_data->municipality,true);
				$municipal_schools->set_where('id !=', $school_data->id);
				$municipal_schools->set_limit(5);
				$municipal_schools->set_order('RAND()');
				$this->template_data->set('municipal_schools', $municipal_schools->populate());

				$provincial_schools = new $this->Schools_model('s', 'schools');
				$provincial_schools->cache_on();
				$provincial_schools->setProvince($school_data->province,true);
				$provincial_schools->set_where('id !=', $school_data->id);
				$provincial_schools->set_limit(5);
				$provincial_schools->set_order('RAND()');
				$this->template_data->set('provincial_schools', $provincial_schools->populate());

				$regional_schools = new $this->Schools_model('s', 'schools');
				$regional_schools->cache_on();
				$regional_schools->setRegion($school_data->region,true);
				$regional_schools->set_where('id !=', $school_data->id);
				$regional_schools->set_limit(5);
				$regional_schools->set_order('RAND()');
				$this->template_data->set('regional_schools', $regional_schools->populate());

				$metas = new $this->Schools_meta_model('m', 'schools');
				$metas->cache_on();
				$metas->setSchoolId($id,true);
				$this->template_data->set('school_metas', $metas->populate() );

				$this->template_data->opengraph(array(
					'fb:app_id'=>$this->config->item('fbAppId'),
					'og:image' => ($school_data->logo_url) ? $school_data->logo_url : $this->config->item('trokis_logo_url'),
					'og:description' => $this->template_data->get('meta_description'),
					'og:site_name' => 'Trokis Philippines',
					'og:title' => $school_data->name,
				));

				$this->template_data->set('amphtml_url', schools_url("school/amp/{$school_data->id}/{$school_slug}") );

				$properties = new $this->Properties_model('p', 'realestate');
				$properties->setActive('1',true);
				$properties->set_order('RAND()');
				$properties->set_limit(5);
				$properties->set_select('p.*');
				$properties->set_select('(SELECT pm.media_value FROM properties_media pm WHERE pm.media_type="picture" AND pm.media_key="thumbnail" AND pm.property_id=p.id LIMIT 1) as logo_url');
				$this->template_data->set('properties', $properties->populate());

				$this->load->view('directory/schools/schools_view', $this->template_data->get_data());	
			} else {
				$this->load->view('homepage/page_not_found', $this->template_data->get_data());
			}
		} else {
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}
	}

	public function amp($id,$slug) {
		
		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		$schools->setId($id,true);
		$schools->set_select("s.*");

		if( $schools->nonEmpty() ) {
			$school_data = $schools->get_results();
			$school_slug = url_title($school_data->name,"-",true);
			if( $school_slug == urldecode($slug)) {

				$schools->set_join("{$this->db_configs['locations']['database']}.regions r", 's.region=r.id');
				$schools->set_select("r.name as region_name");

				$schools->set_join("{$this->db_configs['locations']['database']}.provinces p", 's.province=p.id');
				$schools->set_select("p.name as province_name");

				$schools->set_join("{$this->db_configs['locations']['database']}.municipals m", 's.municipality=m.id');
				$schools->set_select("m.name as municipal_name");

				$schools->set_join("school_categories sc", 's.category=sc.id');
				$schools->set_select("sc.name as category_name");

				$schools->set_join("schools_logo sl", 'sl.school_id=s.id');
				$schools->set_select("sl.url as logo_url");

				$school_data = $schools->get(); 
				$this->template_data->set('current_school', $school_data);
				$this->template_data->set('page_title', $school_data->name . " | Schools Directory | Trokis Philippines");
	        	$this->template_data->set('meta_description', "{$school_data->name} is a {$school_data->category_name} located at {$school_data->municipal_name}, {$school_data->province_name}, {$school_data->region_name}.");
	        	$this->template_data->set('meta_keywords', "{$school_data->category_name}, {$school_data->municipal_name}, {$school_data->province_name}, {$school_data->region_name}");

	        	$this->template_data->set('current_school', $school_data);

				$municipal_schools = new $this->Schools_model('s', 'schools');
				$municipal_schools->cache_on();
				$municipal_schools->setMunicipality($school_data->municipality,true);
				$municipal_schools->set_where('id !=', $school_data->id);
				$municipal_schools->set_limit(5);
				$municipal_schools->set_order('RAND()');
				$this->template_data->set('municipal_schools', $municipal_schools->populate());

				$provincial_schools = new $this->Schools_model('s', 'schools');
				$provincial_schools->cache_on();
				$provincial_schools->setProvince($school_data->province,true);
				$provincial_schools->set_where('id !=', $school_data->id);
				$provincial_schools->set_limit(5);
				$provincial_schools->set_order('RAND()');
				$this->template_data->set('provincial_schools', $provincial_schools->populate());

				$regional_schools = new $this->Schools_model('s', 'schools');
				$regional_schools->cache_on();
				$regional_schools->setRegion($school_data->region,true);
				$regional_schools->set_where('id !=', $school_data->id);
				$regional_schools->set_limit(5);
				$regional_schools->set_order('RAND()');
				$this->template_data->set('regional_schools', $regional_schools->populate());

				$metas = new $this->Schools_meta_model('m', 'schools');
				$metas->cache_on();
				$metas->setSchoolId($id,true);
				$this->template_data->set('school_metas', $metas->populate() );

				$this->template_data->opengraph(array(
					'fb:app_id'=>$this->config->item('fbAppId'),
					'og:image' => ($school_data->logo_url) ? $school_data->logo_url : $this->config->item('trokis_logo_url'),
					'og:description' => $this->template_data->get('meta_description'),
					'og:site_name' => 'Trokis Philippines',
					'og:title' => $school_data->name,
				));

				$this->template_data->set('amphtml_url', schools_url("school/amp/{$school_data->id}/{$school_slug}") );

				$this->load->view('directory/schools/schools_amp', $this->template_data->get_data());	
			} else {
				$this->load->view('homepage/page_not_found', $this->template_data->get_data());
			}
		} else {
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}

	}

	public function redirect($key, $id, $slug) {

		$this->template_data->set('school_id', $id);
		$this->template_data->set('meta_key', $key);

		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		$schools->setId($id,true);
		$schools->set_select("s.id,s.name");
		$schools->set_select("(SELECT sm.meta_value FROM schools_meta sm WHERE sm.school_id=s.id AND sm.meta_key='{$key}' AND sm.active=1) as social_url");

		if( $schools->nonEmpty() ) {
			$school_data = $schools->get();
			$this->template_data->set('current_school', $school_data);

			$this->template_data->set('page_title', "Redirect to " . $school_data->name . "'s {$key} URL | Schools Directory | Trokis Philippines");
	        $this->template_data->set('meta_description', "Redirect to " . $school_data->name . "'s {$key} URL");
	        $this->template_data->set('meta_keywords', "{$key} url, {$key} redirect, {$key} link, {$key} external, {$key} social media, {$key} social");

		$municipal_schools = new $this->Schools_model('s', 'schools');
		$municipal_schools->cache_on();
		$municipal_schools->set_limit(12);
		$municipal_schools->set_order('RAND()');
		$municipal_schools->set_select('s.*');
		$municipal_schools->set_select('sc.name as category_name');
		$municipal_schools->set_select('sl.url as logo_url');
		$municipal_schools->set_join('school_categories sc', 'sc.id=s.category');
		$municipal_schools->set_join('schools_logo sl', 'sl.school_id=s.id');
		if( get_cookie('current_location') ) {
			$municipal_schools->setMunicipality( get_cookie('current_location'), true );
		}
		$this->template_data->set('municipal_schools', $municipal_schools->populate());

			$this->load->view('directory/schools/schools_redirect', $this->template_data->get_data());
		} else {
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}
	}

}
