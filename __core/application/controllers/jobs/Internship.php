<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Internship extends JOBS_Controller {
	
		public function __construct() {
		parent::__construct();

		$this->load->model('locations/Municipals_model');

	}

	public function index() {

		$municipals = new $this->Municipals_model(NULL, 'locations');
		$municipals->cache_on();
		$municipals->set_limit(0);
		$municipals->set_order('name', 'ASC');
		$municipals->set_select("*");
		$this->template_data->set('municipals', $municipals->populate());

		$this->load->view('jobs/internship', $this->template_data->get_data());
	}

}
