<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regions extends ADMIN_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->model('locations/Regions_model');

	}

	public function index($start=0) {

		$regions = new $this->Regions_model(NULL, 'locations');
		$regions->set_limit(10);
		$regions->set_order('name', 'ASC');
		$regions->set_select("*");
		$regions->set_start( $start );

		if( $this->input->get('q') ) {
			if( $start ) {
				redirect( site_url('regions/index') . "?q=" . $this->input->get('q') );
			}
			$regions->set_where( "name LIKE '%{$this->input->get('q')}%'" );
		}

		$this->template_data->set('regions', $regions->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item('index_page') . "/regions/index"),
			'total_rows' => $regions->count_all_results(),
			'per_page' => $regions->get_limit()
		)));

		$this->load->view('main/regions/regions', $this->template_data->get_data());
	}

	public function edit($id) {

		$region = new $this->Regions_model(NULL, 'locations');
		$region->setId($id,true);

		if( $this->input->post('region_name') ) {
			$region->setName($this->input->post('region_name', true),false,true);
			$region->update();
		}
		$this->template_data->set('region', $region->get());

		$this->load->view('main/regions/region_edit', $this->template_data->get_data());
	}

}
