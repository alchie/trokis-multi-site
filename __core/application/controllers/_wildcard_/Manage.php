<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends SCHOOLS_Controller {

	public function __construct() {
		parent::__construct();
		$this->must_admin();
	}

	public function clear_cache()
	{
		$uri_string = ($this->input->get('uri') && ($this->input->get('uri') != '')) ? explode("/",$this->input->get('uri')) : array('default', 'index');
		$this->db->cache_delete($uri_string[0], $uri_string[1]); 
		redirect( site_url($this->input->get('uri')) . "?src=clear_cache" );
	}

	public function clear_all_cache()
	{
		$this->db->cache_delete_all(); 
		redirect( site_url($this->input->get('uri')) . "?src=clear_all_cache" );
	}
}
