<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinces extends ADMIN_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->model('locations/Provinces_model');
		$this->load->model('locations/Regions_model');

	}

	public function index($start=0) {

		$provinces = new $this->Provinces_model(NULL, 'locations');
		$provinces->set_limit(10);
		$provinces->set_order('name', 'ASC');
		$provinces->set_select("*");
		$provinces->set_select('(SELECT name FROM regions WHERE provinces.region=regions.id) as region_name');
		$provinces->set_start( $start );

		if( $this->input->get('q') ) {
			if( $start ) {
				redirect( site_url('provinces/index') . "?q=" . $this->input->get('q') );
			}
			$provinces->set_where( "name LIKE '%{$this->input->get('q')}%'" );
		}

		$this->template_data->set('provinces', $provinces->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item('index_page') . "/provinces/index"),
			'total_rows' => $provinces->count_all_results(),
			'per_page' => $provinces->get_limit()
		)));

		$this->load->view('main/provinces/provinces', $this->template_data->get_data());
	}

	public function edit($id) {

		$regions = new $this->Regions_model(NULL, 'locations');
		$regions->cache_on();
		$regions->set_limit(0);
		$regions->set_order('name', 'ASC');
		$this->template_data->set('regions', $regions->populate());

		$province = new $this->Provinces_model(NULL, 'locations');
		$province->setId($id,true);

		if( $this->input->post('province_name') ) {
			$province->setName($this->input->post('province_name', true),false,true);
			if( $this->input->post('region_id') ) {
				$province->setRegion($this->input->post('region_id', true),false,true);
			}
			$province->update();
		} 

		$this->template_data->set('province', $province->get());

		$this->load->view('main/provinces/province_edit', $this->template_data->get_data());
	}

}
