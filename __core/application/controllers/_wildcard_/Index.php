<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('homepage/homepage', $this->template_data->get_data());
	}

	public function directory() {
		$this->load->view('homepage/homepage', $this->template_data->get_data());
	}



}
