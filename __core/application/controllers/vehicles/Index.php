<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends VEHICLES_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->view('homepage/coming_soon', $this->template_data->get_data());
	}



}
