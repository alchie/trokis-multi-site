<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends ADMIN_Controller {

	public function __construct() {
		parent::__construct();
		$this->must_admin();

		$this->load->model('locations/Municipals_model');
		$this->load->model('locations/Provinces_model');
		$this->load->model('locations/Regions_model');

		 $this->template_data->set('page_title', "ADMINISTRATION : Main");
	}

	public function clear_cache()
	{
		$uri_string = ($this->input->get('uri') && ($this->input->get('uri') != '')) ? explode("/",$this->input->get('uri')) : array('default', 'index');
		$this->db->cache_delete($uri_string[0], $uri_string[1]); 
		redirect( site_url($this->input->get('uri')) . "?src=clear_cache" );
	}

	public function clear_all_cache()
	{
		$this->db->cache_delete_all(); 
		redirect( site_url($this->input->get('uri')) . "?src=clear_all_cache" );
	}

	public function regions($start=0) {

		$regions = new $this->Regions_model(NULL, 'locations');
		$regions->set_limit(10);
		$regions->set_order('name', 'ASC');
		$regions->set_select("*");
		$regions->set_start( $start );

		if( $this->input->get('q') ) {
			if( $start ) {
				redirect( site_url('regions/index') . "?q=" . $this->input->get('q') );
			}
			$regions->set_where( "name LIKE '%{$this->input->get('q')}%'" );
		}

		$this->template_data->set('regions', $regions->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => main_base_url( $this->config->item('index_page') . "/manage/regions"),
			'total_rows' => $regions->count_all_results(),
			'per_page' => $regions->get_limit()
		)));

		$this->load->view('homepage/manage/regions/regions', $this->template_data->get_data());
	}

	public function regions_edit($id) {

		$region = new $this->Regions_model(NULL, 'locations');
		$region->setId($id,true);

		if( $this->input->post('region_name') ) {
			$region->setName($this->input->post('region_name', true),false,true);
			$region->update();
		}
		$this->template_data->set('region', $region->get());

		$this->load->view('homepage/manage/regions/region_edit', $this->template_data->get_data());
	}

	public function provinces($start=0) {

		$provinces = new $this->Provinces_model(NULL, 'locations');
		$provinces->set_limit(10);
		$provinces->set_order('name', 'ASC');
		$provinces->set_select("*");
		$provinces->set_select('(SELECT name FROM regions WHERE provinces.region=regions.id) as region_name');
		$provinces->set_start( $start );

		if( $this->input->get('q') ) {
			if( $start ) {
				redirect( site_url('provinces/index') . "?q=" . $this->input->get('q') );
			}
			$provinces->set_where( "name LIKE '%{$this->input->get('q')}%'" );
		}

		$this->template_data->set('provinces', $provinces->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => main_base_url( $this->config->item('index_page') . "/manage/provinces"),
			'total_rows' => $provinces->count_all_results(),
			'per_page' => $provinces->get_limit()
		)));

		$this->load->view('homepage/manage/provinces/provinces', $this->template_data->get_data());
	}

	public function provinces_edit($id) {

		$regions = new $this->Regions_model(NULL, 'locations');
		$regions->cache_on();
		$regions->set_limit(0);
		$regions->set_order('name', 'ASC');
		$this->template_data->set('regions', $regions->populate());

		$province = new $this->Provinces_model(NULL, 'locations');
		$province->setId($id,true);

		if( $this->input->post('province_name') ) {
			$province->setName($this->input->post('province_name', true),false,true);
			if( $this->input->post('region_id') ) {
				$province->setRegion($this->input->post('region_id', true),false,true);
			}
			$province->update();
		} 

		$this->template_data->set('province', $province->get());

		$this->load->view('homepage/manage/provinces/province_edit', $this->template_data->get_data());
	}

	public function municipals($start=0) {

		$municipals = new $this->Municipals_model(NULL, 'locations');
		$municipals->set_limit(10);
		$municipals->set_order('name', 'ASC');
		$municipals->set_select("*");
		$municipals->set_select('(SELECT name FROM provinces WHERE municipals.province=provinces.id) as province_name');
		$municipals->set_start( $start );

		if( $this->input->get('q') ) {
			if( $start ) {
				redirect( site_url('municipals/index') . "?q=" . $this->input->get('q') );
			}
			$municipals->set_where( "name LIKE '%{$this->input->get('q')}%'" );
		}

		$this->template_data->set('municipals', $municipals->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => main_base_url( $this->config->item('index_page') . "/manage/municipals"),
			'total_rows' => $municipals->count_all_results(),
			'per_page' => $municipals->get_limit()
		)));

		$this->load->view('homepage/manage/municipals/municipals', $this->template_data->get_data());
	}

	public function municipals_edit($id) {

		$provinces = new $this->Provinces_model(NULL, 'locations');
		$provinces->cache_on();
		$provinces->set_limit(0);
		$provinces->set_order('((SELECT name FROM regions WHERE regions.id=provinces.region))', 'ASC');
		$provinces->set_select('*, (SELECT name FROM regions WHERE regions.id=provinces.region) as region_name');
		$this->template_data->set('provinces', $provinces->populate());

		$municipal = new $this->Municipals_model(NULL, 'locations');
		$municipal->setId($id,true);

		if( $this->input->post('municipal_name') ) {
			$municipal->setName($this->input->post('municipal_name', true),false,true);
			if( $this->input->post('pr_id') ) {
				$pr_id = explode("|", $this->input->post('pr_id'));
				$municipal->setProvince($pr_id[0],false,true);
				$municipal->setRegion($pr_id[1],false,true);
			}
			$municipal->update();
		}
		$this->template_data->set('municipal', $municipal->get());

		$this->load->view('homepage/manage/municipals/municipal_edit', $this->template_data->get_data());
	}
}
