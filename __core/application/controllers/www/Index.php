<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MY_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->model('companies/Companies_data_model');
		$this->load->model('schools/Schools_model');

	}

	public function index() {

		// companies
		$companies = new $this->Companies_data_model('c', 'companies');
		$companies->set_order('RAND()');
		$companies->set_limit(6);
		$companies->cache_on();
		$this->template_data->set('companies', $companies->populate());

		// schools
		$schools = new $this->Schools_model('s', 'schools');
		$schools->cache_on();
		$schools->set_order('RAND()');
		$schools->set_limit(6);
		$schools->set_order('s.name', 'ASC');
		$schools->set_select("s.*");
		$schools->set_select('sc.name as category_name');
		$schools->set_select('sl.url as logo_url');
		$schools->set_join('school_categories sc', 'sc.id=s.category');
		$schools->set_join('schools_logo sl', 'sl.school_id=s.id');
		$this->template_data->set('schools_list', $schools->populate());

		$this->load->view('homepage/homepage', $this->template_data->get_data());
	}

	public function directory() {
		$this->load->view('homepage/homepage', $this->template_data->get_data());
	}



}
