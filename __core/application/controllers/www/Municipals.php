<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Municipals extends ADMIN_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->model('locations/Municipals_model');
		$this->load->model('locations/Provinces_model');

	}

	public function index($start=0) {

		$municipals = new $this->Municipals_model(NULL, 'locations');
		$municipals->set_limit(10);
		$municipals->set_order('name', 'ASC');
		$municipals->set_select("*");
		$municipals->set_select('(SELECT name FROM provinces WHERE municipals.province=provinces.id) as province_name');
		$municipals->set_start( $start );

		if( $this->input->get('q') ) {
			if( $start ) {
				redirect( site_url('municipals/index') . "?q=" . $this->input->get('q') );
			}
			$municipals->set_where( "name LIKE '%{$this->input->get('q')}%'" );
		}

		$this->template_data->set('municipals', $municipals->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item('index_page') . "/municipals/index"),
			'total_rows' => $municipals->count_all_results(),
			'per_page' => $municipals->get_limit()
		)));

		$this->load->view('main/municipals/municipals', $this->template_data->get_data());
	}

	public function edit($id) {

		$provinces = new $this->Provinces_model(NULL, 'locations');
		$provinces->cache_on();
		$provinces->set_limit(0);
		$provinces->set_order('((SELECT name FROM regions WHERE regions.id=provinces.region))', 'ASC');
		$provinces->set_select('*, (SELECT name FROM regions WHERE regions.id=provinces.region) as region_name');
		$this->template_data->set('provinces', $provinces->populate());

		$municipal = new $this->Municipals_model(NULL, 'locations');
		$municipal->setId($id,true);

		if( $this->input->post('municipal_name') ) {
			$municipal->setName($this->input->post('municipal_name', true),false,true);
			if( $this->input->post('pr_id') ) {
				$pr_id = explode("|", $this->input->post('pr_id'));
				$municipal->setProvince($pr_id[0],false,true);
				$municipal->setRegion($pr_id[1],false,true);
			}
			$municipal->update();
		}
		$this->template_data->set('municipal', $municipal->get());

		$this->load->view('main/municipals/municipal_edit', $this->template_data->get_data());
	}

}
