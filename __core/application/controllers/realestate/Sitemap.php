<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends Realestate_Controller {

	var $limit = 5000;

	public function __construct() {
		parent::__construct();

		$this->load->model('realestate/Properties_model');

		$this->output->set_content_type('application/xml');
	}

	public function index() {

		$properties = new $this->Properties_model('c', 'realestate');
		$this->template_data->set('properties', $properties->count_all_results());

		$this->load->view('realestate/sitemap_list', $this->template_data->get_data());
	}

	public function properties($page=1) {

		$properties = new $this->Properties_model('c', 'realestate');
		$properties->set_limit($this->limit);
		$properties->set_start(($page-1)*$this->limit);
		$properties->set_order('c.id', 'ASC');
		$this->template_data->set('properties', $properties->populate());

		$this->load->view('realestate/sitemap_content', $this->template_data->get_data());
	}


}
