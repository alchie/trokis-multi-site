<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Land extends REALESTATE_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->model('realestate/Properties_model');
	}

	public function index() {

		$properties = new $this->Properties_model('p', 'realestate');
		$properties->setActive('1',true);
		$properties->setType('land',true);
		$properties->set_order('RAND()');
		$properties->set_limit(12);
		$properties->set_select('p.*');
		$properties->set_select('(SELECT pm.media_value FROM properties_media pm WHERE pm.media_type="picture" AND pm.media_key="thumbnail" AND pm.property_id=p.id LIMIT 1) as logo_url');
		$this->template_data->set('properties', $properties->populate());

		$this->load->view('realestate/land', $this->template_data->get_data());
	}

}
