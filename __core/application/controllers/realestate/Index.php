<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends REALESTATE_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->model('realestate/Partners_model');
		$this->load->model('realestate/Properties_model');

		$this->template_data->set('page_id', 'index');
	}

	public function index() {

		$page_title = "For Sale and For Rent Properties in the Philippines";
		if($this->input->get('status')=='sale') {
			$page_title = "For Sale Properties in the Philippines";
		} elseif($this->input->get('status')=='rent') {
			$page_title = "For Rent Properties in the Philippines";
		}
		$this->template_data->set('page_title', $page_title);
	    $this->template_data->set('meta_description', "For Sale and For Rent Properties in the Philippines");
	    $this->template_data->set('meta_keywords', "For Sale and For Rent Properties in the Philippines");

		$partners = new $this->Partners_model('p', 'realestate');
		$partners->set_order('id', 'DESC');
		$partners->setActive('1',true);
		$this->template_data->set('partners', $partners->populate());

		$properties = new $this->Properties_model('p', 'realestate');
		$properties->setActive('1',true);
		$properties->set_order('RAND()');
		$properties->set_limit(12);
		$properties->set_select('p.*');
		$properties->set_select('(SELECT pm.media_value FROM properties_media pm WHERE pm.media_type="picture" AND pm.media_key="thumbnail" AND pm.property_id=p.id LIMIT 1) as logo_url');
		$this->template_data->set('properties', $properties->populate());

		$this->load->view('realestate/realestate_index', $this->template_data->get_data());
	}

}
