<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends REALESTATE_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->model('realestate/Projects_model');

		$this->template_data->set('page_id', 'projects');
	}

	public function index() {

		$page_title = "For Sale and For Rent Properties in the Philippines";
		if($this->input->get('status')=='sale') {
			$page_title = "For Sale Properties in the Philippines";
		} elseif($this->input->get('status')=='rent') {
			$page_title = "For Rent Properties in the Philippines";
		}
		$this->template_data->set('page_title', $page_title);
	    $this->template_data->set('meta_description', "For Sale and For Rent Properties in the Philippines");
	    $this->template_data->set('meta_keywords', "For Sale and For Rent Properties in the Philippines");

		$projects = new $this->Projects_model('p', 'realestate');
		$projects->set_select('p.*');
		$this->template_data->set('projects', $projects->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 5,
			'base_url' => realestate_base_url( $this->config->item('index_page') . "/projects/index"),
			'total_rows' => $projects->count_all_results(),
			'per_page' => $projects->get_limit()
		)));
		
		$this->load->view('realestate/projects', $this->template_data->get_data());
	}

}
