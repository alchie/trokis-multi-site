<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Developers extends REALESTATE_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->model('realestate/Developers_model');
		$this->load->model('realestate/Properties_model');

		$this->template_data->set('page_id', 'developers');
	}

	public function index($start=0) {

		$page_title = "Real Estate Developers in the Philippines";

		$this->template_data->set('page_title', $page_title);
	    $this->template_data->set('meta_description', "For Sale and For Rent Properties in the Philippines");
	    $this->template_data->set('meta_keywords', "For Sale and For Rent Properties in the Philippines");

		$developers = new $this->Developers_model('p', 'realestate');
		$developers->set_order('id', 'DESC');
		$developers->setActive('1',true);
		$developers->set_limit(10);
		$developers->set_start($start);
		$this->template_data->set('developers', $developers->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 5,
			'base_url' => realestate_base_url( $this->config->item('index_page') . "/developers/index"),
			'total_rows' => $developers->count_all_results(),
			'per_page' => $developers->get_limit()
		)));

		$this->load->view('realestate/developers', $this->template_data->get_data());
	}

}
