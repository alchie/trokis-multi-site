<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Developer extends REALESTATE_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('realestate/Properties_model');
		$this->load->model('realestate/Properties_media_model');
		$this->load->model('realestate/Messages_model');
		$this->load->model('realestate/Developers_model');
		$this->load->model('realestate/Projects_model');
	}

	public function index() {
		redirect( realestate_url("properties") );
	}

	public function view($id, $slug) {

		$developer = new $this->Developers_model('d', 'realestate');
		$developer->setId($id,true);
		$developer->setSlug($slug,true);
		if( $developer->nonEmpty() ) {
			
			$developer_data = $developer->get();

			$this->template_data->set('developer', $developer_data);

			$this->template_data->set('page_title', $developer_data->name);

			$this->template_data->set('meta_description', $developer_data->name);

			$this->template_data->opengraph(array(
				'fb:app_id'=>$this->config->item('fbAppId'),
				'og:image' => ($developer_data->logo_url) ? $developer_data->logo_url : $this->config->item('trokis_logo_url'),
				'og:image' => ($developer_data->logo_url) ? $developer_data->logo_url : $this->config->item('trokis_logo_url'),
				'og:description' => "CALL or TEXT: Alchie Tagudin (+639462559955 TNT)",
				'og:site_name' => 'Trokis Philippines',
				'og:title' => $developer_data->name,
			));

			$projects = new $this->Projects_model('p', 'realestate');
			$projects->set_select('p.*');
			$projects->setDevId($id,true);
			$this->template_data->set('projects', $projects->populate());

			$this->load->view('realestate/developer_view', $this->template_data->get_data());

		} else {
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}
	}

	public function media($media_id) {
		
		$medias = new $this->Properties_media_model('m', 'realestate');
		$medias->setMediaId($media_id,true);
		$media = $medias->get();
		$this->template_data->set('media', $media);

		$property = new $this->Properties_model('p', 'realestate');
		$property->setId($media->property_id,true);
		$developer_data = $property->get();
		$this->template_data->set('property', $developer_data);

		$related = new $this->Properties_model('p', 'realestate');
		$related->setActive('1',true);
		$related->set_order('RAND()');
		$related->set_limit(12);
		$related->set_select('p.*');
		$related->set_select('(SELECT pm.media_value FROM properties_media pm WHERE pm.media_type="picture" AND pm.media_key="thumbnail" AND pm.property_id=p.id LIMIT 1) as logo_url');
		$this->template_data->set('properties', $related->populate());

		$this->load->view('realestate/realestate_media', $this->template_data->get_data());

	}

	public function add_message($slug) {
		

		$property = new $this->Properties_model('p', 'realestate');
		$property->setSlug($slug,true);
		$developer_data = $property->get();

		if( $this->input->post("full_name") && $this->input->post("phone_number") ) {
			$hash = md5($this->input->post("full_name") . time() );
			$message = new $this->Messages_model(NULL, 'realestate');
			$message->setPropertyId($developer_data->id);
			$message->setFullName($this->input->post("full_name"));
			$message->setEmail($this->input->post("email"));
			$message->setPhoneNumber($this->input->post("phone_number"));
			$message->setMessage($this->input->post("message"));
			$message->setHash( $hash );
			if( $message->insert() ) {
				redirect( realestate_url("property/add_message/{$slug}") . "?hash={$hash}" );
			}
		}

		if( !$this->input->get('hash') ) {
			redirect( realestate_url("property/view/{$slug}") );
		}

		$this->template_data->set('property', $developer_data);

		$message = new $this->Messages_model(NULL, 'realestate');
		$message->setHash($this->input->get('hash'),true);
		$this->template_data->set('message', $message->get());

		$related = new $this->Properties_model('p', 'realestate');
		$related->setActive('1',true);
		$related->set_order('RAND()');
		$related->set_limit(12);
		$related->set_select('p.*');
		$related->set_select('(SELECT pm.media_value FROM properties_media pm WHERE pm.media_type="picture" AND pm.media_key="thumbnail" AND pm.property_id=p.id LIMIT 1) as logo_url');
		$this->template_data->set('properties', $related->populate());

		$this->load->view('realestate/realestate_add_message', $this->template_data->get_data());

	}

}
