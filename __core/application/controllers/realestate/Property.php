<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends REALESTATE_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('realestate/Properties_model');
		$this->load->model('realestate/Properties_media_model');
		$this->load->model('realestate/Messages_model');
	}

	public function index() {
		redirect( realestate_url("properties") );
	}

	public function view($slug) {

		$property = new $this->Properties_model('p', 'realestate');
		$property->setSlug($slug,true);
		if( $property->nonEmpty() ) {
			$property->set_select('p.*');
			$property->set_select('(SELECT pm.media_value FROM properties_media pm WHERE pm.media_type="picture" AND pm.media_key="cover" AND pm.property_id=p.id LIMIT 1) as logo_url');
			$property->set_select('(SELECT pd.detail_content FROM properties_details pd WHERE pd.detail_key="address" AND pd.property_id=p.id LIMIT 1) as address');

			$property->set_select('(SELECT pd.detail_content FROM properties_details pd WHERE pd.detail_key="geo_lat" AND pd.property_id=p.id LIMIT 1) as geo_lat');

			$property->set_select('(SELECT pd.detail_content FROM properties_details pd WHERE pd.detail_key="geo_lng" AND pd.property_id=p.id LIMIT 1) as geo_lng');

			$property_data = $property->get();

			$this->template_data->set('property', $property_data);

			$this->template_data->set('page_title', $property_data->title);

			$this->template_data->set('meta_description', $property_data->title);

			$this->template_data->opengraph(array(
				'fb:app_id'=>$this->config->item('fbAppId'),
				'og:image' => ($property_data->logo_url) ? $property_data->logo_url : $this->config->item('trokis_logo_url'),
				'og:image' => ($property_data->logo_url) ? $property_data->logo_url : $this->config->item('trokis_logo_url'),
				'og:description' => "CALL or TEXT: Alchie Tagudin (+639462559955 TNT)",
				'og:site_name' => 'Trokis Philippines',
				'og:title' => $property_data->title,
			));

			$medias = new $this->Properties_media_model('m', 'realestate');
			$medias->setPropertyId($property_data->id,true);
			$medias->setMediaKey('default',true);
			$medias->set_order('m.media_id', 'DESC');
			$medias->set_limit(0);
			if( ($this->session->userdata('type')==null) && ($this->session->userdata('type') != 'admin') ) {
				$medias->setActive(1,true);
		 	}
			$this->template_data->set('medias', $medias->populate());


			$related = new $this->Properties_model('p', 'realestate');
			$related->setActive('1',true);
			$related->set_order('RAND()');
			$related->set_limit(5);
			$related->set_select('p.*');
			$related->set_select('(SELECT pm.media_value FROM properties_media pm WHERE pm.media_type="picture" AND pm.media_key="thumbnail" AND pm.property_id=p.id LIMIT 1) as logo_url');
			$this->template_data->set('related', $related->populate());


			$this->load->view('realestate/realestate_view', $this->template_data->get_data());

		} else {
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}
	}

	public function media($media_id) {
		
		$medias = new $this->Properties_media_model('m', 'realestate');
		$medias->setMediaId($media_id,true);
		$media = $medias->get();
		$this->template_data->set('media', $media);

		$property = new $this->Properties_model('p', 'realestate');
		$property->setId($media->property_id,true);
		$property_data = $property->get();
		$this->template_data->set('property', $property_data);

		$related = new $this->Properties_model('p', 'realestate');
		$related->setActive('1',true);
		$related->set_order('RAND()');
		$related->set_limit(12);
		$related->set_select('p.*');
		$related->set_select('(SELECT pm.media_value FROM properties_media pm WHERE pm.media_type="picture" AND pm.media_key="thumbnail" AND pm.property_id=p.id LIMIT 1) as logo_url');
		$this->template_data->set('properties', $related->populate());

		$this->load->view('realestate/realestate_media', $this->template_data->get_data());

	}

	public function add_message($slug) {
		

		$property = new $this->Properties_model('p', 'realestate');
		$property->setSlug($slug,true);
		$property_data = $property->get();

		if( $this->input->post("full_name") && $this->input->post("phone_number") ) {
			$hash = md5($this->input->post("full_name") . time() );
			$message = new $this->Messages_model(NULL, 'realestate');
			$message->setPropertyId($property_data->id);
			$message->setFullName($this->input->post("full_name"));
			$message->setEmail($this->input->post("email"));
			$message->setPhoneNumber($this->input->post("phone_number"));
			$message->setMessage($this->input->post("message"));
			$message->setHash( $hash );
			if( $message->insert() ) {
				redirect( realestate_url("property/add_message/{$slug}") . "?hash={$hash}" );
			}
		}

		if( !$this->input->get('hash') ) {
			redirect( realestate_url("property/view/{$slug}") );
		}

		$this->template_data->set('property', $property_data);

		$message = new $this->Messages_model(NULL, 'realestate');
		$message->setHash($this->input->get('hash'),true);
		$this->template_data->set('message', $message->get());

		$related = new $this->Properties_model('p', 'realestate');
		$related->setActive('1',true);
		$related->set_order('RAND()');
		$related->set_limit(12);
		$related->set_select('p.*');
		$related->set_select('(SELECT pm.media_value FROM properties_media pm WHERE pm.media_type="picture" AND pm.media_key="thumbnail" AND pm.property_id=p.id LIMIT 1) as logo_url');
		$this->template_data->set('properties', $related->populate());

		$this->load->view('realestate/realestate_add_message', $this->template_data->get_data());

	}

}
