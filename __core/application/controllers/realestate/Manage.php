<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage extends ADMIN_Controller {

	public function __construct() {
		parent::__construct();
		$this->must_admin();

		$this->load->model('realestate/Partners_model');
		$this->load->model('realestate/Properties_model');
		$this->load->model('realestate/Properties_media_model');
		$this->load->model('realestate/Properties_details_model');
		$this->load->model('realestate/Developers_model');
		$this->load->model('realestate/Projects_model');
		$this->load->model('realestate/Messages_model');

		$this->template_data->set('page_title', "ADMINISTRATION : Real Estate");

		$this->template_data->set('app_id', 'realestate');
	}

	public function clear_cache()
	{
		$uri_string = ($this->input->get('uri') && ($this->input->get('uri') != '')) ? explode("/",$this->input->get('uri')) : array('default', 'index');
		$this->db->cache_delete($uri_string[0], $uri_string[1]); 
		redirect( site_url($this->input->get('uri')) . "?src=clear_cache" );
	}

	public function clear_all_cache()
	{
		$this->db->cache_delete_all(); 
		redirect( site_url($this->input->get('uri')) . "?src=clear_all_cache" );
	}

	public function partners($start=0)
	{
		$partners = new $this->Partners_model('p', 'realestate');
		$partners->set_order('id', 'DESC');
		$partners->set_limit(10);
		$partners->set_start($start);
		$this->template_data->set('partners', $partners->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => realestate_base_url( $this->config->item('index_page') . "/manage/partners"),
			'total_rows' => $partners->count_all_results(),
			'per_page' => $partners->get_limit()
		)));

		$this->load->view('realestate/manage/partners', $this->template_data->get_data());
	}

	public function partners_add()
	{
		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Partner Name', 'trim|required');
			$this->form_validation->set_rules('logo', 'Logo URL', 'trim|required|valid_url');
			if( $this->form_validation->run() ) {
				$partner = new $this->Partners_model(NULL, 'realestate');
				$partner->setName($this->input->post('name'));
				$partner->setLogoUrl($this->input->post('logo'));
				$partner->setWebsite($this->input->post('website'));
				$partner->setActive(($this->input->post('active')?1:0));
				if( $partner->insert() ) {
					redirect( realestate_url("manage/partners") );
				}
			}
		}
		$this->load->view('realestate/manage/partners_add', $this->template_data->get_data());
	}

	public function partners_edit($id)
	{
		$partner = new $this->Partners_model(NULL, 'realestate');
		$partner->setId($id,true);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Partner Name', 'trim|required');
			$this->form_validation->set_rules('logo', 'Logo URL', 'trim|required|valid_url');
			if( $this->form_validation->run() ) {
				$partner->setName($this->input->post('name'));
				$partner->setLogoUrl($this->input->post('logo'));
				$partner->setWebsite($this->input->post('website'));
				$partner->setActive(($this->input->post('active')?1:0));
				if( $partner->update() ) {
					redirect( realestate_url("manage/partners") );
				}
			}
		}

		$this->template_data->set('partner', $partner->get());

		$this->load->view('realestate/manage/partners_edit', $this->template_data->get_data());
	}

	public function partners_delete($id)
	{
		$partner = new $this->Partners_model(NULL, 'realestate');
		$partner->setId($id,true);
		$partner->delete();
		redirect( realestate_url( $this->input->get('back') ) );

	}

	public function developers($start=0)
	{
		$developers = new $this->Developers_model('d', 'realestate');
		if($this->input->get('q')) {
			$developers->set_where("d.name LIKE '%".$this->input->get('q')."%'");
		}
		$developers->set_order('id', 'DESC');
		$developers->set_limit(10);
		$developers->set_start($start);
		$this->template_data->set('developers', $developers->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => realestate_base_url( $this->config->item('index_page') . "/manage/developers"),
			'total_rows' => $developers->count_all_results(),
			'per_page' => $developers->get_limit()
		)));

		$this->load->view('realestate/manage/developers', $this->template_data->get_data());
	}

	public function developers_add()
	{
		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Developer Name', 'trim|required');
			if( $this->form_validation->run() ) {
				$developer = new $this->Developers_model(NULL, 'realestate');
				$developer->setName($this->input->post('name'));
				$developer->setSlug(slugify($this->input->post('name')));
				$developer->setLogoUrl($this->input->post('logo_url'));
				$developer->setWebsite($this->input->post('website'));
				$developer->setContent($this->input->post('content'));
				$developer->setActive(($this->input->post('active')?1:0));
				if( $developer->insert() ) {
					redirect( realestate_url("manage/developers") );
				}
			}
		}
		$this->load->view('realestate/manage/developers_add', $this->template_data->get_data());
	}

	public function developers_edit($id)
	{
		$developer = new $this->Developers_model(NULL, 'realestate');
		$developer->setId($id,true);
		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Partner Name', 'trim|required');
			if( $this->form_validation->run() ) {
				$developer->setName($this->input->post('name'),false,true);
				if( $this->input->post('slug') ) {
					$developer->setSlug(slugify($this->input->post('slug')),false,true);
				} else {
					$developer->setSlug(slugify($this->input->post('name')),false,true);
				}
				$developer->setLogoUrl($this->input->post('logo_url'),false,true);
				$developer->setWebsite($this->input->post('website'),false,true);
				$developer->setContent($this->input->post('content'),false,true);
				$developer->setActive(($this->input->post('active')?1:0),false,true);
				if( $developer->update() ) {
					redirect( realestate_url(($this->input->get('back')) ? $this->input->get('back') : "manage/developers") );
				}
			}
		}
		$this->template_data->set('developer', $developer->get());
		$this->load->view('realestate/manage/developers_edit', $this->template_data->get_data());
	}

	public function developers_delete($id)
	{
		$developer = new $this->Developers_model(NULL, 'realestate');
		$developer->setId($id,true);
		$developer->delete();
		redirect( realestate_url( $this->input->get('back') ) );

	}

	public function developers_projects($dev_id, $start=0)
	{
		$developer = new $this->Developers_model('d', 'realestate');
		$developer->setId($dev_id,true);
		$this->template_data->set('developer', $developer->get());

		$projects = new $this->Projects_model('p', 'realestate');
		$projects->setDevId($dev_id,true);
		$this->template_data->set('projects', $projects->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => realestate_base_url( $this->config->item('index_page') . "/manage/developers_projects/{$dev_id}"),
			'total_rows' => $projects->count_all_results(),
			'per_page' => $projects->get_limit()
		)));

		$this->load->view('realestate/manage/developers_projects', $this->template_data->get_data());
	}

	public function developers_projects_add($dev_id)
	{

		$developer = new $this->Developers_model('d', 'realestate');
		$developer->setId($dev_id,true);
		$developer_data = $developer->get();
		$this->template_data->set('developer', $developer_data);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Project Name', 'trim|required');
			if( $this->form_validation->run() ) {
				$project = new $this->Projects_model(NULL, 'realestate');
				$project->setDevId($dev_id);
				$project->setName($this->input->post('name'));
				$project->setSlug(slugify($this->input->post('name')));
				$project->setActive(($this->input->post('active')?1:0));
				if( $project->insert() ) {
					redirect( realestate_url("manage/developers_projects/{$developer_data->id}") );
				}
			}
		}

		$this->load->view('realestate/manage/developers_projects_add', $this->template_data->get_data());
	}

	public function developers_projects_edit($proj_id)
	{

		$project = new $this->Projects_model(NULL, 'realestate');
		$project->setId($proj_id,true);

		if( $this->input->post() ) {
			$this->form_validation->set_rules('name', 'Project Name', 'trim|required');
			if( $this->form_validation->run() ) {
				$slug = slugify($this->input->post('name'));
				if( $this->input->post('slug') ) {
					$slug = slugify($this->input->post('slug'));
				}
				$project->setName($this->input->post('name'),false,true);
				$project->setSlug($slug,false,true);
				$project->setActive(($this->input->post('active')?1:0),false,true);
				$project->update();
			}
		}

		$project_data = $project->get();
		$this->template_data->set('project', $project_data);

		$developer = new $this->Developers_model('d', 'realestate');
		$developer->setId($project_data->dev_id,true);
		$developer_data = $developer->get();
		$this->template_data->set('developer', $developer_data);

		$this->load->view('realestate/manage/developers_projects_edit', $this->template_data->get_data());
	}

	public function developers_projects_delete($id)
	{
		$project = new $this->Projects_model(NULL, 'realestate');
		$project->setId($id,true);
		$project->delete();
		redirect( realestate_url( $this->input->get('back') ) );

	}

	private function property_slug($slug) {
		$slug2 = $slug;
		$properties = new $this->Properties_model('p', 'realestate');
		$properties->setSlug($slug2,true);
		$is_exist = $properties->nonEmpty();
		$n=0;
		while( $is_exist ) {
			$n++;
			$slug2 = $slug . $n;
			$properties = new $this->Properties_model('p', 'realestate');
			$properties->setSlug($slug2,true);
			$is_exist = $properties->nonEmpty();
		}
		return $slug2;
	}

	public function properties($dev_id=0, $proj_id=0, $start=0)
	{

		$properties = new $this->Properties_model('p', 'realestate');
		$properties->set_order('id', 'DESC');
		$properties->set_limit(10);
		$properties->set_start($start);

		$this->template_data->set('dev_id', $dev_id);
		if( $dev_id ) {
			$developer = new $this->Developers_model(NULL, 'realestate');
			$developer->setId($dev_id, true);
			$this->template_data->set('developer', $developer->get());
			$properties->setDevId($dev_id,true);
		}
		
		$this->template_data->set('proj_id', $proj_id);
		if( $proj_id ) {
			$project = new $this->Projects_model(NULL, 'realestate');
			$project->setId($proj_id,true);
			$project->setDevId($dev_id,true);
			$this->template_data->set('project', $project->get());
			$properties->setProjId($proj_id,true);
		}

		$this->template_data->set('properties', $properties->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => realestate_base_url( $this->config->item('index_page') . "/manage/properties/{$dev_id}/{$proj_id}"),
			'total_rows' => $properties->count_all_results(),
			'per_page' => $properties->get_limit()
		)));

		$this->load->view('realestate/manage/properties', $this->template_data->get_data());
	}

	public function properties_add($dev_id=0, $proj_id=0)
	{
		if( $this->input->post() ) {
			$this->form_validation->set_rules('title', 'Property Title', 'trim|required');
			$this->form_validation->set_rules('type', 'Property Type', 'trim|required');
			if( $this->form_validation->run() ) {
				$property = new $this->Properties_model(NULL, 'realestate');
				$slug = $this->property_slug( slugify($this->input->post('title')) );
				$property->setTitle($this->input->post('title'));
				$property->setSlug(slugify($slug));
				$property->setType($this->input->post('type'));
				$property->setDetails($this->input->post('details'));
				$property->setPrice(str_replace(",", "", $this->input->post('price')));
				$property->setBeds($this->input->post('beds'));
				$property->setBaths($this->input->post('baths'));
				$property->setFloorArea($this->input->post('floor_area'));
				$property->setLotArea($this->input->post('lot_area'));
				$property->setActive(($this->input->post('active')?1:0));
				$property->setRent(($this->input->post('rent')?1:0));
				$property->setDevId($dev_id);
				$property->setProjId($proj_id);
				if( $property->insert() ) {
					redirect( realestate_url("manage/properties/{$dev_id}/{$proj_id}") );
				}
			}
		}

		$this->template_data->set('dev_id', $dev_id);
		if( $dev_id ) {
			$developer = new $this->Developers_model(NULL, 'realestate');
			$developer->setId($dev_id, true);
			$this->template_data->set('developer', $developer->get());
		}
		
		$this->template_data->set('proj_id', $proj_id);
		if( $proj_id ) {
			$project = new $this->Projects_model(NULL, 'realestate');
			$project->setId($proj_id,true);
			$project->setDevId($dev_id,true);
			$this->template_data->set('project', $project->get());
		}

		$this->load->view('realestate/manage/properties_add', $this->template_data->get_data());
	}

	private function _generate_property_slug($text, $id) {
		$slug = slugify($text);
		$i=0;
		$property = new $this->Properties_model('p', 'realestate');
		$property->setSlug($slug, true);
		$property->set_where("p.id != {$id}");
		$goodtogo = $property->nonEmpty();
		while( $goodtogo ) {
			if( $i ) {
				$slug .= "-" . $i;
			}
			$property->setSlug($slug, true);
			$goodtogo = $property->nonEmpty();
			$i++;
		}
		return $slug;
	}

	public function properties_edit($id)
	{
		$property = new $this->Properties_model('p', 'realestate');
		$property->setId($id,true);
		if( $this->input->post() ) {
			$this->form_validation->set_rules('title', 'Property Title', 'trim|required');
			$this->form_validation->set_rules('type', 'Property Type', 'trim|required');
			if( $this->form_validation->run() ) {
				$property->setTitle($this->input->post('title'),false,true);
				$slug = ($this->input->post('slug')) ? $this->input->post('slug') : $this->input->post('title');
				$property->setSlug($this->_generate_property_slug($slug, $id),false,true);
				$property->setType($this->input->post('type'),false,true);
				$property->setDetails($this->input->post('details'),false,true);
				$property->setPrice($this->input->post('price'),false,true);
				$property->setBeds($this->input->post('beds'),false,true);
				$property->setBaths($this->input->post('baths'),false,true);
				$property->setFloorArea($this->input->post('floor_area'),false,true);
				$property->setLotArea($this->input->post('lot_area'),false,true);
				$property->setActive(($this->input->post('active')?1:0),false,true);
				$property->setRent(($this->input->post('rent')?1:0),false,true);
				if( $property->update() ) {
					redirect( realestate_url(uri_string()) . "?action=update" );
				}
			}
		}
		$property->set_select("p.*");
			
			$developer = new $this->Developers_model('d');
			$developer->set_select("d.name");
			$developer->set_where("p.dev_id=d.id");
			$developer->set_limit(1);
		$property->set_select("(".$developer->get_compiled_select().") as developer_name");

			$project = new $this->Projects_model('j');
			$project->set_select("j.name");
			$project->set_where("p.proj_id=j.id");
			$project->set_limit(1);
		$property->set_select("(".$project->get_compiled_select().") as project_name");

		$this->template_data->set('property', $property->get());
		$this->load->view('realestate/manage/properties_edit', $this->template_data->get_data());
	}

	public function properties_delete($id)
	{
		$property = new $this->Properties_model(NULL, 'realestate');
		$property->setId($id,true);
		$property->delete();
		redirect( realestate_url( $this->input->get('back') ) );
	}

	public function properties_add_project($id)
	{

		$properties = new $this->Properties_model('p', 'realestate');
		$properties->setId($id,true);
		$properties->set_select("*");

		if( $this->input->post("proj_id") ) {
			$proj = explode("|", $this->input->post("proj_id"));
			$properties->setProjId($proj[0],false,true);
			$properties->setDevId($proj[1],false,true);
			$properties->update();
			redirect( realestate_url("manage/properties_edit/{$id}") );
		}
		
			$project = new $this->Projects_model('j');
			$project->set_select("j.name");
			$project->set_where("p.proj_id=j.id");
			$project->set_limit(1);
			$properties->set_select("(".$project->get_compiled_select().") as project_name");

		$this->template_data->set('property', $properties->get());

		if( $this->input->get("q") ) {
			$projects = new $this->Projects_model('p', 'realestate');
			$projects->set_select("*");
			$projects->set_where("p.name LIKE", "%{$this->input->get("q")}%");
				$developer = new $this->Developers_model('d');
				$developer->set_select("d.name");
				$developer->set_where("p.dev_id=d.id");
				$developer->set_limit(1);
			$projects->set_select("(".$developer->get_compiled_select().") as developer_name");
			$this->template_data->set('projects', $projects->populate());
		}

		$this->load->view('realestate/manage/properties_add_project', $this->template_data->get_data());
	}

	public function properties_media($id, $start=0)
	{
		$properties = new $this->Properties_model('p', 'realestate');
		$properties->setId($id,true);
		$this->template_data->set('property', $properties->get());

		$medias = new $this->Properties_media_model(NULL, 'realestate');
		$medias->setPropertyId($id,true);
		$this->template_data->set('medias', $medias->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => realestate_base_url( $this->config->item('index_page') . "/manage/properties_media/{$id}"),
			'total_rows' => $medias->count_all_results(),
			'per_page' => $medias->get_limit()
		)));

		$this->load->view('realestate/manage/properties_media', $this->template_data->get_data());
	}

	public function properties_media_add($prop_id)
	{
		$properties = new $this->Properties_model('p', 'realestate');
		$properties->setId($prop_id,true);

		if( $properties->nonEmpty() ) {
		$this->template_data->set('property', $properties->get_results());

			if( $this->input->post() ) {
				$this->form_validation->set_rules('media_type', 'Media Type', 'trim|required');
				$this->form_validation->set_rules('media_key', 'Media Key', 'trim|required');
				$this->form_validation->set_rules('media_value', 'Media Value', 'trim|required');
				if( $this->form_validation->run() ) {
					$media = new $this->Properties_media_model(NULL, 'realestate');
					$media->setPropertyId($prop_id);
					$media->setMediaType($this->input->post('media_type'));
					$media->setMediaKey($this->input->post('media_key'));
					$media->setMediaValue($this->input->post('media_value'));
					$media->setActive(($this->input->post('active')?1:0));
					$media->setPrimary(($this->input->post('primary')?1:0));
					$media->setThumbnail($this->input->post('media_thumbnail'));
					$media->setAltText($this->input->post('alt_text'));
					if( $media->insert() ) {
						redirect( realestate_url("manage/properties_media/{$prop_id}") );
					}
				}
			}

		}
		$this->load->view('realestate/manage/properties_media_add', $this->template_data->get_data());
	}

	public function properties_media_edit($media_id)
	{
		$media = new $this->Properties_media_model(NULL, 'realestate');
		$media->setMediaId($media_id,true);
		$media_data = $media->get();
		if( $this->input->post() ) {
			$this->form_validation->set_rules('media_type', 'Media Type', 'trim|required');
				$this->form_validation->set_rules('media_key', 'Media Key', 'trim|required');
				$this->form_validation->set_rules('media_value', 'Media Value', 'trim|required');
			if( $this->form_validation->run() ) {
				
					$media->setMediaType($this->input->post('media_type'),false,true);
					$media->setMediaKey($this->input->post('media_key'),false,true);
					$media->setMediaValue($this->input->post('media_value'),false,true);
					$media->setActive(($this->input->post('active')?1:0),false,true);
					$media->setPrimary(($this->input->post('primary')?1:0),false,true);
					$media->setThumbnail($this->input->post('media_thumbnail'),false,true);
					$media->setAltText($this->input->post('alt_text'),false,true);
					if( $media->update() ) {
						redirect( realestate_url("manage/properties_media/{$media_data->property_id}") );
					}
			}
		}
		$this->template_data->set('media', $media->get());
		$this->load->view('realestate/manage/properties_media_edit', $this->template_data->get_data());
	}

	public function properties_media_disable($media_id)
	{
		$media = new $this->Properties_media_model(NULL, 'realestate');
		$media->setMediaId($media_id,true);
		$media->setActive(0,false,true);
		$media->update();
		redirect( realestate_url( $this->input->get('back') ) );
	}

	public function properties_media_enable($media_id)
	{
		$media = new $this->Properties_media_model(NULL, 'realestate');
		$media->setMediaId($media_id,true);
		$media->setActive(1,false,true);
		$media->update();
		redirect( realestate_url( $this->input->get('back') ) );
	}

	public function properties_media_delete($media_id)
	{
		$media = new $this->Properties_media_model(NULL, 'realestate');
		$media->setMediaId($media_id,true);
		$media->delete();
		redirect( realestate_url( $this->input->get('back') ) );
	}

	public function properties_details($id, $start=0)
	{
		$properties = new $this->Properties_model('p', 'realestate');
		$properties->setId($id,true);
		$this->template_data->set('property', $properties->get());

		$details = new $this->Properties_details_model(NULL, 'realestate');
		$details->setPropertyId($id,true);
		$this->template_data->set('details', $details->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => realestate_base_url( $this->config->item('index_page') . "/manage/properties_details/{$id}"),
			'total_rows' => $details->count_all_results(),
			'per_page' => $details->get_limit()
		)));

		$this->load->view('realestate/manage/properties_details', $this->template_data->get_data());
	}

	public function properties_details_add($prop_id)
	{
		$properties = new $this->Properties_model('p', 'realestate');
		$properties->setId($prop_id,true);

		if( $properties->nonEmpty() ) {
		$this->template_data->set('property', $properties->get_results());

			if( $this->input->post() ) {
				$this->form_validation->set_rules('detail_key', 'Detail Key', 'trim|required');
				$this->form_validation->set_rules('detail_content', 'Detail Content', 'trim|required');
				if( $this->form_validation->run() ) {
					$detail = new $this->Properties_details_model(NULL, 'realestate');
					$detail->setPropertyId($prop_id,true);
					$detail->setDetailKey($this->input->post('detail_key'),true);
					$detail->setDetailContent($this->input->post('detail_content'));
					$detail->setActive(($this->input->post('active')?1:0));
					if( !$detail->nonEmpty() ) {
						if( $detail->insert() ) {
							redirect( realestate_url("manage/properties_details/{$prop_id}") );
						}
					}
				}
			}

		}
		$this->load->view('realestate/manage/properties_details_add', $this->template_data->get_data());
	}

	public function properties_details_edit($detail_id)
	{
		$detail = new $this->Properties_details_model(NULL, 'realestate');
		$detail->setDetailId($detail_id,true);
		$detail_data = $detail->get();
		if( $this->input->post() ) {
			$this->form_validation->set_rules('detail_key', 'Detail Key', 'trim|required');
			$this->form_validation->set_rules('detail_content', 'Detail Content', 'trim|required');
			if( $this->form_validation->run() ) {
				
					$detail->setDetailKey($this->input->post('detail_key'),false,true);
					$detail->setDetailContent($this->input->post('detail_content'),false,true);
					$detail->setActive(($this->input->post('active')?1:0),false,true);
					
					if( $detail->update() ) {
						redirect( realestate_url("manage/properties_details/{$detail_data->property_id}") );
					}
			}
		}
		$this->template_data->set('detail', $detail->get());
		$this->load->view('realestate/manage/properties_details_edit', $this->template_data->get_data());
	}

	public function properties_details_delete($detail_id)
	{
		$detail = new $this->Properties_details_model(NULL, 'realestate');
		$detail->setDetailId($detail_id,true);
		$detail->delete();
		redirect( realestate_url( $this->input->get('back') ) );
	}

	public function messages($start=0)
	{
		$messages = new $this->Messages_model('p', 'realestate');
		$messages->set_order('id', 'DESC');
		$messages->set_limit(20);
		$messages->set_start($start);
		$this->template_data->set('messages', $messages->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => realestate_base_url( $this->config->item('index_page') . "/manage/messages"),
			'total_rows' => $messages->count_all_results(),
			'per_page' => $messages->get_limit()
		)));

		$this->load->view('realestate/manage/messages', $this->template_data->get_data());
	}

	public function message_property($property_id) {
		$property = new $this->Properties_model(NULL, 'realestate');
		$property->setId($property_id,true);
		
		if( $property->nonEmpty() ) {
			$property_data = $property->get_results();
			redirect(realestate_url("property/view/{$property_data->slug}"));
		} else {
			redirect(realestate_url("manage/messages"));
		}


	}
}
