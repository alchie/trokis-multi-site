<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends REALESTATE_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('realestate/Properties_model');
		$this->load->model('realestate/Properties_media_model');
		$this->load->model('realestate/Messages_model');
		$this->load->model('realestate/Developers_model');
		$this->load->model('realestate/Projects_model');
	}

	public function index() {
		redirect( realestate_url("properties") );
	}

	public function view($id, $slug) {

		$project = new $this->Projects_model('p', 'realestate');
		$project->setId($id,true);
		$project->setSlug($slug,true);
		if( $project->nonEmpty() ) {
			
			$project_data = $project->get();

			$this->template_data->set('developer', $project_data);
			$this->template_data->set('page_title', $project_data->name);
			$this->template_data->set('meta_description', $project_data->name);

			$this->template_data->opengraph(array(
				'fb:app_id'=>$this->config->item('fbAppId'),
				'og:image' => ($project_data->logo_url) ? $project_data->logo_url : $this->config->item('trokis_logo_url'),
				'og:image' => ($project_data->logo_url) ? $project_data->logo_url : $this->config->item('trokis_logo_url'),
				'og:description' => "CALL or TEXT: Alchie Tagudin (+639462559955 TNT)",
				'og:site_name' => 'Trokis Philippines',
				'og:title' => $project_data->name,
			));

			$this->load->view('realestate/project_view', $this->template_data->get_data());

		} else {
			$this->load->view('homepage/page_not_found', $this->template_data->get_data());
		}
	}

}
