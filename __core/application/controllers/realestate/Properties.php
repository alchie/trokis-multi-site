<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Properties extends REALESTATE_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('realestate/Partners_model');
		$this->load->model('realestate/Properties_model');
	}

	public function index($status='all', $type='all', $start=0) {

		$page_title = "For Sale and For Rent Properties in the Philippines";
		if($status=='sale') {
			$page_title = "For Sale Properties in the Philippines";
		} elseif($status=='rent') {
			$page_title = "For Rent Properties in the Philippines";
		}
		
		$this->template_data->set('page_title', $page_title);
	    $this->template_data->set('meta_description', "For Sale and For Rent Properties in the Philippines");
	    $this->template_data->set('meta_keywords', "For Sale and For Rent Properties in the Philippines");

		$partners = new $this->Partners_model('p', 'realestate');
		$partners->set_order('id', 'DESC');
		$partners->setActive('1',true);
		$this->template_data->set('partners', $partners->populate());

		$properties = new $this->Properties_model('p', 'realestate');
		$properties->setActive('1',true);
		$properties->set_order('RAND()');
		$properties->set_limit(12);
		$properties->set_select('p.*');
		$properties->set_select('p.details as logo_url');
		$this->template_data->set('properties', $properties->populate());
		
		$this->load->view('realestate/realestate_index', $this->template_data->get_data());
	}

	public function all($property_type='all', $start=0) {
		$property_status = 'all';
		$this->template_data->set('property_status', $property_status);

		$properties = new $this->Properties_model('p', 'realestate');
		$properties->setActive('1',true);
		$properties->set_order('RAND()');
		$properties->set_limit(12);
		$properties->set_select('p.*');
		$properties->set_select('p.details as logo_url');
		$this->template_data->set('properties', $properties->populate());

		$this->index($property_status,$property_type,$start);
	}

	public function sale($property_type='all', $start=0) {
		$property_status = 'sale';
		$this->template_data->set('property_status', $property_status);
		$this->index($property_status,$property_type,$start);
	}

	public function rent($property_type='all', $start=0) {
		$property_status = 'rent';
		$this->template_data->set('property_status', $property_status);
		$this->index($property_status,$property_type,$start);
	}
}
