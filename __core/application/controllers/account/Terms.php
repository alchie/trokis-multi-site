<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms extends ACCOUNT_Controller {

	public function __construct() {
		
		parent::__construct();

	}


	public function index()
	{
		$this->load->view('account/terms_of_use', $this->template_data->get_data());
	}

}
