<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook extends ACCOUNT_Controller {

	public function __construct() {
		
		parent::__construct();

	}

	public function index()
	{
		redirect("login");
	}

}
