<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends ACCOUNT_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->must_login();
		
	}

	public function index() {
		$this->load->view('account/profile/profile', $this->template_data->get_data());
	}

}
