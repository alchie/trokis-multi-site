<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_account extends ACCOUNT_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->must_login();
		
	}

	public function index() {
		$this->load->view('account/my_account/my_account', $this->template_data->get_data());
	}

}
