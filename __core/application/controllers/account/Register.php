<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends ACCOUNT_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('account/Users_model');

	}


	public function index()
	{

		if( $this->input->post() && $this->input->post('agree') ) {
			$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password]');
			$this->form_validation->set_rules('password2', 'Repeat Password', 'trim|required');
			$this->form_validation->set_rules('agree', 'Agree on Terms', 'trim|required');
			if( $this->form_validation->run() ) {

				$full_name = $this->input->post('full_name', true);
				$email = $this->input->post('email', true);
				$date_registered = date('Y-m-d h:i:s');

				$user = new $this->Users_model(NULL, 'account');
				$user->setFullName($full_name);
				$user->setEmail($email, true);

				$this->load->library('encrypt');
				$encrypted_password = $this->encrypt->encode( $this->input->post('password', true) );
				$verification_code = sha1( $this->input->post('email', true) . time() );

				$user->setPasswd($encrypted_password);
				$user->setType('user');
				$user->setVerified(0);
				$user->setCode( $verification_code );
				$user->setDateRegistered( $date_registered );

				if( $user->nonEmpty() === FALSE ) {
					if( $user->insert() ) {
						$this->_send_verification_email($email, $verification_code);
						$this->session->set_userdata( 'loggedIn', true );
						$this->session->set_userdata( 'userId', $user->get_inserted_id() );
						$this->session->set_userdata( 'fullName', $full_name );
						$this->session->set_userdata( 'email', $email );
						$this->session->set_userdata( 'type', 'user' );
						$this->session->set_userdata( 'dateRegistered', $date_registered );
						$this->session->set_userdata( 'emailVerified', false );
						redirect(account_url("register/successful") . "?app=" . $this->input->get('app'));
					}
				}
			}
		}

		$this->load->view('account/register', $this->template_data->get_data());
	}

	public function successful()
	{
		$this->template_data->set("meta_refresh", true);
		$this->template_data->set("redirect_url", account_url("login"));
		$this->load->view('account/register_successful', $this->template_data->get_data());
	}

	public function verify($code) {
		$user = new $this->Users_model(NULL, 'account');
		$user->setCode($code, true);
		$user->setVerified(0,true);
		if( $user->nonEmpty() ) {
			$user2 = new $this->Users_model(NULL, 'account');
			$user2->setVerified(1,false,true);
			$user2->setCode('',false,true);
			$user2->set_where('code', $code);
			$user2->update();
			redirect(account_url("register/verified"));
		}
		redirect(account_url("register"));

	}

	public function verified()
	{
		$this->template_data->set("meta_refresh", true);
		$this->template_data->set("redirect_url", account_url("login"));
		$this->load->view('account/register_verified', $this->template_data->get_data());
	}


	private function _send_verification_email($email, $code) {
    	mail($email,
        "Verify Your Email - Trokis Philippines",
        "Click Link: ". account_url("register/verify/{$code}"),
        "From: noreply@trokis.com" . "\r\n" . "Content-Type: text/plain; charset=utf-8",
        "-fnoreply@trokis.com");
	}

}
