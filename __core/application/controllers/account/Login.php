<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends ACCOUNT_Controller {

	public function __construct() {
		parent::__construct();
		
        $this->load->model('account/Users_model');
        $this->load->model('sessions/App_sessions_model');
        $this->load->model('sessions/User_sessions_model');

        if( $this->session->userdata('loggedIn') )  { 
            $this->init_session( $this->input->get('app'), $this->session->userdata('userId'), session_id() );
            $this->app_redirect( $this->input->get('app'), session_id() );
        }


	}

	public function index()
	{
		if( $this->input->post('email') && $this->input->post('password') ) {
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if( $this->form_validation->run() ) {

				$user = new $this->Users_model(NULL, 'account');
				$user->setEmail($this->input->post('email', true), true);
				//$user->setVerified(1,true);
				
				if( $user->nonEmpty() ) {
					$results = $user->get_results(); 
					$this->load->library('encrypt');
					if( $this->input->post('password', true) == $this->encrypt->decode( $results->passwd ) ) {
						        $this->session->set_userdata( 'loggedIn', true );
					            $this->session->set_userdata( 'userId', $results->uid );
					            $this->session->set_userdata( 'fullName', $results->full_name );
					            $this->session->set_userdata( 'email', $results->email );
                                $this->session->set_userdata( 'type', $results->type );
					            $this->session->set_userdata( 'dateRegistered', $results->date_registered );
					            $this->session->set_userdata( 'emailVerified', (bool) $results->verified );
                                $this->add_user_session($results->uid, session_id() );

						if( $this->input->post('app_id') ) {
							$app_id = $this->input->post( 'app_id',true ); 
							$this->init_session( $app_id, $results->uid, session_id() );
							$this->app_redirect( $app_id, session_id() );
						} else {
							redirect( account_url("profile") );
						}
					}
				}
			}
		}
		$this->load->view('account/login', $this->template_data->get_data());
		
	}

        private function app_redirect($app_id, $session_id) {
            $session_id = urlencode( $session_id );
            $uri = "sessions/start/{$session_id}";
            $redirect_url = main_url($uri);
            switch($app_id) {
                case 'realestate':
                    $redirect_url = realestate_url($uri);
                break;
                case 'jobs':
                    $redirect_url = jobs_url($uri);
                break;
                case 'vehicles':
                    $redirect_url = vehicles_url($uri);
                break;
                case 'motorcycles':
                    $redirect_url = motorcycles_url($uri);
                break;
                case 'events':
                    $redirect_url = events_url($uri);
                break;
                case 'schools':
                    $redirect_url = schools_url($uri);
                break;
                case 'companies':
                    $redirect_url = companies_url($uri);
                break;
                case 'www':
                default:
                    $redirect_url = main_url($uri);
                break;
            }
            //$redirect_url; exit;
            redirect( $redirect_url  );
        }

        private function init_session($app_id, $user_id, $session_id) {
            $session = new $this->App_sessions_model(NULL, 'default');
            $session->setAppId($app_id, true);
            $session->setSessionId($session_id, true);
            $session->setUserId($user_id, true);
            if( $session->nonEmpty() === FALSE ) {
                $session->insert();
            }
        }

        private function add_user_session($user_id, $session_id) {
            $session = new $this->User_sessions_model(NULL, 'default');
            $session->setUserId($user_id,true);
            $session->setSessionId($session_id,true);
            if( !$session->nonEmpty() ) {
                $session->insert();
            }
        }

}
