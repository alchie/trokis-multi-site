<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends ACCOUNT_Controller {

	public function __construct() {
		
		parent::__construct();

	}

	public function index()
	{
		redirect("login");		
	}

}
