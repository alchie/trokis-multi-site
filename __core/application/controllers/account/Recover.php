<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recover extends ACCOUNT_Controller {

	public function __construct() {
		
		parent::__construct();

	}


	public function index()
	{
		$this->load->view('account/recover', $this->template_data->get_data());
	}

}
