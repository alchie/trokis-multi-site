<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MY_Controller {

	public function __construct() {
		
		parent::__construct();

		$this->load->model('sessions/App_sessions_model');
    	$this->load->model('sessions/Ci_sessions_model');
        $this->load->model('sessions/User_sessions_model');

	}

	public function index()
	{
		$this->delete_user_sessions( $this->session->userId );
		$this->session->sess_destroy();
        redirect( account_url("login") . "?app=" . $this->input->get('app') );	
	}

    private function delete_user_sessions($user_id) {
        $user_sessions = new $this->User_sessions_model(NULL, 'default');
        $user_sessions->setUserId($user_id,true);
        foreach( $user_sessions->populate() as $session ) {
        	$ci_session = new $this->Ci_sessions_model(NULL, 'default');
        	$ci_session->setId($session->session_id,true);
        	$ci_session->delete();
        }
        $user_sessions->delete();
        $app_sessions = new $this->App_sessions_model(NULL, 'default');
        $app_sessions->setUserId($user_id,true);
        $app_sessions->delete();
    }

}
