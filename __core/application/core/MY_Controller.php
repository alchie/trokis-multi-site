<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

        var $db_configs = array();

        public function __construct()
        {
                parent::__construct();
                $this->template_data->set('app_id', 'www');
                $this->template_data->set('app_name', 'Search');

                $this->template_data->set('page_title', "Trokis Philippines");
                $this->template_data->set('meta_description', "Trokis Philippines");
                $this->template_data->set('meta_keywords', "Trokis Philippines");
                $this->template_data->set('google_analytics_tracking_id', $this->config->item('general_gat_id'));

                $this->db_configs = $this->config->item('db_configs');

                $this->template_data->set('facebook_app_id', $this->config->item('fbAppId'));

                $this->current_location();
                
        }

        public function check_login_status($url=NULL) {
            $url = ($url && $url != '') ? $url : account_url( "profile" );
            if( $this->session->userdata('loggedIn') ) {
                redirect( $url );
            }
        }

        public function must_login($uri='login') {
            if( !$this->session->userdata('loggedIn') ) {
                $this->session->sess_destroy();
                redirect( account_url( $uri ) );
            }
        }

        public function must_admin($uri='login') {
                if( $this->session->userdata('type') != 'admin' ) {
                    redirect( account_url( $uri ) );
                }
        }

        public function page_not_found() {
            $this->load->view('homepage/page_not_found', $this->template_data->get_data());
        }

        public function current_location() {
            if( get_cookie('current_location') ) {
                $this->load->model('locations/Municipals_model');
                $current_location = new $this->Municipals_model('m', 'locations');
                $current_location->setId( get_cookie('current_location') , true);
                $current_location->cache_on();
                $current_location->set_select("m.*");
                $current_location->set_join("regions r", 'm.region=r.id');
                $current_location->set_select("r.name as region_name");
                $current_location->set_join("provinces p", 'm.province=p.id');
                $current_location->set_select("p.name as province_name");
                $current_location_data = $current_location->get(); 
                $this->template_data->set('current_location', $current_location_data);
            }

        }

}

class ADMIN_Controller extends CI_Controller {

        var $db_configs = array();

        public function __construct()
        {
                parent::__construct();
                $this->template_data->set('app_id', 'www');

                $this->template_data->set('page_title', "ADMINISTRATION");
                $this->template_data->set('meta_description', "ADMINISTRATION | Trokis Philippines");
                $this->template_data->set('meta_keywords', "ADMINISTRATION, Trokis Philippines");
                $this->template_data->set('google_analytics_tracking_id', false);

                $this->db_configs = $this->config->item('db_configs');

                $this->must_login();
                $this->must_admin();
                
        }

        public function check_login_status($url=NULL) {
            $url = ($url && $url != '') ? $url : account_url( "profile" );
            if( $this->session->userdata('loggedIn') ) {
                redirect( $url );
            }
        }

        public function must_login($uri='login') {
            if( !$this->session->userdata('loggedIn') ) {
                $this->session->sess_destroy();
                redirect( account_url( $uri ) );
            }
        }

        public function must_admin($uri='login') {
            if( $this->session->userdata('type') != 'admin' ) {
                redirect( account_url( $uri ) );
            }
        }

        public function page_not_found() {
            $this->load->view('homepage/page_not_found', $this->template_data->get_data());
        }

}

class ACCOUNT_Controller extends MY_Controller {

        public function __construct()
        {
                parent::__construct();  
                $this->template_data->set('app_id', 'accounts');
                
        }

}

class SCHOOLS_Controller extends MY_Controller {

        public function __construct()
        {

            parent::__construct();
            $this->template_data->set('app_id', 'schools');
            $this->template_data->set('app_name', 'Schools');

            $this->template_data->set('page_title', "Schools Directory | Trokis Philippines");
            $this->template_data->set('meta_description', "Schools Directory at Trokis Philippines");
            $this->template_data->set('meta_keywords', "Schools, Business Directory, Trokis Philippines");
            $this->template_data->set('canonical_url', schools_url( uri_string() ) );
            $this->template_data->set('google_analytics_tracking_id', $this->config->item('schools_gat_id'));
        }

}

class COMPANIES_Controller extends MY_Controller {

        public function __construct()
        {

            parent::__construct();
            $this->template_data->set('app_id', 'companies');
            $this->template_data->set('app_name', 'Companies');

            $this->template_data->set('page_title', "Companies Directory | Trokis Philippines");
            $this->template_data->set('meta_description', "Companies Directory at Trokis Philippines");
            $this->template_data->set('meta_keywords', "Companies, Business Directory, Trokis Philippines");
            $this->template_data->set('canonical_url', companies_url( uri_string() ) );
            $this->template_data->set('google_analytics_tracking_id', $this->config->item('companies_gat_id'));
        }

}

class REALESTATE_Controller extends MY_Controller {

        public function __construct()
        {
                parent::__construct();  
                $this->template_data->set('app_id', 'realestate');
                $this->template_data->set('app_name', 'Real Estate Listings');

                $this->template_data->set('page_title', "Real Estate Listings | Trokis Philippines");
                $this->template_data->set('meta_description', "Real Estate Listings at Trokis Philippines");
                $this->template_data->set('meta_keywords', "Properties, Real Estate, Real Estate Listings, Trokis Philippines");
                $this->template_data->set('canonical_url', realestate_url( uri_string() ) );
                $this->template_data->set('google_analytics_tracking_id', $this->config->item('realestate_gat_id'));
        }
        
}

class JOBS_Controller extends MY_Controller {
        public function __construct()
        {
                parent::__construct();  
                $this->template_data->set('app_id', 'jobs');
        }
}

class MOTORCYCLES_Controller extends MY_Controller {
        public function __construct()
        {
                parent::__construct();  
                $this->template_data->set('app_id', 'motorcycles');
        }
}

class VEHICLES_Controller extends MY_Controller {
        public function __construct()
        {
                parent::__construct();  
                $this->template_data->set('app_id', 'vehicles');
        }
}

class EVENTS_Controller extends MY_Controller {
        public function __construct()
        {
                parent::__construct();  
                $this->template_data->set('app_id', 'events');
        }
}

class WILDCARD_Controller extends MY_Controller {

        public $subdomain = false;

        public function __construct()
        {

            parent::__construct();
            $http_host = explode(".", $_SERVER['HTTP_HOST']);
            if( isset($http_host[0]) ) {
                $this->subdomain = $http_host[0];
            }

            $this->template_data->set('subdomain', $this->subdomain);
            
        }

}